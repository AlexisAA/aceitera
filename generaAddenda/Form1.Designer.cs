﻿namespace generaAddenda
{
    partial class frmCreaAddenda
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPedido = new System.Windows.Forms.Label();
            this.lblPosicion = new System.Windows.Forms.Label();
            this.lblEntrada = new System.Windows.Forms.Label();
            this.txtPedido = new System.Windows.Forms.TextBox();
            this.txtPosicion = new System.Windows.Forms.TextBox();
            this.txtEntrada = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblArchivo = new System.Windows.Forms.Label();
            this.txtArchivo = new System.Windows.Forms.TextBox();
            this.btnSeleccionar = new System.Windows.Forms.Button();
            this.gbpFactura = new System.Windows.Forms.GroupBox();
            this.lblEmisor = new System.Windows.Forms.Label();
            this.lblReceptor = new System.Windows.Forms.Label();
            this.lblSerie = new System.Windows.Forms.Label();
            this.lblFolio = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.lblImporte = new System.Windows.Forms.Label();
            this.txtEmisor = new System.Windows.Forms.TextBox();
            this.txtReceptor = new System.Windows.Forms.TextBox();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.txtFolio = new System.Windows.Forms.TextBox();
            this.txtFecha = new System.Windows.Forms.TextBox();
            this.txtImporte = new System.Windows.Forms.TextBox();
            this.gbpAddenda = new System.Windows.Forms.GroupBox();
            this.dgvAddenda = new System.Windows.Forms.DataGridView();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.brnCrear = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.gbpFactura.SuspendLayout();
            this.gbpAddenda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddenda)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPedido
            // 
            this.lblPedido.AutoSize = true;
            this.lblPedido.Location = new System.Drawing.Point(28, 25);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(46, 13);
            this.lblPedido.TabIndex = 0;
            this.lblPedido.Text = "Pedido :";
            // 
            // lblPosicion
            // 
            this.lblPosicion.AutoSize = true;
            this.lblPosicion.Location = new System.Drawing.Point(28, 50);
            this.lblPosicion.Name = "lblPosicion";
            this.lblPosicion.Size = new System.Drawing.Size(53, 13);
            this.lblPosicion.TabIndex = 1;
            this.lblPosicion.Text = "Posicion :";
            // 
            // lblEntrada
            // 
            this.lblEntrada.AutoSize = true;
            this.lblEntrada.Location = new System.Drawing.Point(28, 76);
            this.lblEntrada.Name = "lblEntrada";
            this.lblEntrada.Size = new System.Drawing.Size(50, 13);
            this.lblEntrada.TabIndex = 2;
            this.lblEntrada.Text = "Entrada :";
            // 
            // txtPedido
            // 
            this.txtPedido.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtPedido.Location = new System.Drawing.Point(80, 22);
            this.txtPedido.MaxLength = 10;
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.Size = new System.Drawing.Size(83, 20);
            this.txtPedido.TabIndex = 3;
            this.txtPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPosicion
            // 
            this.txtPosicion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtPosicion.Location = new System.Drawing.Point(80, 47);
            this.txtPosicion.MaxLength = 10;
            this.txtPosicion.Name = "txtPosicion";
            this.txtPosicion.Size = new System.Drawing.Size(83, 20);
            this.txtPosicion.TabIndex = 4;
            this.txtPosicion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtEntrada
            // 
            this.txtEntrada.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtEntrada.Location = new System.Drawing.Point(80, 73);
            this.txtEntrada.MaxLength = 10;
            this.txtEntrada.Name = "txtEntrada";
            this.txtEntrada.Size = new System.Drawing.Size(83, 20);
            this.txtEntrada.TabIndex = 5;
            this.txtEntrada.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblArchivo
            // 
            this.lblArchivo.AutoSize = true;
            this.lblArchivo.Location = new System.Drawing.Point(31, 25);
            this.lblArchivo.Name = "lblArchivo";
            this.lblArchivo.Size = new System.Drawing.Size(49, 13);
            this.lblArchivo.TabIndex = 6;
            this.lblArchivo.Text = "Archivo :";
            // 
            // txtArchivo
            // 
            this.txtArchivo.Location = new System.Drawing.Point(86, 22);
            this.txtArchivo.Name = "txtArchivo";
            this.txtArchivo.Size = new System.Drawing.Size(409, 20);
            this.txtArchivo.TabIndex = 7;
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.Location = new System.Drawing.Point(501, 20);
            this.btnSeleccionar.Name = "btnSeleccionar";
            this.btnSeleccionar.Size = new System.Drawing.Size(75, 23);
            this.btnSeleccionar.TabIndex = 8;
            this.btnSeleccionar.Text = "Seleccionar";
            this.btnSeleccionar.UseVisualStyleBackColor = true;
            this.btnSeleccionar.Click += new System.EventHandler(this.btnSeleccionar_Click);
            // 
            // gbpFactura
            // 
            this.gbpFactura.Controls.Add(this.txtImporte);
            this.gbpFactura.Controls.Add(this.txtFecha);
            this.gbpFactura.Controls.Add(this.txtFolio);
            this.gbpFactura.Controls.Add(this.txtSerie);
            this.gbpFactura.Controls.Add(this.txtReceptor);
            this.gbpFactura.Controls.Add(this.txtEmisor);
            this.gbpFactura.Controls.Add(this.lblImporte);
            this.gbpFactura.Controls.Add(this.lblFecha);
            this.gbpFactura.Controls.Add(this.lblFolio);
            this.gbpFactura.Controls.Add(this.lblSerie);
            this.gbpFactura.Controls.Add(this.lblReceptor);
            this.gbpFactura.Controls.Add(this.lblEmisor);
            this.gbpFactura.Location = new System.Drawing.Point(34, 48);
            this.gbpFactura.Name = "gbpFactura";
            this.gbpFactura.Size = new System.Drawing.Size(542, 115);
            this.gbpFactura.TabIndex = 9;
            this.gbpFactura.TabStop = false;
            this.gbpFactura.Text = "Datos Factura";
            // 
            // lblEmisor
            // 
            this.lblEmisor.AutoSize = true;
            this.lblEmisor.Location = new System.Drawing.Point(17, 27);
            this.lblEmisor.Name = "lblEmisor";
            this.lblEmisor.Size = new System.Drawing.Size(44, 13);
            this.lblEmisor.TabIndex = 0;
            this.lblEmisor.Text = "Emisor :";
            // 
            // lblReceptor
            // 
            this.lblReceptor.AutoSize = true;
            this.lblReceptor.Location = new System.Drawing.Point(237, 27);
            this.lblReceptor.Name = "lblReceptor";
            this.lblReceptor.Size = new System.Drawing.Size(57, 13);
            this.lblReceptor.TabIndex = 1;
            this.lblReceptor.Text = "Receptor :";
            // 
            // lblSerie
            // 
            this.lblSerie.AutoSize = true;
            this.lblSerie.Location = new System.Drawing.Point(17, 53);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(37, 13);
            this.lblSerie.TabIndex = 2;
            this.lblSerie.Text = "Serie :";
            // 
            // lblFolio
            // 
            this.lblFolio.AutoSize = true;
            this.lblFolio.Location = new System.Drawing.Point(237, 53);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(35, 13);
            this.lblFolio.TabIndex = 3;
            this.lblFolio.Text = "Folio :";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(17, 79);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(43, 13);
            this.lblFecha.TabIndex = 4;
            this.lblFecha.Text = "Fecha :";
            // 
            // lblImporte
            // 
            this.lblImporte.AutoSize = true;
            this.lblImporte.Location = new System.Drawing.Point(237, 82);
            this.lblImporte.Name = "lblImporte";
            this.lblImporte.Size = new System.Drawing.Size(48, 13);
            this.lblImporte.TabIndex = 5;
            this.lblImporte.Text = "Importe :";
            // 
            // txtEmisor
            // 
            this.txtEmisor.Enabled = false;
            this.txtEmisor.Location = new System.Drawing.Point(80, 24);
            this.txtEmisor.Name = "txtEmisor";
            this.txtEmisor.Size = new System.Drawing.Size(106, 20);
            this.txtEmisor.TabIndex = 6;
            // 
            // txtReceptor
            // 
            this.txtReceptor.Enabled = false;
            this.txtReceptor.Location = new System.Drawing.Point(300, 24);
            this.txtReceptor.Name = "txtReceptor";
            this.txtReceptor.Size = new System.Drawing.Size(106, 20);
            this.txtReceptor.TabIndex = 7;
            // 
            // txtSerie
            // 
            this.txtSerie.Enabled = false;
            this.txtSerie.Location = new System.Drawing.Point(80, 50);
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.Size = new System.Drawing.Size(106, 20);
            this.txtSerie.TabIndex = 8;
            // 
            // txtFolio
            // 
            this.txtFolio.Enabled = false;
            this.txtFolio.Location = new System.Drawing.Point(300, 50);
            this.txtFolio.Name = "txtFolio";
            this.txtFolio.Size = new System.Drawing.Size(106, 20);
            this.txtFolio.TabIndex = 9;
            // 
            // txtFecha
            // 
            this.txtFecha.Enabled = false;
            this.txtFecha.Location = new System.Drawing.Point(80, 76);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.Size = new System.Drawing.Size(106, 20);
            this.txtFecha.TabIndex = 10;
            // 
            // txtImporte
            // 
            this.txtImporte.Enabled = false;
            this.txtImporte.Location = new System.Drawing.Point(300, 79);
            this.txtImporte.Name = "txtImporte";
            this.txtImporte.Size = new System.Drawing.Size(106, 20);
            this.txtImporte.TabIndex = 11;
            // 
            // gbpAddenda
            // 
            this.gbpAddenda.Controls.Add(this.btnAgregar);
            this.gbpAddenda.Controls.Add(this.dgvAddenda);
            this.gbpAddenda.Controls.Add(this.txtPosicion);
            this.gbpAddenda.Controls.Add(this.lblPedido);
            this.gbpAddenda.Controls.Add(this.lblPosicion);
            this.gbpAddenda.Controls.Add(this.lblEntrada);
            this.gbpAddenda.Controls.Add(this.txtPedido);
            this.gbpAddenda.Controls.Add(this.txtEntrada);
            this.gbpAddenda.Location = new System.Drawing.Point(34, 170);
            this.gbpAddenda.Name = "gbpAddenda";
            this.gbpAddenda.Size = new System.Drawing.Size(542, 329);
            this.gbpAddenda.TabIndex = 10;
            this.gbpAddenda.TabStop = false;
            this.gbpAddenda.Text = "Datos Addenda";
            // 
            // dgvAddenda
            // 
            this.dgvAddenda.AllowUserToAddRows = false;
            this.dgvAddenda.AllowUserToDeleteRows = false;
            this.dgvAddenda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAddenda.Location = new System.Drawing.Point(6, 99);
            this.dgvAddenda.Name = "dgvAddenda";
            this.dgvAddenda.ReadOnly = true;
            this.dgvAddenda.Size = new System.Drawing.Size(530, 216);
            this.dgvAddenda.TabIndex = 8;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(240, 71);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 9;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            // 
            // brnCrear
            // 
            this.brnCrear.Location = new System.Drawing.Point(479, 505);
            this.brnCrear.Name = "brnCrear";
            this.brnCrear.Size = new System.Drawing.Size(97, 23);
            this.brnCrear.TabIndex = 11;
            this.brnCrear.Text = "Crear Addenda";
            this.brnCrear.UseVisualStyleBackColor = true;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(34, 505);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // frmCreaAddenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 647);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.brnCrear);
            this.Controls.Add(this.gbpAddenda);
            this.Controls.Add(this.gbpFactura);
            this.Controls.Add(this.btnSeleccionar);
            this.Controls.Add(this.txtArchivo);
            this.Controls.Add(this.lblArchivo);
            this.Name = "frmCreaAddenda";
            this.Text = "Crea Addenda";
            this.gbpFactura.ResumeLayout(false);
            this.gbpFactura.PerformLayout();
            this.gbpAddenda.ResumeLayout(false);
            this.gbpAddenda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddenda)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPedido;
        private System.Windows.Forms.Label lblPosicion;
        private System.Windows.Forms.Label lblEntrada;
        private System.Windows.Forms.TextBox txtPedido;
        private System.Windows.Forms.TextBox txtPosicion;
        private System.Windows.Forms.TextBox txtEntrada;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblArchivo;
        private System.Windows.Forms.TextBox txtArchivo;
        private System.Windows.Forms.Button btnSeleccionar;
        private System.Windows.Forms.GroupBox gbpFactura;
        private System.Windows.Forms.Label lblSerie;
        private System.Windows.Forms.Label lblReceptor;
        private System.Windows.Forms.Label lblEmisor;
        private System.Windows.Forms.TextBox txtImporte;
        private System.Windows.Forms.TextBox txtFecha;
        private System.Windows.Forms.TextBox txtFolio;
        private System.Windows.Forms.TextBox txtSerie;
        private System.Windows.Forms.TextBox txtReceptor;
        private System.Windows.Forms.TextBox txtEmisor;
        private System.Windows.Forms.Label lblImporte;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label lblFolio;
        private System.Windows.Forms.GroupBox gbpAddenda;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.DataGridView dgvAddenda;
        private System.Windows.Forms.Button brnCrear;
        private System.Windows.Forms.Button btnCancelar;
    }
}

