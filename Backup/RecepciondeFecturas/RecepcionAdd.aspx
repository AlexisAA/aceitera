﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RecepcionAdd.aspx.cs" Inherits="RecepciondeFecturas.RecepcionAdd" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        }

        function pageLoad(sender, args) {
            //
            document.getElementsByClassName('ajax__fileupload_selectFileButton')[0].innerHTML = "Seleccionar archivo (s)";
            document.getElementsByClassName('ajax__fileupload_dropzone')[0].innerHTML = "Suelte aqui los archivos";
            document.getElementsByClassName('ajax__fileupload_topFileStatus')[0].innerHTML = "";
            document.getElementsByClassName('ajax__fileupload_uploadbutton')[0].innerHTML = "Subir";
            document.getElementsByClassName('ajax__fileupload_removeButton')[0].innerHTML = "Borrar";
            document.getElementsByClassName('ajax__fileupload_Pending ')[0].innerHTML = "Pendiente";
        }

        function numeralsOnly(evt)
        {
            evt = (evt) ? evt : event;
            var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));
            if (charCode > 31 && (charCode < 48 || charCode > 57))
            {
                //alert("Enter numerals only in this field.");
                return false;
            }
            return true;
        }
//        window.onbeforeunload = confirmaSalida;
//        function confirmaSalida() {
//            return "Vas a abandonar esta pagina. Si iniciaste sesión, debes dar clic en Salir para cerrar correctamente la sesión";  
//        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"  EnableScriptLocalization="true" EnableScriptGlobalization="True"/>

    <div class="mainRecepcion">
        <div class="divBienvenido">
            <asp:Label ID="lblBienvenida" runat="server" Text="Seleccion de Ordenes y Carga de facturas" CssClass="Titulo1"></asp:Label>
        </div>
        <div class="divCarga">
            <table style="width: 100%; height: auto;">
                <tr>
                    <td style="width: 70%; height: auto; text-align: left; vertical-align: top;">
                        <asp:GridView ID="dgvOrdenes" runat="server" AutoGenerateColumns="False"
                            AllowPaging="True" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="10"
                            DataKeyNames="ID_Pedido" Width="100%" 
                            AlternatingRowStyle-CssClass="alt" 
                            onpageindexchanging="dgvOrdenes_PageIndexChanging">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
                                <asp:BoundField DataField="ID_Pedido" HeaderText="" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="20px" /></asp:BoundField>
                                <asp:BoundField DataField="Pedido" HeaderText="Pedido" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="75px" /></asp:BoundField>
                                <asp:BoundField DataField="Entrada" HeaderText="Entrada" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="75px"/></asp:BoundField>
                                <asp:BoundField DataField="Posicion" HeaderText="Posición" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"  Width="350px"/></asp:BoundField>
                                <asp:BoundField DataField="Importe" HeaderText="Importe" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:c2}">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"  Width="100px"/></asp:BoundField>
                                <asp:TemplateField HeaderText="Seleccion" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"  Width="70px"/>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chbAgregar" runat="server" AutoPostBack="True" Style="position: static" 
                                            oncheckedchanged="chbAgregar_CheckedChanged"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="#B9B9B9" Font-Bold="True" ForeColor="White" />
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>            
                    </td>
                    <td style="width: 30%; height: auto; text-align: right; vertical-align: top;">
                        <ajaxToolkit:AjaxFileUpload ID="afuXML" runat="server" Width="100%"
                            AllowedFileTypes="xml,pdf"
                            MaximumNumberOfFiles="2" onuploadcomplete="afuXML_UploadComplete" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 70%; height: auto; text-align: right; vertical-align: top;">
                        <asp:Label ID="lblImporte" runat="server" Text="Sumatoria :"></asp:Label>
                        &nbsp;&nbsp;
                        <asp:TextBox ID="txtImporte" runat="server" Width="110px" CssClass="alineaDerecha"></asp:TextBox>
                    </td>
                    <td style="width: 30%; height: auto; text-align: center; vertical-align: middle;">
<%--                        <asp:UpdatePanel ID="updpanelProcesar" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnProcesar" runat="server" Text="Procesar" 
                                    CssClass="botonchico" Visible="false" onclick="btnProcesar_Click"/>
                            </ContentTemplate>
                        </asp:UpdatePanel>--%>
                        <asp:Button ID="btnProcesar" runat="server" Text="Procesar" 
                                    CssClass="botonchico" Visible="false" onclick="btnProcesar_Click"/>
                    </td>
                </tr>
            </table>
        </div>
<%--        <asp:UpdateProgress ID="updprogressProcesar" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="updpanelProcesar">
            <ProgressTemplate>
                <div class="overlay"></div>
                <div class="overlayContent">
                <h2>Cargando....</h2>
                    <img src="Imagenes/ajax-loader.gif" alt="Loading" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
        <br />
        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="botonchico" />
        &nbsp;&nbsp;
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" Visible="false" CssClass="botonchico" />
        
        <asp:Panel ID="pnlBuscar" runat="server" Visible="false">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblSociedad" runat="server" Text="Sociedad: "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSociedad" runat="server" Width="500px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblNombrearchivo" runat="server" Text="Nombre archivo :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombrearchivo" runat="server"  Width="500px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblSerie" runat="server" Text="Serie :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSerie" runat="server"  Width="100px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblFolio" runat="server" Text="Folio :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFolio" runat="server"  Width="100px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblfechaIni" runat="server" Text="Inicio recepción:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaini" runat="server" Width="72px"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cleFechaini" runat="server" TargetControlID="txtFechaini" Format="dd/MM/yyyy"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFechafin" runat="server" Text="Fin recepción :"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechafin" runat="server" Width="72px"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="cleFechafin" runat="server" TargetControlID="txtFechafin" Format="dd/MM/yyyy"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEstatus" runat="server" Text="Estatus"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEstatus" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table> 
        </asp:Panel>
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnActualizar" runat="server" 
                        ImageUrl="~/Imagenes/Actualizar2.jpg" Width="30px" ToolTip="Actualizar" 
                        onclick="btnActualizar_Click1" />
                </td>
            </tr>
        </table>
        <div class="divgrvProcesados">
            <table style="width: 100%; height: 100%; text-align:center;">
                <%--Grid--%>
                <tr>
                    <td>
                        <asp:GridView ID="dgvRecepcionArchivos" runat="server" AutoGenerateColumns="False" 
                            AllowPaging="True" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="10"
                            DataKeyNames="Id_Recepcion" Width="100%" 
                            AlternatingRowStyle-CssClass="alt"
                            onrowdatabound="dgvRecepcionArchivos_RowDataBound" 
                            onrowcommand="dgvRecepcionArchivos_RowCommand" 
                            onpageindexchanging="dgvRecepcionArchivos_PageIndexChanging">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
                                <asp:BoundField DataField="Id_Recepcion" HeaderText="ID" ><ItemStyle HorizontalAlign="Center" Width="20px" /></asp:BoundField>
                                <asp:BoundField DataField="Fecha_Recepcion" HeaderText="Fecha de Recepción" DataFormatString="{0:g}" ><ItemStyle HorizontalAlign="Center" Width="200px" /></asp:BoundField>
                                <asp:BoundField DataField="Nombre_XML" HeaderText="Archivo XML" ><ItemStyle HorizontalAlign="Left" Width="250px"/></asp:BoundField>
                                <asp:BoundField DataField="Nombre_PDF" HeaderText="Archivo PDF" ><ItemStyle HorizontalAlign="Left"  Width="250px"/></asp:BoundField>
                                <asp:BoundField DataField="Serie" HeaderText="Serie" ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Folio" HeaderText="Folio" ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" ><ItemStyle HorizontalAlign="Left"  Width="250px"/></asp:BoundField>
                                <asp:BoundField DataField="ID_Estatus" HeaderText="Estatus"><ItemStyle HorizontalAlign="Left"  Width="20px"/></asp:BoundField>
                            
                                <asp:TemplateField HeaderText="Estatus" ItemStyle-HorizontalAlign="Center" meta:resourcekey="BoundFieldResource8" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:Image ID="estatus" runat="server" Height="20px"></asp:Image>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PDF" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgPdf" runat="server" Height="20px" ImageUrl="~/Imagenes/pdf.png" CommandName="cmdPDF" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="XML" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgXml" runat="server" Height="20px" ImageUrl="~/Imagenes/xml.png" CommandName="cmdXML" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="#B9B9B9" Font-Bold="True" ForeColor="White" />
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
