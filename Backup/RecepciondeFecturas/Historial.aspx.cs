﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using BLRecepcion.Portal;
using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using DALRecepcion.Bean.Portal;

using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

namespace RecepciondeFecturas
{
    public partial class Historial : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                    
                    llenaddlMoneda();
                    llenaddlSocieddes();
                    CargarFechas();

                    txtFechaini.Text = ceFechaini.ToString();
                    txtFechafin.Text = ceFechafin.ToString();

                    llenaGrid();
                }
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            ceFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            ceFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
        }

        protected void btnActualizar_Click(object sender, ImageClickEventArgs e)
        {
            ceFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            ceFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
        }

        protected void btnExportar_Click(object sender, ImageClickEventArgs e)
        {
            ceFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            ceFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);

            DataTable dtExporta = null;
            try
            {
                dtExporta = new DataTable();
                dtExporta = buscaHistorial();
                if (dtExporta.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    StringWriter sw = new StringWriter(sb);
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    Page pagina = new Page();
                    HtmlForm hForm = new HtmlForm();
                    GridView gv = new GridView();
                    gv.EnableViewState = false;
                    gv.DataSource = dtExporta;
                    gv.DataBind();
                    pagina.EnableEventValidation = false;
                    pagina.DesignerInitialize();
                    pagina.Controls.Add(hForm);
                    hForm.Controls.Add(gv);
                    pagina.RenderControl(htw);
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=Historial.xls");
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(sb.ToString());
                    Response.End();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                //btnExportar
                muestraError("btnExportar", "btnExportar", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        private void llenaGrid()
        {

            dgvHistorial.DataSource = null;
            dgvHistorial.DataBind();

            DataTable dtBusca = new DataTable();
            try
            {
                dtBusca = buscaHistorial();
                if (dtBusca.Rows.Count > 0)
                {
                    dgvHistorial.DataSource = dtBusca;
                    dgvHistorial.DataBind();
                }
                else
                    inicializaFrid();

            }
            catch (Exception ex)
            {
                muestraError("llenaGrid", "llenaGrid", "Error", ex.Message);
            }

        }

        /// <summary>
        /// Metodo para llenar Historial
        /// </summary>
        /// <returns></returns>
        private DataTable buscaHistorial()
        {

            #region VAriables locales
            int ID_Proveedor = -1;
            int ID_Sociedad = -1;
            DateTime Fecha_Ini = new DateTime();
            DateTime Fecha_Fin = new DateTime();
            string ID_Moneda = string.Empty;
            #endregion

            #region Asignación
            ID_Proveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            ID_Sociedad = Convert.ToInt32(ddlSociedad.SelectedValue.ToString());
            Fecha_Ini = Convert.ToDateTime(ceFechaini.SelectedDate.ToString());
            Fecha_Fin = Convert.ToDateTime(ceFechafin.SelectedDate.ToString());
            ID_Moneda = ddlMoneda.SelectedValue.ToString();
            #endregion

            DataTable dtHistorial = null;
            BLHistorial busca = new BLHistorial();
            try
            {
                dtHistorial = new DataTable();
                dtHistorial = busca.buscaHistorial(ID_Proveedor, ID_Sociedad, Fecha_Ini, Fecha_Fin, ID_Moneda);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtHistorial;
        }

        /// <summary>
        /// Metodo para inicializar fechas
        /// </summary>
        private void inicializaFrid()
        {
            DataTable dtHistorial = new DataTable();
            dtHistorial.Columns.Add(new DataColumn("Id_Historial", typeof(Int32)));
            dtHistorial.Columns.Add(new DataColumn("Nombre_Sociedad", typeof(String)));
            dtHistorial.Columns.Add(new DataColumn("Factura", typeof(String)));
            dtHistorial.Columns.Add(new DataColumn("Fecha_Factura", typeof(DateTime)));
            dtHistorial.Columns.Add(new DataColumn("Moneda", typeof(String)));
            dtHistorial.Columns.Add(new DataColumn("Importe", typeof(decimal)));
            dtHistorial.Columns.Add(new DataColumn("Docto_Compensacion", typeof(String)));
            dtHistorial.Columns.Add(new DataColumn("Fecha_Pago", typeof(DateTime)));

            DataRow drVacio = dtHistorial.NewRow();
            drVacio[0] = 0;
            drVacio[1] = "";
            drVacio[2] = "";
            drVacio[3] = DateTime.Now;
            drVacio[4] = "";
            drVacio[5] = 0;
            drVacio[6] = "";
            drVacio[7] = DateTime.Now;

            dtHistorial.Rows.Add(drVacio);
            dgvHistorial.DataSource = dtHistorial;
            dgvHistorial.DataBind();
            int totalcolums = dgvHistorial.Rows[0].Cells.Count;
            dgvHistorial.Rows[0].Cells.Clear();
            dgvHistorial.Rows[0].Cells.Add(new TableCell());
            dgvHistorial.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvHistorial.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }

        /// <summary>
        /// Metodo para llenar DDL Monedas
        /// </summary>
        private void llenaddlMoneda()
        {
            BLMonedas buscaMoneda = new BLMonedas();
            try
            {
                List<Monedas> csMoneda = buscaMoneda.buscaMoneda();
                if (csMoneda.Count > 0)
                {
                    ddlMoneda.DataValueField = "Moneda";
                    ddlMoneda.DataTextField = "Descripcion";
                    ddlMoneda.Items.Clear();
                    ddlMoneda.Items.Add(new ListItem("Todas", "0"));
                    ddlMoneda.AppendDataBoundItems = true;
                    ddlMoneda.DataSource = csMoneda;
                    ddlMoneda.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("llenaddlMoneda", "llenaddlMoneda", "Error", ex.Message);
            }

        }

        /// <summary>
        /// Metodo para llenad DDL de sociedades
        /// </summary>
        private void llenaddlSocieddes()
        {
            ddlSociedad.Items.Clear();

            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                List<Sociedades> csSociedad = buscaSociedad.buscaSociedad();
                if (csSociedad.Count > 0)
                {
                    ddlSociedad.DataValueField = "Id_Sociedad";
                    ddlSociedad.DataTextField = "Razon_Social";
                    ddlSociedad.Items.Clear();
                    ddlSociedad.Items.Add(new ListItem("Todas", "0"));
                    ddlSociedad.AppendDataBoundItems = true;
                    ddlSociedad.DataSource = csSociedad;
                    ddlSociedad.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("llenaddlSocieddes", "llenaddlSocieddes", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo que inicializa las fechas
        /// </summary>
        private void CargarFechas()
        {
            DateTime fecha = DateTime.Now.Date;
            this.ceFechaini.SelectedDate = new DateTime(fecha.Year, fecha.Month, 1);
            this.ceFechafin.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
        }

        #region Manejo de errores
        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Historial", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Historial", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        #endregion

    }
}