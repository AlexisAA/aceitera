﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cat_Proveedores.aspx.cs" Inherits="RecepciondeFecturas.Cat_Proveedores" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mainDatos">
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" CssClass="Textonormal"
            meta:resourcekey="lblMensajeResource1"></asp:Label>
        <div class="divBienvenido">
            <asp:Label ID="lblProveedores" runat="server" Text="Proveedores" CssClass="Titulo1"></asp:Label>
        </div>
        <asp:Panel ID="pnlDatos" runat="server" Visible="false">
            <table style="width: 100%; height: 100%; text-align: center; vertical-align: middle;">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblIdProveedor" runat="server" Text="ID Proveedor :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtIdProveedor" runat="server" CssClass="Textonormal" 
                            Width="500px" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblIdSAP" runat="server" Text="ID Proveedor SAP :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtIdSAP" runat="server" CssClass="Textonormal" Width="500px" 
                            Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblNombre" runat="server" Text="Nombre :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="Textonormal" Width="500px" 
                            Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblRFC" runat="server" Text="RFC :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtRFC" runat="server" CssClass="Textonormal" Width="500px" 
                            Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblTelefono" runat="server" Text="Teléfono :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtTelefono" runat="server" CssClass="Textonormal" 
                            Width="500px" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblFax" runat="server" Text="Fax :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtFax" runat="server" CssClass="Textonormal" Width="500px" 
                            Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblBanco" runat="server" Text="Banco :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtBanco" runat="server" CssClass="Textonormal" Width="500px" 
                            TextMode="MultiLine" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblCuenta" runat="server" Text="Cta. Bancaria :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtCuenta" runat="server" CssClass="Textonormal" Width="500px" 
                            TextMode="MultiLine" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblCredito" runat="server" Text="Días Credito :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtCredito" runat="server" CssClass="Textonormal" 
                            Width="500px" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblDireccion" runat="server" Text="Dirección :" CssClass="Textonormal"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDireccion" runat="server" CssClass="Textonormal" 
                            Width="500px" TextMode="MultiLine" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                    
                    </td>
                    <td align="left">
                        <asp:CheckBox ID="chbValido" runat="server" Text="Valido ?" 
                            CssClass="Textonormal" Enabled="False" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                    
                    </td>
                    <td align="left">
                        <asp:CheckBox ID="chbAddenda" runat="server" Text="Crear Addenda?" 
                            CssClass="Textonormal" Enabled="False" />
                    </td>
                </tr>
            </table>    
        </asp:Panel>
    </div>
    <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" CssClass="botonchico" 
        onclick="btnNuevo_Click" />&nbsp;
    <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" 
        CssClass="botonchico" Visible="false" onclick="btnActualizar_Click" />&nbsp;
    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="botonchico" 
        onclick="btnBuscar_Click" />&nbsp;
    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="botonchico" 
        Visible="false" onclick="btnGuardar_Click" />&nbsp;
    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" 
        CssClass="botonchico" Visible="false" onclick="btnCancelar_Click"/>
    <div class="divgrvCatalogos">
            <table style="width: 100%; height: 100%; text-align:center;">
                <%--Grid--%>
                <tr>
                    <td>
                        <asp:GridView ID="dgvProveedores" runat="server" AutoGenerateColumns="False" 
                            AllowPaging="True" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="10"
                            DataKeyNames="Id_Proveedor" Width="100%" 
                            AlternatingRowStyle-CssClass="alt" 
                            onpageindexchanging="dgvProveedores_PageIndexChanging" 
                            onrowcommand="dgvProveedores_RowCommand" 
                            onrowdatabound="dgvProveedores_RowDataBound">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Seleccionar" runat="server" ImageUrl="~/Imagenes/magnifier.png" CommandName="CSeleccionar" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Id_Proveedor" HeaderText="ID" ><ItemStyle HorizontalAlign="center" Width="20px"/></asp:BoundField>
                                <asp:BoundField DataField="ID_Proveedor_SAP" HeaderText="ID SAP" ><ItemStyle HorizontalAlign="Left" Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" ><ItemStyle HorizontalAlign="Left" Width="300px"/></asp:BoundField>
                                <asp:BoundField DataField="Fecha_Alta" HeaderText="Fecha Alta" DataFormatString="{0:d}" ><ItemStyle HorizontalAlign="Center" Width="50px" /></asp:BoundField>
                                <asp:BoundField DataField="RFC" HeaderText="RFC" ><ItemStyle HorizontalAlign="Left" Width="100px"/></asp:BoundField>
                                <asp:BoundField DataField="Telefono" HeaderText="Telefono" ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Fax" HeaderText="Fax" ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Dias_Credito" HeaderText="Credito" ><ItemStyle HorizontalAlign="Left"  Width="30px"/></asp:BoundField>
                            </Columns>
                            <SelectedRowStyle BackColor="#B9B9B9" Font-Bold="True" ForeColor="White" />
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>

</asp:Content>
