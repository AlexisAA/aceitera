﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using BLRecepcion.Portal;
using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using DALRecepcion.Bean.Portal;

using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

namespace RecepciondeFecturas
{
    public partial class Programacion : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                    llenaddlMoneda();
                    llenaddlSocieddes();
                    CargarFechas();

                    txtFechaini.Text = cleFechaini.ToString();
                    txtFechafin.Text = cleFechafin.ToString();

                    llenaGrid();

                }
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["Idioma"] == null)
                UICulture = "es-MX";
            else
                UICulture = Session["Idioma"].ToString();

            base.InitializeCulture();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
        }

        protected void btnActualizar_Click(object sender, ImageClickEventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
        }

        protected void btnExportar_Click(object sender, ImageClickEventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);

            DataTable dtExporta = null;
            try
            {
                dtExporta = new DataTable();
                dtExporta = buscaProgramacion();
                if (dtExporta.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    StringWriter sw = new StringWriter(sb);
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    Page pagina = new Page();
                    HtmlForm hForm = new HtmlForm();
                    GridView gv = new GridView();
                    gv.EnableViewState = false;
                    gv.DataSource = dtExporta;
                    gv.DataBind();
                    pagina.EnableEventValidation = false;
                    pagina.DesignerInitialize();
                    pagina.Controls.Add(hForm);
                    hForm.Controls.Add(gv);
                    pagina.RenderControl(htw);
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=Programacion.xls");
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(sb.ToString());
                    Response.End();
                }
                else
                {
                    muestraMensaje((string)GetLocalResourceObject("Error_Sindatos"));
                }
            }
            catch (Exception ex)
            {
                muestraError("btnExportar", "btnExportar", "Error", ex.Message);
            }
        }

        protected void dgvProgramacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvProgramacion.PageIndex = e.NewPageIndex;
            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        private void llenaGrid()
        {
            dgvProgramacion.DataSource = null;
            dgvProgramacion.DataBind();

            DataTable dtBusca = new DataTable();
            try
            {
                dtBusca = buscaProgramacion();
                if (dtBusca.Rows.Count > 0)
                {
                    dgvProgramacion.DataSource = dtBusca;
                    dgvProgramacion.DataBind();
                }
                else
                    inicializaFrid();
                    
            }
            catch (Exception ex)
            {
                muestraError("llenaGrid", "llenaGrid", "Error", ex.Message);
            }
            
        }

        /// <summary>
        /// Metodo para traer información de la busqueda
        /// </summary>
        /// <returns></returns>
        private DataTable buscaProgramacion()
        {

            #region VAriables locales
            int ID_Proveedor = -1;
            int ID_Sociedad = -1;
            DateTime Fecha_Ini = new DateTime();
            DateTime Fecha_Fin = new DateTime();
            string ID_Moneda = string.Empty;
            #endregion

            #region Asignación
            ID_Proveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            ID_Sociedad = Convert.ToInt32(ddlSociedad.SelectedValue.ToString());
            Fecha_Ini = Convert.ToDateTime(cleFechaini.SelectedDate.ToString());
            Fecha_Fin = Convert.ToDateTime(cleFechafin.SelectedDate.ToString());
            ID_Moneda = ddlMoneda.SelectedValue.ToString();
            #endregion

            DataTable dtProgramacion = null;
            BLProgramacion busca = new BLProgramacion();
            try
            {
                dtProgramacion = new DataTable();
                dtProgramacion = busca.buscaProgramacion(ID_Proveedor, ID_Sociedad, Fecha_Ini, Fecha_Fin, ID_Moneda);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtProgramacion;
        }

        /// <summary>
        /// Metodo para iniciarlizar Grid
        /// </summary>
        private void inicializaFrid()
        {
            string x = string.Empty;
            if (Session["Idioma"].ToString() == "es-MX")
                x = "No hay datos para mostrar";
            else
                x = "No data to display";

            DataTable dtProgramacion = new DataTable();
            dtProgramacion.Columns.Add(new DataColumn("Id_Programacion", typeof(Int32)));
            dtProgramacion.Columns.Add(new DataColumn("Fecha_Recepcion", typeof(DateTime)));
            dtProgramacion.Columns.Add(new DataColumn("Nombre_Sociedad", typeof(String)));
            dtProgramacion.Columns.Add(new DataColumn("FActura", typeof(String)));
            dtProgramacion.Columns.Add(new DataColumn("Fecha_Factura", typeof(DateTime)));
            dtProgramacion.Columns.Add(new DataColumn("Pedido", typeof(String)));
            dtProgramacion.Columns.Add(new DataColumn("Moneda", typeof(String)));
            dtProgramacion.Columns.Add(new DataColumn("Importe", typeof(decimal)));
            dtProgramacion.Columns.Add(new DataColumn("Acuse", typeof(String)));
            dtProgramacion.Columns.Add(new DataColumn("Fecha_Vencimiento", typeof(DateTime)));

            DataRow drVacio = dtProgramacion.NewRow();
            drVacio[0] = 0;
            drVacio[1] = DateTime.Now;
            drVacio[2] = "";
            drVacio[3] = "";
            drVacio[4] = DateTime.Now;
            drVacio[5] = "";
            drVacio[6] = "";
            drVacio[7] = 0;
            drVacio[8] = "";
            drVacio[9] = DateTime.Now;

            dtProgramacion.Rows.Add(drVacio);
            dgvProgramacion.DataSource = dtProgramacion;
            dgvProgramacion.DataBind();
            int totalcolums = dgvProgramacion.Rows[0].Cells.Count;
            dgvProgramacion.Rows[0].Cells.Clear();
            dgvProgramacion.Rows[0].Cells.Add(new TableCell());
            dgvProgramacion.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvProgramacion.Rows[0].Cells[0].Text = x;
        }

        /// <summary>
        /// Metodo para llenar DDL Monedas
        /// </summary>
        private void llenaddlMoneda()
        {
            string x = string.Empty;
            if (Session["Idioma"].ToString() == "es-MX")
                x = "Todo";
            else
                x = "All";

            BLMonedas buscaMoneda = new BLMonedas();
            try
            {
                List<Monedas> csMoneda = buscaMoneda.buscaMoneda();
                if (csMoneda.Count > 0)
                {
                    ddlMoneda.DataValueField = "Moneda";
                    ddlMoneda.DataTextField = "Moneda";
                    ddlMoneda.Items.Clear();
                    ddlMoneda.Items.Add(new ListItem(x, "0"));
                    ddlMoneda.AppendDataBoundItems = true;
                    ddlMoneda.DataSource = csMoneda;
                    ddlMoneda.DataBind();
                }
            }
            catch(Exception ex)
            {
                muestraError("llenaddlMoneda", "llenaddlMoneda", "Error", ex.Message);
            }

        }

        /// <summary>
        /// Metodo para llenad DDL de sociedades
        /// </summary>
        private void llenaddlSocieddes()
        {
            ddlSociedad.Items.Clear();
            string x = string.Empty;
            if (Session["Idioma"].ToString() == "es-MX")
                x = "Todo";
            else
                x = "All";

            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                List<Sociedades> csSociedad = buscaSociedad.buscaSociedad();
                if (csSociedad.Count > 0)
                {
                    ddlSociedad.DataValueField = "Id_Sociedad";
                    ddlSociedad.DataTextField = "Razon_Social";
                    ddlSociedad.Items.Clear();
                    ddlSociedad.Items.Add(new ListItem(x, "0"));
                    ddlSociedad.AppendDataBoundItems = true;
                    ddlSociedad.DataSource = csSociedad;
                    ddlSociedad.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("llenaddlSocieddes", "llenaddlSocieddes", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo que inicializa las fechas
        /// </summary>
        private void CargarFechas()
        {
            DateTime fecha = DateTime.Now.Date;
            this.cleFechaini.SelectedDate = new DateTime(fecha.Year, fecha.Month, 1);
            this.cleFechafin.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
        }

        #region Manejo de errores
        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Programacion", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Programacion", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }
        #endregion

        
    }
}