﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using System.Data;
using BLRecepcion.Varios;

namespace RecepciondeFecturas
{
    public partial class Cat_Usuarios : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                    Session["AccionUsuarios"] = null;
                    
                    llenaGrid();
                    llenaddlProveedores();
                    llenaddlPerfiles();

                }
            }
        }

        protected void dgvUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton btn = (ImageButton)e.Row.FindControl("Seleccionar");
                string idUsuarios = string.Empty;
                idUsuarios = e.Row.Cells[1].Text;
                btn.CommandArgument = idUsuarios;
            }
        }

        protected void dgvUsuarios_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            if (e.CommandName == "CSeleccionar")
            {
                int idUsuario = -1;
                idUsuario = Convert.ToInt32(e.CommandArgument.ToString());

                if (idUsuario > -1)
                {
                    datosUsuario(idUsuario);
                    if (btnGuardar.Text == "Aceptar")
                    {
                        habilitaActualiza();
                    }
                    pnlDatos.Visible = true;
                    btnNuevo.Visible = false;
                    btnActualizar.Visible = true;
                    btnBuscar.Visible = false;
                    btnGuardar.Visible = false;
                    btnCancelar.Visible = true;
                }
            }
        }

        protected void dgvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvUsuarios.PageIndex = e.NewPageIndex;
            llenaGrid();
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            //Limpia textos
            limpiaTextos();

            //Habilita textos
            habiltaTextos();

            //Muestra panel
            pnlDatos.Visible = true;
            
            //Habilitamos botones
            btnNuevo.Visible = false;
            btnActualizar.Visible = false;
            btnBuscar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            dgvUsuarios.Enabled = false;
            Session["AccionUsuarios"] = "Nuevo";
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            habilitaActualiza();
            btnNuevo.Visible = false;
            btnActualizar.Visible = false;
            btnBuscar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            dgvUsuarios.Enabled = false;
            Session["AccionUsuarios"] = "Actualizar";
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Limpia textos
            limpiaTextos();

            //Habilita busqueda
            habilitaBusqueda();

            //Muestra panel
            pnlDatos.Visible = true;

            //Habilitamos botones
            btnNuevo.Visible = false;
            btnBuscar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            //dgvUsuarios.Enabled = false;
            btnGuardar.Text = "Aceptar";

            chbValido.Checked = true;

            Session["AccionUsuarios"] = "Buscar";
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            string strAccion = string.Empty;

            if (Session["AccionUsuarios"] != null)
            {
                strAccion = Session["AccionUsuarios"].ToString();

                switch (strAccion)
                {
                    case "Nuevo":
                        #region Guarda registro
                        //Validamos datos
                        if (validaDatos())
                        {
                            if (validaDuplicado(txtUsuario.Text.Trim()))
                            {
                                //Guradamos
                                string Usuario = string.Empty;
                                string Nombre = string.Empty;
                                string APaterno = string.Empty;
                                string AMaterno = string.Empty;
                                string Contrasenia = string.Empty;
                                int NoIntentos = 0;
                                string Bloqueado = string.Empty;
                                bool Activo = false;
                                bool Valido = false;
                                int IdProveedor = -1;
                                int IdPerfil = -1;

                                Usuario = txtUsuario.Text.Trim();
                                Nombre = txtNombre.Text.Trim();
                                APaterno = txtAPaterno.Text.Trim() == "" ? null : txtAPaterno.Text.Trim();
                                AMaterno = txtAMaterno.Text.Trim() == "" ? null : txtAMaterno.Text.Trim();
                                Seguridad seg = new Seguridad();
                                Contrasenia = seg.encriptar(txtContrasenia.Text.Trim());
                                NoIntentos = Convert.ToInt32(txtIntentos.Text.Trim());
                                Bloqueado = chbBloqueado.Checked == true ? "Si" : "No";
                                Activo = chbActivo.Checked;
                                Valido = chbValido.Checked;
                                IdProveedor = Convert.ToInt32(ddlProveedor.SelectedValue.ToString());
                                IdPerfil = Convert.ToInt32(ddlPerfil.SelectedValue.ToString());

                                int Id_Usuario = -1;
                                Id_Usuario = insertaUsuario(Usuario, Nombre, APaterno, AMaterno, Contrasenia, NoIntentos, Bloqueado, Activo, Valido, IdProveedor);
                                if (IdProveedor > 0)
                                {
                                    //Asignamos el perfil
                                    bool resp = false;
                                    resp = insertaUenP(Id_Usuario, IdPerfil);
                                    if (resp)
                                    {
                                        muestraError("btnGuardar", "Guarda", "Ok", "Registro guardado correctamente. Usuario : " + Usuario);
                                        guardaLog("Guardar", "Alta", "Ok", "Registro guardado correctamente. Usuario : " + Usuario);
                                    }
                                }

                                llenaGrid();
                                limpiaTextos();
                                inhabilitaTextos();

                                btnNuevo.Visible = true;
                                btnActualizar.Visible = false;
                                btnBuscar.Visible = true;
                                btnGuardar.Visible = false;
                                btnCancelar.Visible = false;
                                pnlDatos.Visible = false;
                                dgvUsuarios.Enabled = true;
                            }
                        }
                        #endregion
                        break;
                    case "Actualizar":
                        #region Actualiza Registro
                        if (validaDatosActualiza())
                        {
                            //Guradamos
                            int IdUsuario = -1;
                            string Nombre = string.Empty;
                            string APaterno = string.Empty;
                            string AMaterno = string.Empty;
                            string Contrasenia = string.Empty;
                            int NoIntentos = 0;
                            string Bloqueado = string.Empty;
                            bool Activo = false;
                            bool Valido = false;

                            IdUsuario = Convert.ToInt32(txtIdUsuario.Text.Trim());
                            Nombre = txtNombre.Text.Trim();
                            APaterno = txtAPaterno.Text.Trim();
                            AMaterno = txtAMaterno.Text.Trim();
                            Seguridad seg = new Seguridad();
                            Contrasenia = seg.encriptar(txtContrasenia.Text.Trim());
                            NoIntentos = Convert.ToInt32(txtIntentos.Text.Trim());
                            Bloqueado = chbBloqueado.Checked == true ? "Si" : "No";
                            Activo = chbActivo.Checked;
                            Valido = chbValido.Checked;

                            bool respuesta = false;
                            respuesta = actualizaUsuario(IdUsuario, Nombre, APaterno, AMaterno, Contrasenia, NoIntentos, Bloqueado, Activo, Valido);
                            if (respuesta)
                            {
                                muestraError("btnGuardar", "Actualiza", "Ok", "Registro Actualizado correctamente. Usuario : " + Nombre);
                                guardaLog("Guardar", "Actualiza", "Ok", "Registro Actualizado correctamente. Usuario : " + Nombre);
                            }

                            llenaGrid();
                            limpiaTextos();
                            inhabilitaTextos();

                            btnNuevo.Visible = true;
                            btnActualizar.Visible = false;
                            btnBuscar.Visible = true;
                            btnGuardar.Visible = false;
                            btnCancelar.Visible = false;
                            pnlDatos.Visible = false;
                            dgvUsuarios.Enabled = true;
                        }
                        #endregion
                        break;
                    case "Buscar":
                        #region Busca Registro
                        string usuario = string.Empty;
                        string nombre = string.Empty;
                        string apaterno = string.Empty;
                        string amaterno = string.Empty;
                        int idproveedor = -1;
                        int idperfil = -1;
                        string bloqueado = string.Empty;
                        bool activo = false;
                        bool valido = false;

                        usuario = txtUsuario.Text.Trim() == "" ? null : txtUsuario.Text.Trim();
                        nombre = txtNombre.Text.Trim() == "" ? null : txtNombre.Text.Trim();
                        apaterno = txtAPaterno.Text.Trim() == "" ? null : txtAPaterno.Text.Trim();
                        amaterno = txtAMaterno.Text.Trim() == "" ? null : txtAMaterno.Text.Trim();
                        idproveedor = Convert.ToInt32(ddlProveedor.SelectedValue.ToString());
                        idperfil = Convert.ToInt32(ddlPerfil.SelectedValue.ToString());
                        bloqueado = chbBloqueado.Checked == true ? "Si" : "No";
                        activo = chbActivo.Checked == true ? true : false;
                        valido = chbValido.Checked == true ? true : false;

                        buscaRegistros(usuario, nombre, apaterno, amaterno, idproveedor, idperfil, bloqueado, activo, valido);
                        #endregion
                        break;
                }

            }
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiaTextos();
            inhabilitaTextos();

            btnNuevo.Visible = true;
            btnActualizar.Visible = false;
            btnBuscar.Visible = true;
            btnGuardar.Visible = false;
            btnGuardar.Text = "Guardar";
            btnCancelar.Visible = false;
            dgvUsuarios.Enabled = true;
            pnlDatos.Visible = false;
            Session["AccionUsuarios"] = null;
            llenaGrid();
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        private void llenaGrid()
        {

            BLUsuarios buca = new BLUsuarios();
            try
            {
                List<Usuarios> csUsuarios = buca.buscaUsuario();
                if (csUsuarios.Count > 0)
                {
                    dgvUsuarios.DataSource = csUsuarios;
                    dgvUsuarios.DataBind();
                }
                else
                {
                    inicializaGrid();
                }
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaGrid", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para inicializar el Grid
        /// </summary>
        private void inicializaGrid()
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("Id_Usuario", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Usuario", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Nombre", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("APaterno", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("AMaterno", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Fecha_Alta", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("Fecha_Acpt_Contrato", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("FUltimo_Acceso", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("No_Intentos", typeof(Int32)));

            DataRow drVacio = dtArchivos.NewRow();
            drVacio[0] = 0;
            drVacio[1] = 0;
            drVacio[2] = "";
            drVacio[3] = "";
            drVacio[4] = "";
            drVacio[5] = DateTime.Now;
            drVacio[6] = DateTime.Now;
            drVacio[7] = DateTime.Now;
            drVacio[8] = 0;

            dtArchivos.Rows.Add(drVacio);
            dgvUsuarios.DataSource = dtArchivos;
            dgvUsuarios.DataBind();
            int totalcolums = dgvUsuarios.Rows[0].Cells.Count;
            dgvUsuarios.Rows[0].Cells.Clear();
            dgvUsuarios.Rows[0].Cells.Add(new TableCell());
            dgvUsuarios.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvUsuarios.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }

        /// <summary>
        /// Metodo para llenar el DDL de provedores
        /// </summary>
        private void llenaddlProveedores()
        {
            BLProveedores busca = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = busca.buscaProveedor();
                if (csProveedores.Count > 0)
                {

                    ddlProveedor.DataTextField = "Nombre";
                    ddlProveedor.DataValueField = "ID_Proveedor";
                    ddlProveedor.Items.Add(new ListItem("----- Seleccionar -----", "-1"));
                    ddlProveedor.AppendDataBoundItems = true;
                    ddlProveedor.DataSource = csProveedores;
                    ddlProveedor.DataBind();
                }
                else
                {
                    ddlProveedor.DataTextField = "Nombre";
                    ddlProveedor.DataValueField = "ID_Proveedor";
                    ddlProveedor.Items.Add(new ListItem("No hay datos para mostrar", "-1"));
                    ddlProveedor.AppendDataBoundItems = true;
                }
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaddlProveedores", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar DDL Perfiles
        /// </summary>
        private void llenaddlPerfiles()
        {
            BLPerfiles busca = new BLPerfiles();
            try
            {
                List<Perfiles> csPerfiles = busca.buscaPerfil();
                if (csPerfiles.Count > 0)
                {
                    ddlPerfil.DataTextField = "Nombre";
                    ddlPerfil.DataValueField = "ID_Perfil";
                    ddlPerfil.Items.Add(new ListItem("----- Seleccionar -----", "-1"));
                    ddlPerfil.AppendDataBoundItems = true;
                    ddlPerfil.DataSource = csPerfiles;
                    ddlPerfil.DataBind();
                }
                else
                {
                    ddlPerfil.DataTextField = "Nombre";
                    ddlPerfil.DataValueField = "ID_Perfil";
                    ddlPerfil.Items.Add(new ListItem("No hay datos para mostrar", "-1"));
                    ddlPerfil.AppendDataBoundItems = true;
                }
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaddlProveedores", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para habilitar textos para actualizar
        /// </summary>
        private void habilitaActualiza()
        {
            txtContrasenia.Enabled = true;
            txtNombre.Enabled = true;
            txtAPaterno.Enabled = true;
            txtAMaterno.Enabled = true;
            txtIntentos.Enabled = true;
            chbBloqueado.Enabled = true;
            chbActivo.Enabled = true;
            chbValido.Enabled = true;
        }

        /// <summary>
        /// Metodo para mostrar datos
        /// </summary>
        /// <param name="_Id_Usuario"></param>
        private void datosUsuario(int _Id_Usuario)
        {
            BLUsuarios buscar = new BLUsuarios();
            Seguridad seg = new Seguridad();
            BLUsuarioenPerfil buscauenp = new BLUsuarioenPerfil();
            try
            {
                List<Usuarios> csUsuarios = buscar.buscaUsuarioXId(_Id_Usuario);
                if (csUsuarios.Count > 0)
                {
                    txtIdUsuario.Text = csUsuarios[0].Id_Usuario.ToString();
                    txtUsuario.Text = csUsuarios[0].Usuario;
                    txtContrasenia.Text = seg.desencriptar(csUsuarios[0].Contrasenia);
                    txtNombre.Text = csUsuarios[0].Nombre;
                    txtAPaterno.Text = csUsuarios[0].APaterno;
                    txtAMaterno.Text = csUsuarios[0].AMaterno;
                    ddlProveedor.SelectedValue = csUsuarios[0].Id_Proveedor.ToString();

                    List<UsuarioenPerfil> csUenP = buscauenp.buscaporUsuario(csUsuarios[0].Id_Usuario);
                    if (csUenP.Count > 0)
                    {
                        ddlPerfil.SelectedValue = csUenP[0].ID_Perfil.ToString();
                    }

                    txtIntentos.Text = csUsuarios[0].No_Intentos.ToString();
                    chbBloqueado.Checked = csUsuarios[0].Bloqueado == "Si" ? true : false;
                    chbActivo.Checked = csUsuarios[0].Activo;
                    chbValido.Checked = csUsuarios[0].Valido;
                }
            }
            catch (Exception ex)
            {
                muestraError("dgvUsuarios", "datosUsuario", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para limpiar textos
        /// </summary>
        private void limpiaTextos()
        {
            txtIdUsuario.Text = string.Empty;
            txtUsuario.Text = string.Empty;
            txtContrasenia.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtAPaterno.Text = string.Empty;
            txtAMaterno.Text = string.Empty;
            ddlProveedor.SelectedValue = null;
            ddlPerfil.SelectedValue = null;
            txtIntentos.Text = string.Empty;
            chbActivo.Checked = false;
            chbValido.Checked = true;
            chbBloqueado.Checked = false;
        }

        /// <summary>
        /// Metodo para habilitar campos
        /// </summary>
        private void habiltaTextos()
        {
            txtUsuario.Enabled = true;
            txtContrasenia.Enabled = true;
            txtNombre.Enabled = true;
            txtAPaterno.Enabled = true;
            txtAMaterno.Enabled = true;
            ddlProveedor.Enabled = true;
            ddlPerfil.Enabled = true;
            txtIntentos.Enabled = true;
            chbActivo.Enabled = true;
            chbValido.Enabled = true;
            chbBloqueado.Enabled = true;
        }

        /// <summary>
        /// Metodo para habilitar textos de busqueda
        /// </summary>
        private void habilitaBusqueda()
        {
            txtUsuario.Enabled = true;
            txtAPaterno.Enabled = true;
            txtNombre.Enabled = true;
            txtAMaterno.Enabled = true;
            ddlProveedor.Enabled = true;
            ddlPerfil.Enabled = true;
            chbActivo.Enabled = true;
            chbValido.Enabled = true;
            chbBloqueado.Enabled = true;
        }

        /// <summary>
        /// Metodo para inhabilitar textos
        /// </summary>
        private void inhabilitaTextos()
        {
            txtIdUsuario.Enabled = false;
            txtUsuario.Enabled = false;
            txtContrasenia.Enabled = false;
            txtNombre.Enabled = false;
            txtAPaterno.Enabled = false;
            txtAMaterno.Enabled = false;
            ddlProveedor.Enabled = false;
            ddlPerfil.Enabled = false;
            txtIntentos.Enabled = false;
            chbActivo.Enabled = false;
            chbValido.Enabled = false;
            chbBloqueado.Enabled = false;
        }

        private void buscaRegistros(string _usuario, string _nombre, string _apaterno, string _amaterno, int _idproveedor,
            int _idperfil, string _bloqueado, bool _activo, bool _valido)
        {
            DataTable dtUsuarios = new DataTable();
            dtUsuarios.Columns.Add(new DataColumn("Id_Usuario", typeof(Int32)));
            dtUsuarios.Columns.Add(new DataColumn("Usuario", typeof(String)));
            dtUsuarios.Columns.Add(new DataColumn("Nombre", typeof(String)));
            dtUsuarios.Columns.Add(new DataColumn("APaterno", typeof(String)));
            dtUsuarios.Columns.Add(new DataColumn("AMaterno", typeof(String)));
            dtUsuarios.Columns.Add(new DataColumn("Fecha_Alta", typeof(DateTime)));
            dtUsuarios.Columns.Add(new DataColumn("Fecha_Acpt_Contrato", typeof(DateTime)));
            dtUsuarios.Columns.Add(new DataColumn("FUltimo_Acceso", typeof(DateTime)));
            dtUsuarios.Columns.Add(new DataColumn("No_Intentos", typeof(Int32)));

            if (_idperfil >= 0)
            {
                //Primero buscamos todos los usuarios que estan en ese perfil
                BLUsuarioenPerfil buscaUenP = new BLUsuarioenPerfil();
                BLUsuarios buscaUsuario = new BLUsuarios();
                try
                {
                    int noReg = -1;
                    List<UsuarioenPerfil> csUenP = buscaUenP.buscaporPerfil(_idperfil);
                    noReg = csUenP.Count;
                    for (int n = 0; n < noReg; n++)
                    {
                        int IdUsuario = -1;
                        IdUsuario = csUenP[n].ID_Usuario;
                        List<Usuarios> csUsuarios = buscaUsuario.buscaUsuario(IdUsuario, _usuario, _nombre, _apaterno, _amaterno, _idproveedor, _bloqueado, _activo, _valido);
                        if (csUsuarios.Count > 0)
                        {
                            DataRow drVacio = dtUsuarios.NewRow();
                            drVacio[0] = csUsuarios[0].Id_Usuario;
                            drVacio[1] = csUsuarios[0].Usuario;
                            drVacio[2] = csUsuarios[0].Nombre;
                            drVacio[3] = csUsuarios[0].APaterno;
                            drVacio[4] = csUsuarios[0].AMaterno;
                            drVacio[5] = csUsuarios[0].Fecha_Alta;
                            drVacio[6] = csUsuarios[0].Fecha_Acpt_Contrato;
                            drVacio[7] = csUsuarios[0].FUltimo_Acceso;
                            drVacio[8] = Convert.ToInt32(csUsuarios[0].No_Intentos.ToString());

                            dtUsuarios.Rows.Add(drVacio);
                        }
                    }
                }
                catch (Exception ex)
                {
                    muestraError("btnGuardar", "buscaRegistros", "Error", ex.Message);
                }
            }
            else
            {
                int IdUsuario = -1;
                int noReg = -1;
                BLUsuarios busca = new BLUsuarios();
                try
                {
                    List<Usuarios> csUsuarios = busca.buscaUsuario(IdUsuario, _usuario, _nombre, _apaterno, _amaterno, _idproveedor, _bloqueado, _activo, _valido);
                    noReg = csUsuarios.Count;
                    for (int n = 0; n < noReg; n++)
                    {
                        DataRow drVacio = dtUsuarios.NewRow();
                        drVacio[0] = csUsuarios[n].Id_Usuario;
                        drVacio[1] = csUsuarios[n].Usuario;
                        drVacio[2] = csUsuarios[n].Nombre;
                        drVacio[3] = csUsuarios[n].APaterno;
                        drVacio[4] = csUsuarios[n].AMaterno;
                        drVacio[5] = csUsuarios[n].Fecha_Alta;
                        drVacio[6] = csUsuarios[n].Fecha_Acpt_Contrato;
                        drVacio[7] = csUsuarios[n].FUltimo_Acceso;
                        drVacio[8] = Convert.ToInt32(csUsuarios[n].No_Intentos.ToString());

                        dtUsuarios.Rows.Add(drVacio);
                    }
                }
                catch (Exception ex)
                {
                    muestraError("btnGuardar", "buscaRegistros", "Error", ex.Message);
                }
            }

            if (dtUsuarios.Rows.Count > 0)
            {
                dgvUsuarios.DataSource = dtUsuarios;
                dgvUsuarios.DataBind();
            }
            else
                inicializaGrid();

        }

        /// <summary>
        /// Metodo para valira datos a actualizar
        /// </summary>
        /// <returns></returns>
        private bool validaDatosActualiza()
        {
            bool bResp = true;
            string Contrasenia = string.Empty;
            string Nombre = string.Empty;
            int noIntentos = 0;

            Contrasenia = txtContrasenia.Text.Trim();
            Nombre = txtNombre.Text.Trim();

            try
            {
                noIntentos = Convert.ToInt32(txtIntentos.Text.Trim());
            }
            catch(Exception ex)
            {
                muestraMensaje("Debes ingresar un nuemor de intentos valido" + ", " + ex.Message);
                return false;
            }

            if (Contrasenia.Length < 8 && Contrasenia.Length > 10)
            {
                muestraMensaje("Debes ingresar un contraseña valida. De 8 a 10 caracteres alfanumericos");
                return false;
            }
            else
                txtContrasenia.Text = Contrasenia;

            if (Nombre.Length < 4)
            {
                muestraMensaje("Debes ingresar un nombre valido");
                return false;
            }
            else
                txtNombre.Text = Nombre;

            return bResp;
        }

        /// <summary>
        /// Metodo para ctualizar usuario
        /// </summary>
        /// <param name="_idusuario"></param>
        /// <param name="_nombre"></param>
        /// <param name="_apaterno"></param>
        /// <param name="_amaterno"></param>
        /// <param name="_contrasenia"></param>
        /// <param name="_intentos"></param>
        /// <param name="_bloqueado"></param>
        /// <param name="_activo"></param>
        /// <param name="_valido"></param>
        /// <returns></returns>
        private bool actualizaUsuario(int _idusuario, string _nombre, string _apaterno, string _amaterno, string _contrasenia, int _intentos, string _bloqueado, bool _activo, bool _valido)
        {
            bool bResp = true;
            BLUsuarios actualiza = new BLUsuarios();
            try
            {
                bResp = actualiza.actualizaUsuario(_idusuario, _nombre, _apaterno, _amaterno, _contrasenia, _intentos, _bloqueado, _activo, _valido);
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "actualizaUsuario", "Error", ex.Message);
            }
            return bResp;
        }

        /// <summary>
        /// Metodo para validar textos
        /// </summary>
        /// <returns></returns>
        private bool validaDatos()
        {
            bool bResp = true;
            string Usuario = string.Empty;
            string Contrasenia = string.Empty;
            string Nombre = string.Empty;
            string idProveedor = string.Empty;
            string idPerfil = string.Empty;

            Usuario = txtUsuario.Text.Trim();
            Contrasenia = txtContrasenia.Text.Trim();
            Nombre = txtNombre.Text.Trim();
            idProveedor = ddlProveedor.SelectedValue;
            idPerfil = ddlPerfil.SelectedValue;

            if (Usuario.Length < 4)
            {
                muestraMensaje("Debes ingresar un usuario valida.");
                return false;
            }
            else
                txtUsuario.Text = Usuario;

            if (Contrasenia.Length < 8 && Contrasenia.Length > 10)
            {
                muestraMensaje("Debes ingresar un contraseña valida. De 8 a 10 caracteres alfanumericos");
                return false;
            }
            else
                txtContrasenia.Text = Contrasenia;

            if (Nombre.Length < 5)
            {
                muestraMensaje("Debes ingresar un nombre valid");
                return false;
            }
            else
                txtNombre.Text = Nombre;

            if (idProveedor == "-1")
            {
                muestraMensaje("Debes seleccionar un proveedor");
                return false;
            }

            if (idPerfil == "-1")
            {
                muestraMensaje("Debes seleccionar un perfil");
                return false;
            }
            return bResp;
        }

        /// <summary>
        /// Metodo para validar duplicado
        /// </summary>
        /// <param name="_Usuario"></param>
        /// <returns></returns>
        private bool validaDuplicado(string _Usuario)
        {
            bool bResp = true;
            BLUsuarios busca = new BLUsuarios();
            try
            {
                List<Usuarios> csUsuarios = busca.buscaUsuario(_Usuario);
                if (csUsuarios.Count > 0)
                {
                    int idUsuario = -1;
                    idUsuario = csUsuarios[0].Id_Usuario;
                    muestraMensaje("No se puede duplicar el Usuario. El Id de usuario : " + idUsuario.ToString() + " ya tiene ese usuario");
                    return false;
                }
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "validaDuplicado", "Error", ex.Message);
            }
            return bResp;
        }

        /// <summary>
        /// Metodo para insertar usuarios
        /// </summary>
        /// <param name="_usuario"></param>
        /// <param name="_nombre"></param>
        /// <param name="_apaterno"></param>
        /// <param name="_amaterno"></param>
        /// <param name="_contrasenia"></param>
        /// <param name="_intentos"></param>
        /// <param name="_bloqueado"></param>
        /// <param name="_activo"></param>
        /// <param name="_valido"></param>
        /// <param name="_idproveedor"></param>
        /// <returns></returns>
        private int insertaUsuario(string _usuario, string _nombre, string _apaterno, string _amaterno, string _contrasenia,
            int _intentos, string _bloqueado, bool _activo, bool _valido, int _idproveedor)
        {
            int idResp = -1;
            BLUsuarios inserta = new BLUsuarios();
            try
            {
                idResp = inserta.insertaUsuario(_usuario, _nombre, _apaterno, _amaterno, _contrasenia, _intentos, _bloqueado, _activo, _valido, _idproveedor);
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "insertaUsuario", "Error", ex.Message);
            }
            return idResp;
        }

        /// <summary>
        /// Metodo para insertar unsario en perfil
        /// </summary>
        /// <param name="_idusuario"></param>
        /// <param name="_idperfil"></param>
        /// <returns></returns>
        private bool insertaUenP(int _idusuario, int _idperfil)
        {
            bool bresp = false;
            BLUsuarioenPerfil inserta = new BLUsuarioenPerfil();
            try
            {
                bresp = inserta.insertaUenP(_idusuario, _idperfil);
            }
            catch (Exception ex)
            {
                muestraError("insertaUenP", "insertaUsuario", "Error", ex.Message);
            }
            return bresp;
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Usuarios", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Usuarios", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

    }
}