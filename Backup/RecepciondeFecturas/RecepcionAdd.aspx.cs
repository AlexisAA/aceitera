﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLRecepcion.Portal;
using DALRecepcion.Bean.Portal;
using System.Globalization;
using System.IO;
using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;
using System.Net;

namespace RecepciondeFecturas
{
    public partial class RecepcionAdd : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                    
                    //CArgamos la información requerida del Formulario
                    llenaddlSocieddes();
                    llenaddlEstatus();
                    limpiaTextos();
                    llenaGridPedidos();
                    llenaGridRecepcion();
                    dgvRecepcionArchivos.Columns[7].Visible = false;
                    //dgvOrdenes.Columns[0].Visible = false;

                    //if (Session["Errores"] != null)
                    //{
                    //    lblMensaje.Visible = true;
                    //    lblMensaje.Text = Session["Errores"].ToString();
                    //    Session["Errores"] = null;
                    //}
                }
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["Idioma"] == null)
                UICulture = "es-MX";
            else
                UICulture = Session["Idioma"].ToString();

            base.InitializeCulture();
        }

        #region Eventos y metodos del GridPedidos
        protected void chbAgregar_CheckedChanged(object sender, EventArgs e)
        {
            decimal sumatoria = 0;
            foreach (GridViewRow gvRow in dgvOrdenes.Rows)
            {
                CheckBox columnChb = (CheckBox)gvRow.FindControl("chbAgregar");
                if (columnChb.Checked)
                {
                    int rIndex = -1;
                    string x = "";
                    decimal importe = 0;
                    rIndex = gvRow.RowIndex;
                    x = dgvOrdenes.Rows[rIndex].Cells[5].Text;
                    importe = decimal.Parse(x, NumberStyles.Currency);

                    sumatoria = sumatoria + importe;
                }
            }

            txtImporte.Text = string.Format("{0:C2}", sumatoria);
            if (sumatoria > 0)
            {
                
                btnProcesar.Visible = true;
            }
            else
            {
                
                btnProcesar.Visible = false;
            }

            llenaGridRecepcion();
        }

        protected void dgvOrdenes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvOrdenes.PageIndex = e.NewPageIndex;
            llenaGridPedidos();
        }

        /// <summary>
        /// Metodo para llenar el Grid Pedidos
        /// </summary>
        private void llenaGridPedidos()
        {
            int ID_Proveedor = 0;
            ID_Proveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            BLPedidos blpedido = new BLPedidos();
            try
            {
                List<Pedidos> csPedidos = blpedido.buscaPedidos(ID_Proveedor);
                if (csPedidos.Count > 0)
                {
                    dgvOrdenes.DataSource = csPedidos;
                    dgvOrdenes.DataBind();
                }
                else
                    inicializadgvPedidos();
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaGridPedidos", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para inicializar el Grid Pedidos
        /// </summary>
        private void inicializadgvPedidos()
        {
            DataTable dtPedidos = new DataTable();
            dtPedidos.Columns.Add(new DataColumn("ID_Pedido", typeof(Int32)));
            dtPedidos.Columns.Add(new DataColumn("Pedido", typeof(String)));
            dtPedidos.Columns.Add(new DataColumn("Entrada", typeof(String)));
            dtPedidos.Columns.Add(new DataColumn("Posicion", typeof(String)));
            dtPedidos.Columns.Add(new DataColumn("Descripcion", typeof(String)));
            dtPedidos.Columns.Add(new DataColumn("Importe", typeof(Decimal)));

            DataRow drVacio = dtPedidos.NewRow();
            drVacio[0] = 0;
            drVacio[1] = "";
            drVacio[2] = "";
            drVacio[3] = "";
            drVacio[4] = "";
            drVacio[5] = 0;

            dtPedidos.Rows.Add(drVacio);
            dgvOrdenes.DataSource = dtPedidos;
            dgvOrdenes.DataBind();
            int totalcolums = dgvOrdenes.Rows[0].Cells.Count;
            dgvOrdenes.Rows[0].Cells.Clear();
            dgvOrdenes.Rows[0].Cells.Add(new TableCell());
            dgvOrdenes.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvOrdenes.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }
        #endregion

        #region Eventos y metodos del GridRecepcion
        protected void dgvRecepcionArchivos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Text != "0")
                {
                    Image imgEstatus = (Image)e.Row.FindControl("estatus");
                    ImageButton btnPdf = (ImageButton)e.Row.FindControl("imgPdf");
                    ImageButton btnXml = (ImageButton)e.Row.FindControl("imgXml");
                    int idEstatus = -1;
                    string idRecepcion = "";
                    idEstatus = Convert.ToInt32(e.Row.Cells[7].Text);
                    idRecepcion = e.Row.Cells[0].Text;

                    btnPdf.CommandArgument = idRecepcion;
                    btnXml.CommandArgument = idRecepcion;
                    if (idEstatus > 0)
                    {

                        BLEstatus buscaEstatus = new BLEstatus();
                        try
                        {
                            List<Estatus> csEstatus = buscaEstatus.buscaEstatus(idEstatus);
                            if (csEstatus.Count > 0)
                            {
                                imgEstatus.ImageUrl = csEstatus[0].Ruta.ToString();
                                imgEstatus.ToolTip = csEstatus[0].Descripcion.ToString();
                                //btnEstatus.CommandArgument = csEstatus[0].ID_Estatus.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            muestraError("dgvRecepcionArchivos", "buscaEstatus", "Error", ex.Message);
                        }

                    }
                }
            }
        }

        protected void dgvRecepcionArchivos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int IdRecepcion = -1;
            string Tipo = string.Empty;
            switch (e.CommandName)
            {
                case "cmdPDF":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/pdf";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
                case "cmdXML":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/xml";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
            }
        }

        protected void dgvRecepcionArchivos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvRecepcionArchivos.PageIndex = e.NewPageIndex;

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);

            dgvRecepcionArchivos.Columns[7].Visible = true;
            llenaGridRecepcion();
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        private void llenaGridRecepcion()
        {
            dgvRecepcionArchivos.DataSource = null;
            dgvRecepcionArchivos.DataBind();

            dgvRecepcionArchivos.Columns[7].Visible = true;
            DataTable dtBusca = new DataTable();
            try
            {
                dtBusca = buscaArchivos();
                if (dtBusca.Rows.Count > 0)
                {
                    dgvRecepcionArchivos.DataSource = dtBusca;
                    dgvRecepcionArchivos.DataBind();
                }
                else
                    inicializadgvRecepcion();

                dgvRecepcionArchivos.Columns[7].Visible = false;
            }
            catch (Exception ex)
            {
                muestraError("btnBuscar", "llenaGrid", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para buscar archivos
        /// </summary>
        /// <returns></returns>
        private DataTable buscaArchivos()
        {
            #region VAriables locales
            int ID_Proveedor = -1;
            int ID_Sociedad = -1;
            string Nombre_archivo = string.Empty;
            string Serie = string.Empty;
            string Folio = string.Empty;
            DateTime Fecha_Ini = new DateTime();
            DateTime Fecha_Fin = new DateTime();
            int Id_Estatus = -1;
            #endregion

            #region Asignación
            ID_Proveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            ID_Sociedad = Convert.ToInt32(ddlSociedad.SelectedValue.ToString());
            Nombre_archivo = txtNombrearchivo.Text.Trim() == string.Empty ? "" : txtNombrearchivo.Text.Trim();
            Serie = txtSerie.Text.Trim() == string.Empty ? "" : txtSerie.Text.Trim();
            Folio = txtFolio.Text.Trim() == string.Empty ? "" : txtFolio.Text.Trim();
            Fecha_Ini = Convert.ToDateTime(cleFechaini.SelectedDate.ToString());
            Fecha_Fin = Convert.ToDateTime(cleFechafin.SelectedDate.ToString());
            Id_Estatus = Convert.ToInt32(ddlEstatus.SelectedValue.ToString());
            #endregion

            DataTable dtArchivos = null;
            BLArchivos busca = new BLArchivos();
            try
            {
                dtArchivos = new DataTable();
                dtArchivos = busca.buscaArchivos(ID_Proveedor, ID_Sociedad, Nombre_archivo, Serie, Folio, Fecha_Ini, Fecha_Fin, Id_Estatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtArchivos;

        }

        /// <summary>
        /// Metodo para inicializar el Grid
        /// </summary>
        private void inicializadgvRecepcion()
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("Id_Recepcion", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Fecha_Recepcion", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("Nombre_XML", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Nombre_PDF", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Folio", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Serie", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("ID_Estatus", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Observaciones", typeof(String)));

            DataRow drVacio = dtArchivos.NewRow();
            drVacio[0] = 0;
            drVacio[1] = DateTime.Now;
            drVacio[2] = "";
            drVacio[3] = "";
            drVacio[4] = "";
            drVacio[5] = "";
            drVacio[6] = 0;
            drVacio[7] = "";

            dtArchivos.Rows.Add(drVacio);
            dgvRecepcionArchivos.DataSource = dtArchivos;
            dgvRecepcionArchivos.DataBind();
            int totalcolums = dgvRecepcionArchivos.Rows[0].Cells.Count;
            dgvRecepcionArchivos.Rows[0].Cells.Clear();
            dgvRecepcionArchivos.Rows[0].Cells.Add(new TableCell());
            dgvRecepcionArchivos.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvRecepcionArchivos.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }

        /// <summary>
        /// Metro para mostrar archivos
        /// </summary>
        /// <param name="_idrecepcion"></param>
        /// <param name="_tipo"></param>
        private void mustrarArchivo(int _idrecepcion, string _tipo)
        {
            if (Session["id_Usuario"] != null)
            {
                int ID_Usuario = -1;
                string nombreArchivo = string.Empty;

                ID_Usuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                //Primero buscmos el archivo
                BLArchivos busca = new BLArchivos();
                try
                {
                    List<Archivos> csArchivos = busca.buscaPdfXml(_idrecepcion);
                    if (csArchivos.Count > 0)
                    {
                        byte[] datosArchivo = null;
                        if (_tipo == "application/pdf")
                        {
                            datosArchivo = csArchivos[0].Archivo_PDF;
                            nombreArchivo = csArchivos[0].Nombre_PDF;
                        }
                        else
                        {
                            datosArchivo = csArchivos[0].Archivo_XML;
                            nombreArchivo = csArchivos[0].Nombre_XML;
                        }

                        Response.Clear();
                        Response.ContentType = _tipo;
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + nombreArchivo);
                        Response.BinaryWrite(datosArchivo);
                        Response.End();
                        Response.Close();
                    }

                    //Response.Redirect("~/Recepcion.aspx", false);
                }
                catch (Exception ex)
                {
                    muestraError("dgvAclraciones", "mustrarArchivo", "Error", ex.Message);
                }
            }
        }
        #endregion

        #region Eventos y metodos del Panel Buscar
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            dgvRecepcionArchivos.Columns[7].Visible = true;
            if (btnBuscar.Text == "Buscar")
            {
                inicializadgvRecepcion();
                limpiaTextos();
                btnCancel.Visible = true;
                btnBuscar.Text = "Aceptar";
                pnlBuscar.Visible = true;
                dgvRecepcionArchivos.Columns[7].Visible = false;
            }
            else
            {
                dgvRecepcionArchivos.DataSource = null;
                dgvRecepcionArchivos.DataBind();

                cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
                cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
                llenaGridRecepcion();
                dgvRecepcionArchivos.Columns[7].Visible = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            dgvRecepcionArchivos.Columns[7].Visible = true;
            CargarFechas();
            limpiaTextos();
            ddlSociedad.SelectedValue = "0";
            ddlEstatus.SelectedValue = "-1";
            llenaGridRecepcion();
            btnBuscar.Text = "Buscar";
            btnCancel.Visible = false;
            pnlBuscar.Visible = false;
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }

        /// <summary>
        /// Metodo para llenad DDL de sociedades
        /// </summary>
        private void llenaddlSocieddes()
        {
            ddlSociedad.Items.Clear();

            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                List<Sociedades> csSociedad = buscaSociedad.buscaSociedad();
                if (csSociedad.Count > 0)
                {
                    ddlSociedad.DataValueField = "Id_Sociedad";
                    ddlSociedad.DataTextField = "Razon_Social";
                    ddlSociedad.Items.Clear();
                    ddlSociedad.Items.Add(new ListItem("Todas", "0"));
                    ddlSociedad.AppendDataBoundItems = true;
                    ddlSociedad.DataSource = csSociedad;
                    ddlSociedad.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("btnBuscar", "llenaddlSocieddes", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar ddl estatus
        /// </summary>
        private void llenaddlEstatus()
        {
            ddlEstatus.Items.Clear();

            BLEstatus buscaEstatus = new BLEstatus();
            try
            {
                List<Estatus> csEstatus = buscaEstatus.buscaEstatus();
                if (csEstatus.Count > 0)
                {
                    ddlEstatus.DataValueField = "ID_Estatus";
                    ddlEstatus.DataTextField = "Nomnre";
                    ddlEstatus.Items.Clear();
                    ddlEstatus.Items.Add(new ListItem("Todas", "-1"));
                    ddlEstatus.AppendDataBoundItems = true;
                    ddlEstatus.DataSource = csEstatus;
                    ddlEstatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("btnBuscar", "llenaddlEstatus", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para limpiar textos
        /// </summary>
        private void limpiaTextos()
        {
            txtNombrearchivo.Text = "";
            CargarFechas();
            txtFolio.Text = "";
            txtSerie.Text = "";
        }

        /// <summary>
        /// Metodo que inicializa las fechas
        /// </summary>
        private void CargarFechas()
        {
            DateTime fecha = DateTime.Now.Date;
            this.cleFechaini.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
            this.cleFechafin.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
        }
        #endregion

        protected void btnActualizar_Click(object sender, ImageClickEventArgs e)
        {
            dgvRecepcionArchivos.Columns[7].Visible = true;
            llenaGridRecepcion();
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }

        protected void afuXML_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        {
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                int idProveedor = -1;
                int idUsuario = -1;
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                string strPath = "";
                string nombreArchivo = "";

                //Ruta en la que se cargaran los archivos
                strPath = Server.MapPath(@"./UpdateFiles/tmpAddenda/" +
                    idProveedor.ToString() + "/" +
                    idUsuario.ToString() + "/");


                try
                {
                    //Si no existe el directorio lo creamos
                    if (!Directory.Exists(strPath))
                        Directory.CreateDirectory(strPath);

                    //Obtenemos el nombre del archivo
                    nombreArchivo = Path.GetFileName(e.FileName);

                    //Verificamos que no existan los archivos, de ser asi los eliminamos para reemplazarlos
                    if (File.Exists(strPath + nombreArchivo))
                        File.Delete(strPath + nombreArchivo);

                    afuXML.SaveAs(strPath + nombreArchivo);
                }
                catch (Exception ex)
                {
                    muestraError("afuXML_UploadComplete", "Upload", "Error", ex.Message);
                }
            }
            
        }

        protected void btnProcesar_Click(object sender, EventArgs e)
        {
            //Sesion para mosrar los mensajes de error
            Session["Errores"] = null;

            lblMensaje.Text = "";
            lblMensaje.Visible = false;

            //Validamos las Sessiones
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                #region Variables locales
                int idProveedor = -1;
                int idProveedorSAP = -1;
                int idUsuario = -1;
                string strPathOld = "";
                string strPath = "";
                string validaArchivos = "";
                string strEvento = "";
                int idSociedad = -1;
                string usuarioPac = "";
                string passPac = "";
                string cadenaAddenda = "";
                string strPathAddenda = "";
                string cadenaFinal = "";
                string respPac = "";
                int idEstatus = 1;
                int intAnio;
                int intMes;
                int idRecepcion = -1;
                #endregion
                
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                idProveedorSAP = Convert.ToInt32(Session["ID_Proveedor_SAP"].ToString());
                strPathOld = "./UpdateFiles/tmpAddenda/";
                strPath = "./UpdateFiles/tmpAddenda/";
                


                #region Validamos Archivos
                strEvento = "Valida_Archivo";
                validaArchivos = buscaArchivosAddenda(idProveedor, idUsuario, strPathOld);
                if (validaArchivos == "")
                {
                    strEvento = "Valida_Factura";
                    #region Validamos contenido  de factura
                    //Archivo a Procesar
                    string archivoXML = string.Empty;
                    string archivoPDF = string.Empty;
                    //Ruta en la que se cargaran los archivos
                    int noArchivo = 0;
                    strPath = strPath + idProveedor.ToString() + "/" + idUsuario.ToString() + "/";

                    strEvento = "Extrae_Archivo";
                    #region Extraemos el primer archivo
                    try
                    {
                        DirectoryInfo rootDir = new DirectoryInfo(Server.MapPath(strPath));
                        if (Directory.Exists(Server.MapPath(strPath)))
                        {
                            FileInfo[] filesXML = null;
                            //int noXML = 0;
                            //Contamos el numero de Archivos XML Cargados
                            filesXML = rootDir.GetFiles("*.xml");
                            noArchivo = filesXML == null ? 0 : filesXML.Count();
                            foreach (FileInfo fXML in filesXML)
                            {
                                archivoXML = fXML.Name;
                                break;
                            }

                            archivoPDF = Path.GetFileNameWithoutExtension(archivoXML);
                            archivoPDF = archivoPDF + ".pdf";
                        }
                    }
                    catch (Exception ex)
                    {
                        Session["Errores"] = ex.Message;
                        muestraError("btnProcesar_Click", strEvento, "Error", ex.Message);
                    }
                    #endregion

                    strEvento = "Valida_DatosFactura";
                    #region Variables para el archivo XML
                    string[] datosXML = new string[9];
                    string xmlVersion = string.Empty;
                    string xmlrfcReceptor = string.Empty;
                    string xmlrfcEmisor = string.Empty;
                    string xmlFolio = string.Empty;
                    string xmlSerie = string.Empty;
                    string xmlUuid = string.Empty;
                    DateTime xmlFfactura = new DateTime();
                    decimal xmlSubtotal = 0;
                    decimal xmlTotal = 0;
                    #endregion

                    #region Valida datos factura
                    if (archivoXML != "")
                    {
                        datosXML = obtenDatosXML(Server.MapPath(strPath) + archivoXML);
                        if (datosXML != null)
                        {
                            xmlVersion = datosXML[0] == null ? string.Empty : datosXML[0].ToString();
                            xmlrfcReceptor = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                            xmlrfcEmisor = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                            xmlFolio = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                            xmlSerie = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                            xmlUuid = datosXML[5] == null ? string.Empty : datosXML[5].ToString();
                            xmlFfactura = datosXML[6] == null ? DateTime.Now : Convert.ToDateTime(datosXML[6].ToString());
                            xmlSubtotal = datosXML[7] == null ? 0 : Convert.ToDecimal(datosXML[7].ToString());
                            xmlTotal = datosXML[8] == null ? 0 : Convert.ToDecimal(datosXML[8].ToString());

                            if (datosXML != null)
                            {
                                //Validamos que las partidas correspondan con el monto de la factura
                                decimal Monto = 0;
                                Monto = decimal.Parse(txtImporte.Text, NumberStyles.Currency);
                                if (Monto == xmlSubtotal)
                                {
                                    try
                                    {
                                        DateTime fechaRecepcion = new DateTime();
                                        DateTime fechaHoy = new DateTime();

                                        fechaHoy = DateTime.Now;
                                        fechaRecepcion = buscaArchivo(fechaHoy, xmlUuid);
                                        if (fechaHoy != fechaRecepcion)
                                        {
                                            muestraMensaje("La factura ya fue procesada el día " + fechaRecepcion.ToString());
                                            //Session["Errores"] = "La factura ya fue procesada el día " + fechaRecepcion.ToString();
                                        }
                                        else
                                        {
                                            if (idProveedor == buscaProveedor(xmlrfcEmisor))
                                            {
                                                List<Sociedades> csSociedad = buscaSociedad(xmlrfcReceptor);
                                                if (csSociedad.Count > 0)
                                                {
                                                    //Hacemos la validación del PAC
                                                    idSociedad = Convert.ToInt32(csSociedad[0].Id_Sociedad.ToString());
                                                    usuarioPac = csSociedad[0].Usuario_ME;
                                                    passPac = csSociedad[0].Contrasenia_ME;
                                                }
                                                else
                                                {
                                                    muestraMensaje("El RFC del Receptor de la factura no existe");
                                                    //Session["Errores"] = "El RFC del Receptor de la factura no existe";
                                                }

                                            }
                                            else
                                            {
                                                muestraMensaje("El RFC del proveedor no corresponde al RFC de la factura");
                                                //Session["Errores"] = "El RFC del proveedor no corresponde al RFC de la factura";
                                            }

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //Session["Errores"] = ex.Message;
                                        muestraError("btnProcesar_Click", strEvento, "Error", ex.Message);
                                    }
                                }
                                else
                                {
                                    muestraMensaje("El monto de la factura no corresponde con la sumatoria de las partidas seleccionadas");
                                    //Session["Errores"] = "El monto de la factura no corresponde con la sumatoria de las partidas seleccionadas";
                                }
                            }
                        }
                        else
                        {
                            muestraMensaje("Error en XML, no se creara Addenda de esta factura, De clic en Crear para continuar");
                            //Session["Errores"] = "Error en XML, no se creara Addenda de esta factura, De clic en Crear para continuar";
                        }
                    }
                    #endregion

                    #endregion

                    strEvento = "Crea_Addenda";
                    #region Crea Addenda
                    if (idSociedad >= 0)
                    {
                        try
                        {
                            cadenaAddenda = creaAddenda(idProveedorSAP.ToString(), archivoXML);

                            //Sustituimos la cadena
                            if (cadenaAddenda != "")
                            {
                                #region Sustituimos la cadena
                                strPathAddenda = Server.MapPath(@"./UpdateFiles/tmpAddenda/" + idProveedor.ToString() + "/" + idUsuario.ToString() + "/");
                                StreamReader leeArchivo = new StreamReader(strPathAddenda + archivoXML);
                                string line = "";
                                line = leeArchivo.ReadToEnd();
                                leeArchivo.Close();
                                leeArchivo.Dispose();

                                int posicion = 0;
                                int total = 0;
                                total = line.Length;
                                posicion = line.IndexOf("</cfdi:Complemento>");
                                posicion = posicion + 19;
                                string inicio = "";
                                string fin = "";
                                inicio = line.Substring(0, posicion);
                                fin = line.Substring(posicion);
                                cadenaFinal = inicio + cadenaAddenda + fin;
                                #endregion
                            }
                            else
                            {
                                idSociedad = -1;
                                muestraMensaje("No se pudo crear la Addenda, contacte al Administrador");
                                //Session["Errores"] = "No se pudo crear la Addenda, contacte al Administrador";
                            }
                        }
                        catch (Exception ex)
                        {
                            //Session["Errores"] = ex.Message;
                            muestraError("btnProcesar_Click", strEvento, "Error", ex.Message);
                        }
                    }
                    #endregion

                    strEvento = "Guarda_Addenda";
                    #region Guarda Addenda
                    bool resp = false;
                    if (idSociedad >= 0)
                    {
                        try
                        {
                            BLAdenda bladdenda = new BLAdenda();
                            resp = bladdenda.insertaAddenda(archivoXML, idProveedor, idProveedorSAP.ToString(), cadenaAddenda);
                            if (resp)
                            {
                                mueveArchivos(idProveedor, idUsuario, archivoXML, cadenaFinal);
                                eliminaArchivosAddenda(idProveedor, idUsuario, @"./UpdateFiles/tmpAddenda/", archivoXML);
                            }
                            else
                            {
                                resp = false;
                                muestraMensaje("No se pudo guardar la Addenda, contacte al Administrador");
                                //Session["Errores"] = "No se pudo guardar la Addenda, contacte al Administrador";
                            }
                        }
                        catch (Exception ex)
                        {
                            //Session["Errores"] = ex.Message;
                            muestraError("btnProcesar_Click", strEvento, "Error", ex.Message);
                        }
                    }
                    #endregion

                    string x = "";
                    x = Server.MapPath(@"./UpdateFiles/tmp/" + idProveedor.ToString() + "/" + idUsuario.ToString() + "/");
                    string msjRespuesta = "";
                    strEvento = "Envio_PAC";
                    #region Envío de archivo al PAC (Validación fiscal)
                    if (resp)
                    {
                        respPac = enviaPac(usuarioPac, passPac, x + archivoXML);
                        respPac = "ok";
                        if (respPac != string.Empty)
                        {
                            #region Comnetamos el siguiente codigo para no enviar al PAC
                            //XmlDocument xmlRespuesta = new XmlDocument();
                            //xmlRespuesta.LoadXml(respPac);
                            //XmlNodeList nodoRespuesta = xmlRespuesta.GetElementsByTagName("Respuesta");
                            ////XmlElement nodoDatos = new XmlElement();
                            //foreach (XmlElement nodoDatos in nodoRespuesta)
                            //{
                            //    string Fecha = string.Empty;
                            //    string Codigo = string.Empty;
                            //    string Descripcion = string.Empty;

                            //    XmlNodeList nFecha = nodoDatos.GetElementsByTagName("fecha");
                            //    XmlNodeList nCodigo = nodoDatos.GetElementsByTagName("codigo");
                            //    XmlNodeList nDescripcion = nodoDatos.GetElementsByTagName("descripcion");

                            //    Fecha = nFecha[0].InnerText.ToString() == "" ? null : nFecha[0].InnerText.ToString();
                            //    Codigo = nCodigo[0].InnerText.ToString() == "" ? null : nCodigo[0].InnerText.ToString();
                            //    Descripcion = nDescripcion[0].InnerText.ToString() == "" ? null : nDescripcion[0].InnerText.ToString();

                            //    if (Codigo.Substring(0, 1) == "S")
                            //    {
                            //        idEstatus = 5;
                            //        msjRespuesta = string.Empty;
                            //    }
                            //    else
                            //    {
                            //        idEstatus = 2;
                            //        resp = false;
                            //        msjRespuesta = Codigo + " " + Descripcion;
                            //        //Session["Errores"] = Codigo + " " + Descripcion;
                            //    }
                            //}
                            #endregion

                            #region Emula la respuesta del PAC
                            idEstatus = 5;
                            msjRespuesta = string.Empty;
                            #endregion
                        }
                        else
                        {
                            resp = false;
                            muestraMensaje("El PAC no envíó respuesta, intentelo nuevamente");
                            //Session["Errores"] = "El PAC no envíó respuesta, intentelo nuevamente";
                        }
                    }
                    #endregion

                    strEvento = "Mueve_Archivos";
                    #region Mueve archivos
                    if (resp)
                    {
                        intAnio = xmlFfactura.Year;
                        intMes = xmlFfactura.Month;
                        resp = mueveArchivos(idProveedor, idUsuario, intAnio, intMes, archivoXML, archivoPDF);
                        if (resp)
                        {
                            msjRespuesta = "Archivo válido fiscalmente y Enviado a SAP para su revisión";
                        }
                        else
                        {
                            msjRespuesta = "Archivo valido fiscalmente pero no puede ser envíado a SAP. contacte al Administrador";
                        }
                    }
                    #endregion

                    if (idSociedad >= 0)
                    {

                        strEvento = "Guara archivo";
                        #region Guardamos la recepción
                        idRecepcion = guardaRecepcion(archivoXML, archivoPDF, x + archivoXML, x + archivoPDF, idProveedor,
                                idSociedad, xmlFolio, xmlSerie, xmlUuid, xmlFfactura,
                                xmlTotal, idEstatus, msjRespuesta);
                        string msj = "Error";
                        if (msjRespuesta == "Archivo válido fiscalmente y Enviado a SAP para su revisión")
                        {
                            #region Quita Check del Grid
                            //dgvOrdenes.Columns[0].Visible = true;
                            foreach (GridViewRow gvRow in dgvOrdenes.Rows)
                            {
                                CheckBox columnChb = (CheckBox)gvRow.FindControl("chbAgregar");
                                if (columnChb.Checked)
                                {
                                    int ID_Pedido = -1;
                                    int rIndex = -1;
                                    string nada = "";
                                    rIndex = gvRow.RowIndex;
                                    nada = dgvOrdenes.Rows[rIndex].Cells[0].Text;
                                    ID_Pedido = Convert.ToInt32(nada);

                                    resp = actualizaPedido(ID_Pedido, idRecepcion);
                                }
                            }
                            //dgvOrdenes.Columns[0].Visible = false;
                            #endregion

                            msj = "Ok";
                            msjRespuesta = "Recepción guardada correctamente. Id Recepción : " + idRecepcion.ToString(); ;
                        }
                        #endregion

                        #region Guarda Log
                        guardaLog("Recepcion archivo", strEvento, msj, msjRespuesta);
                        #endregion
                    }
                    else
                        eliminaArchivosAddenda(idProveedor, idUsuario, strPathOld);

                    eliminaArchivosAddenda(idProveedor, idUsuario, @"./UpdateFiles/tmp/");
                }
                else
                {
                    muestraMensaje(validaArchivos);
                    //Session["Errores"] = validaArchivos;
                    eliminaArchivosAddenda(idProveedor, idUsuario, strPathOld);
                }
                #endregion
            }

            btnProcesar.Visible = false;
            txtImporte.Text = "";
            llenaGridPedidos();
            llenaGridRecepcion();

        }

        /// <summary>
        /// Metodo para validar archivos para crear Addenda
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_Path"></param>
        /// <returns></returns>
        private string buscaArchivosAddenda(int _IdProveedor, int _IdUsuario, string _Path)
        {
            string resp = "";
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(_Path +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            //Validamos los archivos a procesar
            DirectoryInfo rootDir = new DirectoryInfo(strPath);
            if (Directory.Exists(strPath))
            {
                FileInfo[] filesXML = null;
                FileInfo[] filesPDF = null;
                int noXML = 0;
                int noPDF = 0;
                filesXML = rootDir.GetFiles("*.xml");
                filesPDF = rootDir.GetFiles("*.pdf");

                if (filesXML != null && filesXML.Count() > 0)
                {
                    noXML = filesXML.Count();
                    if (filesPDF != null && filesPDF.Count() > 0)
                    {
                        noPDF = filesPDF.Count();
                        if (noXML == noPDF)
                        {
                            bool buscaPDF = true;
                            #region Buscamos archivos PDF
                            foreach (FileInfo fXML in filesXML)
                            {
                                bool hayPDF = false;
                                string nombreXML = "";
                                nombreXML = Path.GetFileNameWithoutExtension(fXML.Name);
                                foreach (FileInfo fPDF in filesPDF)
                                {
                                    string nombrePDF = "";
                                    nombrePDF = Path.GetFileNameWithoutExtension(fPDF.Name);
                                    if (nombreXML == nombrePDF)
                                    {
                                        hayPDF = true;
                                        break;
                                    }
                                }
                                if (!hayPDF)
                                {
                                    buscaPDF = false;
                                    break;
                                }
                            }
                            #endregion

                            if (!buscaPDF)
                                resp = "Los nombres de los archivos XML no son iguales a los PDF";
                        }
                        else
                            resp = "El numero de archivos XML debe ser igual a los PDF";

                    }
                    else
                        resp = "No hay archivos PDF Cargados";

                }
                else
                    resp = "No hay archivos XML Cargados";
            }
            else
                resp = "Primero debes cargar los archivos dando Clic en Seleccionar y despues en Subir";

            return resp;
        }

        /// <summary>
        /// Metodo para extraer información del XML
        /// </summary>
        /// <param name="pathFile"></param>
        /// <returns></returns>
        private string[] obtenDatosXML(string pathFile)
        {
            string[] arrRespuesta = new string[9];
            try
            {
                //Documento XML
                XmlDocument xmlFactura = new XmlDocument();
                xmlFactura.Load(pathFile);
                //Nodos del documento
                XmlNode nodoFactura = xmlFactura.DocumentElement;
                //Atributos Root

                XmlAttribute rootAtributo = nodoFactura.Attributes["version"];
                if (rootAtributo != null)
                    arrRespuesta[0] = rootAtributo.Value.ToString();

                rootAtributo = null;
                if (nodoFactura.Prefix == "cfdi")
                    arrRespuesta[1] = nodoFactura["cfdi:Receptor"].GetAttribute("rfc");
                else
                    arrRespuesta[1] = nodoFactura["Receptor"].GetAttribute("rfc");

                rootAtributo = null;
                if (nodoFactura.Prefix == "cfdi")
                    arrRespuesta[2] = nodoFactura["cfdi:Emisor"].GetAttribute("rfc");
                else
                    arrRespuesta[2] = nodoFactura["Emisor"].GetAttribute("rfc");

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["folio"];
                if (rootAtributo != null)
                    arrRespuesta[3] = rootAtributo.Value.ToString();

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["serie"];
                if (rootAtributo != null)
                    arrRespuesta[4] = rootAtributo.Value.ToString();

                XmlNodeList nodeList = xmlFactura.GetElementsByTagName("cfdi:Complemento");
                foreach (XmlElement nodo in nodeList)
                {
                    arrRespuesta[5] = nodo["tfd:TimbreFiscalDigital"].GetAttribute("UUID");
                }

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["fecha"];
                if (rootAtributo != null)
                    arrRespuesta[6] = rootAtributo.Value.ToString();

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["subTotal"];
                if (rootAtributo != null)
                    arrRespuesta[7] = rootAtributo.Value.ToString();

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["total"];
                if (rootAtributo != null)
                    arrRespuesta[8] = rootAtributo.Value.ToString();
            }
            catch (Exception ex)
            {
                Session["Errores"] = ex.Message;
                muestraError("btnProcesar_Click", "obtenDatosXML", "Error", ex.Message);
            }

            return arrRespuesta;
        }

        /// <summary>
        /// Metodo para buscar al proveedor
        /// </summary>
        /// <param name="_xmlEmisor"></param>
        /// <returns></returns>
        private int buscaProveedor(string _xmlEmisor)
        {
            int idProveedor = -1;
            BLProveedores buscaProveedor = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = buscaProveedor.buscaProveedor(_xmlEmisor);
                if (csProveedores.Count > 0)
                    idProveedor = csProveedores[0].ID_Proveedor;
            }
            catch (Exception ex)
            {
                Session["Errores"] = ex.Message;
                muestraError("buscaProveedor", "buscaProveedor", "Error", ex.Message);
            }
            return idProveedor;
        }

        /// <summary>
        /// Metodo para bscar sociedad
        /// </summary>
        /// <param name="_xmlRFCReceptor"></param>
        /// <returns></returns>
        private List<Sociedades> buscaSociedad(string _xmlRFCReceptor)
        {
            List<Sociedades> csSociedades = null;
            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                csSociedades = buscaSociedad.buscaSociedad(_xmlRFCReceptor);
            }
            catch (Exception ex)
            {
                Session["Errores"] = ex.Message;
                muestraError("buscaSociedad", "buscaSociedad", "Error", ex.Message);
            }
            return csSociedades;
        }

        /// <summary>
        /// Metodo para buscar Factura procesada
        /// </summary>
        /// <param name="_hoy"></param>
        /// <param name="_uuid"></param>
        /// <returns></returns>
        private DateTime buscaArchivo(DateTime _hoy, string _uuid)
        {
            DateTime respFecha = new DateTime();
            int noREg = -1;
            respFecha = _hoy;
            BLArchivos busca = new BLArchivos();
            try
            {
                List<Archivos> csArchivos = busca.buscaArchivoxuuid(_uuid);
                noREg = csArchivos.Count;
                for (int n = 0; n < noREg; n++)
                {
                    int idEstatus = -1;
                    idEstatus = csArchivos[n].ID_Estatus;
                    if (idEstatus != 1)
                    {
                        respFecha = csArchivos[n].Fecha_Recepcion;
                    }
                }
            }
            catch (Exception ex)
            {
                Session["Errores"] = ex.Message;
                muestraError("buscaArchivo", "buscaArchivo", "Error", ex.Message);
            }

            return respFecha;
        }

        /// <summary>
        /// Metodo para crear Addenda
        /// </summary>
        /// <param name="_ID_Proveedor_SAP"></param>
        /// <param name="_archivo"></param>
        /// <returns></returns>
        public string creaAddenda(string _ID_Proveedor_SAP, string _archivo)
        {
            string cadenaAddenda = "";

            List<Addenda> newAddenda = new List<Addenda>();
            List<AddendaIAPartida> newPartida = new List<AddendaIAPartida>();
            AddendaIAPartida[] itemsIAPartida = new AddendaIAPartida[999];

            int noItemsIAPartida = 0;
            try
            {

                #region Rutina para agregar partida
                foreach (GridViewRow gvRow in dgvOrdenes.Rows)
                {
                    CheckBox columnChb = (CheckBox)gvRow.FindControl("chbAgregar");
                    if (columnChb.Checked)
                    {
                        int rIndex = -1;
                        string Pedido = "";
                        string Posicion = "";
                        string[] Entrada = new string[999];
                        rIndex = gvRow.RowIndex;

                        AddendaIAPartida csPartida = new AddendaIAPartida();
                        Pedido = dgvOrdenes.Rows[rIndex].Cells[1].Text;
                        Posicion = dgvOrdenes.Rows[rIndex].Cells[3].Text == "" ? "" : dgvOrdenes.Rows[rIndex].Cells[3].Text;

                        int noEntrada = 0;
                        Entrada[noEntrada] = dgvOrdenes.Rows[rIndex].Cells[2].Text;

                        #region Rutina para agregar mas de una entrada
                        foreach (GridViewRow gvRow2 in dgvOrdenes.Rows)
                        {
                            CheckBox columnChk = (CheckBox)gvRow2.FindControl("chbAgregar");
                            if (columnChk.Checked)
                            {
                                int rIndex2 = -1;
                                rIndex2 = gvRow2.RowIndex;
                                string Pedido2 = "";
                                string Posicion2 = "";
                                string Entrada2 = "";

                                Pedido2 = dgvOrdenes.Rows[rIndex2].Cells[1].Text;
                                Posicion2 = dgvOrdenes.Rows[rIndex2].Cells[3].Text == "" ? "" : dgvOrdenes.Rows[rIndex2].Cells[3].Text;
                                Entrada2 = dgvOrdenes.Rows[rIndex2].Cells[2].Text;

                                if (Pedido == Pedido2 && Posicion == Posicion2 && rIndex != rIndex2)
                                {
                                    bool existeEntrada = false;

                                    //Recorremos el arreglo para ver si existe la entrada
                                    for (int a = 0; a < Entrada.Length; a++)
                                    {
                                        string aEntrada = "";
                                        aEntrada = Entrada[a];
                                        if (aEntrada == Entrada2)
                                        {
                                            existeEntrada = true;
                                            break;
                                        }
                                    }

                                    //Si no existe la entrada la guardamos en el Arreglo
                                    if (!existeEntrada)
                                    {
                                        noEntrada = noEntrada + 1;
                                        Entrada[noEntrada] = Entrada2;
                                    }
                                }

                            }
                        }
                        #endregion

                        #region Agrega Partida
                        bool existePartida = false;
                        int noPartida = 0;
                        noPartida = newPartida.Count;
                        //Recorremos la lista de Partidas para determinar que no exista
                        for (int partida = 0; partida < noPartida; partida++)
                        {
                            string listPedido = "";
                            string listPosicion = "";
                            listPedido = newPartida[partida].Pedido;
                            listPosicion = newPartida[partida].Posicion;
                            if (Pedido == listPedido && Posicion == listPosicion)
                            {
                                existePartida = true;
                                break;
                            }
                        }

                        if (!existePartida)
                        {
                            csPartida.Pedido = Pedido;
                            csPartida.Posicion = Posicion;
                            csPartida.Entrada = Entrada;
                            newPartida.Add(csPartida);
                            itemsIAPartida[noItemsIAPartida] = newPartida[noItemsIAPartida];
                            noItemsIAPartida = noItemsIAPartida + 1;

                        }
                        #endregion

                    }
                }
                #endregion

                #region Creamos la Addenda
                List<AddendaIA> newAddendaIA = new List<AddendaIA>();
                AddendaIA csAddendaIA = new AddendaIA();
                csAddendaIA.Proveedor = _ID_Proveedor_SAP;
                csAddendaIA.Partida = itemsIAPartida;
                newAddendaIA.Add(csAddendaIA);

                Addenda csAddenda = new Addenda();
                csAddenda.IA = newAddendaIA[0];
                newAddenda.Add(csAddenda);
                #endregion

                cadenaAddenda = serializaAddenda(newAddenda, _ID_Proveedor_SAP, _archivo);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return cadenaAddenda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addenda"></param>
        /// <param name="_IDProveedorSAP"></param>
        /// <param name="nombreArchivo"></param>
        /// <returns></returns>
        private string serializaAddenda(List<Addenda> addenda, string _IDProveedorSAP, string nombreArchivo)
        {
            string resp = "";
            string strPath = "";
            strPath = Server.MapPath(@"./UpdateFiles/tmpAddenda/" + _IDProveedorSAP.ToString() + "/");
            DirectoryInfo rootDir = new DirectoryInfo(strPath);

            try
            {
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);

                var serializer = new XmlSerializer(addenda.GetType());
                using (var escribe = XmlWriter.Create(strPath + nombreArchivo))
                {
                    serializer.Serialize(escribe, addenda);
                }

                XmlDocument xml = new XmlDocument();
                xml.Load(strPath + nombreArchivo);
                string xmlString = xml.OuterXml.ToString();

                using (var stringWriter = new StringWriter())
                {
                    using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                    {
                        xml.WriteTo(xmlTextWriter);
                        xmlTextWriter.Flush();
                        xmlString = stringWriter.GetStringBuilder().ToString();
                    }
                }

                int p = -1;
                string cadenaUno = "";
                string cadenaDos = "";
                string cadenaReplace = "";
                cadenaUno = @"<?xml version=""1.0"" encoding=""utf-8""?><ArrayOfAddenda xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""><Addenda><IA xmlns=""http://www.aceitera.com.mx/addenda.xsd"">";
                cadenaDos = @"<?xml version=""1.0"" encoding=""utf-8""?><ArrayOfAddenda xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><Addenda><IA xmlns=""http://www.aceitera.com.mx/addenda.xsd"">";
                cadenaReplace = @"<cfdi:Addenda xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:noNamespaceSchemaLocation=""http://www.aceitera.com.mx/addenda.xsd""><IA>";

                p = xmlString.IndexOf(cadenaUno);
                if (p >= 0)
                {
                    xmlString = xmlString.Replace(cadenaUno, cadenaReplace);
                }
                else
                {
                    xmlString = xmlString.Replace(cadenaDos, cadenaReplace);
                }

                xmlString = xmlString.Replace(@"</Addenda></ArrayOfAddenda>", @"</cfdi:Addenda>");

                resp = xmlString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        /// <summary>
        /// Metodo para mover archivos de Addenda
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_NombreXML"></param>
        private void mueveArchivos(int _IdProveedor, int _IdUsuario, string _NombreXML, string nuevaCadena)
        {
            string strPathOrigen = "";
            string strPathDestino = "";

            //Ruta origen
            strPathOrigen = Server.MapPath(@"./UpdateFiles/tmpAddenda/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            strPathDestino = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");
            try
            {
                DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                if (Directory.Exists(strPathOrigen))
                {
                    //Verificamos que existan los directorios, de lo contrario los creamos
                    if (!Directory.Exists(strPathDestino))
                        Directory.CreateDirectory(strPathDestino);

                    FileInfo[] files = null;
                    string nombre = "";
                    string nombreXML = "";
                    string nombrePDF = "";
                    nombre = Path.GetFileNameWithoutExtension(_NombreXML);
                    nombreXML = nombre + ".xml";
                    nombrePDF = nombre + ".pdf";

                    files = rootDir.GetFiles(nombre + ".*");

                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            if (fi.Name.ToUpper() == nombreXML.ToUpper())
                            {
                                if (File.Exists(strPathDestino + fi.Name))
                                    File.Delete(strPathDestino + fi.Name);

                                StreamWriter ficheroEscritura = new StreamWriter(strPathDestino + nombreXML);
                                ficheroEscritura.WriteLine(nuevaCadena);
                                ficheroEscritura.Close();
                                ficheroEscritura.Dispose();

                            }

                            if (fi.Name.ToUpper() == nombrePDF.ToUpper())
                            {
                                if (File.Exists(strPathDestino + fi.Name))
                                    File.Delete(strPathDestino + fi.Name);

                                File.Copy(strPathOrigen + fi.Name, strPathDestino + fi.Name);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_anio"></param>
        /// <param name="_mes"></param>
        /// <param name="_ArchivoXML"></param>
        /// <param name="_ArchivoPDF"></param>
        /// <returns></returns>
        private bool mueveArchivos(int _IdProveedor, int _IdUsuario, int _anio, int _mes, string _ArchivoXML, string _ArchivoPDF)
        {
            bool resp = false;
            string strPathOrigen = "";
            string strPathDestino = "";

            //Ruta origen
            strPathOrigen = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            //Ruta destino Boveda
            //VAlidamos que la ruta no sea FTP
            try
            {
                bool esFtp = false;
                esFtp = Convert.ToBoolean(ConfigurationManager.AppSettings["esftp"].ToString());
                if (esFtp)
                {
                    #region CArgar por FTP
                    string ftpUsuario = string.Empty;
                    string ftpPass = string.Empty;
                    ftpUsuario = ConfigurationManager.AppSettings["usuarioFtp"].ToString();
                    ftpPass = ConfigurationManager.AppSettings["passwordFtp"].ToString();

                    //Validamos que sea FTP
                    //Uri uriFTP = new Uri(strPathDestino);
                    //if (uriFTP.Scheme != Uri.UriSchemeFtp)
                    //    return false;

                    //VAlidamos que los archivos existan
                    DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                    if (Directory.Exists(strPathOrigen))
                    {
                        FileInfo[] files = null;
                        string nombre = "";
                        nombre = Path.GetFileNameWithoutExtension(_ArchivoXML);
                        files = rootDir.GetFiles(nombre + ".*");

                        if (files != null)
                        {
                            foreach (FileInfo fi in files)
                            {

                                strPathDestino = string.Empty;
                                //Solo la ruta destio del  FTP
                                strPathDestino = ConfigurationManager.AppSettings["ftpXProcesar"].ToString();
                                //Ruta del FTM mas el archivo
                                strPathDestino = strPathDestino + fi.ToString();

                                //Creamos el FTM
                                FtpWebRequest ftpUpload = null;
                                StreamReader archivoOrigen = null;
                                byte[] byteOrigen = null;

                                ftpUpload = (FtpWebRequest)WebRequest.Create(strPathDestino);
                                ftpUpload.Credentials = new NetworkCredential(ftpUsuario, ftpPass);
                                ftpUpload.Method = WebRequestMethods.Ftp.UploadFile;

                                //Leemos el origen
                                archivoOrigen = new StreamReader(strPathOrigen + fi.ToString());
                                byteOrigen = System.Text.Encoding.UTF8.GetBytes(archivoOrigen.ReadToEnd());
                                archivoOrigen.Close();
                                ftpUpload.ContentLength = byteOrigen.Length;

                                //Escribimos en el Destino
                                Stream archivoDestino = null;
                                archivoDestino = ftpUpload.GetRequestStream();
                                archivoDestino.Write(byteOrigen, 0, byteOrigen.Length);
                                archivoDestino.Close();

                                resp = true;
                            }
                        }

                    }
                    #endregion
                }
                else
                {
                    #region Cargar Local
                    strPathDestino = ConfigurationManager.AppSettings["rutaXProcesar"].ToString();
                    //Validamos los archivos a procesar
                    DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                    if (Directory.Exists(strPathOrigen))
                    {
                        //Verificamos que existan los directorios, de lo contrario los creamos
                        if (!Directory.Exists(strPathDestino))
                            Directory.CreateDirectory(strPathDestino);

                        FileInfo[] files = null;
                        string nombre = "";
                        nombre = Path.GetFileNameWithoutExtension(_ArchivoXML);
                        files = rootDir.GetFiles(nombre + ".*");

                        if (files != null)
                        {
                            foreach (FileInfo fi in files)
                            {
                                if (fi.Name == _ArchivoXML)
                                {
                                    if (File.Exists(strPathDestino + _ArchivoXML))
                                        File.Delete(strPathDestino + _ArchivoXML);
                                    File.Copy(strPathOrigen + _ArchivoXML, strPathDestino + _ArchivoXML);
                                }

                                if (fi.Name == _ArchivoPDF)
                                {
                                    if (File.Exists(strPathDestino + _ArchivoPDF))
                                        File.Delete(strPathDestino + _ArchivoPDF);

                                    File.Copy(strPathOrigen + _ArchivoPDF, strPathDestino + _ArchivoPDF);
                                }
                                resp = true;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                muestraError("btnProcesar", "mueveArchivos", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_Path"></param>
        /// <param name="_archivo"></param>
        private void eliminaArchivosAddenda(int _IdProveedor, int _IdUsuario, string _Path, string _archivo)
        {
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(_Path +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            try
            {
                //Validamos los archivos a procesar
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.*");
                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            string nombre = "";
                            string nombreXML = "";
                            string nombrePDF = "";
                            nombre = Path.GetFileNameWithoutExtension(fi.Name);
                            nombreXML = nombre + ".xml";
                            nombrePDF = nombre + ".pdf";
                            if (nombreXML.ToUpper() == _archivo.ToUpper())
                            {
                                File.Delete(strPath + nombreXML);
                                File.Delete(strPath + nombrePDF);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("btnAddenda", "eliminaArchivosAddenda", "Error", ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_Path"></param>
        private void eliminaArchivosAddenda(int _IdProveedor, int _IdUsuario, string _Path)
        {
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(_Path +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            try
            {
                //Validamos los archivos a procesar
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.*");
                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            string nombre = "";
                            nombre = fi.Name;
                            File.Delete(strPath + nombre);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("btnAddenda", "eliminaArchivosAddenda", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para enviar al PAC el XML
        /// </summary>
        /// <param name="_usuarioPac"></param>
        /// <param name="_passPac"></param>
        /// <param name="_pathFile"></param>
        /// <returns></returns>
        private string enviaPac(string _usuarioPac, string _passPac, string _pathFile)
        {
            string resp = string.Empty;
            string cadenaBase64 = string.Empty;

            ValidaCFDI.CFDIStatus validaXML = new ValidaCFDI.CFDIStatus();
            try
            {
                cadenaBase64 = convierteBase64(_pathFile);
                resp = validaXML.consultacfdi(_usuarioPac, _passPac, cadenaBase64);
            }
            catch (Exception ex)
            {
                muestraError("btnProcesar", "enviaPac", "Error", ex.Message);
            }

            ////Simulamos la respuesta
            //System.Threading.Thread.Sleep(3000);
            //resp = "Ok";

            return resp;
        }

        /// <summary>
        /// Metodo para convertir a base 64
        /// </summary>
        /// <param name="_rutaarchivo"></param>
        /// <returns></returns>
        private string convierteBase64(string _rutaarchivo)
        {
            string base64String = string.Empty;

            System.IO.FileStream inFile;
            byte[] binaryData;
            try
            {
                inFile = new System.IO.FileStream(_rutaarchivo, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                binaryData = new byte[inFile.Length];
                long bytesRead = inFile.Read(binaryData, 0, (int)inFile.Length);
                inFile.Close();

                base64String = Convert.ToBase64String(binaryData, 0, binaryData.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return base64String;
        }

        /// <summary>
        /// Metodo para guardar la recepción del archivo
        /// </summary>
        /// <param name="_Nombrexml"></param>
        /// <param name="_Nombrepdf"></param>
        /// <param name="_pathXML"></param>
        /// <param name="_pathPDF"></param>
        /// <param name="_Idpproveedor"></param>
        /// <param name="_Idsociedad"></param>
        /// <param name="_folio"></param>
        /// <param name="_serie"></param>
        /// <param name="_uuid"></param>
        /// <param name="_fechafactura"></param>
        /// <param name="_importe"></param>
        /// <param name="_Idestatus"></param>
        /// <param name="_obsevaciones"></param>
        /// <returns></returns>
        private int guardaRecepcion(string _Nombrexml, string _Nombrepdf, string _pathXML, string _pathPDF, int _Idpproveedor,
            int _Idsociedad, string _folio, string _serie, string _uuid, DateTime _fechafactura,
            decimal _importe, int _Idestatus, string _obsevaciones)
        {
            int intResp = -1;

            //Primero convertimos en Bits los arhivos para almacenarlos en la base de datos
            byte[] archivoXML = null;
            byte[] archivoPDF = null;

            archivoXML = combierteArchivo(_pathXML);
            archivoPDF = combierteArchivo(_pathPDF);

            BLArchivos guarda = new BLArchivos();
            try
            {
                intResp = guarda.insertaArchivo(_Nombrexml, _Nombrepdf, archivoXML, archivoPDF, _Idpproveedor,
                    _Idsociedad, _folio, _serie, _uuid, _fechafactura,
                    _importe, _Idestatus, _obsevaciones);
            }
            catch (Exception ex)
            {
                muestraError("guardaRecepcion", "guardaRecepcion", "Error", ex.Message);
            }
            return intResp;
        }

        /// <summary>
        /// Metodo para convertir archivo para almacenar.
        /// </summary>
        /// <param name="_strRuta"></param>
        /// <returns></returns>
        private byte[] combierteArchivo(string _strRuta)
        {
            byte[] resp = null;
            try
            {
                FileStream fs = new FileStream(_strRuta, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                resp = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                muestraError("guardaRecepcion", "combierteArchivo", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para actualizar pedido
        /// </summary>
        /// <param name="_id_pedido"></param>
        /// <param name="_id_recepcion"></param>
        /// <returns></returns>
        private bool actualizaPedido(int _id_pedido, int _id_recepcion)
        {
            bool res = false;
            BLPedidos blpedido = new BLPedidos();
            try
            {
                res = blpedido.actualizaPedido(_id_pedido, _id_recepcion);
            }
            catch (Exception ex)
            {
                muestraError("actualizaPedido", "actualizaPedido", "Error", ex.Message);
            }

            return res;
        }

        #region Manejo de errores
        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Recepcion", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Recepcion", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        
        #endregion

        protected void btnActualizar_Click1(object sender, ImageClickEventArgs e)
        {
            dgvRecepcionArchivos.Columns[7].Visible = true;
            llenaGridRecepcion();
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }

    }
}