﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Programacion.aspx.cs" Inherits="RecepciondeFecturas.Programacion" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <div class="mainProgramacion">
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" 
            meta:resourcekey="lblMensajeResource1"></asp:Label>
        <asp:Panel ID="pnlBuscar" runat="server" meta:resourcekey="pnlBuscarResource1">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblSociedad" runat="server" Text="Sociedad: " 
                            meta:resourcekey="lblSociedadResource1"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSociedad" runat="server" Width="500px" 
                            meta:resourcekey="ddlSociedadResource1">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblfechaIni" runat="server" Text="Inicio recepción:" 
                                        meta:resourcekey="lblfechaIniResource1"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaini" runat="server" Width="72px" 
                                        meta:resourcekey="txtFechainiResource1"></asp:TextBox>
                                    <cc1:CalendarExtender ID="cleFechaini" runat="server" 
                                        TargetControlID="txtFechaini" Format="dd/MM/yyyy" BehaviorID="cleFechaini"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFechafin" runat="server" Text="Fin recepción :" 
                                        meta:resourcekey="lblFechafinResource1"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechafin" runat="server" Width="72px" 
                                        meta:resourcekey="txtFechafinResource1"></asp:TextBox>
                                    <cc1:CalendarExtender ID="cleFechafin" runat="server" 
                                        TargetControlID="txtFechafin" Format="dd/MM/yyyy" BehaviorID="cleFechafin"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblMoneda" runat="server" Text="Moneda :" 
                                        meta:resourcekey="lblMonedaResource1"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlMoneda" runat="server" 
                                        meta:resourcekey="ddlMonedaResource1">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="botonchico" 
                                        onclick="btnBuscar_Click" meta:resourcekey="btnBuscarResource1" />
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:ImageButton ID="btnExportar" runat="server" 
                            ImageUrl="~/Imagenes/Excel.png" Width="30px" ToolTip="Exportar a Excel" 
                            onclick="btnExportar_Click" meta:resourcekey="btnExportarResource1" />
                        &nbsp;
                        <asp:ImageButton ID="btnActualizar" runat="server" 
                            ImageUrl="~/Imagenes/Actualizar2.jpg" Width="30px" ToolTip="Actualizar" 
                            onclick="btnActualizar_Click" meta:resourcekey="btnActualizarResource1" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <div class="divgrvProcesados">
            <table style="width: 100%; height: 100%; text-align:center;">
                <%--Grid--%>
                <tr>
                    <td>
                        <asp:GridView ID="dgvProgramacion" runat="server" AutoGenerateColumns="False" 
                            AllowPaging="True" CssClass="mGrid" PagerStyle-CssClass="pgr"
                            DataKeyNames="ID_Programacion" Width="100%" 
                            AlternatingRowStyle-CssClass="alt" 
                            onpageindexchanging="dgvProgramacion_PageIndexChanging" 
                            meta:resourcekey="dgvProgramacionResource1">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
                                <asp:BoundField DataField="ID_Programacion" HeaderText="ID" Visible="false" 
                                    meta:resourcekey="BoundFieldResource1" />
                                <asp:BoundField DataField="Fecha_Recepcion" HeaderText="Fecha de recepción" 
                                    DataFormatString="{0:g}" meta:resourcekey="BoundFieldResource2" ><ItemStyle HorizontalAlign="Center" Width="200px" /></asp:BoundField>
                                <asp:BoundField DataField="Nombre_Sociedad" HeaderText="Compañia" 
                                    meta:resourcekey="BoundFieldResource3" ><ItemStyle HorizontalAlign="Left" Width="250px" /></asp:BoundField>
                                <asp:BoundField DataField="Factura" HeaderText="Factura" 
                                    meta:resourcekey="BoundFieldResource4" ><ItemStyle HorizontalAlign="Left" Width="100px" /></asp:BoundField>
                                <asp:BoundField DataField="Fecha_Factura" HeaderText="Fecha de factura" 
                                    DataFormatString="{0:d}" meta:resourcekey="BoundFieldResource5" ><ItemStyle HorizontalAlign="Center" Width="100px" /></asp:BoundField>
                                <asp:BoundField DataField="Pedido" HeaderText="Pedido" 
                                    meta:resourcekey="BoundFieldResource6" ><ItemStyle HorizontalAlign="Left" Width="100px" /></asp:BoundField>
                                <asp:BoundField DataField="Moneda" HeaderText="Moneda" 
                                    meta:resourcekey="BoundFieldResource7" ><ItemStyle HorizontalAlign="Left" Width="50px" /></asp:BoundField>
                                <asp:BoundField DataField="Importe" HeaderText="Importe" DataFormatString="{0:N}"
                                    meta:resourcekey="BoundFieldResource8" ><ItemStyle HorizontalAlign="Left" Width="100px" /></asp:BoundField>
                                <asp:BoundField DataField="Acuse" HeaderText="Acuse de Recibo" 
                                    meta:resourcekey="BoundFieldResource9" ><ItemStyle HorizontalAlign="Left" Width="100px" /></asp:BoundField>
                                <asp:BoundField DataField="Fecha_Vencimiento" HeaderText="Fecha de Vencimiento" 
                                    DataFormatString="{0:d}" meta:resourcekey="BoundFieldResource10" ><ItemStyle HorizontalAlign="Center" Width="100px" /></asp:BoundField>
                            </Columns>
                            <SelectedRowStyle BackColor="#B9B9B9" Font-Bold="True" ForeColor="White" />
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</asp:Content>
