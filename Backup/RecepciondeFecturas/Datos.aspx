﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Datos.aspx.cs" Inherits="RecepciondeFecturas.Datos" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx";
        } 
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="mainDatos">
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" CssClass="Textonormal"
            meta:resourcekey="lblMensajeResource1"></asp:Label>
        <div class="divBienvenido">
            <asp:Label ID="lblMisdatos" runat="server" Text="Datos de Proveedor" CssClass="Titulo1" meta:resourcekey="lblMisdatosResource1"></asp:Label>
        </div>
        <div class="doctos" style="text-align: right;">
            <asp:Button ID="btnCambiar" runat="server" Text="Cambiar Contraseña"  
                CssClass="botonchico" Width="160px" onclick="btnCambiar_Click" 
                meta:resourcekey="btnCambiarResource1"/>
        </div>
        <table style="width: 100%; height: 100%; text-align: center; vertical-align: middle;">
            <tr>
                <td align="left">
                    <asp:Label ID="lblIdproveedor_SAP" runat="server" Text="Código de Proveedor :" CssClass="Textonormal"
                        meta:resourcekey="lblIdproveedor_SAPResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtIdproveedorSAP" runat="server"  CssClass="Textonormal" Width="500px"
                        Enabled="False" meta:resourcekey="txtIdproveedorSAPResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblRazonsocial" runat="server" Text="Razón Social :" CssClass="Textonormal"
                        meta:resourcekey="lblRazonsocialResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtRazonsocial" runat="server" Enabled="False" CssClass="Textonormal" Width="500px"
                        TextMode="MultiLine" meta:resourcekey="txtRazonsocialResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblRFC" runat="server" Text="R. F. C. :" CssClass="Textonormal"
                        meta:resourcekey="lblRFCResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtRFC" runat="server" Enabled="False"  CssClass="Textonormal" Width="500px" meta:resourcekey="txtRFCResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblDireccion" runat="server" Text="Dirección :" CssClass="Textonormal"
                        meta:resourcekey="lblDireccionResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtDireccion" runat="server" Enabled="False" CssClass="Textonormal" Width="500px"
                        TextMode="MultiLine" meta:resourcekey="txtDireccionResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblTelefono" runat="server" Text="Teléfono :" CssClass="Textonormal"
                        meta:resourcekey="lblTelefonoResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtTelefono" runat="server" Enabled="False" CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtTelefonoResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblFax" runat="server" Text="Fax :" CssClass="Textonormal"
                        meta:resourcekey="lblFaxResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtFax" runat="server" Enabled="False"  CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtFaxResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblDiascredito" runat="server" Text="Dias de crédito :" CssClass="Textonormal"
                        meta:resourcekey="lblDiascreditoResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtDiascredito" runat="server" Enabled="False" CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtDiascreditoResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblBanco" runat="server" Text="Banco :" CssClass="Textonormal"
                        meta:resourcekey="lblBancoResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtBanco" runat="server" Enabled="False" TextMode="MultiLine" CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtBancoResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblCtaBancaria" runat="server" Text="Cuenta bancaria :" CssClass="Textonormal"
                        meta:resourcekey="lblCtaBancariaResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtCtabancaria" runat="server" Enabled="False" TextMode="MultiLine" CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtCtabancariaResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblCreaAddenda" runat="server" Text="Crear Addenda :" CssClass="Textonormal"
                        meta:resourcekey="lblCreaAddendaResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtCreaAddenda" runat="server" Enabled="False" CssClass="Textonormal"
                        meta:resourcekey="txtCreaAddendaResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
                <td align="left">
                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
                <td align="left">
                </td>
            </tr>
        </table>
        <div class="divBienvenido">
            <h3>
                <asp:Label ID="lblContacto" runat="server" Text="Datos de Contacto" CssClass="Titulo1" meta:resourcekey="lblContactoResource1"></asp:Label>
            </h3>
        </div>
        <table style="width: 100%; height: 100%; text-align: center; vertical-align: middle;">
            <tr>
                <td align="left">
                    <asp:Label ID="lblNombreContacto" runat="server" Text="Nombre :" CssClass="Textonormal"
                        meta:resourcekey="lblNombreContactoResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtNombreContacto" runat="server" CssClass="Textonormal" Width="500px"
                        Enabled="False" meta:resourcekey="txtNombreContactoResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblTelContacto" runat="server" Text="Telefóno :" CssClass="Textonormal"
                        meta:resourcekey="lblTelContactoResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtTelContacto" runat="server" Enabled="False" CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtTelContactoResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblCelContacto" runat="server" Text="Celular :" CssClass="Textonormal"
                        meta:resourcekey="lblCelContactoResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtCelContacto" runat="server" Enabled="False" CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtCelContactoResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblCorreoContacto" runat="server" Text="Correo electrónico :" CssClass="Textonormal"
                        meta:resourcekey="lblCorreoContactoResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtCorreoContacto" runat="server" CssClass="Textonormal" Width="500px"
                        Enabled="False" meta:resourcekey="txtCorreoContactoResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
                <td align="left">
                </td>
            </tr>
        </table>
        <table style="width: 100%; text-align: center;">
            <tr>
                <td align="center">
                    <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" CssClass="botonchico" meta:resourcekey="btnNuevoResource1" onclick="btnNuevo_Click" Visible="False"/>
                    &nbsp;
                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="botonchico" meta:resourcekey="btnGuardarResource1" onclick="btnGuardar_Click" Visible="False" />
                    &nbsp;
                    <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" CssClass="botonchico" meta:resourcekey="btnActualizarResource1" Visible="False" onclick="btnActualizar_Click" />
                    &nbsp;
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="botonchico" meta:resourcekey="btnCancelarResource1" Visible="False" onclick="btnCancelar_Click" />
                </td>
            </tr>
        </table>
    </div>

    <%--Boton oculto para los PopUp's--%>
    <asp:Button runat="server" ID="bntHidden" Style="display: none" 
        meta:resourcekey="bntHiddenResource1"/>

    <%--Pop Up para mostrar cambio de contraseña--%>
    <cc1:ModalPopupExtender ID="mppCambia" 
        runat="server"
        PopupControlID="pnlCambia" 
        TargetControlID="bntHidden" 
        BackgroundCssClass="modalBackground" 
        DynamicServicePath="" BehaviorID="mppCambia">
    </cc1:ModalPopupExtender>

    <asp:Panel ID="pnlCambia" 
        runat="server"
        CssClass="modalPopupCambiar" 
        align="center" meta:resourcekey="pnlCambiaResource1">

        <div style="width: 100%; height: 100%;">
            <table width="100%">
                <tr>
                    <td style="background:#004288; height: 10%; text-align:left;">
                        <asp:Label ID="lblTituloCambia" runat="server" Font-Bold="True" 
                            CssClass="subTitulo1Blanco" Text="CAMBIA CONTRASEÑA" 
                            meta:resourcekey="lblTituloCambiaResource1"></asp:Label>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblOldpass" runat="server" Text="Contraseña :" 
                                        CssClass="Textonormal" meta:resourcekey="lblOldpassResource1"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtOldpass" runat="server" CssClass="Textonormal" MaxLength="15"
                                        TextMode="Password" meta:resourcekey="txtOldpassResource1"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblNewpass" runat="server" Text="Nueva Contraseña :" 
                                        CssClass="Textonormal" meta:resourcekey="lblNewpassResource1"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtNewpass" runat="server" CssClass="Textonormal" MaxLength="15"
                                        TextMode="Password" meta:resourcekey="txtNewpassResource1"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblConfpass" runat="server" Text="Confirma Contraseña :" 
                                        CssClass="Textonormal" meta:resourcekey="lblConfpassResource1"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtConfpass" runat="server" CssClass="Textonormal" MaxLength="15"
                                        TextMode="Password" meta:resourcekey="txtConfpassResource1"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnAceptaCambia" runat="server" Text="Aceptar" 
                            CssClass="botonchico" onclick="btnAceptaCambia_Click" 
                            meta:resourcekey="btnAceptaCambiaResource1"/>
                    </td>
                </tr>
            </table>
        </div>


    </asp:Panel>

</asp:Content>
