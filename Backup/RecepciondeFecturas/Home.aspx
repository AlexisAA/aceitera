﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="RecepciondeFecturas.Home" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        } 

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="mainHome">
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" CssClass="Textonormal"
            meta:resourcekey="lblMensajeResource1"></asp:Label>
        <div class="divBienvenido">
            <asp:Label ID="lblBienvenida" runat="server" Text="Bienvenido" CssClass="Titulo1" meta:resourcekey="lblBienvenidaResource1"></asp:Label>
        </div>
        <div class="container">
            <div class="divLeft">
                <table style="width: 98%; border: 1px solid; ">
                    <tr style="vertical-align: top; text-align: left; ">
                        <td style="text-align: center; border: 1px solid; background: #00359B;">
                            <asp:Label ID="lblTituloAviso" runat="server" Text="Aviso importante" CssClass="Titulo1Blanco" meta:resourcekey="lblTituloAvisoResource1"></asp:Label>
                        </td>
                    </tr>
                    <tr style="vertical-align: top; background: #E0E0E0;">
                        <td style="text-align: left;">
                            <asp:Label ID="lblTituloAvisoAdmin" runat="server" CssClass="Textonormal" 
                                Font-Bold="True" meta:resourcekey="lblTituloAvisoAdminResource1"></asp:Label>
                            <br />
                            <br />
                            <asp:Label ID="lblAvisoAdmin" runat="server" ForeColor="Black" CssClass="Textonormal" meta:resourcekey="lblAvisoAdminResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="divRight">
                <table style="width: 98%;">
                    <tr style="vertical-align: top; ">
                        <td style="text-align: left;">
                            <asp:Label ID="lblTituloPoliticas" runat="server" Text="Politicas de pago" CssClass="Titulo1" Font-Bold="True" 
                                meta:resourcekey="lblTituloPoliticasResource1"></asp:Label>
                            <br />
                        </td>
                    </tr>
                    <tr style="vertical-align: top;">
                        <td style="text-align: justify">
                            <asp:Label ID="lblPoliticas" runat="server" ForeColor="Black" CssClass="Textonormal"
                                meta:resourcekey="lblPoliticasResource1"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="doctos" style="text-align: right;">
            <asp:HyperLink ID="hplManual" runat="server" target="_blank" CssClass="Textonormal"
                NavigateUrl="~/Doctos/Manual.pdf" meta:resourcekey="hplManualResource1">Manual</asp:HyperLink>
            &nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="hplPreguntas" runat="server" target="_blank" CssClass="Textonormal"
                NavigateUrl="~/Doctos/Preguntas.pdf" meta:resourcekey="hplPreguntasResource1">Preguntas frecuentes</asp:HyperLink>
            &nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="hplPrivacidad" runat="server" target="_blank" CssClass="Textonormal"
                NavigateUrl="~/Doctos/CARTA AVISO PRIVACIDAD.pdf" 
                meta:resourcekey="hplPrivacidadResource1">Aviso de privacidad</asp:HyperLink>
        </div>
    </div>

</asp:Content>
