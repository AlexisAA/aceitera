﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RecepciondeFecturas.Default" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Style/Site.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" 
        meta:resourcekey="lblMensajeResource1"></asp:Label>

    <div class="mainLogin">
        <table class="TablaUno">
            <tr>
                <td style="text-align: left; padding-bottom: 10px;">
                    <asp:Label ID="lblTitulologin" runat="server" Text="Bienvenido" CssClass="Titulo1" meta:resourcekey="lblTitulologinResource1"></asp:Label>            
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="text-align: center;">
                                <table width="100%">
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lblIdioma" runat="server" Text="Idioma :" Font-Bold="True" CssClass="Textonormal"
                                                meta:resourcekey="lblIdiomaResource1"></asp:Label>
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:DropDownList ID="ddlIdioma" runat="server" AutoPostBack="True"
                                                meta:resourcekey="ddlIdiomaResource1" CssClass="txtLogin"
                                                onselectedindexchanged="ddlIdioma_SelectedIndexChanged">
                                                <asp:ListItem Text="Español" Value="es-MX" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                                <asp:ListItem Text="Inglés" Value="en-US" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lblUsuario" runat="server" Text="Usuario :" Font-Bold="True" CssClass="Textonormal"
                                                meta:resourcekey="lblUsuarioResource1"></asp:Label>
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:TextBox ID="txtUsuario" runat="server" MaxLength="10" CssClass="txtLogin"
                                                meta:resourcekey="txtUsuarioResource1"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lblContrasenia" runat="server" Text="Contraseña :" Font-Bold="True" CssClass="Textonormal"
                                                meta:resourcekey="lblContraseniaResource1"></asp:Label>
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:TextBox ID="txtContrasenia" runat="server" MaxLength="15" CssClass="txtLogin"
                                                TextMode="Password" meta:resourcekey="txtContraseniaResource1"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="botonchico"
                                    meta:resourcekey="btnAceptarResource1" onclick="btnAceptar_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top: 10px;">
                                <asp:HyperLink ID="hplRecuperar" runat="server" 
                                    NavigateUrl="~/frmRecuperapass.aspx" Text="Recuperar contraseña" CssClass="Textonormal"
                                    meta:resourcekey="hplRecuperarResource1"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top: 10px;">
                                <asp:HyperLink ID="hplDesactivar" runat="server" Visible="false"
                                    NavigateUrl="~/frmDesactiva.aspx" Text="Desactivar sesión" CssClass="Textonormal"
                                    meta:resourcekey="hplDesactivarResource1"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <%--Boton oculto para los PopUp's--%>
    <asp:Button runat="server" ID="bntHidden" Style="display: none" 
        meta:resourcekey="bntHiddenResource1"/>
    
    <%--Pop Up para mostrar politicas--%>
    <cc1:ModalPopupExtender ID="mppPoliticas"
        runat="server"  
        PopupControlID="pnlPoliticas" 
        TargetControlID="bntHidden" 
        BackgroundCssClass="modalBackground" 
        DynamicServicePath="" BehaviorID="mppPoliticas">
    </cc1:ModalPopupExtender>

    <%--Style="display: none; overflow:scroll;" Style="display: none;"  --%>
    <asp:Panel ID="pnlPoliticas"
        runat="server" 
        CssClass="modalPopup" 
        align="center"
        Style="display: none;"
        meta:resourcekey="pnlPoliticasResource1">

        <div>
            <table style="text-align:center; vertical-align:top; width:100%; height:100%">
                <tr>
                    <td style="background:#004288; height: 10%; text-align:left;">
                        <asp:Label ID="lblppPoliticasTitulo" runat="server" Font-Bold="True" CssClass="subTitulo1Blanco" Text="TÉRMINOS Y CONDICIONES DE USO DEL SITIO."
                            meta:resourcekey="lblppPoliticasTituloResource1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="height:80%; text-align:left;">
                        <asp:Label ID="lblppPoliticasDescripcion" runat="server" CssClass="Textonormal" meta:resourcekey="lblppPoliticasDescripcionResource1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="height:10%; text-align:center;">
                        <asp:Button ID="btnppPoliticasCancelar" runat="server" CssClass="botonchico" Text="No Acepto" onclick="btnppPoliticasCancelar_Click" 
                            meta:resourcekey="btnppPoliticasCancelarResource1"/>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnppPoliticasAceptar"  runat="server" CssClass="botonchico" Text="Acepto" onclick="btnppPoliticasAceptar_Click" 
                            meta:resourcekey="btnppPoliticasAceptarResource1"/>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <%--Pop Yp para seleccionar perfil--%>
    <cc1:ModalPopupExtender ID="mppPerfil"
        runat="server"  
        PopupControlID="pnlPerfil" 
        TargetControlID="bntHidden" 
        BackgroundCssClass="modalBackground" 
        DynamicServicePath="" BehaviorID="mppPerfil">
    </cc1:ModalPopupExtender>

    <asp:Panel ID="pnlPerfil" 
        runat="server"
        CssClass="modalPopupPerfil" 
        align="center"
        Style="display: none;"
        meta:resourcekey="pnlPerfilResource1">

        <div style="width: 100%; height: 100%;">
            <table width="100%">
                <tr>
                    <td style="background:#004288; height: 10%; text-align:left;">
                        <asp:Label ID="lblTituloPerfil" runat="server" Font-Bold="True" CssClass="subTitulo1Blanco"
                            meta:resourcekey="lblTituloPerfilResource1"></asp:Label>
                            <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblPerfil" runat="server" Text="Perfil :" CssClass="Textonormal"
                                    meta:resourcekey="lblPerfilResource1"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlPerfil" runat="server" CssClass="txtLogin"
                                         meta:resourcekey="ddlPerfilResource1" >
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnAceptaPerfil" runat="server" Text="Aceptar" 
                            CssClass="botonchico" meta:resourcekey="btnAceptaPerfilResource1" 
                            onclick="btnAceptaPerfil_Click"/>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

</asp:Content>
