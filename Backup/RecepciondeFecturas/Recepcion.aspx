﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Recepcion.aspx.cs" Inherits="RecepciondeFecturas.Recepcion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        }

        function pageLoad(sender, args) {
            //
            document.getElementsByClassName('ajax__fileupload_selectFileButton')[0].innerHTML = "Seleccionar archivo (s)";
            document.getElementsByClassName('ajax__fileupload_dropzone')[0].innerHTML = "Suelte aqui los archivos";
            document.getElementsByClassName('ajax__fileupload_topFileStatus')[0].innerHTML = "";
            document.getElementsByClassName('ajax__fileupload_uploadbutton')[0].innerHTML = "Subir";
            document.getElementsByClassName('ajax__fileupload_removeButton')[0].innerHTML = "Borrar";
            document.getElementsByClassName('ajax__fileupload_Pending ')[0].innerHTML = "Pendiente";
        }

//        window.onbeforeunload = confirmaSalida;
//        function confirmaSalida() {
//            return "Vas a abandonar esta pagina. Si iniciaste sesión, debes dar clic en Salir para cerrar correctamente la sesión";  
//        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <div class="mainRecepcion">
        
        <div class="divBienvenido">
            <asp:Label ID="lblBienvenida" runat="server" Text="Carga de archivos XML y PDF" CssClass="Titulo1" meta:resourcekey="lblBienvenidaResource1"></asp:Label>
        </div>
        <div class="divCarga">
            <table style="width: 100%; height: auto; ">
                <tr>
                    <td style="width: 70%; height:50px; text-align: left; vertical-align: top;">
                        <cc1:AjaxFileUpload ID="afuXML" runat="server" ThrobberID="myThrobber"
                            ContextKeys="fred"
                            AllowedFileTypes="xml,pdf"
                            MaximumNumberOfFiles="10"
                            onuploadcomplete="afuXML_UploadComplete"/>
                    </td>
                    <td style="width: 30%; text-align: left; vertical-align: top;">
<%--                        <asp:Image ID="imgSemaforo" runat="server" ImageUrl="~/Imagenes/grandeUno.cs.png" />--%>
                        <asp:UpdatePanel ID="updpanelProcesar" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnProcesar" runat="server" Text="Procesar" CssClass="botonchico" onclick="btnProcesar_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <asp:UpdateProgress ID="updprogressProcesar" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="updpanelProcesar">
            <ProgressTemplate>
                <div class="overlay"></div>
                <div class="overlayContent">
                <h2>Cargando....</h2>
                    <img src="Imagenes/ajax-loader.gif" alt="Loading" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
<%--        <asp:UpdatePanel ID="updpanelProcesar" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnProcesar" runat="server" Text="Procesar" CssClass="botonchico" onclick="btnProcesar_Click" />
                <asp:UpdateProgress ID="updprogressProcesar" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="updpanelProcesar">
                    <ProgressTemplate>
                        <div class="overlay"></div>
                        <div class="overlayContent">
                        <h2>Cargando....</h2>
                            <img src="Imagenes/ajax-loader.gif" alt="Loading" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </ContentTemplate>
        </asp:UpdatePanel>--%>
        <br />
        <asp:Label ID="lblporProcesar" runat="server" Text="Hay archivos pendientes por procesar, Para revisar da clic en el siguiente botón :" CssClass="Textonormal" Font-Bold="true" Visible="false"></asp:Label>
        &nbsp;&nbsp;
        <asp:Button ID="btnMostrar" runat="server" Text="Mostrar" CssClass="botonchico" 
            Visible="false" onclick="btnMostrar_Click"/>
        <br />
        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="botonchico" onclick="btnBuscar_Click" />
        &nbsp;&nbsp;
        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" Visible="false" CssClass="botonchico" onclick="btnCancel_Click"/>
        
        <asp:Panel ID="pnlBuscar" runat="server" Visible="false">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblSociedad" runat="server" Text="Sociedad: "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSociedad" runat="server" Width="500px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblNombrearchivo" runat="server" Text="Nombre archivo :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombrearchivo" runat="server"  Width="500px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblSerie" runat="server" Text="Serie :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSerie" runat="server"  Width="100px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblFolio" runat="server" Text="Folio :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFolio" runat="server"  Width="100px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblfechaIni" runat="server" Text="Inicio recepción:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaini" runat="server" Width="72px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="cleFechaini" runat="server" TargetControlID="txtFechaini" Format="dd/MM/yyyy"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFechafin" runat="server" Text="Fin recepción :"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechafin" runat="server" Width="72px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="cleFechafin" runat="server" TargetControlID="txtFechafin" Format="dd/MM/yyyy"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEstatus" runat="server" Text="Estatus"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEstatus" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table> 
        </asp:Panel>
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnActualizar" runat="server" 
                        ImageUrl="~/Imagenes/Actualizar2.jpg" Width="30px" ToolTip="Actualizar" 
                        onclick="btnActualizar_Click" meta:resourcekey="btnActualizarResource1" />
                </td>
            </tr>
        </table>
        <div class="divgrvProcesados">
            <table style="width: 100%; height: 100%; text-align:center;">
                <%--Grid--%>
                <tr>
                    <td>
                        <asp:GridView ID="dgvRecepcionArchivos" runat="server" AutoGenerateColumns="False" 
                            AllowPaging="True" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="10"
                            DataKeyNames="Id_Recepcion" Width="100%" 
                            AlternatingRowStyle-CssClass="alt"
                            onrowdatabound="dgvRecepcionArchivos_RowDataBound" 
                            onrowcommand="dgvRecepcionArchivos_RowCommand" 
                            onpageindexchanging="dgvRecepcionArchivos_PageIndexChanging">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
                                <asp:BoundField DataField="Id_Recepcion" HeaderText="ID" ><ItemStyle HorizontalAlign="Center" Width="20px" /></asp:BoundField>
                                <asp:BoundField DataField="Fecha_Recepcion" HeaderText="Fecha de Recepción" DataFormatString="{0:g}" ><ItemStyle HorizontalAlign="Center" Width="200px" /></asp:BoundField>
                                <asp:BoundField DataField="Nombre_XML" HeaderText="Archivo XML" ><ItemStyle HorizontalAlign="Left" Width="250px"/></asp:BoundField>
                                <asp:BoundField DataField="Nombre_PDF" HeaderText="Archivo PDF" ><ItemStyle HorizontalAlign="Left"  Width="250px"/></asp:BoundField>
                                <asp:BoundField DataField="Serie" HeaderText="Serie" ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Folio" HeaderText="Folio" ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" ><ItemStyle HorizontalAlign="Left"  Width="250px"/></asp:BoundField>
                                <asp:BoundField DataField="ID_Estatus" HeaderText="Estatus"><ItemStyle HorizontalAlign="Left"  Width="20px"/></asp:BoundField>
                            
                                <asp:TemplateField HeaderText="Estatus" ItemStyle-HorizontalAlign="Center" meta:resourcekey="BoundFieldResource8" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:Image ID="estatus" runat="server" Height="20px"></asp:Image>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PDF" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgPdf" runat="server" Height="20px" ImageUrl="~/Imagenes/pdf.png" CommandName="cmdPDF" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="XML" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgXml" runat="server" Height="20px" ImageUrl="~/Imagenes/xml.png" CommandName="cmdXML" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="#B9B9B9" Font-Bold="True" ForeColor="White" />
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <asp:Button runat="server" ID="bntHidden" Style="display: none" />
    <!-- ModalPopupExtender -->

    <cc1:ModalPopupExtender ID="mppMostrar"
        runat="server" 
        PopupControlID="pnlMostrar" 
        TargetControlID="bntHidden" 
        BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>

    <asp:Panel ID="pnlMostrar"
        runat="server" 
        CssClass="modalPopupMostrar" 
        align="center">

        <%--Tabla Principal--%>
        <table style="text-align:center; vertical-align:top; width:100%; height:100%">
            <%--Tabla encabezado y mensaje--%>
            <tr>
                <td align="center">
                    <table style="text-align:center; vertical-align:top; width:100%; height:46px">
                        <tr>
                            <td style="text-align: center; vertical-align: top;">
                                <asp:Label ID="lblTituloPU" runat="server" Text="Documentos pendientes por cargar" CssClass="Titulo1"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <asp:Label ID="lblMensajePP" runat="server" CssClass="lblMensajes"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%--AjaxFileUpload XML--%>
            <tr valign="top">
                <td>
                    <table style="text-align:center; vertical-align:top; width:100%;">
                        <tr>
                            <td align="left" valign="top">
                                <asp:GridView ID="dgvArchivos" runat="server" AutoGenerateColumns="False" CssClass="mGrid">
                                    <Columns>
                                        <asp:BoundField DataField="No" HeaderText="No" ><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                        <asp:BoundField DataField="ArchivoXML" HeaderText="Archivo" ><ItemStyle HorizontalAlign="Left" /></asp:BoundField>
                                        <asp:BoundField DataField="ArchivoPDF" HeaderText="Archivo PDF" Visible="false" ><ItemStyle HorizontalAlign="Left" /></asp:BoundField>
                                        <%--<asp:BoundField DataField="Accion" HeaderText="Accion" ><ItemStyle HorizontalAlign="Left" /></asp:BoundField>--%>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                    
            <%--Opciones del PopUp--%>
            <tr>
                <td align="center">
                    
                </td>
            </tr>

            <%--Botones--%>
            <tr>
                <td align="center">
                    <table style="text-align:center; vertical-align:top; width:100%; height:46%">
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" CssClass="botonchico" 
                                    onclick="btnLimpiar_Click"/>
                            </td>
                            <td align="center">
                                <asp:Button ID="btnReprocesar" runat="server" Text="Reprocesar" 
                                    CssClass="botonchico" onclick="btnProcesar_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
