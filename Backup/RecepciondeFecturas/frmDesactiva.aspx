﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmDesactiva.aspx.cs" Inherits="RecepciondeFecturas.frmDesactiva" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Style/Site.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" 
        meta:resourcekey="lblMensajeResource1"></asp:Label>
    <div class="mainLogin">
        <table class="TablaUno">
            <tr>
                <td style="text-align: left; padding-bottom: 10px;">
                    <asp:Label ID="lblTituloDesactivar" runat="server" Text="Desactiva Usuario" 
                        CssClass="Titulo1" meta:resourcekey="lblTituloDesactivarResource1"></asp:Label>            
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 300px; text-align: center;">
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblUsuario" runat="server" Text="Usuario" Font-Bold="True" 
                                    CssClass="Textonormal" meta:resourcekey="lblUsuarioResource1"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtUsuario" runat="server" CssClass="txtLogin" Width="100px" 
                                    meta:resourcekey="txtUsuarioResource1"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblCorreo" runat="server" Text="Correo" Font-Bold="True" 
                                    CssClass="Textonormal" meta:resourcekey="lblCorreoResource1"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtCorreo" runat="server" CssClass="txtLogin" Width="250px" 
                                    meta:resourcekey="txtCorreoResource1"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnDesactivar" runat="server" Text="Desactivar" 
                                    CssClass="botonchico" meta:resourcekey="btnDesactivarResource1" 
                                    onclick="btnDesactivar_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top: 10px;">
                                <asp:HyperLink ID="hplRegresar" runat="server" 
                                    NavigateUrl="~/Default.aspx" Text="Regresar" CssClass="Textonormal" 
                                    meta:resourcekey="hplRegresarResource1"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
