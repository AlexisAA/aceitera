﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using System.Data;
using BLRecepcion.Portal;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using DALRecepcion.Bean.Portal;

namespace RecepciondeFecturas
{
    public partial class Cat_Recepcion : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                    
                    //Inicializamos las fechas
                    CargarFechas();
                    llenaddlProveedores();
                    llenaddlSocieddes();
                    llenaddlEstatus();
                    txtNombrearchivo.Text = "";

                    llenaGrid();

                    dgvRecepcionArchivos.Columns[9].Visible = false;

                }
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            dgvRecepcionArchivos.DataSource = null;
            dgvRecepcionArchivos.DataBind();

            dgvRecepcionArchivos.Columns[9].Visible = true;
            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
            dgvRecepcionArchivos.Columns[9].Visible = false;
        }

        protected void btnExportar_Click(object sender, ImageClickEventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);

            DataTable dtExporta = null;
            try
            {
                dtExporta = new DataTable();
                dtExporta = buscaArchivos();
                if (dtExporta.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    StringWriter sw = new StringWriter(sb);
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    Page pagina = new Page();
                    HtmlForm hForm = new HtmlForm();
                    GridView gv = new GridView();
                    gv.EnableViewState = false;
                    gv.DataSource = dtExporta;
                    gv.DataBind();
                    pagina.EnableEventValidation = false;
                    pagina.DesignerInitialize();
                    pagina.Controls.Add(hForm);
                    hForm.Controls.Add(gv);
                    pagina.RenderControl(htw);
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=Archivos_Recibidos.xls");
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(sb.ToString());
                    Response.End();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                muestraError("btnExportar", "btnExportar", "Error", ex.Message);
            }
        }

        protected void btnActualizar_Click(object sender, ImageClickEventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            dgvRecepcionArchivos.DataSource = null;
            dgvRecepcionArchivos.DataBind();

            dgvRecepcionArchivos.Columns[9].Visible = true;
            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
            dgvRecepcionArchivos.Columns[9].Visible = false;
        }

        protected void dgvRecepcionArchivos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvRecepcionArchivos.PageIndex = e.NewPageIndex;

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);

            dgvRecepcionArchivos.Columns[9].Visible = true;
            llenaGrid();
            dgvRecepcionArchivos.Columns[9].Visible = false;
        }

        protected void dgvRecepcionArchivos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int IdRecepcion = -1;
            string Tipo = string.Empty;
            switch (e.CommandName)
            {
                case "cmdPDF":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/pdf";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
                case "cmdXML":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/xml";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
            }
        }

        protected void dgvRecepcionArchivos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Image imgEstatus = (Image)e.Row.FindControl("estatus");
                ImageButton btnPdf = (ImageButton)e.Row.FindControl("imgPdf");
                ImageButton btnXml = (ImageButton)e.Row.FindControl("imgXml");
                int idEstatus = -1;
                string idRecepcion = "";
                idEstatus = Convert.ToInt32(e.Row.Cells[9].Text);
                idRecepcion = e.Row.Cells[0].Text;

                btnPdf.CommandArgument = idRecepcion;
                btnXml.CommandArgument = idRecepcion;
                if (idEstatus > 0)
                {

                    BLEstatus buscaEstatus = new BLEstatus();
                    try
                    {
                        List<Estatus> csEstatus = buscaEstatus.buscaEstatus(idEstatus);
                        if (csEstatus.Count > 0)
                        {
                            imgEstatus.ImageUrl = csEstatus[0].Ruta.ToString();
                            imgEstatus.ToolTip = csEstatus[0].Descripcion.ToString();
                            //btnEstatus.CommandArgument = csEstatus[0].ID_Estatus.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        muestraError("dgvRecepcionArchivos", "buscaEstatus", "Error", ex.Message);
                    }

                }
            }
        }


        /// <summary>
        /// Metodo que inicializa las fechas
        /// </summary>
        private void CargarFechas()
        {
            DateTime fecha = DateTime.Now.Date;
            this.cleFechaini.SelectedDate = new DateTime(fecha.Year, fecha.Month, 1);
            this.cleFechafin.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
        }

        /// <summary>
        /// Metodo para llenad DDL de sociedades
        /// </summary>
        private void llenaddlSocieddes()
        {
            ddlSociedad.Items.Clear();

            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                List<Sociedades> csSociedad = buscaSociedad.buscaSociedad();
                if (csSociedad.Count > 0)
                {
                    ddlSociedad.DataValueField = "Id_Sociedad";
                    ddlSociedad.DataTextField = "Razon_Social";
                    ddlSociedad.Items.Clear();
                    ddlSociedad.Items.Add(new ListItem("Todas", "0"));
                    ddlSociedad.AppendDataBoundItems = true;
                    ddlSociedad.DataSource = csSociedad;
                    ddlSociedad.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("llenaddlSocieddes", "llenaddlSocieddes", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar DDL Proveedores
        /// </summary>
        private void llenaddlProveedores()
        {
            ddlProveedor.Items.Clear();

            BLProveedores buscaProveedor = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = buscaProveedor.buscaProveedor();
                if (csProveedores.Count > 0)
                {
                    ddlProveedor.DataValueField = "Id_Proveedor";
                    ddlProveedor.DataTextField = "Nombre";
                    ddlProveedor.Items.Clear();
                    ddlProveedor.Items.Add(new ListItem("Todas", "0"));
                    ddlProveedor.AppendDataBoundItems = true;
                    ddlProveedor.DataSource = csProveedores;
                    ddlProveedor.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("llenaddlProveedores", "llenaddlProveedores", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar ddl estatus
        /// </summary>
        private void llenaddlEstatus()
        {
            ddlEstatus.Items.Clear();

            BLEstatus buscaEstatus = new BLEstatus();
            try
            {
                List<Estatus> csEstatus = buscaEstatus.buscaEstatus();
                if (csEstatus.Count > 0)
                {
                    ddlEstatus.DataValueField = "ID_Estatus";
                    ddlEstatus.DataTextField = "Nomnre";
                    ddlEstatus.Items.Clear();
                    ddlEstatus.Items.Add(new ListItem("Todas", "-1"));
                    ddlEstatus.AppendDataBoundItems = true;
                    ddlEstatus.DataSource = csEstatus;
                    ddlEstatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("llenaddlEstatus", "llenaddlEstatus", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        private void llenaGrid()
        {
            dgvRecepcionArchivos.DataSource = null;
            dgvRecepcionArchivos.DataBind();

            DataTable dtBusca = new DataTable();
            try
            {
                dtBusca = buscaArchivos();
                if (dtBusca.Rows.Count > 0)
                {
                    dgvRecepcionArchivos.DataSource = dtBusca;
                    dgvRecepcionArchivos.DataBind();
                }
                else
                    inicializadgvRecepcion();

            }
            catch (Exception ex)
            {
                muestraError("btnBuscar", "llenaGrid", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para buscar archivos
        /// </summary>
        /// <returns></returns>
        private DataTable buscaArchivos()
        {
            #region VAriables locales
            int ID_Proveedor = -1;
            int ID_Sociedad = -1;
            string Nombre_archivo = string.Empty;
            string Serie = string.Empty;
            string Folio = string.Empty;
            DateTime Fecha_Ini = new DateTime();
            DateTime Fecha_Fin = new DateTime();
            int Id_Estatus = -1;
            string Id_Moneda = string.Empty;
            #endregion

            #region Asignación
            ID_Proveedor = Convert.ToInt32(ddlProveedor.SelectedValue.ToString());
            ID_Sociedad = Convert.ToInt32(ddlSociedad.SelectedValue.ToString());
            Nombre_archivo = txtNombrearchivo.Text.Trim() == string.Empty ? "" : txtNombrearchivo.Text.Trim();
            Serie = txtSerie.Text.Trim() == string.Empty ? "" : txtSerie.Text.Trim();
            Folio = txtFolio.Text.Trim() == string.Empty ? "" : txtFolio.Text.Trim();
            Fecha_Ini = Convert.ToDateTime(cleFechaini.SelectedDate.ToString());
            Fecha_Fin = Convert.ToDateTime(cleFechafin.SelectedDate.ToString());
            Id_Estatus = Convert.ToInt32(ddlEstatus.SelectedValue.ToString());
            #endregion

            DataTable dtArchivos = null;
            BLArchivos busca = new BLArchivos();
            try
            {
                dtArchivos = new DataTable();
                dtArchivos = busca.buscaArchivos_CAT(ID_Proveedor, ID_Sociedad, Nombre_archivo, Serie, Folio, Fecha_Ini, Fecha_Fin, Id_Estatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtArchivos;

        }

        /// <summary>
        /// Metodo para inicializar el Grid
        /// </summary>
        private void inicializadgvRecepcion()
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("Id_Recepcion", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Nombre", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Nombre_Sociedad", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Fecha_Recepcion", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("Nombre_XML", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Fecha_Factura", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("Folio", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Serie", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Observaciones", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("ID_Estatus", typeof(Int32)));
            

            DataRow drVacio = dtArchivos.NewRow();
            drVacio[0] = 0;
            drVacio[1] = "";
            drVacio[2] = "";
            drVacio[3] = DateTime.Now;
            drVacio[4] = "";
            drVacio[5] = DateTime.Now;
            drVacio[6] = "";
            drVacio[7] = "";
            drVacio[8] = "";
            drVacio[9] = 0;

            dtArchivos.Rows.Add(drVacio);
            dgvRecepcionArchivos.DataSource = dtArchivos;
            dgvRecepcionArchivos.DataBind();
            int totalcolums = dgvRecepcionArchivos.Rows[0].Cells.Count;
            dgvRecepcionArchivos.Rows[0].Cells.Clear();
            dgvRecepcionArchivos.Rows[0].Cells.Add(new TableCell());
            dgvRecepcionArchivos.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvRecepcionArchivos.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }

        /// <summary>
        /// Metro para mostrar archivos
        /// </summary>
        /// <param name="_idrecepcion"></param>
        /// <param name="_tipo"></param>
        private void mustrarArchivo(int _idrecepcion, string _tipo)
        {
            if (Session["id_Usuario"] != null)
            {
                int ID_Usuario = -1;
                string nombreArchivo = string.Empty;

                ID_Usuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                //Primero buscmos el archivo
                BLArchivos busca = new BLArchivos();
                try
                {
                    List<Archivos> csArchivos = busca.buscaPdfXml(_idrecepcion);
                    if (csArchivos.Count > 0)
                    {
                        byte[] datosArchivo = null;
                        if (_tipo == "application/pdf")
                        {
                            datosArchivo = csArchivos[0].Archivo_PDF;
                            nombreArchivo = csArchivos[0].Nombre_PDF;
                        }
                        else
                        {
                            datosArchivo = csArchivos[0].Archivo_XML;
                            nombreArchivo = csArchivos[0].Nombre_XML;
                        }

                        Response.Clear();
                        Response.ContentType = _tipo;
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + nombreArchivo);
                        Response.BinaryWrite(datosArchivo);
                        Response.End();
                        Response.Close();
                    }

                    //Response.Redirect("~/Recepcion.aspx", false);
                }
                catch (Exception ex)
                {
                    muestraError("dgvAclraciones", "mustrarArchivo", "Error", ex.Message);
                }
            }
        }

        #region Manejo de errores
        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("CAT_Recepcion", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("CAT_Recepcion", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }
        #endregion

    }
}