﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Portal;


namespace RecepciondeFecturas
{
    public partial class Cat_Avisos : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                    llenaddlProveedores();

                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            int Id_Proveedor = -1;
            string tituloEs = string.Empty;
            string tituloEn = string.Empty;
            string mensajeEs = string.Empty;
            string mensajeEn = string.Empty;

            tituloEs = txtTituloes.Text.Trim() == "" ? null : txtTituloes.Text.Trim();
            tituloEn = txtTituloen.Text.Trim() == "" ? null : txtTituloen.Text.Trim();
            mensajeEs = txtMensajees.Text.Trim() == "" ? null : txtMensajees.Text.Trim();
            mensajeEn = txtMensajeen.Text.Trim() == "" ? null : txtMensajeen.Text.Trim();

            if (tituloEn != null && tituloEs != null)
            {
                if (mensajeEs != null && mensajeEn != null)
                {
                    Id_Proveedor = Convert.ToInt32(ddlProveedor.SelectedValue.ToString());
                    bool resp = false;

                    resp = insertaAviso(Id_Proveedor, tituloEs, mensajeEs, tituloEn, mensajeEn);
                    if (resp)
                    {
                        muestraError("InsertaAviso", "InsertaAviso", "Ok", "Aviso guradado correctamente");
                        ddlProveedor.SelectedValue = "0";
                        txtTituloes.Text = string.Empty;
                        txtMensajees.Text = string.Empty;
                        txtTituloen.Text = string.Empty;
                        txtMensajeen.Text = string.Empty;
                    }
                    else
                    {
                        muestraError("InsertaAviso", "InsertaAviso", "Error", "El Aviso no se pudo guradar");
                    }
                }
                else
                    muestraMensaje("Debes agregar por lo menos un mensaje");
            }
            else
            {
                muestraMensaje("Debes agregar por lo menos un Titulo");
            }
        }

        /// <summary>
        /// Metodo para llenar el DDL de provedores
        /// </summary>
        private void llenaddlProveedores()
        {
            BLProveedores busca = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = busca.buscaProveedores();
                if (csProveedores.Count > 0)
                {

                    ddlProveedor.DataTextField = "Nombre";
                    ddlProveedor.DataValueField = "ID_Proveedor";
                    ddlProveedor.Items.Add(new ListItem("---------- Todos ----------", "0"));
                    ddlProveedor.AppendDataBoundItems = true;
                    ddlProveedor.DataSource = csProveedores;
                    ddlProveedor.DataBind();
                }
                else
                {
                    ddlProveedor.DataTextField = "Nombre";
                    ddlProveedor.DataValueField = "ID_Proveedor";
                    ddlProveedor.Items.Add(new ListItem("No hay datos para mostrar", "-1"));
                    ddlProveedor.AppendDataBoundItems = true;
                }
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaddlProveedores", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para insertar aviso.
        /// </summary>
        /// <param name="_idproveedor"></param>
        /// <param name="_tituloes"></param>
        /// <param name="_mensajees"></param>
        /// <param name="_tituloen"></param>
        /// <param name="_mensajeen"></param>
        /// <returns></returns>
        private bool insertaAviso(int _idproveedor, string _tituloes, string _mensajees, string _tituloen, string _mensajeen)
        {
            bool resp = false;
            BLAvisos inserta = new BLAvisos();
            try
            {
                resp = inserta.insertaAviso(_idproveedor, _tituloes, _mensajees, _tituloen, _mensajeen);
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "insertaAviso", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Usuarios", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Avisos", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

    }
}