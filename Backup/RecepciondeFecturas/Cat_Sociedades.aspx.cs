﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using System.Data;

namespace RecepciondeFecturas
{
    public partial class Cat_Sociedades : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                    Session["AccionSociedades"] = null;

                    llenaGrid();
                }
            }
        }

        protected void dgvSociedades_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton btn = (ImageButton)e.Row.FindControl("Seleccionar");
                string idSociedad = string.Empty;
                idSociedad = e.Row.Cells[1].Text;
                btn.CommandArgument = idSociedad;
            }
        }

        protected void dgvSociedades_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "CSeleccionar")
            {
                int idSociedad = -1;
                idSociedad = Convert.ToInt32(e.CommandArgument.ToString());

                if (idSociedad > -1)
                {
                    datosSociedad(idSociedad);
                    if (btnGuardar.Text == "Aceptar")
                    {
                        habilitaActualiza();
                    }
                    pnlDatos.Visible = true;
                    btnNuevo.Visible = false;
                    btnActualizar.Visible = true;
                    btnBuscar.Visible = false;
                    btnGuardar.Visible = false;
                    btnCancelar.Visible = true;
                }
            }
        }

        protected void dgvSociedades_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvSociedades.PageIndex = e.NewPageIndex;
            if (btnGuardar.Text == "Aceptar")
            {

            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            //Limpia textos
            limpiaTextos();
            
            //Habilita textos
            habiltaTextos();

            //Muestra panel
            pnlDatos.Visible = true;

            //Habilitamos botones
            btnNuevo.Visible = false;
            btnActualizar.Visible = false;
            btnBuscar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            dgvSociedades.Enabled = false;

            Session["AccionSociedades"] = "Nuevo";
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            habilitaActualiza();
            btnNuevo.Visible = false;
            btnActualizar.Visible = false;
            btnBuscar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            dgvSociedades.Enabled = false;
            Session["AccionSociedades"] = "Actualizar";
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Limpia textos
            limpiaTextos();

            //Habilita busqueda
            habilitaBusqueda();

            //Muestra panel
            pnlDatos.Visible = true;

            //Habilitamos botones
            btnNuevo.Visible = false;
            btnBuscar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;

            btnGuardar.Text = "Aceptar";

            Session["AccionSociedades"] = "Buscar";
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            string strAccion = string.Empty;

            if (Session["AccionSociedades"] != null)
            {
                strAccion = Session["AccionSociedades"].ToString();

                switch (strAccion)
                {
                    case "Nuevo":
                        #region Guarda registro
                        //Validamos datos
                        if (validaDatos())
                        {
                            if (validaDuplicado(txtRFC.Text.Trim()))
                            {
                                //Guradamos la sociedad
                                int idSociedad_SAP = 0;
                                string Nombre = string.Empty;
                                string Razon_Social = string.Empty;
                                string RFC_Sociedad = string.Empty;
                                string Usuario_PAC = string.Empty;
                                string Pass_PAC = string.Empty;
                                bool Valido = false;

                                idSociedad_SAP = Convert.ToInt32(txtIdSAP.Text.Trim());
                                Nombre = txtNombre.Text.Trim();
                                Razon_Social = txtRSocial.Text.Trim();
                                RFC_Sociedad = txtRFC.Text.Trim();
                                Usuario_PAC = txtUsuarioPAC.Text.Trim();
                                Pass_PAC = txtPasPAC.Text.Trim();
                                Valido = chbValido.Checked == true ? true : false;

                                bool respuesta = false;
                                respuesta = insertaSociedad(idSociedad_SAP, Nombre, Razon_Social, RFC_Sociedad, Usuario_PAC, Pass_PAC, Valido);
                                if (respuesta)
                                {
                                    muestraError("btnGuardar", "Guarda", "Ok", "Registro guardado correctamente. Sociedad : " + Nombre);
                                    guardaLog("Guardar", "Alta", "Ok", "Registro guardado correctamente. Sociedad : " + Nombre);
                                }

                                llenaGrid();
                                limpiaTextos();
                                inhabilitaTextos();

                                btnNuevo.Visible = true;
                                btnActualizar.Visible = false;
                                btnBuscar.Visible = true;
                                btnGuardar.Visible = false;
                                btnCancelar.Visible = false;
                                pnlDatos.Visible = false;
                                dgvSociedades.Enabled = true;

                            }
                        }
                        #endregion
                        break;
                    case "Actualizar":
                        #region Actualiza Registro
                        if (validaDatosActualiza())
                        {
                            int IdSociedad = 0;
                            string Nombre = string.Empty;
                            string Razon_Social = string.Empty;
                            string Usuario_PAC = string.Empty;
                            string Pass_PAC = string.Empty;
                            bool Valido = false;

                            IdSociedad = Convert.ToInt32(txtIdSociedad.Text.Trim());
                            Nombre = txtNombre.Text.Trim();
                            Razon_Social = txtRSocial.Text.Trim();
                            Usuario_PAC = txtUsuarioPAC.Text.Trim();
                            Pass_PAC = txtPasPAC.Text.Trim();
                            Valido = chbValido.Checked == true ? true : false;

                            bool respuesta = false;
                            respuesta = actualizaSociedad(IdSociedad, Nombre, Razon_Social, Usuario_PAC, Pass_PAC, Valido);
                            if (respuesta)
                            {
                                muestraError("btnGuardar", "Actualiza", "Ok", "Registro Actualizado correctamente. Sociedad : " + Nombre);
                                guardaLog("Guardar", "Actualiza", "Ok", "Registro Actualizado correctamente. Sociedad : " + Nombre);
                            }

                            llenaGrid();
                            limpiaTextos();
                            inhabilitaTextos();

                            btnNuevo.Visible = true;
                            btnActualizar.Visible = false;
                            btnBuscar.Visible = true;
                            btnGuardar.Visible = false;
                            btnCancelar.Visible = false;
                            pnlDatos.Visible = false;
                            dgvSociedades.Enabled = true;
                        }
                        #endregion
                        break;
                    case "Buscar":
                        #region Busca Registro
                        int idSAP = -1;
                        string nomre = string.Empty;
                        string rsocial = string.Empty;
                        string rfc = string.Empty;
                        bool valido = false;

                        idSAP = txtIdSociedad.Text == "" ? -1 : Convert.ToInt32(txtIdSociedad.Text.Trim());
                        nomre = txtNombre.Text == "" ? null : txtNombre.Text.Trim();
                        rsocial = txtRSocial.Text == "" ? null : txtRSocial.Text.Trim();
                        rfc = txtRFC.Text == "" ? null : txtRFC.Text.Trim();
                        valido = chbValido.Checked == true ? true : false;

                        buscaRegistro(idSAP, nomre, rsocial, rfc, valido);
                        #endregion
                        break;
                }
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiaTextos();
            inhabilitaTextos();

            btnNuevo.Visible = true;
            btnActualizar.Visible = false;
            btnBuscar.Visible = true;
            btnGuardar.Visible = false;
            btnGuardar.Text = "Guardar";
            btnCancelar.Visible = false;
            dgvSociedades.Enabled = true;
            pnlDatos.Visible = false;
            Session["AccionSociedades"] = null;
            llenaGrid();
        }

        /// <summary>
        /// Metodo para llenar Grid
        /// </summary>
        private void llenaGrid()
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            dgvSociedades.DataSource = null;
            dgvSociedades.DataBind();

            BLSociedades busca = new BLSociedades();
            try
            {
                List<Sociedades> csSociedades = busca.buscaSociedad();
                if (csSociedades.Count > 0)
                {
                    dgvSociedades.DataSource = csSociedades;
                    dgvSociedades.DataBind();
                }
                else
                {
                    inicializaGrid();
                }
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaGrid", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para inicializar el Grid
        /// </summary>
        private void inicializaGrid()
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("Id_Sociedad", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Id_Sociedad_SAP", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Nombre_Sociedad", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Razon_Social", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("RFC_Sociedad", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Usuario_ME", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Contrasenia_ME", typeof(String)));

            DataRow drVacio = dtArchivos.NewRow();
            drVacio[0] = 0;
            drVacio[1] = 0;
            drVacio[2] = "";
            drVacio[3] = "";
            drVacio[4] = "";
            drVacio[5] = "";
            drVacio[6] = "";

            dtArchivos.Rows.Add(drVacio);
            dgvSociedades.DataSource = dtArchivos;
            dgvSociedades.DataBind();
            int totalcolums = dgvSociedades.Rows[0].Cells.Count;
            dgvSociedades.Rows[0].Cells.Clear();
            dgvSociedades.Rows[0].Cells.Add(new TableCell());
            dgvSociedades.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvSociedades.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }

        /// <summary>
        /// Metodo para limpiar las cajas de texto
        /// </summary>
        private void limpiaTextos()
        {
            txtIdSociedad.Text = string.Empty;
            txtIdSAP.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtRSocial.Text = string.Empty;
            txtRFC.Text = string.Empty;
            txtUsuarioPAC.Text = string.Empty;
            txtPasPAC.Text = string.Empty;
            chbValido.Checked = false;
        }

        /// <summary>
        /// Metodo pra habilitar campos para dar de alta
        /// </summary>
        private void habiltaTextos()
        {
            txtIdSAP.Enabled = true;
            txtNombre.Enabled = true;
            txtRSocial.Enabled = true;
            txtRFC.Enabled = true;
            txtUsuarioPAC.Enabled = true;
            txtPasPAC.Enabled = true;
            chbValido.Enabled = true;
        }

        /// <summary>
        /// Metodo para habilitar textos de busqueda
        /// </summary>
        private void habilitaBusqueda()
        {
            txtIdSAP.Enabled = true;
            txtNombre.Enabled = true;
            txtRSocial.Enabled = true;
            txtRFC.Enabled = true;
            chbValido.Enabled = true;
            chbValido.Checked = true;
        }

        /// <summary>
        /// Metodo para habilitar textos para actualizar Registro
        /// </summary>
        private void habilitaActualiza()
        {
            txtNombre.Enabled = true;
            txtRSocial.Enabled = true;
            txtUsuarioPAC.Enabled = true;
            txtPasPAC.Enabled = true;
            chbValido.Enabled = true;
        }

        /// <summary>
        /// Metodo para validar textos
        /// </summary>
        /// <returns></returns>
        private bool validaDatos()
        {
            bool bResp = true;
            int idSAP = 0;
            string nombre = string.Empty;
            string rsocial = string.Empty;
            string rfc = string.Empty;
            string usuario = string.Empty;
            string pass = string.Empty;

            nombre = txtNombre.Text.Trim();
            rsocial = txtRSocial.Text.Trim();
            rfc = txtRFC.Text.Trim();
            usuario = txtUsuarioPAC.Text.Trim();
            pass = txtPasPAC.Text.Trim();

            try
            {
                idSAP = Convert.ToInt32(txtIdSAP.Text.Trim());
            }
            catch (Exception ex)
            {
                muestraMensaje("Id de Sociedad SAP invalido" + ", " + ex.Message);
                return false;
            }

            if (idSAP.ToString().Length < 4)
            {
                muestraMensaje("Id de Sociedad SAP demaciado corto");
                return false;
            }
            else
                txtIdSAP.Text = idSAP.ToString();

            if (nombre.Length < 10)
            {
                muestraMensaje("Ingresar Nombre de Sociedad o Nombre demaciado corto");
                return false;
            }
            else
                txtNombre.Text = nombre;

            if (rsocial.Length < 10)
            {
                muestraMensaje("Ingresar Razon Social de Sociedad o Razon Social demaciado corta");
                return false;
            }
            else
                txtRSocial.Text = rsocial;

            if (rfc.Length < 12)
            {
                muestraMensaje("Ingresar RFC de Sociedad o RFC demaciado corta");
                return false;
            }
            else
                txtRFC.Text = rfc;

            if (usuario.Length < 5)
            {
                muestraMensaje("Ingresar Usuario de PAC de Sociedad o Usuario de PAC demaciado corta");
                return false;
            }
            else
                txtUsuarioPAC.Text = usuario;

            if (pass.Length < 5)
            {
                muestraMensaje("Ingresar Contraseña de PAC de Sociedad o Contraseña de PAC demaciado corta");
                return false;
            }
            else
                txtPasPAC.Text = pass;

            return bResp;
        }

        /// <summary>
        /// Metodo para validar los datos de actualizacion
        /// </summary>
        /// <returns></returns>
        private bool validaDatosActualiza()
        {
            bool bResp = true;
            string nombre = string.Empty;
            string rsocial = string.Empty;
            string usuario = string.Empty;
            string pass = string.Empty;

            if (txtNombre.Text.Length < 10)
            {
                muestraMensaje("Ingresar Nombre de Sociedad o Nombre demaciado corto");
                return false;
            }
            if (txtRSocial.Text.Length < 10)
            {
                muestraMensaje("Ingresar Razon Social de Sociedad o Razon Social demaciado corta");
                return false;
            }
            if (txtUsuarioPAC.Text.Length < 5)
            {
                muestraMensaje("Ingresar Usuario de PAC de Sociedad o Usuario de PAC demaciado corta");
                return false;
            }
            if (txtPasPAC.Text.Length < 5)
            {
                muestraMensaje("Ingresar Contraseña de PAC de Sociedad o Contraseña de PAC demaciado corta");
                return false;
            }

            return bResp;
        }

        /// <summary>
        /// Metodo para validar duplicados
        /// </summary>
        /// <param name="_rfc"></param>
        /// <returns></returns>
        private bool validaDuplicado(string _rfc)
        {
            bool bResp = true;
            BLSociedades busca = new BLSociedades();
            try
            {
                List<Sociedades> csSociedades = busca.buscaSociedad(_rfc);
                if (csSociedades.Count > 0)
                {
                    string nombre = "";
                    nombre = csSociedades[0].Nombre_Sociedad;
                    muestraMensaje("No se puede duplicar el RFC. La sociedad : " + nombre + " ya tiene ese RFC");
                    bResp = false;
                }
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "validaDuplicado", "Error", ex.Message);
            }
            return bResp;
        }

        /// <summary>
        /// Metodo para insertar sociedad
        /// </summary>
        /// <param name="_idSAP"></param>
        /// <param name="_nomre"></param>
        /// <param name="_razonsocial"></param>
        /// <param name="_rfc"></param>
        /// <param name="_usuario"></param>
        /// <param name="_pass"></param>
        /// <param name="_valido"></param>
        /// <returns></returns>
        private bool insertaSociedad(int _idSAP, string _nomre, string _razonsocial, string _rfc, string _usuario, string _pass, bool _valido)
        {
            bool bResp = false;
            BLSociedades inserta = new BLSociedades();
            try
            {
                bResp = inserta.insertaSociedad(_idSAP, _nomre, _razonsocial, _rfc, _usuario, _pass, _valido);
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "insertaSociedad", "Error", ex.Message);
            }

            return bResp;
        }

        /// <summary>
        /// Metodo para mostrar datos de sociedad
        /// </summary>
        /// <param name="_IdSociedad"></param>
        private void datosSociedad(int _IdSociedad)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            BLSociedades busca = new BLSociedades();
            try
            {
                List<Sociedades> csSociedades = busca.buscaSociedadxID(_IdSociedad);
                if (csSociedades.Count > 0)
                {
                    txtIdSociedad.Text = csSociedades[0].Id_Sociedad.ToString();
                    txtIdSAP.Text = csSociedades[0].Id_Sociedad_SAP.ToString();
                    txtNombre.Text = csSociedades[0].Nombre_Sociedad;
                    txtRSocial.Text = csSociedades[0].Razon_Social;
                    txtRFC.Text = csSociedades[0].RFC_Sociedad;
                    txtUsuarioPAC.Text = csSociedades[0].Usuario_ME;
                    txtPasPAC.Text = csSociedades[0].Contrasenia_ME;
                    chbValido.Checked = csSociedades[0].Valido;
                }
            }
            catch (Exception ex)
            {
                muestraError("dgvSociedades", "datosSociedad", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para actualizar registro
        /// </summary>
        /// <param name="idSociedad"></param>
        /// <param name="_nomre"></param>
        /// <param name="_razonsocial"></param>
        /// <param name="_usuario"></param>
        /// <param name="_pass"></param>
        /// <param name="_valido"></param>
        /// <returns></returns>
        private bool actualizaSociedad(int idSociedad, string _nomre, string _razonsocial, string _usuario, string _pass, bool _valido)
        {
            bool bResp = false;
            BLSociedades actualiza = new BLSociedades();
            try
            {
                bResp = actualiza.actualizaSociedad(idSociedad, _nomre, _razonsocial, _usuario, _pass, _valido);
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "actualizaSociedad", "Error", ex.Message);
            }
            return bResp;
        }

        /// <summary>
        /// Metodo para inhabilitar textos
        /// </summary>
        private void inhabilitaTextos()
        {
            txtIdSAP.Enabled = false;
            txtNombre.Enabled = false;
            txtRSocial.Enabled = false;
            txtRFC.Enabled = false;
            txtUsuarioPAC.Enabled = false;
            txtPasPAC.Enabled = false;
            chbValido.Enabled = false;
        }

        /// <summary>
        /// Metodo para buscar sociedad
        /// </summary>
        /// <param name="_idSAP"></param>
        /// <param name="_nomre"></param>
        /// <param name="_rsocial"></param>
        /// <param name="_rfc"></param>
        /// <param name="_valido"></param>
        private void buscaRegistro(int _idSAP, string _nomre, string _rsocial, string _rfc, bool _valido)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            dgvSociedades.DataSource = null;
            dgvSociedades.DataBind();

            BLSociedades busca = new BLSociedades();
            try
            {
                List<Sociedades> csSociedad = busca.buscaSociedad(_idSAP, _nomre, _rsocial, _rfc, _valido);
                if (csSociedad.Count > 0)
                {
                    dgvSociedades.DataSource = csSociedad;
                    dgvSociedades.DataBind();
                }
                else
                    inicializaGrid();
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "buscaRegistro", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Sociedades", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Sociedades", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        

    }
}