﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using BLRecepcion.Portal;
using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using DALRecepcion.Bean.Portal;

using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Net;

namespace RecepciondeFecturas
{
    public partial class Aclaraciones : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                    llenaddlSocieddes();
                    CargarFechas();

                    txtFechaini.Text = cleFechaini.ToString();
                    txtFechafin.Text = cleFechafin.ToString();

                    llenaGrid();
                }
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["Idioma"] == null)
                UICulture = "es-MX";
            else
                UICulture = Session["Idioma"].ToString();

            base.InitializeCulture();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
        }

        protected void btnExportar_Click(object sender, ImageClickEventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";
            
            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);

            DataTable dtExporta = null;
            try
            {
                dtExporta = new DataTable();
                dtExporta = buscaAclaraciones();
                if (dtExporta.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    StringWriter sw = new StringWriter(sb);
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    Page pagina = new Page();
                    HtmlForm hForm = new HtmlForm();
                    GridView gv = new GridView();
                    gv.EnableViewState = false;
                    gv.DataSource = dtExporta;
                    gv.DataBind();
                    pagina.EnableEventValidation = false;
                    pagina.DesignerInitialize();
                    pagina.Controls.Add(hForm);
                    hForm.Controls.Add(gv);
                    pagina.RenderControl(htw);
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=Aclaraciones.xls");
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(sb.ToString());
                    Response.End();
                }
                else
                {
                    muestraMensaje((string)GetLocalResourceObject("Error_Sindatos"));
                }
            }
            catch (Exception ex)
            {
                muestraError("btnExportar", "btnExportar", "Error", ex.Message);
            }
        }

        protected void btnActualizar_Click(object sender, ImageClickEventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
        }

        protected void dgvAclraciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton btnPDF = (ImageButton)e.Row.FindControl("imgPdf");
                ImageButton btnXML = (ImageButton)e.Row.FindControl("imgXml");

                string idRecepcion = string.Empty;
                idRecepcion = e.Row.Cells[0].Text;

                btnPDF.CommandArgument = idRecepcion;
                btnXML.CommandArgument = idRecepcion;
            }
        }

        protected void dgvAclraciones_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int IdRecepcion = -1;
            string Tipo = string.Empty;
            switch (e.CommandName)
            {
                case"cmdPDF":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/pdf";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
                case "cmdXML":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/xml";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
            }
        }

        protected void dgvAclraciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvAclraciones.PageIndex = e.NewPageIndex;
            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
            llenaGrid();
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        private void llenaGrid()
        {

            dgvAclraciones.DataSource = null;
            dgvAclraciones.DataBind();

            DataTable dtBusca = new DataTable();
            try
            {
                dtBusca = buscaAclaraciones();
                if (dtBusca.Rows.Count > 0)
                {
                    dgvAclraciones.DataSource = dtBusca;
                    dgvAclraciones.DataBind();
                }
                else
                    inicializaFrid();

            }
            catch (Exception ex)
            {
                muestraError("llenaGrid", "llenaGrid", "Error", ex.Message);
            }

        }

        /// <summary>
        /// Metodo para llenar Historial
        /// </summary>
        /// <returns></returns>
        private DataTable buscaAclaraciones()
        {

            #region VAriables locales
            int ID_Proveedor = -1;
            int ID_Sociedad = -1;
            
            //Variables para llenar el Store, no se emplean
            string Archivoxml = string.Empty;
            string Serie = string.Empty;
            string Folio = string.Empty;

            DateTime Fecha_Ini = new DateTime();
            DateTime Fecha_Fin = new DateTime();
            int Id_Estatus = -1;
            #endregion

            #region Asignación
            ID_Proveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            ID_Sociedad = Convert.ToInt32(ddlSociedad.SelectedValue.ToString());
            
            Archivoxml = "";
            Serie = "";
            Folio = "";

            Fecha_Ini = Convert.ToDateTime(cleFechaini.SelectedDate.ToString());
            Fecha_Fin = Convert.ToDateTime(cleFechafin.SelectedDate.ToString());
            Id_Estatus = 5;     //Factura en revisión
            #endregion

            DataTable dtAclaraciones = null;
            BLArchivos busca = new BLArchivos();
            try
            {
                dtAclaraciones = new DataTable();
                dtAclaraciones = busca.buscaAclaraciones(ID_Proveedor, ID_Sociedad, Archivoxml, Serie, Folio, Fecha_Ini, Fecha_Fin, Id_Estatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtAclaraciones;
        }

        /// <summary>
        /// Metodo para inicializar Grid
        /// </summary>
        private void inicializaFrid()
        {
            string x = string.Empty;
            if (Session["Idioma"].ToString() == "es-MX")
                x = "No hay datos para mostrar";
            else
                x = "No data to display";

            DataTable dtAclaraciones = new DataTable();
            dtAclaraciones.Columns.Add(new DataColumn("Id_Recepcion", typeof(Int32)));
            dtAclaraciones.Columns.Add(new DataColumn("Nombre_Sociedad", typeof(String)));
            dtAclaraciones.Columns.Add(new DataColumn("Fecha_Recepcion", typeof(DateTime)));
            dtAclaraciones.Columns.Add(new DataColumn("Nombre_XML", typeof(String)));
            dtAclaraciones.Columns.Add(new DataColumn("Fecha_Factura", typeof(DateTime)));
            dtAclaraciones.Columns.Add(new DataColumn("Serie", typeof(String)));
            dtAclaraciones.Columns.Add(new DataColumn("Folio", typeof(String)));
            dtAclaraciones.Columns.Add(new DataColumn("Observaciones", typeof(String)));

            DataRow drVacio = dtAclaraciones.NewRow();
            drVacio[0] = 0;
            drVacio[1] = "";
            drVacio[2] = DateTime.Now;
            drVacio[3] = "";
            drVacio[4] = DateTime.Now;
            drVacio[5] = "";
            drVacio[6] = "";
            drVacio[7] = "";

            dtAclaraciones.Rows.Add(drVacio);
            dgvAclraciones.DataSource = dtAclaraciones;
            dgvAclraciones.DataBind();
            int totalcolums = dgvAclraciones.Rows[0].Cells.Count;
            dgvAclraciones.Rows[0].Cells.Clear();
            dgvAclraciones.Rows[0].Cells.Add(new TableCell());
            dgvAclraciones.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvAclraciones.Rows[0].Cells[0].Text = x;
        }

        /// <summary>
        /// Metodo para llenad DDL de sociedades
        /// </summary>
        private void llenaddlSocieddes()
        {
            ddlSociedad.Items.Clear();
            string x = string.Empty;
            if (Session["Idioma"].ToString() == "es-MX")
                x = "Todo";
            else
                x = "All";


            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                List<Sociedades> csSociedad = buscaSociedad.buscaSociedad();
                if (csSociedad.Count > 0)
                {
                    ddlSociedad.DataValueField = "Id_Sociedad";
                    ddlSociedad.DataTextField = "Razon_Social";
                    ddlSociedad.Items.Clear();
                    ddlSociedad.Items.Add(new ListItem(x, "0"));
                    ddlSociedad.AppendDataBoundItems = true;
                    ddlSociedad.DataSource = csSociedad;
                    ddlSociedad.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("llenaddlSocieddes", "llenaddlSocieddes", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo que inicializa las fechas
        /// </summary>
        private void CargarFechas()
        {
            DateTime fecha = DateTime.Now.Date;
            this.cleFechaini.SelectedDate = new DateTime(fecha.Year, fecha.Month, 1);
            this.cleFechafin.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
        }

        /// <summary>
        /// Metro para mostrar archivos
        /// </summary>
        /// <param name="_idrecepcion"></param>
        /// <param name="_tipo"></param>
        private void mustrarArchivo(int _idrecepcion, string _tipo)
        {
            if (Session["id_Usuario"] != null)
            {
                int ID_Usuario = -1;
                ID_Usuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                //Primero buscmos el archivo
                BLArchivos busca = new BLArchivos();
                try
                {
                    List<Archivos> csArchivos = busca.buscaPdfXml(_idrecepcion);
                    if (csArchivos.Count > 0)
                    {
                        byte[] datosArchivo = null;
                        if (_tipo == "application/pdf")
                        {
                            datosArchivo = csArchivos[0].Archivo_PDF;
                        }
                        else
                        {
                            datosArchivo = csArchivos[0].Archivo_XML;
                        }

                        Response.Clear();
                        Response.ContentType = _tipo;
                        //Response.AddHeader("Pragma", datosArchivo.Length.ToString());
                        Response.AddHeader("content-length", datosArchivo.Length.ToString());
                        Response.BinaryWrite(datosArchivo);
                    }
                }
                catch (Exception ex)
                {
                    muestraError("dgvAclraciones", "mustrarArchivo", "Error", ex.Message);
                }
            }
        }

        #region Manejo de errores
        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Aclaraciones", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Aclaraciones", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }
        #endregion

    }
}