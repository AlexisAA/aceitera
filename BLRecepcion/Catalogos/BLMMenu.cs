﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLMMenu
    {
        /// <summary>
        /// Metodo para buscar menu
        /// </summary>
        /// <param name="ID_Menu"></param>
        /// <returns></returns>
        public List<MMenu> buscaMenuxId(int ID_Menu)
        {
            MMenu mmenu = new MMenu();
            mmenu.ID_Menu = ID_Menu;
            mmenu.Valido = true;
            return BUMMenu.Buscar(mmenu);
        }
    }
}
