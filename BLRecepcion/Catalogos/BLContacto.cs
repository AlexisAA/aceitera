﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLContacto
    {
        /// <summary>
        /// Metodo para buscar contacto por correo
        /// </summary>
        /// <param name="Correo"></param>
        /// <returns></returns>
        public List<Contacto> buscaCorreo(string Correo)
        {
            Contacto contacto = new Contacto();
            contacto.Correo = Correo;
            try
            {
                return BUContacto.Buscar(contacto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region buscaContacto sobrecargado
        /// <summary>
        /// Metodo para buscar el contacto del proveedor
        /// </summary>
        /// <param name="Id_Proveedor"></param>
        /// <returns></returns>
        public List<Contacto> buscaContacto(int Id_Proveedor)
        {
            Contacto contacto = new Contacto();
            contacto.ID_Proveedor = Id_Proveedor;
            try
            {
                return BUContacto.Buscar(contacto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar Contacto
        /// </summary>
        /// <param name="Id_Proveedor"></param>
        /// <param name="Correo"></param>
        /// <returns></returns>
        public List<Contacto> buscaContacto(int Id_Proveedor, string Correo)
        {
            Contacto contacto = new Contacto();
            contacto.ID_Proveedor = Id_Proveedor;
            contacto.Correo = Correo;
            try
            {
                return BUContacto.Buscar(contacto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ABC Contacto
        /// <summary>
        /// Metodo ara insertar contacto.
        /// </summary>
        /// <param name="Nombre"></param>
        /// <param name="Telefon"></param>
        /// <param name="Celular"></param>
        /// <param name="Correo"></param>
        /// <param name="Id_Proveedor"></param>
        /// <returns></returns>
        public bool insertaContacto(string Nombre, string Telefon, string Celular, string Correo, int Id_Proveedor)
        {
            Contacto contacto = new Contacto();
            contacto.Nombre = Nombre;
            contacto.Telefono = Telefon;
            contacto.Celular = Celular;
            contacto.Correo = Correo;
            contacto.ID_Proveedor = Id_Proveedor;
            try
            {
                return BUContacto.Insertar(contacto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para actualizar contacto
        /// </summary>
        /// <param name="Id_Contacto"></param>
        /// <param name="Nombre"></param>
        /// <param name="Telefon"></param>
        /// <param name="Celular"></param>
        /// <param name="Correo"></param>
        /// <returns></returns>
        public bool actualizaContacto(int Id_Contacto, string Nombre, string Telefon, string Celular, string Correo)
        {
            Contacto contacto = new Contacto();
            contacto.ID_Contacto = Id_Contacto;
            contacto.Nombre = Nombre;
            contacto.Telefono = Telefon;
            contacto.Celular = Celular;
            contacto.Correo = Correo;
            try
            {
                return BUContacto.Actualizar(contacto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion

    }
}
