﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLMonedas
    {

        #region buscaMoneda sobrecargado
        /// <summary>
        /// Metodo para buscar moneda por ID
        /// </summary>
        /// <returns></returns>
        public List<Monedas> buscaMoneda()
        {
            Monedas moneda = new Monedas();
            try
            {
                return BUMonedas.Buscar(moneda);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar moneda por ID
        /// </summary>
        /// <param name="ID_Moneda"></param>
        /// <returns></returns>
        public List<Monedas> buscaMoneda(string ID_Moneda)
        {
            Monedas moneda = new Monedas();
            moneda.Moneda = ID_Moneda;
            try
            {
                return BUMonedas.Buscar(moneda);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
