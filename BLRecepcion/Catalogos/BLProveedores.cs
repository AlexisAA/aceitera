﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLProveedores
    {

        #region busca Varios
        /// <summary>
        /// Metodo para buscar proveedor por ID
        /// </summary>
        /// <param name="Id_Proveedor"></param>
        /// <returns></returns>
        public List<Proveedores> buscaProveedorxID(int Id_Proveedor)
        {
            Proveedores proveedor = new Proveedores();
            proveedor.ID_Proveedor = Id_Proveedor;
            return BuProveedores.Buscar(proveedor);
        }

        /// <summary>
        /// Metodo para buscar Nombre de proveedor
        /// </summary>
        /// <param name="Nombre"></param>
        /// <returns></returns>
        public string buscaNombreproveedor(string Nombre)
        {
            string respuesta = string.Empty;
            List<Proveedores> resultado = new List<Proveedores>();
            Proveedores proveedor = new Proveedores();
            proveedor.Nombre = Nombre;
            resultado = BuProveedores.Buscar(proveedor);
            if (resultado.Count > 0)
                respuesta = resultado[0].ID_Proveedor.ToString();

            return respuesta;
        }

        /// <summary>
        /// Metodo para buscar ID Proveedor SAP
        /// </summary>
        /// <param name="Id_Proveedor_SAP"></param>
        /// <returns></returns>
        public string buscaIdproveedorSAP(string Id_Proveedor_SAP)
        {
            string respuesta = string.Empty;
            List<Proveedores> resultado = new List<Proveedores>();
            Proveedores proveedor = new Proveedores();
            proveedor.ID_Proveedor_SAP = Id_Proveedor_SAP;
            resultado = BuProveedores.Buscar(proveedor);
            if (resultado.Count > 0)
                respuesta = resultado[0].Nombre;

            return respuesta;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RFC"></param>
        /// <returns></returns>
        public string buscaRFCproveedor(string RFC)
        {
            string respuesta = string.Empty;
            List<Proveedores> resultado = new List<Proveedores>();
            Proveedores proveedor = new Proveedores();
            proveedor.RFC = RFC;
            resultado = BuProveedores.Buscar(proveedor);
            if (resultado.Count > 0)
                respuesta = resultado[0].Nombre;

            return respuesta;
        }
        #endregion

        #region buscaProveedor Sobrecargado
        /// <summary>
        /// Metodo para buscar proveedor
        /// </summary>
        /// <returns></returns>
        public List<Proveedores> buscaProveedor()
        {
            Proveedores proveedor = new Proveedores();
            proveedor.Valido = true;
            try
            {
                return BuProveedores.Buscar(proveedor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar Proveedor por RFC
        /// </summary>
        /// <param name="RFC"></param>
        /// <returns></returns>
        public List<Proveedores> buscaProveedor(string RFC)
        {
            Proveedores proveedor = new Proveedores();
            proveedor.RFC = RFC;
            try
            {
                return BuProveedores.Buscar(proveedor);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Metodo para buscar proveedor del Web Service
        /// </summary>
        /// <param name="ID_Proveedor_SAP"></param>
        /// <param name="RFC"></param>
        /// <returns></returns>
        public List<Proveedores> buscaProveedor(string ID_Proveedor_SAP, string RFC)
        {
            Proveedores proveedor = new Proveedores();
            proveedor.ID_Proveedor_SAP = ID_Proveedor_SAP;
            proveedor.RFC = RFC;
            try
            {
                return BuProveedores.Buscar(proveedor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar proveedor
        /// </summary>
        /// <param name="Id_Proveedor_SAP"></param>
        /// <param name="Nombre"></param>
        /// <param name="RFC"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public List<Proveedores> buscaProveedor(string Id_Proveedor_SAP, string Nombre, string RFC, bool Valido)
        {
            Proveedores proveedor = new Proveedores();
            proveedor.ID_Proveedor_SAP = Id_Proveedor_SAP;
            proveedor.Nombre = Nombre;
            proveedor.RFC = RFC;
            proveedor.Valido = Valido;
            try
            {
                return BuProveedores.Buscar(proveedor);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region buscaProveedores Sobrecargado
        /// <summary>
        /// Metodo para buscar todos los proveedores
        /// </summary>
        /// <returns></returns>
        public List<Proveedores> buscaProveedores()
        {
            Proveedores proveedor = new Proveedores();
            try
            {
                return BuProveedores.Buscar(proveedor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar proveedor con ID SAP
        /// </summary>
        /// <param name="ID_Proveedor_SAP"></param>
        /// <returns></returns>
        public List<Proveedores> buscaProveedores(string ID_Proveedor_SAP)
        {
            Proveedores proveedor = new Proveedores();
            proveedor.ID_Proveedor_SAP = ID_Proveedor_SAP;
            try
            {
                return BuProveedores.Buscar(proveedor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ABC Proveedores
        /// <summary>
        /// Metodo para insertar proveedor
        /// </summary>
        /// <param name="Id_Proveedor_SAT"></param>
        /// <param name="Nombre"></param>
        /// <param name="Fecha_Alta"></param>
        /// <param name="RFC"></param>
        /// <param name="Telefono"></param>
        /// <param name="Fax"></param>
        /// <param name="Banco"></param>
        /// <param name="Cuenta_Banco"></param>
        /// <param name="Dias_Credito"></param>
        /// <param name="Direccion"></param>
        /// <param name="Valido"></param>
        /// <param name="Addenda"></param>
        /// <returns></returns>
        public bool insertaProveedor(string Id_Proveedor_SAT, string Nombre, DateTime Fecha_Alta, string RFC,
            string Telefono, string Fax, string Banco, string Cuenta_Banco, int Dias_Credito, string Direccion,
            bool Valido, bool Addenda)
        {
            Proveedores proveedor = new Proveedores();
            proveedor.ID_Proveedor_SAP = Id_Proveedor_SAT;
            proveedor.Nombre = Nombre;
            proveedor.Fecha_Alta = Fecha_Alta;
            proveedor.RFC = RFC;
            proveedor.Telefono = Telefono;
            proveedor.Fax = Fax;
            proveedor.Banco = Banco;
            proveedor.Cuenta_Bancaria = Cuenta_Banco;
            proveedor.Dias_Credito = Dias_Credito;
            proveedor.Direccion = Direccion;
            proveedor.Valido = Valido;
            proveedor.Addenda = Addenda;
            try
            {
                return BuProveedores.Insertar(proveedor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para actualizar proveedor.
        /// </summary>
        /// <param name="ID_Proveedor"></param>
        /// <param name="Nombre"></param>
        /// <param name="RFC"></param>
        /// <param name="Telefono"></param>
        /// <param name="Fax"></param>
        /// <param name="Banco"></param>
        /// <param name="Cuenta_Banco"></param>
        /// <param name="Dias_Credito"></param>
        /// <param name="Direccion"></param>
        /// <param name="Valido"></param>
        /// <param name="Addenda"></param>
        /// <returns></returns>
        public bool actualizaProveedor(int ID_Proveedor, string Nombre, string RFC, string Telefono, string Fax, 
            string Banco, string Cuenta_Banco, int Dias_Credito, string Direccion, bool Valido, bool Addenda)
        {
            Proveedores proveedor = new Proveedores();
            proveedor.ID_Proveedor = ID_Proveedor;
            proveedor.Nombre = Nombre;
            proveedor.RFC = RFC;
            proveedor.Telefono = Telefono;
            proveedor.Fax = Fax;
            proveedor.Banco = Banco;
            proveedor.Cuenta_Bancaria = Cuenta_Banco;
            proveedor.Dias_Credito = Dias_Credito;
            proveedor.Direccion = Direccion;
            proveedor.Valido = Valido;
            proveedor.Addenda = Addenda;
            try
            {
                return BuProveedores.Actualizar(proveedor);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para actualizar proveedor.
        /// </summary>
        /// <param name="ID_Proveedor"></param>
        /// <param name="Nombre"></param>
        /// <param name="RFC"></param>
        /// <param name="Telefono"></param>
        /// <param name="Fax"></param>
        /// <param name="Banco"></param>
        /// <param name="Cuenta_Banco"></param>
        /// <param name="Dias_Credito"></param>
        /// <param name="Direccion"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public bool actualizaProveedor(int ID_Proveedor, string Nombre, string RFC, string Telefono, string Fax,
            string Banco, string Cuenta_Banco, int Dias_Credito, string Direccion, bool Valido)
        {
            bool Addenda = false;
            List<Proveedores> busca = buscaProveedorxID(ID_Proveedor);
            if (busca.Count > 0)
            {
                Addenda = busca[0].Addenda;
            }

            Proveedores proveedor = new Proveedores();
            proveedor.ID_Proveedor = ID_Proveedor;
            proveedor.Nombre = Nombre;
            proveedor.RFC = RFC;
            proveedor.Telefono = Telefono;
            proveedor.Fax = Fax;
            proveedor.Banco = Banco;
            proveedor.Cuenta_Bancaria = Cuenta_Banco;
            proveedor.Dias_Credito = Dias_Credito;
            proveedor.Direccion = Direccion;
            proveedor.Valido = Valido;
            proveedor.Addenda = Addenda;
            try
            {
                return BuProveedores.Actualizar(proveedor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
