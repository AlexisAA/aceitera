﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLPerfiles
    {
        /// <summary>
        /// Metodo para buscar perfil por ID
        /// </summary>
        /// <param name="Id_Perfil"></param>
        /// <returns></returns>
        public List<Perfiles> buscaPerfil(int Id_Perfil)
        {
            Perfiles perfil = new Perfiles();
            perfil.ID_Perfil = Id_Perfil;
            try
            {
                return BUPerfiles.Buscar(perfil);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar Perfiles Activos
        /// </summary>
        /// <returns></returns>
        public List<Perfiles> buscaPerfil()
        {
            Perfiles perfil = new Perfiles();
            perfil.Valido = true;
            try
            {
                return BUPerfiles.Buscar(perfil);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
