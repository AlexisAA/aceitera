﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLEstatus
    {
        /// <summary>
        /// Metodo para buscar estatus  
        /// </summary>
        /// <returns></returns>
        public List<Estatus> buscaEstatus()
        {
            Estatus estatus = new Estatus();
            estatus.Valido = true;
            try
            {
                return BUEstatus.Buscar(estatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Metodo para buscar estatus por ID
        /// </summary>
        /// <param name="Id_Estatus"></param>
        /// <returns></returns>
        public List<Estatus> buscaEstatus(int Id_Estatus)
        {
            Estatus estatus = new Estatus();
            estatus.ID_Estatus = Id_Estatus;
            try
            {
                return BUEstatus.Buscar(estatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
