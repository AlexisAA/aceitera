﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;
using System.Data;
using DALRecepcion.DataAccess.Catalogos;


namespace BLRecepcion.Catalogos
{
    public class BLLogs
    {

        #region buscaLogs Sobrecargado
        /// <summary>
        /// Metodo para buscar log por ID
        /// </summary>
        /// <param name="ID_Log"></param>
        /// <returns></returns>
        public List<Logs> buscaLogs(int ID_Log)
        {
            Logs log = new Logs();
            log.ID_Log = ID_Log;
            try
            {
                return BULogs.Buscar(log);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar Logs
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <param name="Fecha_Inicio"></param>
        /// <param name="Fecha_Fin"></param>
        /// <returns></returns>
        public DataTable buscaLogs(int Id_Usuario, DateTime Fecha_Inicio, DateTime Fecha_Fin)
        {
            DataTable dtLogs = new DataTable();
            dtLogs = null;
            try
            {
                dtLogs = DALogs_Varios.DAbuscaLogs(Id_Usuario, Fecha_Inicio, Fecha_Fin);

                return dtLogs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar log
        /// </summary>
        /// <param name="Formulario"></param>
        /// <param name="Evento"></param>
        /// <param name="Descripcion"></param>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public List<Logs> buscaLogs(string Formulario, string Evento, string Descripcion, int Id_Usuario)
        {
            Logs log = new Logs();
            log.Formulario = Formulario;
            log.Evento = Evento;
            log.Descripcion = Descripcion;
            log.ID_Usuario = Id_Usuario;

            try
            {
                return BULogs.Buscar(log);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ABC Logs
        /// <summary>
        /// Metodo para insertar log
        /// </summary>
        /// <param name="Formulario"></param>
        /// <param name="Evento"></param>
        /// <param name="Accion"></param>
        /// <param name="Respuesta"></param>
        /// <param name="Descripcion"></param>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public bool insertaLog(string Formulario, string Evento, string Accion, string Respuesta, string Descripcion, int Id_Usuario)
        {
            Logs log = new Logs();
            log.Formulario = Formulario;
            log.Evento = Evento;
            log.Accion = Accion;
            log.Respuesta = Respuesta;
            log.Descripcion = Descripcion;
            log.Fecha = DateTime.Now;
            log.ID_Usuario = Id_Usuario;

            try
            {
                return BULogs.Insertar(log);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para actualizar log
        /// </summary>
        /// <param name="ID_Log"></param>
        /// <param name="Descripcion"></param>
        /// <returns></returns>
        public bool actualizaLog(int ID_Log, string Descripcion)
        {
            Logs log = new Logs();
            log.ID_Log = ID_Log;
            log.Descripcion = Descripcion;
            try
            {
                return BULogs.Actualizar(log);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
