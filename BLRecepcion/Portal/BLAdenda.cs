﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Portal;

namespace BLRecepcion.Portal
{
    public class BLAdenda
    {
        /// <summary>
        /// Metodo para insertar datos addenda
        /// </summary>
        /// <param name="_nombreXML"></param>
        /// <param name="_Id_Proveedor"></param>
        /// <param name="_IdProveedor_SAP"></param>
        /// <param name="_addenda"></param>
        /// <returns></returns>
        public bool insertaAddenda(string _nombreXML, int _Id_Proveedor, string _IdProveedor_SAP, string _addenda)
        {
            Adenda addenda = new Adenda();
            addenda.Nombre_XML = _nombreXML;
            addenda.ID_Proveedor = _Id_Proveedor;
            addenda.ID_Proveedor_SAP = _IdProveedor_SAP;
            addenda.Addenda = _addenda;
            try
            {
                return BUAdenda.Insertar(addenda);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
