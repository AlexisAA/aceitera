﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Portal;
using DALRecepcion.DataAccess.Portal;
using System.Data;

namespace BLRecepcion.Portal
{
    /// <summary>
    /// 
    /// </summary>
    public class BLProgramacion
    {

        /// <summary>
        /// Metodo para bscar programación por ID
        /// </summary>
        /// <param name="Id_Programacion"></param>
        /// <returns></returns>
        public List<Programacion> buscaProgramacionxid(int Id_Programacion)
        {
            Programacion programa = new Programacion();
            programa.ID_Programacion = Id_Programacion;
            try
            {
                return BUProgramacion.Buscar(programa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region buscaProgramacion subrecargado
        /// <summary>
        /// Metodo para buscar programacion
        /// </summary>
        /// <param name="_UUID"></param>
        /// <returns></returns>
        public List<Programacion> buscaProgramacion(string _UUID)
        {
            Programacion programa = new Programacion();
            programa.UUID = _UUID;
            try
            {
                return BUProgramacion.Buscar(programa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar programaciones validas
        /// </summary>
        /// <param name="_UUID"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public List<Programacion> buscaProgramacion(string _UUID, bool Valido)
        {
            Programacion programa = new Programacion();
            programa.UUID = _UUID;
            programa.Valido = Valido;
            try
            {
                return BUProgramacion.Buscar(programa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar programación.
        /// </summary>
        /// <param name="Id_Proveedor"></param>
        /// <param name="Id_Sociedad"></param>
        /// <param name="Fecha_Ini"></param>
        /// <param name="Fecha_Fin"></param>
        /// <param name="Id_Moneda"></param>
        /// <returns></returns>
        public DataTable buscaProgramacion(int Id_Proveedor, int Id_Sociedad, DateTime Fecha_Ini, DateTime Fecha_Fin, string Id_Moneda)
        {
            DataTable dtProgramacion = new DataTable();
            try
            {
                dtProgramacion = DAProgramacion_Varios.daBuscaprogramacion(Id_Proveedor, Id_Sociedad, Fecha_Ini, Fecha_Fin, Id_Moneda);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtProgramacion;
        }
        #endregion

        #region ABC Programacion
        /// <summary>
        /// Metodo para insertar Programacion
        /// </summary>
        /// <param name="Id_Recepcion"></param>
        /// <param name="Moneda"></param>
        /// <param name="Pedido"></param>
        /// <param name="Acuse"></param>
        /// <param name="Fecha_Vencimiento"></param>
        /// <param name="Id_Estatus"></param>
        /// <param name="Observaciones"></param>
        /// <returns></returns>
        public bool insertaProgramacion(int Id_Recepcion, string Moneda, Int64 Pedido, string Acuse, string Fecha_Vencimiento, int Id_Estatus, string Observaciones)
        {
            bool resp = false;
            BLArchivos buscaRecepcion = new BLArchivos();
            try
            {
                List<Archivos> csArchivo = buscaRecepcion.buscaPdfXml(Id_Recepcion);
                if (csArchivo.Count > 0)
                {
                    Programacion programa = new Programacion();
                    programa.ID_Recepcion = Id_Recepcion;
                    programa.ID_Proveedor = csArchivo[0].ID_Proveedor;
                    programa.ID_Sociedad = csArchivo[0].ID_Sociedad;
                    programa.Fecha_Recepcion = csArchivo[0].Fecha_Recepcion;
                    programa.Serie = csArchivo[0].Serie;
                    programa.Folio = csArchivo[0].Folio;
                    programa.UUID = csArchivo[0].UUID;
                    programa.Fecha_Factura = csArchivo[0].Fecha_Factura;
                    programa.Importe = csArchivo[0].Importe;
                    programa.Moneda = Moneda;
                    programa.Pedido = Pedido;
                    programa.Acuse = Acuse;
                    programa.Fecha_Vencimiento = Fecha_Vencimiento;
                    programa.Observaciones = Observaciones;
                    programa.Valido = true;

                    resp = BUProgramacion.Insertar(programa);
                    if (resp)
                        resp = buscaRecepcion.cambiaEstatus(Id_Recepcion, Id_Estatus, Observaciones);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }

        /// <summary>
        /// Metodo para insertar Programación de Facturas qe no entraron por el portal
        /// </summary>
        /// <param name="Id_Recepcion"></param>
        /// <param name="Id_proveedor"></param>
        /// <param name="ID_Sociedad"></param>
        /// <param name="Fecha_Recepcion"></param>
        /// <param name="Serie"></param>
        /// <param name="Folio"></param>
        /// <param name="UUID"></param>
        /// <param name="Fecha_Factura"></param>
        /// <param name="Importe"></param>
        /// <param name="Moneda"></param>
        /// <param name="Pedido"></param>
        /// <param name="Acuse"></param>
        /// <param name="Fecha_Vencimiento"></param>
        /// <param name="Observaciones"></param>
        /// <returns></returns>
        public bool insertaProgramacion(int Id_Recepcion, int Id_proveedor, int ID_Sociedad, DateTime Fecha_Recepcion, string Serie,
            string Folio, string UUID, DateTime Fecha_Factura, decimal Importe, string Moneda,
            Int64 Pedido, string Acuse, string Fecha_Vencimiento, string Observaciones)
        {
            Programacion programa = new Programacion();
            programa.ID_Recepcion = Id_Recepcion;
            programa.ID_Proveedor = Id_proveedor;
            programa.ID_Sociedad = ID_Sociedad;
            programa.Fecha_Recepcion = Fecha_Recepcion;
            programa.Serie = Serie;
            programa.Folio = Folio;
            programa.UUID = UUID;
            programa.Fecha_Factura = Fecha_Factura;
            programa.Importe = Importe;
            programa.Moneda = Moneda;
            programa.Pedido = Pedido;
            programa.Acuse = Acuse;
            programa.Fecha_Vencimiento = Fecha_Vencimiento;
            programa.Observaciones = Observaciones;
            programa.Valido = true;
            try
            {
                return BUProgramacion.Insertar(programa);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Metodo para actualizar Programación
        /// </summary>
        /// <param name="Id_Programacion"></param>
        /// <param name="Serie"></param>
        /// <param name="Folio"></param>
        /// <param name="Importe"></param>
        /// <param name="Moneda"></param>
        /// <param name="Pedido"></param>
        /// <param name="Acuse"></param>
        /// <param name="Fecha_Vencimiento"></param>
        /// <param name="Observaciones"></param>
        /// <returns></returns>
        public bool actualizaProgramacion(int Id_Programacion, string Serie, string Folio, decimal Importe, 
            string Moneda, Int64 Pedido, string Acuse, string Fecha_Vencimiento, string Observaciones)
        {
            Programacion programa = new Programacion();
            programa.ID_Programacion = Id_Programacion;
            programa.Serie = Serie;
            programa.Folio = Folio;
            programa.Importe = Importe;
            programa.Moneda = Moneda;
            programa.Pedido = Pedido;
            programa.Acuse = Acuse;
            programa.Fecha_Vencimiento = Fecha_Vencimiento;
            programa.Observaciones = Observaciones;
            programa.Valido = true;
            try
            {
                return BUProgramacion.Actualizar(programa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Metodo para eliminar (Logicamente) la programacion
        /// </summary>
        /// <param name="Id_Programacion"></param>
        /// <returns></returns>
        public bool eliminarProgramacion(int Id_Programacion)
        {
            Programacion programa = new Programacion();
            programa.ID_Programacion=Id_Programacion;
            programa.Valido = false;
            try
            {
                return BUProgramacion.Eliminar(programa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para ctivar programación
        /// </summary>
        /// <param name="Id_Programacion"></param>
        /// <returns></returns>
        public bool activaProgramacion(int Id_Programacion)
        {
            Programacion programa = new Programacion();
            programa.ID_Programacion = Id_Programacion;
            programa.Valido = true;
            try
            {
                return BUProgramacion.Eliminar(programa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
