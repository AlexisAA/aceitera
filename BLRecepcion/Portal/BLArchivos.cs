﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Portal;
using DALRecepcion.DataAccess.Portal;
using System.Data;

namespace BLRecepcion.Portal
{
    public class BLArchivos
    {

        #region buscaArchivos Sobrecargado
        /// <summary>
        /// Metodo para buscar archivos cargados por proveedor
        /// </summary>
        /// <param name="Id_Proveedor"></param>
        /// <returns></returns>
        public List<Archivos> buscaArchivos(int Id_Proveedor)
        {
            Archivos archivo = new Archivos();
            archivo.ID_Proveedor = Id_Proveedor;
            archivo.Valido = true;
            try
            {
                return BUArchivos.Buscar(archivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar archivos
        /// </summary>
        /// <param name="Id_Proveedor"></param>
        /// <param name="Nombre_XML"></param>
        /// <returns></returns>
        public List<Archivos> buscaArchivos(int Id_Proveedor, string Nombre_XML)
        {
            Archivos archivo = new Archivos();
            archivo.ID_Proveedor = Id_Proveedor;
            archivo.Nombre_XML = Nombre_XML;
            archivo.Valido = true;
            try
            {
                return BUArchivos.Buscar(archivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar archivo insertado
        /// </summary>
        /// <param name="fecha_recepcion"></param>
        /// <param name="uuid"></param>
        /// <param name="fecha_factua"></param>
        /// <returns></returns>
        public int buscaArchivos(DateTime fecha_recepcion, string uuid, DateTime fecha_factua)
        {
            int resp = 0;
            Archivos archivo = new Archivos();
            List<Archivos> csArchivo = new List<Archivos>();
            archivo.Fecha_Recepcion = fecha_recepcion;
            archivo.UUID = uuid;
            archivo.Fecha_Factura = fecha_factua;
            try
            {
                csArchivo = BUArchivos.Buscar(archivo);
                if (csArchivo.Count > 0)
                {
                    resp = csArchivo[0].Id_Recepcion;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        /// <summary>
        /// Metodoas para buscar archivos (Busqueda)
        /// </summary>
        /// <param name="idproveedor"></param>
        /// <param name="idsociedad"></param>
        /// <param name="archivoxml"></param>
        /// <param name="serie"></param>
        /// <param name="folio"></param>
        /// <param name="finicio"></param>
        /// <param name="ffin"></param>
        /// <param name="idestatus"></param>
        /// <returns></returns>
        public DataTable buscaArchivos(int idproveedor, int idsociedad, string archivoxml, string serie, string folio, DateTime finicio, DateTime ffin, int idestatus)
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos = null;
            try
            {
                dtArchivos = DAArchivos_Varios.DAbuscaArchivos(idproveedor, idsociedad, archivoxml, serie, folio, finicio, ffin, idestatus);

                return dtArchivos;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        
        #endregion

        #region Busqueda Varios
        /// <summary>
        /// Metodo para buscar archivo por id de recepcion
        /// </summary>
        /// <param name="ID_Recepcion"></param>
        /// <returns></returns>
        public List<Archivos> buscaPdfXml(int ID_Recepcion)
        {
            Archivos archivo = new Archivos();
            archivo.Id_Recepcion = ID_Recepcion;
            try
            {
                return BUArchivos.Buscar(archivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar archivo por UUID
        /// </summary>
        /// <param name="UUID"></param>
        /// <returns></returns>
        public List<Archivos> buscaArchivoxuuid(string UUID)
        {
            Archivos archivo = new Archivos();
            archivo.UUID = UUID;
            archivo.Valido = true;
            try
            {
                return BUArchivos.Buscar(archivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar achivos por UUID y VAlido
        /// </summary>
        /// <param name="UUID"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public List<Archivos> buscaArchivoxuuid(string UUID, bool Valido)
        {
            Archivos archivo = new Archivos();
            archivo.UUID = UUID;
            archivo.Valido = Valido;
            try
            {
                return BUArchivos.Buscar(archivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar archivos
        /// </summary>
        /// <param name="idproveedor"></param>
        /// <param name="idsociedad"></param>
        /// <param name="archivoxml"></param>
        /// <param name="serie"></param>
        /// <param name="folio"></param>
        /// <param name="finicio"></param>
        /// <param name="ffin"></param>
        /// <param name="idestatus"></param>
        /// <returns></returns>
        public DataTable buscaArchivos_CAT(int idproveedor, int idsociedad, string archivoxml, string serie, string folio, DateTime finicio, DateTime ffin, int idestatus)
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos = null;
            try
            {
                dtArchivos = DAArchivos_Varios.DAbuscaArchivos_CAT(idproveedor, idsociedad, archivoxml, serie, folio, finicio, ffin, idestatus);

                return dtArchivos;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region ABC Archivos
        /// <summary>
        /// Metodo para insertar archivo
        /// </summary>
        /// <param name="Nombre_XML"></param>
        /// <param name="Nombre_PDF"></param>
        /// <param name="Archivo_XML"></param>
        /// <param name="Archivo_PDF"></param>
        /// <param name="ID_Proveedor"></param>
        /// <param name="ID_Sociedad"></param>
        /// <param name="Folio"></param>
        /// <param name="Serie"></param>
        /// <param name="UUID"></param>
        /// <param name="Fecha_Factura"></param>
        /// <param name="Importe"></param>
        /// <param name="ID_Estatus"></param>
        /// <param name="Observaciones"></param>
        /// <returns></returns>
        public int insertaArchivo(string Nombre_XML, string Nombre_PDF, byte[] Archivo_XML, byte[] Archivo_PDF, int ID_Proveedor,
            int ID_Sociedad, string Folio, string Serie, string UUID, DateTime Fecha_Factura,
            decimal Importe, int ID_Estatus, string Observaciones)
        {
            int resp = -1;
            DateTime Fecha_Recepcion = new DateTime();
            Fecha_Recepcion = DateTime.Now;

            Archivos guardaArchivo = new Archivos();
            guardaArchivo.Nombre_XML = Nombre_XML;
            guardaArchivo.Nombre_PDF = Nombre_PDF;
            guardaArchivo.Archivo_XML = Archivo_XML;
            guardaArchivo.Archivo_PDF = Archivo_PDF;
            guardaArchivo.Fecha_Recepcion = Fecha_Recepcion;
            guardaArchivo.ID_Proveedor = ID_Proveedor;
            guardaArchivo.ID_Sociedad = ID_Sociedad;
            guardaArchivo.Folio = Folio;
            guardaArchivo.Serie = Serie;
            guardaArchivo.UUID = UUID;
            guardaArchivo.Fecha_Factura = Fecha_Factura;
            guardaArchivo.Importe = Importe;
            guardaArchivo.ID_Estatus = ID_Estatus;
            guardaArchivo.Observaciones = Observaciones;
            guardaArchivo.Valido = true;

            try
            {
                if (BUArchivos.Insertar(guardaArchivo))
                {
                    resp = buscaArchivos(Fecha_Recepcion, UUID, Fecha_Factura);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }

        /// <summary>
        /// Metodo para cambiar de estatus
        /// </summary>
        /// <param name="Id_Recepcion"></param>
        /// <param name="Id_Estatus"></param>
        /// <param name="Observaciones"></param>
        /// <returns></returns>
        public bool cambiaEstatus(int Id_Recepcion, int Id_Estatus, string Observaciones)
        {
            bool resp = false;
            List<Archivos> csArchivos = new List<Archivos>();
            try
            {
                csArchivos = buscaPdfXml(Id_Recepcion);
                if (csArchivos.Count > 0)
                {
                    Archivos archivo = new Archivos();
                    archivo.Id_Recepcion = Id_Recepcion;
                    archivo.Nombre_XML = csArchivos[0].Nombre_XML;
                    archivo.Nombre_PDF = csArchivos[0].Nombre_PDF;
                    archivo.Archivo_XML = csArchivos[0].Archivo_XML;
                    archivo.Archivo_PDF = csArchivos[0].Archivo_PDF;
                    archivo.Fecha_Recepcion = csArchivos[0].Fecha_Recepcion;
                    archivo.Folio = csArchivos[0].Folio;
                    archivo.Serie = csArchivos[0].Serie;
                    archivo.UUID = csArchivos[0].UUID;
                    archivo.Fecha_Factura = csArchivos[0].Fecha_Factura;
                    archivo.Importe = csArchivos[0].Importe;
                    archivo.ID_Estatus = Id_Estatus;
                    archivo.Observaciones = Observaciones;
                    archivo.Valido = csArchivos[0].Valido;

                    resp = BUArchivos.Actualizar(archivo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }
        #endregion

        #region Busca Aclaraciones
        /// <summary>
        /// Metodo para buscar aclaraciones
        /// </summary>
        /// <param name="idproveedor"></param>
        /// <param name="idsociedad"></param>
        /// <param name="archivoxml"></param>
        /// <param name="serie"></param>
        /// <param name="folio"></param>
        /// <param name="finicio"></param>
        /// <param name="ffin"></param>
        /// <param name="idestatus"></param>
        /// <returns></returns>
        public DataTable buscaAclaraciones(int idproveedor, int idsociedad, string archivoxml, string serie, string folio, DateTime finicio, DateTime ffin, int idestatus)
        {
            DataTable dtAclaraciones = new DataTable();
            dtAclaraciones = null;
            try
            {
                dtAclaraciones = DAArchivos_Varios.DAbuscaAclaraciones(idproveedor, idsociedad, archivoxml, serie, folio, finicio, ffin, idestatus);

                return dtAclaraciones;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }///
        #endregion



        /// <summary>
        /// Metodoas para buscar archivos (Busqueda)
        /// </summary>
        /// <param name="ID_Sociedad"></param>
        /// <param name="ArchivXML"></param>
        /// <param name="Serie"></param>
        /// <param name="Folio"></param>
        /// <param name="FInicio"></param>
        /// <param name="FFin"></param>
        /// <param name="ID_Estatus"></param>
        /// <returns></returns>
        //public List<Archivos> buscaArchivos(int ID_Sociedad, string ArchivXML, string Serie, string Folio, DateTime FInicio, DateTime FFin, int ID_Estatus)
        //{
        //    List<Archivos> resultado = null;
        //    resultado = new List<Archivos>();
        //    int noReg = 0;

        //    Archivos archivos = null;
        //    archivos = new Archivos();
        //    archivos.ID_Sociedad = ID_Sociedad;
        //    archivos.Nombre_XML = ArchivXML;
        //    archivos.Serie = Serie;
        //    archivos.Folio = Folio;
        //    archivos.ID_Estatus = ID_Estatus;

        //    List<Archivos> respArchivos = null;
        //    respArchivos = new List<Archivos>();

        //    Archivos archivoResp = null;
        //    archivoResp = new Archivos();
        //    try
        //    {
        //        resultado = BUArchivos.Buscar(archivos);
        //        noReg = resultado.Count;
        //        for (int n = 0; n < noReg; n++)
        //        {
        //            DateTime fechaResultado = new DateTime();
        //            fechaResultado = resultado[n].Fecha_Recepcion.Date;
        //            if (fechaResultado >= FInicio.Date && fechaResultado <= FFin.Date)
        //            {
        //                archivoResp.Id_Recepcion = resultado[n].Id_Recepcion;
        //                archivoResp.Nombre_XML = resultado[n].Nombre_XML;
        //                archivoResp.Nombre_PDF = resultado[n].Nombre_PDF;
        //                archivoResp.Archivo_XML = resultado[n].Archivo_XML;
        //                archivoResp.Archivo_PDF = resultado[n].Archivo_PDF;
        //                archivoResp.Fecha_Recepcion = resultado[n].Fecha_Recepcion;
        //                archivoResp.ID_Proveedor = resultado[n].ID_Proveedor;
        //                archivoResp.ID_Sociedad = resultado[n].ID_Sociedad;
        //                archivoResp.Folio = resultado[n].Folio;
        //                archivoResp.Serie = resultado[n].Serie;
        //                archivoResp.UUID = resultado[n].UUID;
        //                archivoResp.Fecha_Factura = resultado[n].Fecha_Factura;
        //                archivoResp.Importe = resultado[n].Importe;
        //                archivoResp.ID_Estatus = resultado[n].ID_Estatus;
        //                archivoResp.Observaciones = resultado[n].Observaciones;
        //                archivoResp.Valido = resultado[n].Valido;

        //                respArchivos.Add(archivoResp);
        //            }
        //        }
        //        return respArchivos;
        //    }
        //    catch(Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

    }
}
