﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Portal;

namespace BLRecepcion.Portal
{
    public class BLPedidos
    {
        /// <summary>
        /// Metodo para buscar Pedidos
        /// </summary>
        /// <param name="id_proveedor"></param>
        /// <returns></returns>
        public List<Pedidos> buscaPedidos(int id_proveedor)
        {
            Pedidos pedido = new Pedidos();
            pedido.ID_Proveedor = id_proveedor;
            pedido.ID_Recepcion = 0;
            try
            {
                return BUPedidos.Buscar(pedido);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para actualizar pedidos
        /// </summary>
        /// <param name="ID_Pedido"></param>
        /// <param name="ID_Recepcion"></param>
        /// <returns></returns>
        public bool actualizaPedido(int ID_Pedido, int ID_Recepcion)
        {
            Pedidos pedido = new Pedidos();
            pedido.ID_Pedido = ID_Pedido;
            pedido.ID_Recepcion = ID_Recepcion;
            try
            {
                return BUPedidos.Actualizar(pedido);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
