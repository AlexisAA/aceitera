﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace BLRecepcion.Portal
{
    public class BLCreaAddenda
    {
        /// <summary>
        /// Metodo para addenda
        /// </summary>
        /// <param name="datosAddenda"></param>
        /// <param name="_ID_Proveedor_SAP"></param>
        /// <param name="_archivo"></param>
        /// <returns></returns>
        public string creaAddenda(DataTable datosAddenda, string _ID_Proveedor_SAP, string _archivo)
        {
            string cadenaAddenda = "";
            DataTable dt = new DataTable();
            dt = datosAddenda;

            List<Addenda> newAddenda = new List<Addenda>();
            List<AddendaIAPartida> newPartida = new List<AddendaIAPartida>();
            AddendaIAPartida[] itemsIAPartida = new AddendaIAPartida[999];
            int noItemsIAPartida = 0;

            try
            {
                if (dt != null)
                {
                    #region Rutina para agregar partida
                    int noItemsDT = 0;
                    noItemsDT = dt.Rows.Count;
                    //Recorremos la tabla
                    for (int p = 0; p < noItemsDT; p++)
                    {
                        string Pedido = "";
                        string Posicion = "";
                        string[] Entrada = new string[999];

                        AddendaIAPartida csPartida = new AddendaIAPartida();
                        Pedido = dt.Rows[p]["Pedido"].ToString();
                        Posicion = dt.Rows[p]["Posicion"].ToString();

                        int noEntrada = 0;
                        Entrada[noEntrada] = dt.Rows[p]["Entrada"].ToString();

                        #region Rutina para agregar mas de una entrada
                        for (int e = 0; e < noItemsDT; e++)
                        {
                            string Pedido2 = "";
                            string Posicion2 = "";
                            string Entrada2 = "";

                            Pedido2 = dt.Rows[e]["Pedido"].ToString();
                            Posicion2 = dt.Rows[e]["Posicion"].ToString();
                            Entrada2 = dt.Rows[e]["Entrada"].ToString();

                            if (Pedido == Pedido2 && Posicion == Posicion2 && p != e)
                            {
                                bool existeEntrada = false;
                                
                                //Recorremos el arreglo para ver si existe la entrada
                                for (int a = 0; a < Entrada.Length; a++)
                                {
                                    string aEntrada = "";
                                    aEntrada = Entrada[a];
                                    if (aEntrada == Entrada2)
                                    {
                                        existeEntrada = true;
                                        break;
                                    }
                                }

                                //Si no existe la entrada la guardamos en el Arreglo
                                if (!existeEntrada)
                                {
                                    noEntrada = noEntrada + 1;
                                    Entrada[noEntrada] = Entrada2;
                                }
                            }
                        }
                        #endregion

                        #region Agrega Partida
                        bool existePartida = false;
                        int noPartida = 0;
                        noPartida = newPartida.Count;
                        //Recorremos la lista de Partidas para determinar que no exista
                        for (int partida = 0; partida < noPartida; partida++)
                        {
                            string listPedido = "";
                            string listPosicion = "";
                            listPedido = newPartida[partida].Pedido;
                            listPosicion = newPartida[partida].Posicion;
                            if (Pedido == listPedido && Posicion == listPosicion)
                            {
                                existePartida = true;
                                break;
                            }
                        }

                        if (!existePartida)
                        {
                            csPartida.Pedido = Pedido;
                            csPartida.Posicion = Posicion;
                            csPartida.Entrada = Entrada;
                            newPartida.Add(csPartida);
                            itemsIAPartida[noItemsIAPartida] = newPartida[noItemsIAPartida];
                            noItemsIAPartida = noItemsIAPartida + 1;

                        }
                        #endregion

                    }
                    #endregion

                    #region Creamos la Addenda
                    List<AddendaIA> newAddendaIA = new List<AddendaIA>();
                    AddendaIA csAddendaIA = new AddendaIA();
                    csAddendaIA.Proveedor = _ID_Proveedor_SAP;
                    csAddendaIA.Partida = itemsIAPartida;
                    newAddendaIA.Add(csAddendaIA);

                    Addenda csAddenda = new Addenda();
                    csAddenda.IA = newAddendaIA[0];
                    newAddenda.Add(csAddenda);
                    #endregion

                    cadenaAddenda = serializaAddenda(newAddenda, _ID_Proveedor_SAP, _archivo);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cadenaAddenda;
        }

        /// <summary>
        /// Metodo que crea y extrae el XML
        /// </summary>
        /// <param name="addenda"></param>
        /// <param name="_IDProveedorSAP"></param>
        /// <param name="nombreArchivo"></param>
        /// <returns></returns>
        private string serializaAddenda(List<Addenda> addenda, string _IDProveedorSAP, string nombreArchivo)
        {
            string resp = "";
            string strPath = "";
            //strPath = Server.MapPath(@"./UpdateFiles/tmpAddenda/" + _IDProveedorSAP.ToString());
            DirectoryInfo rootDir = new DirectoryInfo(strPath);

            try
            {
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);

                var serializer = new XmlSerializer(addenda.GetType());
                using (var escribe = XmlWriter.Create(nombreArchivo))
                {
                    serializer.Serialize(escribe, addenda);
                }

                XmlDocument xml = new XmlDocument();
                xml.Load(strPath + nombreArchivo);
                string strXML = "";
                strXML = xml.OuterXml.ToString();

                strXML = strXML.Replace(@"<?xml version=""1.0"" encoding=""utf-8""?><ArrayOfAddenda xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><Addenda><IA xmlns=""http://www.aceitera.com.mx/addenda.xsd"">", 
                    @"<cfdi:Addenda xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:noNamespaceSchemaLocation=""http://www.aceitera.com.mx/addenda.xsd""><IA>");
                strXML = strXML.Replace(@"</Addenda></ArrayOfAddenda>", @"</cfdi:Addenda>");

                resp = strXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

    }
}
