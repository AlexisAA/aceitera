﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cat_Recepcion.aspx.cs" Inherits="RecepciondeFecturas.Cat_Recepcion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>

    <div class="mainProgramacion">
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" 
            meta:resourcekey="lblMensajeResource1"></asp:Label>

        <asp:Panel ID="pnlBuscar" runat="server" meta:resourcekey="pnlBuscarResource1">
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblSociedad" runat="server" Text="Sociedad: " 
                            meta:resourcekey="lblSociedadResource1"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSociedad" runat="server" Width="500px" 
                            meta:resourcekey="ddlSociedadResource1">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblProveedor" runat="server" Text="Proveedor :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlProveedor" runat="server" Width="500px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblNombrearchivo" runat="server" Text="Nombre archivo :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombrearchivo" runat="server"  Width="500px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblSerie" runat="server" Text="Serie :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSerie" runat="server"  Width="100px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblFolio" runat="server" Text="Folio :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFolio" runat="server"  Width="100px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblfechaIni" runat="server" Text="Inicio recepción:" 
                                        meta:resourcekey="lblfechaIniResource1"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaini" runat="server" Width="72px" 
                                        meta:resourcekey="txtFechainiResource1"></asp:TextBox>
                                    <cc1:CalendarExtender ID="cleFechaini" runat="server" 
                                        TargetControlID="txtFechaini" Format="dd/MM/yyyy" BehaviorID="cleFechaini"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFechafin" runat="server" Text="Fin recepción :" 
                                        meta:resourcekey="lblFechafinResource1"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechafin" runat="server" Width="72px" 
                                        meta:resourcekey="txtFechafinResource1"></asp:TextBox>
                                    <cc1:CalendarExtender ID="cleFechafin" runat="server" 
                                        TargetControlID="txtFechafin" Format="dd/MM/yyyy" BehaviorID="cleFechafin"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEstatus" runat="server" Text="Estatus"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEstatus" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="botonchico" 
                                        onclick="btnBuscar_Click" meta:resourcekey="btnBuscarResource1" />
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:ImageButton ID="btnExportar" runat="server" 
                            ImageUrl="~/Imagenes/Excel.png" Width="30px" ToolTip="Exportar a Excel" 
                            onclick="btnExportar_Click" meta:resourcekey="btnExportarResource1" />
                        &nbsp;
                        <asp:ImageButton ID="btnActualizar" runat="server" 
                            ImageUrl="~/Imagenes/Actualizar2.jpg" Width="30px" ToolTip="Actualizar" 
                            onclick="btnActualizar_Click" meta:resourcekey="btnActualizarResource1" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <div class="divgrvProcesados">
            <table style="width: 100%; height: 100%; text-align:center;">
                <%--Grid--%>
                <tr>
                    <td>
                        <asp:GridView ID="dgvRecepcionArchivos" runat="server" AutoGenerateColumns="False" 
                            AllowPaging="True" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="10"
                            DataKeyNames="Id_Recepcion" Width="100%" 
                            AlternatingRowStyle-CssClass="alt"
                            onrowdatabound="dgvRecepcionArchivos_RowDataBound" 
                            onrowcommand="dgvRecepcionArchivos_RowCommand" 
                            onpageindexchanging="dgvRecepcionArchivos_PageIndexChanging">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
                                <asp:BoundField DataField="Id_Recepcion" HeaderText="ID" ><ItemStyle HorizontalAlign="Center" Width="20px" /></asp:BoundField>
                                <asp:BoundField DataField="Nombre" HeaderText="Proveedor" ><ItemStyle HorizontalAlign="Left"  Width="250px"/></asp:BoundField>
                                <asp:BoundField DataField="Nombre_Sociedad" HeaderText="Sociedad" ><ItemStyle HorizontalAlign="Left"  Width="250px"/></asp:BoundField>
                                <asp:BoundField DataField="Fecha_Recepcion" HeaderText="Fecha de Recepción" DataFormatString="{0:g}" ><ItemStyle HorizontalAlign="Center" Width="200px" /></asp:BoundField>
                                <asp:BoundField DataField="Nombre_XML" HeaderText="Archivo XML" ><ItemStyle HorizontalAlign="Left" Width="250px"/></asp:BoundField>
                                <asp:BoundField DataField="Fecha_Factura" HeaderText="Fecha factura" DataFormatString="{0:d}" ><ItemStyle HorizontalAlign="Center" Width="100px" /></asp:BoundField>
                                <asp:BoundField DataField="Serie" HeaderText="Serie" ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Folio" HeaderText="Folio" ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" ><ItemStyle HorizontalAlign="Left"  Width="200px"/></asp:BoundField>
                                <asp:BoundField DataField="ID_Estatus" HeaderText="Estatus"><ItemStyle HorizontalAlign="Left"  Width="20px"/></asp:BoundField>
                            
                                <asp:TemplateField HeaderText="Estatus" ItemStyle-HorizontalAlign="Center" meta:resourcekey="BoundFieldResource8" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:Image ID="estatus" runat="server" Height="20px"></asp:Image>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PDF" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgPdf" runat="server" Height="20px" ImageUrl="~/Imagenes/pdf.png" CommandName="cmdPDF" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="XML" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgXml" runat="server" Height="20px" ImageUrl="~/Imagenes/xml.png" CommandName="cmdXML" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="#B9B9B9" Font-Bold="True" ForeColor="White" />
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>

    </div>

</asp:Content>
