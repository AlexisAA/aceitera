﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Catalogos;

namespace RecepciondeFecturas
{
    public partial class Cerrar : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int idUsuario = -1;
                if (Session["id_Usuario"] != null)
                {
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                    if(cerrarSesión(idUsuario))
                    {
                        if (Session["Idioma"].ToString() == "es-MX")
                        {
                            Session.Clear();
                            Response.Redirect("~/Default.aspx", false);
                        }
                        else
                        {
                            Session.Clear();
                            Response.Redirect("~/Default.aspx?lang=EN", false);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Metodo para cerrar sesión
        /// </summary>
        /// <param name="_idUsuario"></param>
        /// <returns></returns>
        protected bool cerrarSesión(int _idUsuario)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            bool resp = false;
            BLUsuarios cierra = new BLUsuarios();
            try
            {
                resp = cierra.cerrarSesion(_idUsuario);
                muestraError("cerrarSesión", "Cerrar Sesion", "Ok", "Sesión cerrada correctamente. Usuario : " + _idUsuario);
            }
            catch (Exception ex)
            {
                muestraError("cerrarSesión", "Cerrar Sesion", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Cerrar", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }
    }
}