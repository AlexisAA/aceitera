﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Catalogos;
using System.Data;

namespace RecepciondeFecturas
{
    public partial class Site : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMasterMsj.Visible = false;
            lblMasterMsj.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null && Session["Nombre_Usuario"] != null && Session["iD_Perfil"] != null && Session["Idioma"] != null)
                    {
                        if (Session["Idioma"].ToString() == "es-MX")
                        {
                            lblFecha.Text = DateTime.Now.ToShortDateString();
                            lblTituloProveedor.Text = "Proveedor :";
                            lblTituloUsuario.Text = "Usuario :";
                            imgEncabezado.ImageUrl = "~/Imagenes/Encabezado.png";
                        }
                        else
                        {
                            lblFecha.Text = DateTime.Now.ToString("yyyy/MM/dd");
                            lblTituloProveedor.Text = "Supplier :";
                            lblTituloUsuario.Text = "User :";
                            imgEncabezado.ImageUrl = "~/Imagenes/Header.png";
                        }
                            
                        lblUsuario.Text = Session["Nombre_Usuario"].ToString();
                        lblProveedor.Text = buscaNombre(Convert.ToInt32(Session["Id_Proveedor"].ToString()));
                        pnlMenu.Visible = true;
                        pnlDatos.Visible = true;
                    }
                    else
                    {
                        imgEncabezado.ImageUrl = "~/Imagenes/Encabezado.png";
                        pnlMenu.Visible = false;
                        pnlDatos.Visible = false;
                    }

                    if (Request.QueryString["lang"] == null || Request.QueryString["lang"] == "ES")
                        imgEncabezado.ImageUrl = "~/Imagenes/Encabezado.png";
                    else
                        imgEncabezado.ImageUrl = "~/Imagenes/Header.png";

                }
                catch (Exception ex)
                {
                    mustraError("Page_Load", "Load", "Error", ex.Message);
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null && Session["Nombre_Usuario"] != null && Session["iD_Perfil"] != null && Session["Idioma"]!= null)
            {
                string strIdioma = Session["Idioma"].ToString();
                buscaMenu(strIdioma);

                string path = "";
                int idItem = -1;
                int noReg = -1;

                path = HttpContext.Current.Request.Url.AbsolutePath;

                noReg = mnuPrincipal.Items.Count;
                for (int i = 0; i < noReg; i++)
                {
                    string urlMenu = mnuPrincipal.Items[i].NavigateUrl.ToString().Substring(2);
                    if (path.IndexOf(urlMenu) > 0)
                    {
                        idItem = i;
                    }
                }

                if (idItem >= 0)
                    mnuPrincipal.Items[idItem].Selected = true;
            }
            base.Render(writer);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (Session["id_Usuario"] != null)
            {
                pnlMenu.Visible = true;
                pnlDatos.Visible = true;
            }
            else
            {
                pnlMenu.Visible = false;
                pnlDatos.Visible = false;
                //pnlDoctos.Visible = false;
            }
            base.OnPreRender(e);
        }

        /// <summary>
        /// Metodo para buscar el nombe del proveedor
        /// </summary>
        /// <param name="_id_Proveedor"></param>
        /// <returns></returns>
        private string buscaNombre(int _id_Proveedor)
        {
            lblMasterMsj.Visible = false;
            lblMasterMsj.Text = "";

            string nombreProveedor = string.Empty;
            BLProveedores buscaProveedor = new BLProveedores();
            int idProveedor = 0;
            string nombreUsuario = string.Empty;
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null && Session["Nombre_Usuario"] != null && Session["iD_Perfil"] != null)
            {
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                nombreUsuario = Session["Nombre_Usuario"].ToString();
                try
                {
                    List<Proveedores> csProveedores = buscaProveedor.buscaProveedorxID(idProveedor);
                    if (csProveedores.Count > 0)
                    {
                        nombreProveedor = csProveedores[0].ID_Proveedor_SAP.ToString();
                        Session["ID_Proveedor_SAP"] = csProveedores[0].ID_Proveedor_SAP.ToString();
                    }
                }
                catch (Exception ex)
                {
                    mustraError("buscaNombre", "buscaNombre", "Error", ex.Message);
                }
            }
            else
            {
                pnlDatos.Visible = false;
                pnlMenu.Visible = false;
            }
            return nombreProveedor;
        }

        /// <summary>
        /// Meyodo para mostrar menu
        /// </summary>
        /// <param name="_idioma"></param>
        private void buscaMenu(string _idioma)
        {
            lblMasterMsj.Visible = false;
            lblMasterMsj.Text = "";

            if (Session["iD_Perfil"] != null)
            {
                int idPerfil = Convert.ToInt32(Session["iD_Perfil"].ToString());
                int ctaMenP = 0;
                BLMenuenPerfil buscaMenp = new BLMenuenPerfil();
                BLMMenu buscaMenu = new BLMMenu();
                try
                {
                    List<MenuenPerfil> csMenP = buscaMenp.buscaMenPxperfil(idPerfil);
                    ctaMenP = csMenP.Count;
                    for (int p = 0; p < ctaMenP; p++)
                    {
                        int idMenu = -1;
                        idMenu = csMenP[p].ID_Menu;
                        List<MMenu> csMenu = buscaMenu.buscaMenuxId(idMenu);
                        if (csMenu.Count > 0)
                        {
                            string nombreMenu = string.Empty;
                            string urlMenu = string.Empty;

                            MenuItem itemMenu = new MenuItem();
                            itemMenu.Value = idMenu.ToString();
                            itemMenu.NavigateUrl = csMenu[0].URL_Menu;
                            if (_idioma == "es-MX")
                                itemMenu.Text = csMenu[0].Nombre;
                            else
                                itemMenu.Text = csMenu[0].Nombre_en;

                            mnuPrincipal.Items.Add(itemMenu);
                        }
                    }
                }
                catch (Exception ex)
                {
                    mustraError("buscaMenu", "buscaMenu", "Error", ex.Message);
                }
            }
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void mustraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            BLLogs guardaLog = new BLLogs();
            try
            {
                bool resp = guardaLog.insertaLog("Site", _evento, _accion, _respuesta, _descripción, 0);
                muestrarMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestrarMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para mostrar mensaje en pantalla
        /// </summary>
        /// <param name="_descripcion"></param>
        private void muestrarMensaje(string _descripcion)
        {
            lblMasterMsj.Text = _descripcion;
            lblMasterMsj.Visible = true;
        }

    }
}