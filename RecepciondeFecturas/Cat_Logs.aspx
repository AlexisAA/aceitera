﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cat_Logs.aspx.cs" Inherits="RecepciondeFecturas.Cat_Logs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div class="mainDatos">
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" CssClass="Textonormal"
            meta:resourcekey="lblMensajeResource1"></asp:Label>
        <div class="divBienvenido">
            <asp:Label ID="lblProveedores" runat="server" Text="Logs" CssClass="Titulo1"></asp:Label>
        </div>
        <table style="width: 100%; height: 100%; text-align: center; vertical-align: middle;">
            <tr>
                <td align="left">
                    <asp:Label ID="lblUsuario" runat="server" Text="Usuario :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlUsuarios" runat="server" CssClass="Textonormal" Width="500px">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblfechaIni" runat="server" Text="Inicio recepción:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaini" runat="server" Width="72px"></asp:TextBox>
                                <cc1:CalendarExtender ID="cleFechaini" runat="server" TargetControlID="txtFechaini" Format="dd/MM/yyyy"/>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblFechafin" runat="server" Text="Fin recepción :"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechafin" runat="server" Width="72px"></asp:TextBox>
                                <cc1:CalendarExtender ID="cleFechafin" runat="server" TargetControlID="txtFechafin" Format="dd/MM/yyyy"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="botonchico" 
        onclick="btnBuscar_Click" />&nbsp;
    <div class="divgrvCatalogos">
            <table style="width: 100%; height: 100%; text-align:center;">
                <%--Grid--%>
                <tr>
                    <td>
                        <asp:GridView ID="dgvLogs" runat="server" AutoGenerateColumns="False" 
                            AllowPaging="True" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20"
                            DataKeyNames="Id_Log" Width="100%" 
                            AlternatingRowStyle-CssClass="alt" 
                            onpageindexchanging="dgvLogs_PageIndexChanging">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
<%--                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Seleccionar" runat="server" ImageUrl="~/Imagenes/magnifier.png" CommandName="CSeleccionar" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:BoundField DataField="Id_Log" HeaderText="ID" ><ItemStyle HorizontalAlign="center" Width="20px"/></asp:BoundField>
                                <asp:BoundField DataField="Fecha" HeaderText="Fecha" ><ItemStyle HorizontalAlign="Left"  Width="200px"/></asp:BoundField>
                                <asp:BoundField DataField="Usuario" HeaderText="Usuario" ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Formulario" HeaderText="Formulario" ><ItemStyle HorizontalAlign="center" Width="100px"/></asp:BoundField>
                                <asp:BoundField DataField="Evento" HeaderText="Evento" ><ItemStyle HorizontalAlign="Left" Width="100px"/></asp:BoundField>
                                <asp:BoundField DataField="Accion" HeaderText="Accion" ><ItemStyle HorizontalAlign="Left" Width="100px"/></asp:BoundField>
                                <asp:BoundField DataField="Respuesta" HeaderText="Respuesta" ><ItemStyle HorizontalAlign="Left" Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" ><ItemStyle HorizontalAlign="Left"  Width="420px"/></asp:BoundField>
                            </Columns>
                            <SelectedRowStyle BackColor="#B9B9B9" Font-Bold="True" ForeColor="White" />
                                
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
</asp:Content>
