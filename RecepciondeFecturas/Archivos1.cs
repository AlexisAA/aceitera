﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecepciondeFecturas
{
    public class Archivos1
    {
        #region Atributos
        private int _id_Recepcion;
        private string _nombre_XML;
        private string _nombre_PDF;
        private byte[] _archivo_XML;
        private byte[] _archivo_PDF;
        private DateTime _fecha_Recepcion;
        private int _iD_Proveedor;
        private int _iD_Sociedad;
        private string _folio;
        private string _serie;
        private string _uUID;
        private DateTime _fecha_Factura;
        private decimal _importe;
        private int _iD_Estatus;
        private string _observaciones;
        private bool _valido;
        #endregion

        #region Gets y Sets
        public int Id_Recepcion
        {
            get { return _id_Recepcion; }
            set { _id_Recepcion = value; }
        }
        public string Nombre_XML
        {
            get { return _nombre_XML; }
            set { _nombre_XML = value; }
        }
        public string Nombre_PDF
        {
            get { return _nombre_PDF; }
            set { _nombre_PDF = value; }
        }
        public byte[] Archivo_XML
        {
            get { return _archivo_XML; }
            set { _archivo_XML = value; }
        }
        public byte[] Archivo_PDF
        {
            get { return _archivo_PDF; }
            set { _archivo_PDF = value; }
        }
        public DateTime Fecha_Recepcion
        {
            get { return _fecha_Recepcion; }
            set { _fecha_Recepcion = value; }
        }
        public int ID_Proveedor
        {
            get { return _iD_Proveedor; }
            set { _iD_Proveedor = value; }
        }
        public int ID_Sociedad
        {
            get { return _iD_Sociedad; }
            set { _iD_Sociedad = value; }
        }
        public string Folio
        {
            get { return _folio; }
            set { _folio = value; }
        }
        public string Serie
        {
            get { return _serie; }
            set { _serie = value; }
        }
        public string UUID
        {
            get { return _uUID; }
            set { _uUID = value; }
        }
        public DateTime Fecha_Factura
        {
            get { return _fecha_Factura; }
            set { _fecha_Factura = value; }
        }
        public decimal Importe
        {
            get { return _importe; }
            set { _importe = value; }
        }
        public int ID_Estatus
        {
            get { return _iD_Estatus; }
            set { _iD_Estatus = value; }
        }
        public string Observaciones
        {
            get { return _observaciones; }
            set { _observaciones = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; }
        }
        #endregion

        public Archivos1()
        {
            
        }
    }
}