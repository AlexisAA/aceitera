﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cat_Usuarios.aspx.cs" Inherits="RecepciondeFecturas.Cat_Usuarios" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        }

        function pageLoad(sender, args) {
            //
            document.getElementsByClassName('ajax__fileupload_selectFileButton')[0].innerHTML = "Buscar";
            document.getElementsByClassName('ajax__fileupload_dropzone')[0].innerHTML = "Suelte aqui los archivos";
            document.getElementsByClassName('ajax__fileupload_topFileStatus')[0].innerHTML = "";
            document.getElementsByClassName('ajax__fileupload_uploadbutton')[0].innerHTML = "Subir";
            document.getElementsByClassName('ajax__fileupload_removeButton')[0].innerHTML = "Borrar";
//            if (Session["Idioma"] == "ES") {
//                
//            }
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mainDatos">
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" CssClass="Textonormal"
            meta:resourcekey="lblMensajeResource1"></asp:Label>
        <div class="divBienvenido">
            <asp:Label ID="lblUsuarios" runat="server" Text="Usuarios" CssClass="Titulo1"></asp:Label>
        </div>
        <asp:Panel ID="pnlDatos" runat="server" Visible="false">
            <table style="width: 100%; height: 100%; text-align: center; vertical-align: middle;">
            <tr>
                <td align="left">
                    <asp:Label ID="lblIdUsuario" runat="server" Text="ID Usuario :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtIdUsuario" runat="server" CssClass="Textonormal" 
                        Width="500px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblUsuario" runat="server" Text="Usuario :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtUsuario" runat="server" CssClass="Textonormal" 
                        Width="500px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblContrasenia" runat="server" Text="Contraseña :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtContrasenia" runat="server" CssClass="Textonormal" MaxLength="15"
                        Width="500px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblNombre" runat="server" Text="Nombre :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="Textonormal" Width="500px" 
                        Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblAPaterno" runat="server" Text="A. Paterno :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtAPaterno" runat="server" CssClass="Textonormal" 
                        Width="500px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblAMaterno" runat="server" Text="A. Materno :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtAMaterno" runat="server" CssClass="Textonormal" 
                        Width="500px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblProveedor" runat="server" Text="Proveedor :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlProveedor" runat="server" CssClass="Textonormal" 
                        Width="500px" Enabled="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblPerfil" runat="server" Text="Perfil :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlPerfil" runat="server" CssClass="Textonormal" 
                        Width="500px" Enabled="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblIntentos" runat="server" Text="No. Intentos :" CssClass="Textonormal"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtIntentos" runat="server" CssClass="Textonormal" 
                        Width="500px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <%--<asp:Label ID="lblBanco" runat="server" Text="Banco :" CssClass="Textonormal"></asp:Label>--%>
                </td>
                <td align="left">
                    <%--<asp:TextBox ID="txtBanco" runat="server" CssClass="Textonormal" Width="500px" TextMode="MultiLine"></asp:TextBox>--%>
                    <asp:CheckBox ID="chbBloqueado" runat="server" Text="Bloqueado" 
                        CssClass="Textonormal" Enabled="False" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <%--<asp:Label ID="lblCuenta" runat="server" Text="Cta. Bancaria :" CssClass="Textonormal"></asp:Label>--%>
                </td>
                <td align="left">
                    <%--<asp:TextBox ID="txtCuenta" runat="server" CssClass="Textonormal" Width="500px" TextMode="MultiLine"></asp:TextBox>--%>
                    <asp:CheckBox ID="chbActivo" runat="server" Text="Activo" 
                        CssClass="Textonormal" Enabled="False" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    
                </td>
                <td align="left">
                    <asp:CheckBox ID="chbValido" runat="server" Text="Valido" 
                        CssClass="Textonormal" Enabled="False" />
                </td>
            </tr>
        </table>
        </asp:Panel>
    </div>
    <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" CssClass="botonchico" 
        onclick="btnNuevo_Click" />&nbsp;
    <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" 
        CssClass="botonchico" Visible="false" onclick="btnActualizar_Click" />&nbsp;
    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="botonchico" 
        onclick="btnBuscar_Click" />&nbsp;
    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="botonchico" 
        Visible="false" onclick="btnGuardar_Click" />&nbsp;
    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" 
        CssClass="botonchico" Visible="false" onclick="btnCancelar_Click" />
    <div class="divgrvCatalogos">
            <table style="width: 100%; height: 100%; text-align:center;">
                <%--Grid--%>
                <tr>
                    <td>
                        <asp:GridView ID="dgvUsuarios" runat="server" AutoGenerateColumns="False" 
                            AllowPaging="True" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="10"
                            DataKeyNames="Id_Usuario" Width="100%" 
                            AlternatingRowStyle-CssClass="alt" onrowcommand="dgvUsuarios_RowCommand" 
                            onrowdatabound="dgvUsuarios_RowDataBound" 
                            onpageindexchanging="dgvUsuarios_PageIndexChanging">
                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Seleccionar" runat="server" ImageUrl="~/Imagenes/magnifier.png" CommandName="CSeleccionar" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Id_Usuario" HeaderText="ID" ><ItemStyle HorizontalAlign="center" Width="20px"/></asp:BoundField>
                                <asp:BoundField DataField="Usuario" HeaderText="Usuario" ><ItemStyle HorizontalAlign="Left" Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" ><ItemStyle HorizontalAlign="Left" Width="150px"/></asp:BoundField>
                                <asp:BoundField DataField="APaterno" HeaderText="A. Paterno" ><ItemStyle HorizontalAlign="Left" Width="150px"/></asp:BoundField>
                                <asp:BoundField DataField="AMaterno" HeaderText="A. Maerno" ><ItemStyle HorizontalAlign="Left" Width="150px"/></asp:BoundField>
                                <asp:BoundField DataField="Fecha_Alta" HeaderText="Fecha Alta" DataFormatString="{0:d}" ><ItemStyle HorizontalAlign="Center" Width="50px" /></asp:BoundField>
                                <asp:BoundField DataField="Fecha_Acpt_Contrato" HeaderText="F Contrato" DataFormatString="{0:d}"  ><ItemStyle HorizontalAlign="Left" Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="FUltimo_Acceso" HeaderText="U. Acceso" DataFormatString="{0:d}"  ><ItemStyle HorizontalAlign="Left"  Width="50px"/></asp:BoundField>
                                <asp:BoundField DataField="No_Intentos" HeaderText="Intentos" DataFormatString="{0:d}"  ><ItemStyle HorizontalAlign="Left"  Width="30px"/></asp:BoundField>
                            </Columns>
                            <SelectedRowStyle BackColor="#B9B9B9" Font-Bold="True" ForeColor="White" />
                                
                            <PagerStyle CssClass="pgr"></PagerStyle>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>

</asp:Content>
