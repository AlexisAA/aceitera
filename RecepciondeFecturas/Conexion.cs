﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RecepciondeFecturas
{
    public class Conexion
    {
        private static SqlConnection conSQL;
        
        public static SqlConnection getConSQL ()
        {
            string connectionString = string.Empty;
            connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            conSQL = new SqlConnection();
            conSQL.ConnectionString = connectionString;

            try
            {
                conSQL.Open();
            }
            catch (Exception e)
            {
                throw e;
            }
            return conSQL;
        }
        public static void cerrarConSQL ()
        {
            if (conSQL != null)
                conSQL.Close();
        }
    }
}