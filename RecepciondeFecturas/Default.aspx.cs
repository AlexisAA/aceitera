﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Catalogos;
using BLRecepcion.Varios;

namespace RecepciondeFecturas
{
    public partial class Default : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            lblMensaje.Visible = false;
            if (!IsPostBack)
            {
                if (Request.QueryString["lang"] == null || Request.QueryString["lang"] == "ES")
                {
                    Session["Idioma"] = "es-MX";
                    ddlIdioma.SelectedValue = "es-MX";
                }
                else
                {
                    Session["Idioma"] = Request.QueryString["Lang"].ToString();
                    if (Session["Idioma"].ToString() == "es-MX")
                        ddlIdioma.SelectedValue = "es-MX";
                    else
                        ddlIdioma.SelectedValue = "en-US";
                }

                Session["id_Usuario"] = null;
                Session["Nombre_Usuario"] = null;
                Session["Id_Proveedor"] = null;
                Session["iD_Perfil"] = null;
                Session["No_Intentos"] = "0";
                Session["ID_Proveedor_SAP"] = null;
            }
        }

        protected override void InitializeCulture()
        {
            if (Request.QueryString["lang"] == null)
            {
                UICulture = "ES";
            }
            else
            {
                UICulture = Request.QueryString["lang"].ToString();
            }

            base.InitializeCulture();
        }

        protected void ddlIdioma_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            string lenguaje = string.Empty;
            try
            {
                lenguaje = ddlIdioma.SelectedValue.ToString();
                if (!string.IsNullOrEmpty(lenguaje))
                {
                    if (lenguaje == "es-MX")
                        Response.Redirect("~/Default.aspx?lang=ES", false);
                    else
                        Response.Redirect("~/Default.aspx?lang=EN", false);
                }

            }
            catch (Exception Ex)
            {
                muestraError("ddlIdioma", "SelectedIndexChanged", "Error", Ex.Message);
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = string.Empty;
            lblMensaje.Visible = false;

            if (validaTextos() == string.Empty)
            {
                //Buscamos la información del usuario
                string strUsuario = string.Empty;
                string strContrasenia = string.Empty;
                string strEncriptada = string.Empty;

                strUsuario = txtUsuario.Text.Trim();
                strContrasenia = txtContrasenia.Text;
               
                BLUsuarios buscaUsuario = new BLUsuarios();
                try
                {
                    List<Usuarios> csUsuarios = buscaUsuario.buscaUsuario(strUsuario);
                    if (csUsuarios.Count > 0)
                    {
                        #region Asignamos idioma
                        if (Request.QueryString["lang"] == null || Request.QueryString["lang"] == "ES")
                        {
                            Session["Idioma"] = "es-MX";
                            ddlIdioma.SelectedValue = "es-MX";
                        }
                        else
                        {
                            Session["Idioma"] = Request.QueryString["Lang"].ToString();
                            if (Session["Idioma"].ToString() == "es-MX")
                                ddlIdioma.SelectedValue = "es-MX";
                            else
                                ddlIdioma.SelectedValue = "en-US";
                        }
                        #endregion

                        #region Variables del metodo
                        int usrIdUsuario = -1;
                        string usrNombre = string.Empty;
                        int usrIdProveedor = -1;
                        bool usrValido = false;
                        string usrBloqueado = string.Empty;
                        DateTime usrFechabloqueo = new DateTime();
                        bool usrActivo = false;
                        string usrContrasenia = string.Empty;
                        bool usrAceptacontrato = false;
                        int usrNoIntentos = -1;

                        usrIdUsuario = csUsuarios[0].Id_Usuario;
                        usrNombre = csUsuarios[0].Nombre;
                        usrIdProveedor = csUsuarios[0].Id_Proveedor;
                        usrValido = csUsuarios[0].Valido;
                        usrBloqueado = csUsuarios[0].Bloqueado;
                        usrFechabloqueo = csUsuarios[0].Fecha_Bloqueo;
                        usrActivo = csUsuarios[0].Activo;
                        usrContrasenia = csUsuarios[0].Contrasenia;
                        usrAceptacontrato = csUsuarios[0].Acepta_Contrato;
                        usrNoIntentos = csUsuarios[0].No_Intentos;
                        #endregion


                        Seguridad seguridad = new Seguridad();
                        string respC = seguridad.encriptar(strContrasenia);
                        string resp1 = seguridad.desencriptar(respC); //usrContrasenia


                        #region Validacion: Usuario valido
                        if (usrValido)
                        {
                            #region Validación: Usuario Bloqueado
                            if (usrBloqueado == "NO")
                            {
                                #region Validación: Sesión activa
                                if (!usrActivo)
                                {
                                    #region Validación: Contraseña
                                   
                                    strEncriptada = encroptaPass(strContrasenia);

                                    if (strEncriptada == usrContrasenia)
                                    {
                                        //Asignamos las variables de sesión
                                        Session["id_Usuario"] = usrIdUsuario.ToString();
                                        Session["Nombre_Usuario"] = usrNombre.ToString();
                                        Session["Id_Proveedor"] = usrIdProveedor.ToString();

                                        #region Valida: Contrato
                                        if (usrAceptacontrato)
                                        {
                                            buscaPerfil(usrIdUsuario);
                                        }
                                        else
                                        {
                                            mppPoliticas.Show();
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Manejo de intentos
                                        if (usrNoIntentos > 3)
                                        {
                                            bloqueaUsuario(usrIdUsuario);
                                            muestraError("btnAceptar", "bloqueaUsuario", "Ok", (string)GetLocalResourceObject("Error_UsuarioBloqueado"));
                                        }
                                        else
                                        {
                                            usrNoIntentos = usrNoIntentos + 1;
                                            agregaIntento(usrIdUsuario, usrNoIntentos);
                                            muestraMensaje((string)GetLocalResourceObject("Error_UsuarioContrasenia"));
                                        }
                                        #endregion
                                    }

                                    #endregion
                                }
                                else
                                {
                                    muestraMensaje((string)GetLocalResourceObject("Error_UsuarioActivo"));
                                    hplDesactivar.Visible = true;
                                }
                                    
                                #endregion
                            }
                            else
                            {
                                #region Manejo de bloqueo
                                int minutos = -1;
                                minutos = verificaBloqueo(usrFechabloqueo);
                                if (minutos < 30)
                                {
                                    int faltan = 0;
                                    faltan = 30 - minutos;
                                    muestraMensaje((string)GetLocalResourceObject("Error_UsuarioBloqueadoPre") +
                                        " " + faltan + " " +
                                        (string)GetLocalResourceObject("Error_UsuarioBloqueadoSu"));
                                }
                                else
                                {
                                    if (desbloqueaUsuario(usrIdUsuario))
                                        muestraError("btnAceptar", "desbloqueaUsuario", "Ok", (string)GetLocalResourceObject("Error_UsuarioDesbloqueado"));
                                    else
                                        muestraMensaje((string)GetLocalResourceObject("Error_UsuarioNoDesbloqueado"));
                                }
                                #endregion
                            }
                            #endregion
                        }
                        else
                            muestraMensaje((string)GetLocalResourceObject("Error_UsuarioValido"));
                        #endregion
                    }
                    else
                        muestraMensaje((string)GetLocalResourceObject("Error_UsuarioContrasenia"));
                }
                catch (Exception ex)
                {
                    muestraError("btnAceptar", "Click", "Error", ex.Message);
                }
            }
            else
            {
                muestraMensaje(validaTextos());
            }
        }

        #region Metodos del formulario
        /// <summary>
        /// Metodo para validar textos
        /// </summary>
        /// <returns></returns>
        private string validaTextos()
        {
            if (txtUsuario.Text == "")
                return ((string)GetLocalResourceObject("Error_NombreUsuario"));
            if (txtContrasenia.Text == "")
                return ((string)GetLocalResourceObject("Error_ConraseniaUsuario"));

            return (string.Empty);
        }

        /// <summary>
        /// Metodo para calcular los minutos restantes del bloqueo
        /// </summary>
        /// <param name="fechabloqueo">Fecha de Bloqueo</param>
        /// <returns>Minutos</returns>
        private int verificaBloqueo(DateTime fechabloqueo)
        {
            DateTime fechaAhora = new DateTime();
            TimeSpan fechaResultado = new TimeSpan();
            int minRestantes = 0;
            fechaAhora = DateTime.Now;
            fechaResultado = fechaAhora.Subtract(fechabloqueo);
            minRestantes = (fechaResultado.Days * 24 * 60) + (fechaResultado.Hours * 60) + fechaResultado.Minutes;
            return minRestantes;
        }

        /// <summary>
        /// Metodo para desbloquear usuario
        /// </summary>
        /// <param name="_idUsuario">ID de usuario</param>
        /// <returns>Tru si se desbloqueo</returns>
        private bool desbloqueaUsuario(int _idUsuario)
        {
            BLUsuarios csUsuarios = new BLUsuarios();
            bool bRespuesta = false;
            try
            {
                bRespuesta = csUsuarios.desbloquea(_idUsuario);
            }
            catch (Exception ex)
            {
                muestraError("desbloqueaUsuario", "desbloqueaUsuario", "Error", ex.Message);
            }

            return bRespuesta;
        }

        /// <summary>
        /// Metodo para encriptar contraseña
        /// </summary>
        /// <param name="_contrasenia"></param>
        /// <returns></returns>
        private string encroptaPass(string _contrasenia)
        {
            string resp = string.Empty;
            Seguridad seguridad = new Seguridad();
            try
            {
                resp = seguridad.encriptar(_contrasenia);
            }
            catch (Exception ex)
            {
                muestraError("encroptaPass", "encroptaPass", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para agregar intento de acceso
        /// </summary>
        /// <param name="_idUsuario"></param>
        /// <param name="_No_Intento"></param>
        private void agregaIntento(int _idUsuario, int _No_Intento)
        {
            BLUsuarios csUsuarios = new BLUsuarios();
            bool bRespuesta = false;
            try
            {
                bRespuesta = csUsuarios.intentos(_idUsuario, _No_Intento);
            }
            catch (Exception ex)
            {
                muestraError("agregaIntento", "agregaIntento", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para bloquear usuario
        /// </summary>
        /// <param name="_idUsuario"></param>
        private void bloqueaUsuario(int _idUsuario)
        {
            BLUsuarios csUsuarios = new BLUsuarios();
            bool bRespuesta = false;
            try
            {
                bRespuesta = csUsuarios.bloquea(_idUsuario);
            }
            catch (Exception ex)
            {
                muestraError("bloqueaUsuario", "bloqueaUsuario", "Error", ex.Message);
            }
        }
        #endregion

        #region Eventos y metodos del Pop Up Politicas
        protected void btnppPoliticasCancelar_Click(object sender, EventArgs e)
        {
            Session["id_Usuario"] = null;
            Session["Nombre_Usuario"] = null;
            Session["Id_Proveedor"] = null;
            Session["iD_Perfil"] = null;
            Session["No_Intentos"] = "0";
            Response.Redirect("~/Default.aspx", false);
        }

        protected void btnppPoliticasAceptar_Click(object sender, EventArgs e)
        {
            int idUsuario = 0;
            bool resp = false;
            if (Session["id_Usuario"] != null)
            {
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                //Registramos que el usuario ha aceptado las politicas.
                BLUsuarios csUsuarios = new BLUsuarios();
                try
                {
                    resp = csUsuarios.aceptaPoliticas(idUsuario);
                    if (resp)
                    {
                        muestraError("btnppPoliticasAceptar", "Click", "Ok", "El Usuario : " + idUsuario.ToString() + " ha aceptado las politicas");
                        //Buscamos el perfil
                        buscaPerfil(idUsuario);
                    }
                }
                catch (Exception ex)
                {
                    muestraError("btnppPoliticasAceptar", "Click", "Error", ex.Message);
                }
            }
        }
        #endregion

        #region Eventos y metodos el Pop Up Perfiles
        protected void btnAceptaPerfil_Click(object sender, EventArgs e)
        {
            if (ddlPerfil.SelectedValue != "-1")
            {
                Session["iD_Perfil"] = ddlPerfil.SelectedValue.ToString();
                Response.Redirect("~/Home.aspx", false);
            }
        }
        #endregion

        /// <summary>
        /// Metodo para buscar perfil de usuario
        /// </summary>
        /// <param name="_IdUsuario"></param>
        private void buscaPerfil(int _IdUsuario)
        {
            BLUsuarioenPerfil buscaPerfilUsuario = new BLUsuarioenPerfil();
            try
            {
                List<UsuarioenPerfil> uenp = buscaPerfilUsuario.buscaporUsuario(_IdUsuario);
                if (uenp.Count > 0)
                {
                    if (uenp.Count > 1)
                    {
                        //Llenamos el DDL de Perfiles
                        llenaDDlPerfiles(uenp);
                        //Mostramos el PopUp
                        mppPerfil.Show();
                    }
                    else
                    {
                        Session["iD_Perfil"] = uenp[0].ID_Perfil.ToString();
                        Response.Redirect("~/Home.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("buscaPerfil", "buscaPerfil", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar el DDL de Perfiles
        /// </summary>
        /// <param name="csUenP"></param>
        private void llenaDDlPerfiles(List<UsuarioenPerfil> csUenP)
        {
            ddlPerfil.Items.Clear();

            int ctaRegistros = -1;
            ctaRegistros = csUenP.Count;
            for (int n = 0; n < ctaRegistros; n++)
            {
                int idPerfil = -1;
                idPerfil = csUenP[n].ID_Perfil;
                BLPerfiles buscaPerfil = new BLPerfiles();
                try
                {
                    List<Perfiles> csPerfiles = buscaPerfil.buscaPerfil(idPerfil);
                    if (csPerfiles.Count > 0)
                    {
                        string nombre = string.Empty;
                        string nombre_en = string.Empty;
                        nombre = csPerfiles[0].Nombre;
                        nombre_en = csPerfiles[0].Nombre_en;

                        if (Session["Idioma"].ToString() == "es-MX")
                        {
                            ddlPerfil.Items.Add(new ListItem(nombre, idPerfil.ToString()));
                        }
                        else
                        {
                            ddlPerfil.Items.Add(new ListItem(nombre_en, idPerfil.ToString()));
                        }
                        ddlPerfil.AppendDataBoundItems = true;
                    }
                }
                catch (Exception ex)
                {
                    muestraError("llenaDDlPerfiles", "llenaDDlPerfiles", "Error", ex.Message);
                }
            }
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Default", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

    }
}