﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using System.Data;

namespace RecepciondeFecturas
{
    public partial class Cat_Logs : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                    
                    CargarFechas();
                    llenaddlUsuarios();

                    llenaGrid(Convert.ToInt32(ddlUsuarios.SelectedValue), Convert.ToDateTime(cleFechaini.SelectedDate), Convert.ToDateTime(cleFechafin.SelectedDate));
                }
            }
        }

        protected void dgvLogs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvLogs.PageIndex = e.NewPageIndex;
            llenaGrid(Convert.ToInt32(ddlUsuarios.SelectedValue), Convert.ToDateTime(cleFechaini.SelectedDate), Convert.ToDateTime(cleFechafin.SelectedDate));
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            int Id_Usuario = -1;
            DateTime fechaIni = new DateTime();
            DateTime fechaFin = new DateTime();
            Id_Usuario = Convert.ToInt32(ddlUsuarios.SelectedValue);
            fechaIni = Convert.ToDateTime(txtFechaini.Text.ToString());
            fechaFin = Convert.ToDateTime(txtFechafin.Text.ToString());
            llenaGrid(Id_Usuario, fechaIni, fechaFin);
            cleFechaini.SelectedDate = fechaIni;
            cleFechafin.SelectedDate = fechaFin;
        }

        /// <summary>
        /// Metodo que inicializa las fechas
        /// </summary>
        private void CargarFechas()
        {
            DateTime fecha = DateTime.Now.Date;
            this.cleFechaini.SelectedDate = new DateTime(fecha.Year, fecha.Month, 1);
            this.cleFechafin.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
        }

        /// <summary>
        /// Metodo para llenar DDL
        /// </summary>
        private void llenaddlUsuarios()
        {
            BLUsuarios busca = new BLUsuarios();
            try
            {
                List<Usuarios> csUsuarios = busca.buscaUsuario();
                if (csUsuarios.Count > 0)
                {

                    ddlUsuarios.DataTextField = "Usuario";
                    ddlUsuarios.DataValueField = "ID_Usuario";
                    ddlUsuarios.Items.Add(new ListItem("----- Seleccionar -----", "0"));
                    ddlUsuarios.AppendDataBoundItems = true;
                    ddlUsuarios.DataSource = csUsuarios;
                    ddlUsuarios.DataBind();
                }
                else
                {
                    ddlUsuarios.DataTextField = "Usuario";
                    ddlUsuarios.DataValueField = "ID_Usuario";
                    ddlUsuarios.Items.Add(new ListItem("No hay datos para mostrar", "-1"));
                    ddlUsuarios.AppendDataBoundItems = true;
                }
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaddlUsuarios", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        /// <param name="_idusuario"></param>
        /// <param name="_fechaini"></param>
        /// <param name="_fechafin"></param>
        private void llenaGrid(int _idusuario, DateTime _fechaini, DateTime _fechafin)
        {
            dgvLogs.DataSource = null;
            dgvLogs.DataBind();

            BLLogs busca = new BLLogs();
            try
            {
                DataTable dtLogs = new DataTable();
                dtLogs = null;
                dtLogs = busca.buscaLogs(_idusuario, _fechaini, _fechafin);
                if (dtLogs.Rows.Count > 0)
                {
                    dgvLogs.DataSource = dtLogs;
                    dgvLogs.DataBind();
                }
                else
                    inicializaGrid();
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaGrid", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para inicializar el Grid
        /// </summary>
        private void inicializaGrid()
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("Id_Log", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Fecha", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("Usuario", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Formulario", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Evento", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Accion", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Respuesta", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Descripcion", typeof(String)));

            DataRow drVacio = dtArchivos.NewRow();
            drVacio[0] = 0;
            drVacio[1] = DateTime.Now;
            drVacio[2] = "";
            drVacio[3] = "";
            drVacio[4] = "";
            drVacio[5] = "";
            drVacio[6] = "";
            drVacio[7] = "";

            dtArchivos.Rows.Add(drVacio);
            dgvLogs.DataSource = dtArchivos;
            dgvLogs.DataBind();
            int totalcolums = dgvLogs.Rows[0].Cells.Count;
            dgvLogs.Rows[0].Cells.Clear();
            dgvLogs.Rows[0].Cells.Add(new TableCell());
            dgvLogs.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvLogs.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Proveedores", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Logs", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

    }
}