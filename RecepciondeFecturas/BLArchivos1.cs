﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RecepciondeFecturas
{
    public class BLArchivos1
    {
        public SqlConnection conexion;

        public BLArchivos1()
        {
            this.conexion = Conexion.getConSQL();
        }

        public List<Archivos1> buscaArchivoUUID(string _uuid)
        {
            List<Archivos1> catalogo = new List<Archivos1>();
            SqlCommand command = new SqlCommand();
            command.Connection = conexion;
            command.CommandText = "select ID_Estatus, Fecha_Recepcion from TBL_Archivos_Recibidos where UUID = '"+ _uuid + "'";

            try
            {
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Archivos1 archivo = new Archivos1();
                    archivo.ID_Estatus = reader.GetInt32(0);
                    archivo.Fecha_Recepcion = reader.GetDateTime(1);

                    catalogo.Add(archivo);
                }

                reader.Close();
            }
            catch (Exception e)
            {
                throw e;
            }

            return catalogo;
        }

        public List<Archivos1> buscar(Archivos1 archivo)
        {
            List<Archivos1> catalogo = new List<Archivos1>();
            SqlCommand command = new SqlCommand();
            command.Connection = conexion;
            command.CommandText = "SELECT	* FROM TBL_Archivos_Recibidos"+
            "WHERE(@Id_Recepcion_in     IS NULL OR Id_Recepcion = @Id_Recepcion_in)" +
            "AND(@Nombre_XML_in         IS NULL OR Nombre_XML       LIKE '%' + @Nombre_XML_in + '%')"+
            "AND(@Nombre_PDF_in         IS NULL OR Nombre_PDF       LIKE '%' + @Nombre_PDF_in + '%')"+
            "AND(@Fecha_Recepcion_in    IS NULL OR Fecha_Recepcion >= @Fecha_Recepcion_in)"+
            "AND(@ID_Proveedor_in       IS NULL OR ID_Proveedor = @ID_Proveedor_in)"+
            "AND(@ID_Sociedad_in        IS NULL OR ID_Sociedad = @ID_Sociedad_in)"+
            "AND(@Folio_in              IS NULL OR Folio            LIKE '%' + @Folio_in + '%')"+
            "AND(@Serie_in              IS NULL OR Serie            LIKE '%' + @Serie_in + '%')"+
            "AND(@UUID_in               IS NULL OR UUID = @UUID_in)"+
            "AND(@Fecha_Factura_in      IS NULL OR Fecha_Factura >= @Fecha_Factura_in)"+
            "AND(@ID_Estatus_in         IS NULL OR ID_Estatus = @ID_Estatus_in)"+
            "AND(@Valido_in             IS NULL OR Valido = @Valido_in)";

            command.Parameters.AddWithValue("@Nombre_XML_in", archivo.Nombre_XML);
            command.Parameters.AddWithValue("@Nombre_PDF_in", archivo.Nombre_PDF);
            command.Parameters.AddWithValue("@Archivo_XML_in", archivo.Archivo_XML);
            command.Parameters.AddWithValue("@Archivo_PDF_in", archivo.Archivo_PDF);
            command.Parameters.AddWithValue("@ID_Proveedor_in", archivo.ID_Proveedor);
            command.Parameters.AddWithValue("@ID_Sociedad_in", archivo.ID_Sociedad);
            command.Parameters.AddWithValue("@Folio_in", archivo.Folio);
            command.Parameters.AddWithValue("@Serie_in", archivo.Serie);
            command.Parameters.AddWithValue("@UUID_in", archivo.UUID);
            command.Parameters.AddWithValue("@Importe_in", archivo.Importe);
            command.Parameters.AddWithValue("@ID_Estatus_in", archivo.ID_Estatus);
            command.Parameters.AddWithValue("@Observaciones_in", archivo.Observaciones);

            try
            {
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Archivos1 _archivo = new Archivos1();
                    _archivo.ID_Estatus = reader.GetInt32(0);
                    _archivo.Fecha_Recepcion = reader.GetDateTime(1);

                    catalogo.Add(archivo);
                }

                reader.Close();
            }
            catch (Exception e)
            {
                throw e;
            }

            return catalogo;
        }

        public int buscaArchivos(DateTime fecha_recepcion, string uuid, DateTime fecha_factua)
        {
            int resp = 0;
            Archivos1 archivo = new Archivos1();
            List<Archivos1> csArchivo = new List<Archivos1>();
            archivo.Fecha_Recepcion = fecha_recepcion;
            archivo.UUID = uuid;
            archivo.Fecha_Factura = fecha_factua;
            try
            {
                csArchivo = buscar(archivo);


                if (csArchivo.Count > 0)
                {
                    resp = csArchivo[0].Id_Recepcion;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        public int insertaArchivo(string Nombre_XML, string Nombre_PDF, byte[] Archivo_XML, byte[] Archivo_PDF, int ID_Proveedor,
            int ID_Sociedad, string Folio, string Serie, string UUID, DateTime Fecha_Factura,
            decimal Importe, int ID_Estatus, string Observaciones, string Version_XML)
        {
            int resp = -1;
            DateTime Fecha_Recepcion = new DateTime();
            Fecha_Recepcion = DateTime.Now;

            SqlCommand command = new SqlCommand();
            command.Connection = conexion;
            command.CommandText = "insert into TBL_Archivos_Recibidos "+
                "(Nombre_XML, Nombre_PDF, Archivo_XML, Archivo_PDF, Fecha_Recepcion, ID_Proveedor, ID_Sociedad, Folio, Serie, UUID, Fecha_Factura, Importe, ID_Estatus, Observaciones, Valido, Version_Xml)" +
                "values (@Nombre_XML, @Nombre_PDF, @Archivo_XML, @Archivo_PDF, @Fecha_Recepcion, @ID_Proveedor, @ID_Sociedad, @Folio, @Serie, @UUID, @Fecha_Factura, @Importe, @ID_Estatus, @Observaciones, 1, @Version_Xml)";

            command.Parameters.AddWithValue("@Nombre_XML", Nombre_XML);
            command.Parameters.AddWithValue("@Nombre_PDF", Nombre_PDF);
            command.Parameters.AddWithValue("@Archivo_XML", Archivo_XML);
            command.Parameters.AddWithValue("@Archivo_PDF", Archivo_PDF);
            command.Parameters.AddWithValue("@Fecha_Recepcion", Fecha_Recepcion);
            command.Parameters.AddWithValue("@ID_Proveedor", ID_Proveedor);
            command.Parameters.AddWithValue("@ID_Sociedad", ID_Sociedad);
            command.Parameters.AddWithValue("@Folio", Folio);
            command.Parameters.AddWithValue("@Serie", Serie);
            command.Parameters.AddWithValue("@UUID", UUID);
            command.Parameters.AddWithValue("@Fecha_Factura", Fecha_Factura);
            command.Parameters.AddWithValue("@Importe", Importe);
            command.Parameters.AddWithValue("@ID_Estatus", ID_Estatus);
            command.Parameters.AddWithValue("@Observaciones", Observaciones);
            command.Parameters.AddWithValue("@Version_Xml", Version_XML);

            try
            {
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }
    }
}