﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using DALRecepcion.Bean.Portal;
using BLRecepcion.Portal;
using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Catalogos;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Net;
using BLRecepcion.Varios;
using System.Xml.Serialization;
using System.Data.SqlClient;

namespace RecepciondeFecturas
{
    public partial class Recepcion2 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    Session["Addenda"] = null;
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                    //Buscamos si el proveedor requiere addenda
                    if (buscaAddenda(idProveedor))
                    {
                        Session["Addenda"] = "True";
                        btnAddenda.Visible = true;
                        btnProcesar.Visible = false;
                    }
                    else
                    {
                        Session["Addenda"] = "False";
                        btnAddenda.Visible = true;
                        btnProcesar.Visible = false;
                    }
                        

                    //Mostramos que hay archivos pendientes por procesar
                    if (buscaArchivos(idProveedor, idUsuario))
                    {
                        lblporProcesar.Visible = true;
                        btnMostrar.Visible = true;
                    }

                    //Inicializamos las fechas
                    CargarFechas();
                    llenaddlSocieddes();
                    llenaddlEstatus();
                    limpiaTextos();

                    llenaGrid();

                    dgvRecepcionArchivos.Columns[7].Visible = false;

                    if (Session["Errores"] != null)
                    {
                        lblMensaje.Visible = true;
                        lblMensaje.Text = Session["Errores"].ToString();
                        Session["Errores"] = null;
                    }

                    //Solo numero en las cajas de texto
                    txtppAPedido.Attributes.Add("onkeypress", "return numeralsOnly(event)");
                    txtppAPosicion.Attributes.Add("onkeypress", "return numeralsOnly(event)");
                    txtppAEntrada.Attributes.Add("onkeypress", "return numeralsOnly(event)");
                }
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["Idioma"] == null)
                UICulture = "es-MX";
            else
                UICulture = Session["Idioma"].ToString();

            base.InitializeCulture();
        }

        protected void afuXML_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        {
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                bool creaAddenda = false;
                int idProveedor = -1;
                int idUsuario = -1;
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                string strPath = "";
                string nombreArchivo = "";

                if (Session["Addenda"].ToString() != null)
                    creaAddenda = Convert.ToBoolean(Session["Addenda"].ToString());

                //Ruta en la que se cargaran los archivos dependiendo si se Crea o no la Addenda
                if (creaAddenda)
                {
                    strPath = Server.MapPath(@"./UpdateFiles/tmpAddenda/" +
                    idProveedor.ToString() + "/" +
                    idUsuario.ToString() + "/");
                }
                else
                {
                    strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                    idProveedor.ToString() + "/" +
                    idUsuario.ToString() + "/");
                }

                try
                {
                    //Si no existe el directorio lo creamos
                    if (!Directory.Exists(strPath))
                        Directory.CreateDirectory(strPath);

                    //Obtenemos el nombre del archivo
                    nombreArchivo = Path.GetFileName(e.FileName);

                    //Verificamos que no existan los archivos, de ser asi los eliminamos para reemplazarlos
                    if (File.Exists(strPath + nombreArchivo))
                        File.Delete(strPath + nombreArchivo);

                    afuXML.SaveAs(strPath + nombreArchivo);
                }
                catch (Exception ex)
                {
                    muestraError("afuXML_UploadComplete", "Upload", "Error", ex.Message);
                }

            }
        }

        protected void btnProcesar_Click(object sender, EventArgs e)
        {
            //Sesion para mosrar los mensajes de error
            Session["Errores"] = null;
            lblMensaje.Visible = true;
            lblMensaje.Text = "";

            //Validamos que halla archivos en el directorio
            int idProveedor = -1;
            int idUsuario = -1;
            idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            //Validamos que para cada XML corresponda un PDF con el mismo nombre
            string strPath = "";
            string[] respuestaSAT = new string[2];
            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                idProveedor.ToString() + "/" +
                idUsuario.ToString() + "/");

            //Validamos los archivos a procesar
            if (Directory.Exists(strPath))
            {
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                FileInfo[] files = null;
                files = rootDir.GetFiles("*.xml");

                #region Cuenta archivos en directorio
                if (files != null && files.Count() > 0)
                {
                    foreach (FileInfo fi in files)
                    {
                        string nombrexml = "";
                        string nombre = "";
                        string nombrepdf = "";

                        //Variable para manejar los eventos
                        string msjError = string.Empty;
                        //string msjErrorSistema = string.Empty;
                        string accion = "";
                        string msj = "Error";
                        string msjCopia = "";
                        string msjInsert = "";
                        string strResp = "";
                        

                        #region Variables para el archivo XML
                        string[] datosXML = new string[9];
                        string xmlVersion = string.Empty;
                        string xmlrfcReceptor = string.Empty;
                        string xmlrfcEmisor = string.Empty;
                        string xmlFolio = string.Empty;
                        string xmlSerie = string.Empty;
                        string xmlUuid = string.Empty;
                        DateTime xmlFfactura = new DateTime();
                        decimal xmlImporte = 0;
                        
                        #endregion

                        #region Variables de la recepción del archivo
                        //string respPac = string.Empty;
                        int idEstatus = 1;
                        int idRecepcion = -1;
                        #endregion

                        #region Variables de busqueda
                        int idSociedad = 1;
                        //string usuarioPac = string.Empty;
                        //string passPac = string.Empty;
                        #endregion

                        nombrexml = fi.Name;
                        nombre = Path.GetFileNameWithoutExtension(nombrexml);
                        nombrepdf = nombre + ".pdf";

                        #region Existe PDF?
                        if (existePDF(idProveedor, idUsuario, nombrepdf))
                        {

                            #region Variables para mover archivos
                            int intAnio = -1;
                            int intMes = -1;
                            bool respuesta = false;
                            #endregion

                            accion = "Lee archivo";
                            #region Extraemos información del XML
                            datosXML = obtenDatosXML(strPath + nombrexml);
                            if (datosXML != null)
                            {
                                xmlVersion = datosXML[0] == null ? string.Empty : datosXML[0].ToString();
                                xmlrfcReceptor = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                                xmlrfcEmisor = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                                xmlFolio = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                                xmlSerie = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                                xmlUuid = datosXML[5] == null ? string.Empty : datosXML[5].ToString();
                                xmlFfactura = datosXML[6] == null ? DateTime.Now : Convert.ToDateTime(datosXML[6].ToString());
                                xmlImporte = datosXML[7] == null ? 0 : Convert.ToDecimal(datosXML[7].ToString());
                            }
                            else
                            {
                                msjError = "Error en XML";
                            }
                            #endregion

                            accion = "Busca Proveedor";
                            #region Validaciones del portal
                            if (msjError == string.Empty)
                            {
                                if (idProveedor == buscaProveedor(xmlrfcEmisor))
                                {
                                    accion = "Busca Sociedad";
                                    List<Sociedades> csSociedad = buscaSociedad(xmlrfcReceptor);
                                    if (csSociedad.Count > 0)
                                    {
                                        //Hacemos la validación del PAC
                                        idSociedad = Convert.ToInt32(csSociedad[0].Id_Sociedad.ToString());
                                        //usuarioPac = csSociedad[0].Usuario_ME;
                                        //passPac = csSociedad[0].Contrasenia_ME;
                                    }
                                    else
                                        msjError = "El RFC del Receptor de la factura no existe.";

                                }
                                else
                                    msjError = "El RFC del proveedor no corresponde al RFC de la factura.";
                            }
                            #endregion

                            accion = "Busca Archivo en base d datos";
                            #region Busca Archivo en TBL_ARchivos_Recibidos
                            if (msjError == string.Empty)
                            {
                                DateTime fechaRecepcion = new DateTime();
                                DateTime fechaHoy = new DateTime();
                                fechaHoy = DateTime.Now;
                                fechaRecepcion = buscaArchivo(fechaHoy, xmlUuid);
                                if (fechaHoy != fechaRecepcion)
                                {
                                    msjError = "La factura ya fue procesada el día " + fechaRecepcion.ToString();
                                }
                            }
                            #endregion
                            if (msjError == "")
                            {

                            
                            accion = "Envio de XML al SAT";
                            #region valido factura en el SAT

                            RespuestaSAT respue = new RespuestaSAT();
                            srefer.WebService1SoapClient fade = new srefer.WebService1SoapClient();
                            srefer.ValidaCFDIRequest xchecaSAT = new srefer.ValidaCFDIRequest();
                            srefer.RespuestaSAT refa = new srefer.RespuestaSAT();
                            refa = fade.ValidaCFDI(datosXML[5].ToString(), datosXML[2].ToString(), datosXML[1].ToString(), datosXML[7].ToString());

                            respuestaSAT[0] = refa.CodigoEstatus.ToString();
                            respuestaSAT[1] = refa.Estado.ToString();

                            if (respuestaSAT[1].ToString() == "Vigente")
                            {
                                respuesta = mueveArchivos(idProveedor, idUsuario, intAnio, intMes, nombrexml, nombrepdf);

                                if (respuesta)
                                {
                                    idEstatus = 5;         //Factura en revisión

                                    msjInsert = "Factura en revisión.";
                                    msj = "Factura : " + nombrexml + " " + msjInsert;
                                    //lblMensaje.Text = msj;

                                }
                                else
                                {
                                    idEstatus = 3;         //Factura valida fiscalmente

                                    msjInsert = "Factura valida fiscalmente pero no se pudo envíar a SAP.";
                                    msj = "Factura : " + nombrexml + " " + msjInsert;
                                    //lblMensaje.Text = msj;
                                    muestraMensaje(msj);

                                }
                            }
                            else if (respuestaSAT[1].ToString() != "Vigente")
                            {
                                //respuesta = eliminaArchivos(idProveedor, idUsuario);

                                msjError = "Factura : " + nombrexml + "  " + respuestaSAT[0].ToString() + " en el portal de SAT";
                                //lblMensaje.Text = msj;

                            }
                            else
                            {

                                idEstatus = 2;         //Error en factura o no valida fiscalmente
                                                       //msjCopia = copiaArchivosFacturasaError(ID_Proveedor, ID_Usuario, nombrexml, nombrepdf, nombrexmlSalida, nombrepdfSalida)
                                msjInsert = "Error en factura o no valida fiscalmente pero no se pudo envíar a la carpeta Error.";
                                if (msjCopia == "")
                                {
                                    msjInsert = respuestaSAT[0].ToString();
                                    msj = "Factura : " + nombrexml + " " + msjInsert;
                                    //lblMensaje.Text = msj;
                                    muestraMensaje(msj);
                                }
                                else
                                {
                                    idEstatus = 2;         //Factura valida fiscalmente
                                    msjInsert = "Error en factura o no valida fiscalmente pero no se pudo envíar a la carpeta Error.";
                                    msj = "Factura : " + nombrexml + " " + msjInsert;
                                    //lblMensaje.Text = msj;
                                    muestraMensaje(msj);
                                }

                            }
                        } else
                            {
                                idEstatus = 1;
                            }

                            #endregion

                            accion = "Mueve archivo";
                            #region Mueve archivos XML
                            if (msjError == string.Empty)
                            {
                                intAnio = xmlFfactura.Year;
                                intMes = xmlFfactura.Month;
                                respuesta = mueveArchivos(idProveedor, idUsuario, intAnio, intMes, nombrexml, nombrepdf);
                                if (respuesta)
                                {
                                    msjError = "Archivo válido fiscalmente y Enviado a SAP para su revisión";
                                }
                                else
                                {
                                    msjError = "Archivo valido fiscalmente pero no puede ser envíado a SAP. contacte al Administrador";
                                }
                            }
                            #endregion

                            #region Guardamos la recepción
                            accion = "Guara archivo";
                            idRecepcion = guardaRecepcion(nombrexml, nombrepdf, strPath + nombrexml, strPath + nombrepdf, idProveedor,
                                    idSociedad, xmlFolio, xmlSerie, xmlUuid, xmlFfactura,
                                    xmlImporte, idEstatus, msjError, xmlVersion);

                            if (msjError == "Archivo válido fiscalmente y Enviado a SAP para su revisión")
                            {
                                msj = "Ok";
                                msjError = "Recepción guardada correctamente. Id Recepción : " + idRecepcion.ToString(); ;
                            }
                            #endregion

                            #region Guarda Log
                            guardaLog("Recepcion archivo", accion, msj, msjError);
                            #endregion
                        }
                        else
                        {
                            if (Session["Errores"] == null)
                                Session["Errores"] = "Archivo no procesado, No se encontró PDF con el mismo nombre del XML : " + nombre;
                            else
                                Session["Errores"] = Session["Errores"].ToString() + ", " + "Archivo no procesado, No se encontró PDF con el mismo nombre del XML : " + nombre;
                            idEstatus = 1;
                        }
                        #endregion
                    }

                    bool r = eliminaArchivos(idProveedor, idUsuario);
                    //Response.Redirect("~/Recepcion2.aspx");
                }
                else
                {
                    Session["Errores"] = "No existe archivos XML para procesar";
                    bool r = eliminaArchivos(idProveedor, idUsuario);
                }
                #endregion
            }
            else
            {
                Session["Errores"] = "No existe el directorio tmp en el servidor, favor de contactar al administrador";
            }

            Response.Redirect("~/Recepcion2.aspx");
        }

        #region Metodos y eventos del PopUp Mostrar
        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            int idProveedor = -1;
            int idUsuario = -1;
            idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            muestraArchivos(idProveedor, idUsuario);
            mppMostrar.Show();
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                int idProveedor = -1;
                int idUsuario = -1;
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                eliminaArchivos(idProveedor, idUsuario);

                int idSociedad = Convert.ToInt32(ddlSociedad.SelectedValue.ToString());
                string nombreArchivo = txtNombrearchivo.Text.Trim() == string.Empty ? "" : txtNombrearchivo.Text.Trim();
                string serie = txtSerie.Text.Trim() == string.Empty ? "" : txtSerie.Text.Trim();
                string folio = txtFolio.Text.Trim() == string.Empty ? "" : txtFolio.Text.Trim();
                DateTime finicio = Convert.ToDateTime(cleFechaini.SelectedDate);
                DateTime ffin = Convert.ToDateTime(cleFechafin.SelectedDate);
                int idEstatus = Convert.ToInt32(ddlEstatus.SelectedValue.ToString());

                Response.Redirect("~/Recepcion2.aspx");
            }
        }

        /// <summary>
        /// Metodo para mostrar Archivos pendientes por cargar
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        private void muestraArchivos(int _IdProveedor, int _IdUsuario)
        {
            //Creamos el datatTable
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("No", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("ArchivoXML", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("ArchivoPDF", typeof(String)));
            //dtArchivos.Columns.Add(new DataColumn("Accion", typeof(String)));

            string strPath = "";
            string nombrexml = "";
            string nombre = "";
            //string accion = "";

            int noArchivos = 0;

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            try
            {
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.xml");

                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            nombrexml = fi.Name;
                            nombre = Path.GetFileNameWithoutExtension(nombrexml);
                            noArchivos = noArchivos + 1;

                            DataRow drArchivo = dtArchivos.NewRow();
                            drArchivo["No"] = noArchivos;
                            drArchivo["ArchivoXML"] = nombrexml;
                            drArchivo["ArchivoPDF"] = nombre + ".pdf";

                            dtArchivos.Rows.Add(drArchivo);
                        }
                    }
                }
                dgvArchivos.DataSource = dtArchivos;
                dgvArchivos.DataBind();
            }
            catch (Exception ex)
            {
                muestraError("llenagrvArchivos", "validaArchivos", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Meodos para eliminar archivs de el Path
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        private bool eliminaArchivos(int _IdProveedor, int _IdUsuario)
        {
            bool resp = true;
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");
            try
            {
                //Validamos los archivos a procesar
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.*");

                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            fi.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("validaArchivospendientes", "Buscaarchivo", "Error", ex.Message);
            }

            return resp;
        }
        #endregion

        protected void btnActualizar_Click(object sender, ImageClickEventArgs e)
        {
            dgvRecepcionArchivos.Columns[7].Visible = true;
            llenaGrid();
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }

        #region Eventos del Grid
        protected void dgvRecepcionArchivos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvRecepcionArchivos.PageIndex = e.NewPageIndex;

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);

            dgvRecepcionArchivos.Columns[7].Visible = true;
            llenaGrid();
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }

        protected void dgvRecepcionArchivos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int IdRecepcion = -1;
            string Tipo = string.Empty;
            switch (e.CommandName)
            {
                case "cmdPDF":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/pdf";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
                case "cmdXML":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/xml";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
            }
        }

        protected void dgvRecepcionArchivos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0] != null)
                {
                    Image imgEstatus = (Image)e.Row.FindControl("estatus");
                    ImageButton btnPdf = (ImageButton)e.Row.FindControl("imgPdf");
                    ImageButton btnXml = (ImageButton)e.Row.FindControl("imgXml");
                    int idEstatus = -1;
                    string idRecepcion = "";
                    idEstatus = Convert.ToInt32(e.Row.Cells[7].Text);
                    idRecepcion = e.Row.Cells[0].Text;

                    btnPdf.CommandArgument = idRecepcion;
                    btnXml.CommandArgument = idRecepcion;
                    if (idEstatus > 0)
                    {

                        BLEstatus buscaEstatus = new BLEstatus();
                        try
                        {
                            List<Estatus> csEstatus = buscaEstatus.buscaEstatus(idEstatus);
                            if (csEstatus.Count > 0)
                            {
                                imgEstatus.ImageUrl = csEstatus[0].Ruta.ToString();
                                imgEstatus.ToolTip = csEstatus[0].Descripcion.ToString();
                                //btnEstatus.CommandArgument = csEstatus[0].ID_Estatus.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            muestraError("dgvRecepcionArchivos", "buscaEstatus", "Error", ex.Message);
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Metro para mostrar archivos
        /// </summary>
        /// <param name="_idrecepcion"></param>
        /// <param name="_tipo"></param>
        private void mustrarArchivo(int _idrecepcion, string _tipo)
        {
            if (Session["id_Usuario"] != null)
            {
                int ID_Usuario = -1;
                string nombreArchivo = string.Empty;

                ID_Usuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                //Primero buscmos el archivo
                BLArchivos busca = new BLArchivos();
                try
                {
                    int csArch = 0;
                    string sql = "Select * from TBL_Archivos_Recibidos where Id_Recepcion = " + _idrecepcion;
                    


                    SqlConnection cadena = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ToString());
                    SqlCommand cmd = new SqlCommand(sql, cadena);
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    cadena.Open();
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    cadena.Close();


                    DataRow row = dt.Rows[0];

                    csArch = dt.Rows.Count;

                    //List<Archivos> csArchivos = busca.buscaPdfXml(_idrecepcion);
                    //if (csArchivos.Count > 0)
                    if(csArch > 0)
                    {
                        byte[] datosArchivo = null;
                        if (_tipo == "application/pdf")
                        {
                            nombreArchivo = dt.Rows[0].Field<string>("Nombre_PDF"); //csArch[0].Nombre_PDF;
                            datosArchivo = (byte[])row["Archivo_PDF"];  //csArch[0].Archivo_PDF;
                        }
                        else
                        {
                            nombreArchivo = dt.Rows[0].Field<string>("Nombre_XML");
                            datosArchivo = (byte[])row["Archivo_XML"];
                        }

                        Response.Clear();
                        Response.ContentType = _tipo;
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + nombreArchivo);
                        Response.BinaryWrite(datosArchivo);
                        Response.End();
                        Response.Close();
                    }

                    //Response.Redirect("~/Recepcion2.aspx", false);
                }
                catch (Exception ex)
                {
                    muestraError("dgvAclraciones", "mustrarArchivo", "Error", ex.Message);
                }
            }
        }
        #endregion

        #region Metodos y eventos de busqueda
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            dgvRecepcionArchivos.Columns[7].Visible = true;
            if (btnBuscar.Text == "Buscar")
            {
                inicializadgvRecepcion();
                limpiaTextos();
                btnCancel.Visible = true;
                btnBuscar.Text = "Aceptar";
                pnlBuscar.Visible = true;
                dgvRecepcionArchivos.Columns[7].Visible = false;
            }
            else
            {
                dgvRecepcionArchivos.DataSource = null;
                dgvRecepcionArchivos.DataBind();

                cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
                cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
                llenaGrid();
                dgvRecepcionArchivos.Columns[7].Visible = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            dgvRecepcionArchivos.Columns[7].Visible = true;
            CargarFechas();
            limpiaTextos();
            ddlSociedad.SelectedValue = "0";
            ddlEstatus.SelectedValue = "-1";
            llenaGrid();
            btnBuscar.Text = "Buscar";
            btnCancel.Visible = false;
            pnlBuscar.Visible = false;
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }
        #endregion

        #region Eventos y metodos del Load
        /// <summary>
        /// Metodo para buscar archivos pendientes por procesar
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <returns></returns>
        private bool buscaArchivos(int _IdProveedor, int _IdUsuario)
        {
            bool resp = false;
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            //Validamos los archivos a procesar
            DirectoryInfo rootDir = new DirectoryInfo(strPath);
            if (Directory.Exists(strPath))
            {
                FileInfo[] files = null;
                //files = rootDir.GetFiles("*.xml");
                files = rootDir.GetFiles("*.*");

                if (files != null)
                    if(files.Count() > 0)
                        resp = true;
            }

            return resp;

        }

        /// <summary>
        /// Metodo para llenad DDL de sociedades
        /// </summary>
        private void llenaddlSocieddes()
        {
            ddlSociedad.Items.Clear();

            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                List<Sociedades> csSociedad = buscaSociedad.buscaSociedad();
                if (csSociedad.Count > 0)
                {
                    ddlSociedad.DataValueField = "Id_Sociedad";
                    ddlSociedad.DataTextField = "Razon_Social";
                    ddlSociedad.Items.Clear();
                    ddlSociedad.Items.Add(new ListItem("Todas", "0"));
                    ddlSociedad.AppendDataBoundItems = true;
                    ddlSociedad.DataSource = csSociedad;
                    ddlSociedad.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("PageLoad", "llenaddlSocieddes", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar ddl estatus
        /// </summary>
        private void llenaddlEstatus()
        {
            ddlEstatus.Items.Clear();

            BLEstatus buscaEstatus = new BLEstatus();
            try
            {
                List<Estatus> csEstatus = buscaEstatus.buscaEstatus();
                if (csEstatus.Count > 0)
                {
                    ddlEstatus.DataValueField = "ID_Estatus";
                    ddlEstatus.DataTextField = "Nomnre";
                    ddlEstatus.Items.Clear();
                    ddlEstatus.Items.Add(new ListItem("Todas", "-1"));
                    ddlEstatus.AppendDataBoundItems = true;
                    ddlEstatus.DataSource = csEstatus;
                    ddlEstatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("PageLoad", "llenaddlEstatus", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para buscar si el proveedor requiere Addenda
        /// </summary>
        /// <param name="_Id_Proveedor"></param>
        /// <returns></returns>
        private bool buscaAddenda(int _Id_Proveedor)
        {
            bool resp = false;
            BLProveedores blproveedor = new BLProveedores();
            try
            {
                List<Proveedores> csProveedor = blproveedor.buscaProveedorxID(_Id_Proveedor);
                if (csProveedor.Count > 0)
                    resp = csProveedor[0].Addenda;
            }
            catch (Exception ex)
            {
                muestraError("PageLoad", "buscaAddenda", "Error", ex.Message);
            }
            return resp;
        }
        #endregion

        #region Metodos comunes
        /// <summary>
        /// Metodo que inicializa las fechas
        /// </summary>
        private void CargarFechas()
        {
            DateTime fecha = DateTime.Now.Date;
            this.cleFechaini.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
            this.cleFechafin.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
        }

        /// <summary>
        /// Metodo para limpiar textos de busqueda
        /// </summary>
        private void limpiaTextos()
        {
            txtNombrearchivo.Text = "";
            CargarFechas();
            txtFolio.Text = "";
            txtSerie.Text = "";
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        private void llenaGrid()
        {
            dgvRecepcionArchivos.DataSource = null;
            dgvRecepcionArchivos.DataBind();

            DataTable dtBusca = new DataTable();
            try
            {
                dtBusca = buscaArchivos();
                if (dtBusca.Rows.Count > 0)
                {
                    dgvRecepcionArchivos.DataSource = dtBusca;
                    dgvRecepcionArchivos.DataBind();
                }
                else
                    inicializadgvRecepcion();

            }
            catch (Exception ex)
            {
                muestraError("btnBuscar", "llenaGrid", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para buscar archivos
        /// </summary>
        /// <returns></returns>
        private DataTable buscaArchivos()
        {
            #region VAriables locales
            int ID_Proveedor = -1;
            int ID_Sociedad = -1;
            string Nombre_archivo = string.Empty;
            string Serie = string.Empty;
            string Folio = string.Empty;
            DateTime Fecha_Ini = new DateTime();
            DateTime Fecha_Fin = new DateTime();
            int Id_Estatus = -1;
            #endregion

            #region Asignación
            ID_Proveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            ID_Sociedad = Convert.ToInt32(ddlSociedad.SelectedValue.ToString());
            Nombre_archivo = txtNombrearchivo.Text.Trim() == string.Empty ? "" : txtNombrearchivo.Text.Trim();
            Serie = txtSerie.Text.Trim() == string.Empty ? "" : txtSerie.Text.Trim();
            Folio = txtFolio.Text.Trim() == string.Empty ? "" : txtFolio.Text.Trim();
            Fecha_Ini = Convert.ToDateTime(cleFechaini.SelectedDate.ToString());
            Fecha_Fin = Convert.ToDateTime(cleFechafin.SelectedDate.ToString());
            Id_Estatus = Convert.ToInt32(ddlEstatus.SelectedValue.ToString());
            #endregion

            DataTable dtArchivos = null;
            BLArchivos busca = new BLArchivos();
            try
            {
                dtArchivos = new DataTable();
                dtArchivos = busca.buscaArchivos(ID_Proveedor, ID_Sociedad, Nombre_archivo, Serie, Folio, Fecha_Ini, Fecha_Fin, Id_Estatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtArchivos;

        }

        /// <summary>
        /// Metodo para inicializar el Grid
        /// </summary>
        private void inicializadgvRecepcion()
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("Id_Recepcion", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Fecha_Recepcion", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("Nombre_XML", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Nombre_PDF", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Folio", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Serie", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("ID_Estatus", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Observaciones", typeof(String)));

            DataRow drVacio = dtArchivos.NewRow();
            drVacio[0] = 0;
            drVacio[1] = DateTime.Now;
            drVacio[2] = "";
            drVacio[3] = "";
            drVacio[4] = "";
            drVacio[5] = "";
            drVacio[6] = 0;
            drVacio[7] = "";

            dtArchivos.Rows.Add(drVacio);
            dgvRecepcionArchivos.DataSource = dtArchivos;
            dgvRecepcionArchivos.DataBind();
            int totalcolums = dgvRecepcionArchivos.Rows[0].Cells.Count;
            dgvRecepcionArchivos.Rows[0].Cells.Clear();
            dgvRecepcionArchivos.Rows[0].Cells.Add(new TableCell());
            dgvRecepcionArchivos.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvRecepcionArchivos.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }
        #endregion

        #region Manejo de errores
        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Recepcion", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Recepcion", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }
        #endregion

        #region Metodos y eventos del PopUp Addenda
        protected void btnAddenda_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            lblMensaje.Visible = false;

            //Validamos las Sessiones
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                bool creaAddenda = false;
                int idProveedor = -1;
                int idUsuario = -1;
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                string strPath = "";

                if (Session["Addenda"].ToString() != null)
                    creaAddenda = Convert.ToBoolean(Session["Addenda"].ToString());

                //Ruta en la que se cargaran los archivos dependiendo si se Crea o no la Addenda
                //Verificamos que tenga acceso a la Addenda
                if (creaAddenda)
                {
                    strPath = @"./UpdateFiles/tmpAddenda/";
                    try
                    {
                        string validaArchivos = "";
                        validaArchivos = buscaArchivosAddenda(idProveedor, idUsuario, strPath);
                        if (validaArchivos == "")
                        {
                            //Archivo a Procesar
                            string archivoXML = string.Empty;

                            //Ruta en la que se cargaran los archivos
                            int noArchivo = 0;
                            strPath = strPath +
                                idProveedor.ToString() + "/" +
                                idUsuario.ToString() + "/";

                            #region Extraemos el primer archivo
                            DirectoryInfo rootDir = new DirectoryInfo(Server.MapPath(strPath));
                            if (Directory.Exists(Server.MapPath(strPath)))
                            {
                                FileInfo[] filesXML = null;
                                //int noXML = 0;
                                //Contamos el numero de Archivos XML Cargados
                                filesXML = rootDir.GetFiles("*.xml");
                                noArchivo = filesXML == null ? 0 : filesXML.Count();
                                foreach (FileInfo fXML in filesXML)
                                {
                                    archivoXML = fXML.Name;
                                    break;
                                }
                            }
                            #endregion

                            #region Extraemos la información del XML
                            string[] datosXML = new string[9];
                            datosXML = obtenDatosXML(Server.MapPath(strPath) + archivoXML);
                            if (datosXML != null)
                            {
                                txtppAArchivo.Text = archivoXML;
                                txtppAReceptor.Text = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                                txtppAEmisor.Text = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                                txtppAFolio.Text = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                                txtppASerie.Text = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                                txtppAFecha.Text = datosXML[6] == null ? string.Empty : datosXML[6].ToString();
                                txtppAImporte.Text = datosXML[7] == null ? string.Empty : datosXML[7].ToString();
                                btnppAAgregar.Enabled = true;
                            }
                            else
                            {
                                lblppAMensaje.Text = "Error en XML, no se creara Addenda de esta factura, De clic en Crear para continuar";
                                lblppAMensaje.Visible = true;
                                eliminaArchivosAddenda(idProveedor, idUsuario, strPath);
                            }
                            #endregion

                            pnlAddenda.Visible = true;
                            pnlSinAddenda.Visible = false;
                            //mppAddenda.Show();
                        }
                        else
                        {
                            eliminaArchivosAddenda(idProveedor, idUsuario, strPath);
                            muestraMensaje(validaArchivos);
                        }
                    }
                    catch (Exception ex)
                    {
                        muestraError("btnAddenda", "Click", "Error", ex.Message);
                    }
                }
                else
                {
                    strPath = @"./UpdateFiles/tmp/";
                    try
                    {
                        string validaArchivos = "";
                        validaArchivos = buscaArchivosAddenda(idProveedor, idUsuario, strPath);
                        if (validaArchivos == "")
                        {
                            btnProcesar_Click(sender, e);
                            Session["ERRORES"] = "Se procesaron los documentos correctamente.";
                        }
                        else
                        {
                            eliminaArchivosAddenda(idProveedor, idUsuario, strPath);
                            muestraMensaje(validaArchivos);
                        }

                    }
                    catch (Exception ex)
                    {
                        muestraError("btnAddenda", "Click_SinA", "Error", ex.Message);
                    }
                }
            }
        }

        protected void grvAddenda_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton ibtnBorrar = (ImageButton)e.Row.FindControl("imgBorrar");
                string registro = "";
                registro = e.Row.Cells[0].Text;
                ibtnBorrar.CommandArgument = registro;
            }
        }

        protected void grvAddenda_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int noRow = 0;
            if (e.CommandName == "cmdBorrar")
            {
                DataTable dtCopia = new DataTable();
                dtCopia.Columns.Add(new DataColumn("NoRegistro", typeof(Int32)));
                dtCopia.Columns.Add(new DataColumn("Pedido", typeof(String)));
                dtCopia.Columns.Add(new DataColumn("Entrada", typeof(string)));
                dtCopia.Columns.Add(new DataColumn("Posicion", typeof(string)));

                int noRegistros = -1;
                int newNo = 0;
                noRow = Convert.ToInt32(e.CommandArgument.ToString());
                DataTable dtAddenda = (DataTable)Session["dtAddenda"];
                noRegistros = dtAddenda.Rows.Count;
                for (int n = 0; n < noRegistros; n++)
                {
                    int noRowAddenda = -1;
                    noRowAddenda = Convert.ToInt32(dtAddenda.Rows[n]["NoRegistro"]);
                    if (noRowAddenda != noRow)
                    {
                        newNo = newNo + 1;
                        DataRow drAgrega = dtCopia.NewRow();
                        drAgrega["NoRegistro"] = newNo;
                        drAgrega["Pedido"] = dtAddenda.Rows[n]["Pedido"];
                        drAgrega["Entrada"] = dtAddenda.Rows[n]["Entrada"];
                        drAgrega["Posicion"] = dtAddenda.Rows[n]["Posicion"];
                        dtCopia.Rows.Add(drAgrega);
                    }
                }

                if (dtCopia.Rows.Count > 0)
                    btnppACrear.Visible = true;
                else
                    btnppACrear.Visible = false;

                Session["dtAddenda"] = dtCopia;
                grvAddenda.DataSource = dtCopia;
                grvAddenda.DataBind();

                //btnppAAgregar.Visible = false;
                //btnppACancelar.Visible = false;
                //btnppACrear.Visible = false;
                //btnppAActualizar.Visible = true;
                //mppAddenda.Show();
            }
        }

        protected void btnppACancelar_Click(object sender, EventArgs e)
        {
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                int idProveedor = -1;
                int idUsuario = -1;
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                string strPathAddenda = "";
                strPathAddenda = @"./UpdateFiles/tmpAddenda/";
                #region Eliminamos el archivo
                if (txtppAArchivo.Text.Trim() != "")
                {
                    eliminaArchivosAddenda(idProveedor, idUsuario, strPathAddenda, txtppAArchivo.Text.Trim());
                }
                #endregion

                #region Extraemos el siguiente archivo
                string strPathOrigen = "";
                int noArchivo = 0;
                string archivoXML = "";

                strPathOrigen = Server.MapPath(@"./UpdateFiles/tmpAddenda/" +
                idProveedor.ToString() + "/" +
                idUsuario.ToString() + "/");

                DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                if (Directory.Exists(strPathOrigen))
                {
                    FileInfo[] filesXML = null;
                    //int noXML = 0;
                    //Contamos el numero de Archivos XML Cargados
                    filesXML = rootDir.GetFiles("*.xml");
                    noArchivo = filesXML == null ? 0 : filesXML.Count();
                    foreach (FileInfo fXML in filesXML)
                    {
                        archivoXML = fXML.Name;
                        break;
                    }
                }
                #endregion

                if (archivoXML != "")
                {
                    #region Extraemos la información del XML
                    txtppAArchivo.Text = archivoXML;
                    string[] datosXML = new string[9];
                    datosXML = obtenDatosXML(strPathOrigen + archivoXML);
                    if (datosXML != null)
                    {
                        txtppAReceptor.Text = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                        txtppAEmisor.Text = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                        txtppAFolio.Text = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                        txtppASerie.Text = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                        txtppAFecha.Text = datosXML[6] == null ? string.Empty : datosXML[6].ToString();
                        txtppAImporte.Text = datosXML[7] == null ? string.Empty : datosXML[7].ToString();
                        btnppAAgregar.Enabled = true;
                    }
                    else
                    {
                        txtppAReceptor.Text = "";
                        txtppAEmisor.Text = "";
                        txtppAFolio.Text = "";
                        txtppASerie.Text = "";
                        txtppAFecha.Text = "";
                        txtppAImporte.Text = "";
                        btnppAAgregar.Enabled = false;
                        lblppAMensaje.Text = "Error en XML, no se creara Addenda de esta factura";
                    }

                    Session["dtAddenda"] = null;
                    DataTable dtAddenda = (DataTable)Session["dtAddenda"];
                    grvAddenda.DataSource = dtAddenda;
                    grvAddenda.DataBind();
                    txtppAPedido.Text = "";
                    txtppAEntrada.Text = "";
                    txtppAPosicion.Text = "";
                    btnppACrear.Visible = false;

                    //mppAddenda.Show();
                    #endregion
                }
                else
                {
                    Session["dtAddenda"] = null;
                    DataTable dtAddenda = (DataTable)Session["dtAddenda"];
                    grvAddenda.DataSource = dtAddenda;
                    grvAddenda.DataBind();
                    txtppAPedido.Text = "";
                    txtppAEntrada.Text = "";
                    txtppAPosicion.Text = "";
                    btnppACrear.Visible = false;

                    llenaGrid();

                    pnlAddenda.Visible = false;
                    pnlSinAddenda.Visible = true;
                }
            }
        }

        protected void btnppACrear_Click(object sender, EventArgs e)
        {
            lblppAMensaje.Text = "";
            lblppAMensaje.Visible = false;

            //Validamos las Sessiones
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                int idProveedor = -1;
                int idProveedorSAP = -1;
                int idUsuario = -1;
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                idProveedorSAP = Convert.ToInt32(Session["ID_Proveedor_SAP"].ToString());

                string strPathAddenda = "";
                strPathAddenda = Server.MapPath(@"./UpdateFiles/tmpAddenda/" + idProveedor.ToString() + "/" + idUsuario.ToString() + "/");
                #region Crea Addenda
                string cadenaAddenda = "";
                string cadenaFinal = "";
                try
                {
                    if (Session["dtAddenda"] != null)
                    {
                        cadenaAddenda = creaAddenda((DataTable)Session["dtAddenda"], idProveedorSAP.ToString(), txtppAArchivo.Text.Trim());

                        #region Sustituimos la cadena
                        StreamReader leeArchivo = new StreamReader(strPathAddenda + txtppAArchivo.Text.Trim());
                        string line = "";
                        line = leeArchivo.ReadToEnd();
                        leeArchivo.Close();
                        leeArchivo.Dispose();

                        int posicion = 0;
                        int total = 0;
                        total = line.Length;
                        posicion = line.IndexOf("</cfdi:Complemento>");
                        posicion = posicion + 19;
                        string inicio = "";
                        string fin = "";
                        inicio = line.Substring(0, posicion);
                        fin = line.Substring(posicion);
                        cadenaFinal = inicio + cadenaAddenda + fin;
                        #endregion

                        //Insetamos la Addenda en la base de datos
                        bool resp = false;
                        BLAdenda bladdenda = new BLAdenda();
                        resp = bladdenda.insertaAddenda(txtppAArchivo.Text.Trim(), idProveedor, idProveedorSAP.ToString(), cadenaAddenda);

                        Session["dtAddenda"] = null;
                    }
                }
                catch (Exception ex)
                {
                    lblppAMensaje.Text = ex.Message;
                    lblppAMensaje.Visible = true;
                }
                #endregion

                #region Mueve Archivos
                string nombreArchivo = "";
                nombreArchivo = txtppAArchivo.Text.Trim();
                try
                {
                    mueveArchivos(idProveedor, idUsuario, nombreArchivo, cadenaFinal);
                    eliminaArchivosAddenda(idProveedor, idUsuario, @"./UpdateFiles/tmpAddenda/", nombreArchivo);
                }
                catch (Exception ex)
                {
                    muestraError("", "", "", ex.Message);
                }
                

                #endregion

                #region Extraemos el siguiente archivo
                string strPathOrigen = "";
                int noArchivo = 0;
                string archivoXML = "";
                try
                {
                    strPathOrigen = Server.MapPath(@"./UpdateFiles/tmpAddenda/" +
                    idProveedor.ToString() + "/" +
                    idUsuario.ToString() + "/");

                    DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                    if (Directory.Exists(strPathOrigen))
                    {
                        FileInfo[] filesXML = null;
                        //int noXML = 0;
                        //Contamos el numero de Archivos XML Cargados
                        filesXML = rootDir.GetFiles("*.xml");
                        noArchivo = filesXML == null ? 0 : filesXML.Count();
                        foreach (FileInfo fXML in filesXML)
                        {
                            archivoXML = fXML.Name;
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    muestraError("", "", "", ex.Message);
                }
                #endregion


                if (archivoXML != "")
                {
                    #region Extraemos la información del XML
                    txtppAArchivo.Text = archivoXML;
                    string[] datosXML = new string[9];
                    datosXML = obtenDatosXML(strPathOrigen + archivoXML);
                    if (datosXML != null)
                    {
                        txtppAReceptor.Text = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                        txtppAEmisor.Text = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                        txtppAFolio.Text = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                        txtppASerie.Text = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                        txtppAFecha.Text = datosXML[6] == null ? string.Empty : datosXML[6].ToString();
                        txtppAImporte.Text = datosXML[7] == null ? string.Empty : datosXML[7].ToString();
                        btnppAAgregar.Enabled = true;
                    }
                    else
                    {
                        txtppAReceptor.Text = "";
                        txtppAEmisor.Text = "";
                        txtppAFolio.Text = "";
                        txtppASerie.Text = "";
                        txtppAFecha.Text = "";
                        txtppAImporte.Text = "";
                        btnppAAgregar.Enabled = false;
                        lblppAMensaje.Text = "Error en XML, no se creara Addenda de esta factura";
                    }
                    Session["dtAddenda"] = null;
                    DataTable dtAddenda = (DataTable)Session["dtAddenda"];
                    grvAddenda.DataSource = dtAddenda;
                    grvAddenda.DataBind();
                    txtppAPedido.Text = "";
                    txtppAEntrada.Text = "";
                    txtppAPosicion.Text = "";
                    btnppACrear.Visible = false;

                    //mppAddenda.Show();
                    #endregion
                }
                else
                {
                    Session["dtAddenda"] = null;
                    DataTable dtAddenda = (DataTable)Session["dtAddenda"];
                    grvAddenda.DataSource = dtAddenda;
                    grvAddenda.DataBind();
                    txtppAPedido.Text = "";
                    txtppAEntrada.Text = "";
                    txtppAPosicion.Text = "";
                    btnppACrear.Visible = false;

                    pnlAddenda.Visible = false;
                    pnlSinAddenda.Visible = true;

                    btnProcesar_Click(sender, e);
                }
            }
        }

        protected void btnppAAgregar_Click(object sender, EventArgs e)
        {
            //Validamos los datos de la Addenda
            lblppAMensaje.Visible = false;
            lblppAMensaje.Text = "";

            string validaEntrada = "";

            if (Session["dtAddenda"] == null)
            {
                DataTable dtAddenda = new DataTable();
                dtAddenda.Columns.Add(new DataColumn("NoRegistro", typeof(Int32)));
                dtAddenda.Columns.Add(new DataColumn("Pedido", typeof(String)));
                dtAddenda.Columns.Add(new DataColumn("Posicion", typeof(string)));
                dtAddenda.Columns.Add(new DataColumn("Entrada", typeof(string)));

                validaEntrada = validaDatosAddenda();
                if (validaEntrada == "")
                {
                    DataRow drNuevo = dtAddenda.NewRow();
                    drNuevo["NoRegistro"] = 1;
                    drNuevo["Pedido"] = txtppAPedido.Text.Trim();
                    drNuevo["Posicion"] = txtppAPosicion.Text.Trim();
                    drNuevo["Entrada"] = txtppAEntrada.Text.Trim();

                    dtAddenda.Rows.Add(drNuevo);

                    grvAddenda.DataSource = dtAddenda;
                    grvAddenda.DataBind();

                    txtppAPosicion.Text = "";
                    btnppACrear.Visible = true;
                    Session["dtAddenda"] = dtAddenda;
                }
                else
                    muestraMensajeppAddenda(validaEntrada);
            }
            else
            {
                DataTable dtAddenda = (DataTable)Session["dtAddenda"];
                int noReg = 0;
                noReg = dtAddenda.Rows.Count + 1;
                validaEntrada = validaDatosAddenda();
                if (validaEntrada == "")
                {
                    DataRow drNuevo = dtAddenda.NewRow();
                    drNuevo["NoRegistro"] = noReg;
                    drNuevo["Pedido"] = txtppAPedido.Text.Trim();
                    drNuevo["Posicion"] = txtppAPosicion.Text.Trim();
                    drNuevo["Entrada"] = txtppAEntrada.Text.Trim();

                    dtAddenda.Rows.Add(drNuevo);

                    grvAddenda.DataSource = dtAddenda;
                    grvAddenda.DataBind();

                    txtppAPosicion.Text = "";

                    Session["dtAddenda"] = dtAddenda;
                }
                else
                    muestraMensajeppAddenda(validaEntrada);
            }
        }

        protected void btnppAActualizar_Click(object sender, EventArgs e)
        {
            if (Session["dtAddenda"] != null)
            {
                DataTable dtCopia = new DataTable();
                dtCopia.Columns.Add(new DataColumn("NoRegistro", typeof(Int32)));
                dtCopia.Columns.Add(new DataColumn("Pedido", typeof(String)));
                dtCopia.Columns.Add(new DataColumn("Entrada", typeof(string)));
                dtCopia.Columns.Add(new DataColumn("Posicion", typeof(string)));

                DataTable dtAddenda = (DataTable)Session["dtAddenda"];
                int noRegistros = -1;
                noRegistros = dtAddenda.Rows.Count;

                for (int n = 0; n < noRegistros; n++)
                {
                    DataRow drAgrega = dtCopia.NewRow();
                    drAgrega["NoRegistro"] = n + 1;
                    drAgrega["Pedido"] = dtAddenda.Rows[n]["Pedido"];
                    drAgrega["Entrada"] = dtAddenda.Rows[n]["Entrada"];
                    drAgrega["Posicion"] = dtAddenda.Rows[n]["Posicion"];
                    dtCopia.Rows.Add(drAgrega);
                }

                Session["dtAddenda"] = dtCopia;
                dgvArchivos.DataSource = dtCopia;
                grvAddenda.DataBind();
                grvAddenda.Visible = true;
                btnppAAgregar.Visible = true;
                btnppACancelar.Visible = true;
                btnppACrear.Visible = true;
                btnppAActualizar.Visible = false;
                mppAddenda.Show();
            }
        }

        /// <summary>
        /// Metodo para validar archivos para crear Addenda
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_Path"></param>
        /// <returns></returns>
        private string buscaArchivosAddenda(int _IdProveedor, int _IdUsuario, string _Path)
        {
            string resp = "";
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(_Path +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            //Validamos los archivos a procesar
            DirectoryInfo rootDir = new DirectoryInfo(strPath);
            if (Directory.Exists(strPath))
            {
                FileInfo[] filesXML = null;
                FileInfo[] filesPDF = null;
                int noXML = 0;
                int noPDF = 0;
                filesXML = rootDir.GetFiles("*.xml");
                filesPDF = rootDir.GetFiles("*.pdf");

                if (filesXML != null && filesXML.Count() > 0)
                {
                    noXML = filesXML.Count();
                    if (filesPDF != null && filesPDF.Count() > 0)
                    {
                        noPDF = filesPDF.Count();
                        if (noXML == noPDF)
                        {
                            bool buscaPDF = true;
                            #region Buscamos archivos PDF
                            foreach (FileInfo fXML in filesXML)
                            {
                                bool hayPDF = false;
                                string nombreXML = "";
                                nombreXML = Path.GetFileNameWithoutExtension(fXML.Name);
                                foreach (FileInfo fPDF in filesPDF)
                                {
                                    string nombrePDF = "";
                                    nombrePDF = Path.GetFileNameWithoutExtension(fPDF.Name);
                                    if (nombreXML == nombrePDF)
                                    {
                                        hayPDF = true;
                                        break;
                                    }
                                }
                                if (!hayPDF)
                                {
                                    buscaPDF = false;
                                    break;
                                }
                            }
                            #endregion

                            if (!buscaPDF)
                                resp = "Los nombres de los archivos XML no son iguales a los PDF";
                        }
                        else
                            resp = "El numero de archivos XML debe ser igual a los PDF";

                    }
                    else
                        resp = "No hay archivos PDF Cargados";
                    
                }
                else
                    resp = "No hay archivos XML Cargados";

            }
            else
                resp = "Primero debes cargar los archivos dando Clic en Seleccionar y despues en Subir";

            return resp;
        }

        /// <summary>
        /// Metodo para eliminar archivos que no se les creara Addenda
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_Path"></param>
        private void eliminaArchivosAddenda(int _IdProveedor, int _IdUsuario, string _Path)
        {
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(_Path +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            try
            {
                //Validamos los archivos a procesar
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.*");

                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            fi.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("btnAddenda", "eliminaArchivosAddenda", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para extraer información del XML
        /// </summary>
        /// <param name="pathFile"></param>
        /// <returns></returns>
        private string[] obtenDatosXML(string pathFile)
        {
            string[] arrRespuesta = new string[8];
            try
            {
                //Documento XML
                XmlDocument xmlFactura = new XmlDocument();
                xmlFactura.Load(pathFile);
                //Nodos del documento
                XmlNode nodoFactura = xmlFactura.DocumentElement;
                //Atributos Root

                XmlAttribute rootAtributo = nodoFactura.Attributes["Version"];
                if (rootAtributo != null)
                    arrRespuesta[0] = rootAtributo.Value.ToString();

                rootAtributo = null;
                if (nodoFactura.Prefix == "cfdi")
                    arrRespuesta[1] = nodoFactura["cfdi:Receptor"].GetAttribute("Rfc");
                else
                    arrRespuesta[1] = nodoFactura["Receptor"].GetAttribute("Rfc");

                rootAtributo = null;
                if (nodoFactura.Prefix == "cfdi")
                    arrRespuesta[2] = nodoFactura["cfdi:Emisor"].GetAttribute("Rfc");
                else
                    arrRespuesta[2] = nodoFactura["Emisor"].GetAttribute("Rfc");

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["Folio"];
                if (rootAtributo != null)
                    arrRespuesta[3] = rootAtributo.Value.ToString();

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["Serie"];
                if (rootAtributo != null)
                    arrRespuesta[4] = rootAtributo.Value.ToString();

                XmlNodeList nodeList = xmlFactura.GetElementsByTagName("cfdi:Complemento");
                foreach (XmlElement nodo in nodeList)
                {
                    arrRespuesta[5] = nodo["tfd:TimbreFiscalDigital"].GetAttribute("UUID");
                }

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["Fecha"];
                if (rootAtributo != null)
                    arrRespuesta[6] = rootAtributo.Value.ToString();

                rootAtributo = null;
                rootAtributo = nodoFactura.Attributes["Total"];
                if (rootAtributo != null)
                    arrRespuesta[7] = rootAtributo.Value.ToString();

            }
            catch (Exception ex)
            {
                muestraError("", "", "Error", ex.Message);
            }

            return arrRespuesta;
        }

        /// <summary>
        /// Metodo para validar la captura de la Addenda
        /// </summary>
        /// <returns></returns>
        private string validaDatosAddenda()
        {
            string resp = "";
            long pedido = 0;
            long entrada = 0;
            int posicion = 0;

            pedido = txtppAPedido.Text.Trim() == "" ? 0 : Convert.ToInt64(txtppAPedido.Text.Trim());
            entrada = txtppAEntrada.Text.Trim() == "" ? 0 : Convert.ToInt64(txtppAEntrada.Text.Trim());
            posicion = txtppAPosicion.Text.Trim() == "" ? 0 : Convert.ToInt32(txtppAPosicion.Text.Trim());

            if (pedido != 0)
                if (pedido < 4500000000 || pedido > 4699999999)
                {
                    resp = "El numero de pedido no es valido";
                    return resp;
                }

            if (entrada != 0)
                if (entrada < 5000000000 || entrada > 5299999999)
                {
                    resp = "El numero de entrada no es valida";
                    return resp;
                }

            if (posicion != 0)
                if (posicion < 0 || posicion > 9990)
                {
                    resp = "El numero de posicion no es valida";
                    return resp;
                }

            if (pedido == 0 && entrada == 0)
            {
                resp = "Por lo menos debes capturar el Pedido o la Entrada";
                return resp;
            }

            return resp;
        }

        /// <summary>
        /// Metodo para mover archivos de Addenda
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_NombreXML"></param>
        private void mueveArchivos(int _IdProveedor, int _IdUsuario, string _NombreXML, string nuevaCadena)
        {
            string strPathOrigen = "";
            string strPathDestino = "";

            //Ruta origen
            strPathOrigen = Server.MapPath(@"./UpdateFiles/tmpAddenda/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            strPathDestino = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");
            try
            {
                DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                if (Directory.Exists(strPathOrigen))
                {
                    //Verificamos que existan los directorios, de lo contrario los creamos
                    if (!Directory.Exists(strPathDestino))
                        Directory.CreateDirectory(strPathDestino);

                    FileInfo[] files = null;
                    string nombre = "";
                    string nombreXML = "";
                    string nombrePDF = "";
                    nombre = Path.GetFileNameWithoutExtension(_NombreXML);
                    nombreXML = nombre + ".xml";
                    nombrePDF = nombre + ".pdf";

                    files = rootDir.GetFiles(nombre + ".*");

                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            if (fi.Name.ToUpper() == nombreXML.ToUpper())
                            {
                                if (File.Exists(strPathDestino + fi.Name))
                                    File.Delete(strPathDestino + fi.Name);

                                StreamWriter ficheroEscritura = new StreamWriter(strPathDestino + nombreXML);
                                ficheroEscritura.WriteLine(nuevaCadena);
                                ficheroEscritura.Close();
                                ficheroEscritura.Dispose();

                            }

                            if (fi.Name.ToUpper() == nombrePDF.ToUpper())
                            {
                                if (File.Exists(strPathDestino + fi.Name))
                                    File.Delete(strPathDestino + fi.Name);

                                File.Copy(strPathOrigen + fi.Name, strPathDestino + fi.Name);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraMensajeppAddenda(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para mostrar mensajes en el Pop Up
        /// </summary>
        /// <param name="_msj"></param>
        private void muestraMensajeppAddenda(string _msj)
        {
            lblppAMensaje.Text = _msj;
            lblppAMensaje.Visible = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datosAddenda"></param>
        /// <param name="_ID_Proveedor_SAP"></param>
        /// <param name="_archivo"></param>
        /// <returns></returns>
        public string creaAddenda(DataTable datosAddenda, string _ID_Proveedor_SAP, string _archivo)
        {
            string cadenaAddenda = "";
            DataTable dt = new DataTable();
            dt = datosAddenda;

            List<Addenda> newAddenda = new List<Addenda>();
            List<AddendaIAPartida> newPartida = new List<AddendaIAPartida>();
            AddendaIAPartida[] itemsIAPartida = new AddendaIAPartida[999];
            int noItemsIAPartida = 0;

            try
            {
                if (dt != null)
                {
                    #region Rutina para agregar partida
                    int noItemsDT = 0;
                    noItemsDT = dt.Rows.Count;
                    //Recorremos la tabla
                    for (int p = 0; p < noItemsDT; p++)
                    {
                        string Pedido = "";
                        string Posicion = "";
                        string[] Entrada = new string[999];

                        AddendaIAPartida csPartida = new AddendaIAPartida();
                        Pedido = dt.Rows[p]["Pedido"].ToString();
                        Posicion = dt.Rows[p]["Posicion"].ToString() == "" ? "" : dt.Rows[p]["Posicion"].ToString();

                        int noEntrada = 0;
                        Entrada[noEntrada] = dt.Rows[p]["Entrada"].ToString();

                        #region Rutina para agregar mas de una entrada
                        for (int e = 0; e < noItemsDT; e++)
                        {
                            string Pedido2 = "";
                            string Posicion2 = "";
                            string Entrada2 = "";

                            Pedido2 = dt.Rows[e]["Pedido"].ToString();
                            Posicion2 = dt.Rows[e]["Posicion"].ToString() == "" ? "" : dt.Rows[e]["Posicion"].ToString();
                            Entrada2 = dt.Rows[e]["Entrada"].ToString();

                            if (Pedido == Pedido2 && Posicion == Posicion2 && p != e)
                            {
                                bool existeEntrada = false;

                                //Recorremos el arreglo para ver si existe la entrada
                                for (int a = 0; a < Entrada.Length; a++)
                                {
                                    string aEntrada = "";
                                    aEntrada = Entrada[a];
                                    if (aEntrada == Entrada2)
                                    {
                                        existeEntrada = true;
                                        break;
                                    }
                                }

                                //Si no existe la entrada la guardamos en el Arreglo
                                if (!existeEntrada)
                                {
                                    noEntrada = noEntrada + 1;
                                    Entrada[noEntrada] = Entrada2;
                                }
                            }
                        }
                        #endregion

                        #region Agrega Partida
                        bool existePartida = false;
                        int noPartida = 0;
                        noPartida = newPartida.Count;
                        //Recorremos la lista de Partidas para determinar que no exista
                        for (int partida = 0; partida < noPartida; partida++)
                        {
                            string listPedido = "";
                            string listPosicion = "";
                            listPedido = newPartida[partida].Pedido;
                            listPosicion = newPartida[partida].Posicion;
                            if (Pedido == listPedido && Posicion == listPosicion)
                            {
                                existePartida = true;
                                break;
                            }
                        }

                        if (!existePartida)
                        {
                            csPartida.Pedido = Pedido;
                            csPartida.Posicion = Posicion;
                            csPartida.Entrada = Entrada;
                            newPartida.Add(csPartida);
                            itemsIAPartida[noItemsIAPartida] = newPartida[noItemsIAPartida];
                            noItemsIAPartida = noItemsIAPartida + 1;

                        }
                        #endregion

                    }
                    #endregion

                    #region Creamos la Addenda
                    List<AddendaIA> newAddendaIA = new List<AddendaIA>();
                    AddendaIA csAddendaIA = new AddendaIA();
                    csAddendaIA.Proveedor = _ID_Proveedor_SAP;
                    csAddendaIA.Partida = itemsIAPartida;
                    newAddendaIA.Add(csAddendaIA);

                    Addenda csAddenda = new Addenda();
                    csAddenda.IA = newAddendaIA[0];
                    newAddenda.Add(csAddenda);
                    #endregion

                    cadenaAddenda = serializaAddenda(newAddenda, _ID_Proveedor_SAP, _archivo);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cadenaAddenda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addenda"></param>
        /// <param name="_IDProveedorSAP"></param>
        /// <param name="nombreArchivo"></param>
        /// <returns></returns>
        private string serializaAddenda(List<Addenda> addenda, string _IDProveedorSAP, string nombreArchivo)
        {
            string resp = "";
            string strPath = "";
            strPath = Server.MapPath(@"./UpdateFiles/tmpAddenda/" + _IDProveedorSAP.ToString() + "/");
            DirectoryInfo rootDir = new DirectoryInfo(strPath);

            try
            {
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);

                var serializer = new XmlSerializer(addenda.GetType());
                using (var escribe = XmlWriter.Create(strPath + nombreArchivo))
                {
                    serializer.Serialize(escribe, addenda);
                }

                XmlDocument xml = new XmlDocument();
                xml.Load(strPath + nombreArchivo);
                string xmlString = xml.OuterXml.ToString();

                //StreamReader xml = new StreamReader(strPath + nombreArchivo);
                //string xmlString = "";
                //xmlString = xml.ReadToEnd();
                //xml.Close();
                //xml.Dispose();

                using (var stringWriter = new StringWriter())
                {
                    using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                    {
                        xml.WriteTo(xmlTextWriter);
                        xmlTextWriter.Flush();
                        xmlString = stringWriter.GetStringBuilder().ToString();
                    }
                }

                #region Comentar en Desarrollo
                xmlString = xmlString.Replace(@"<?xml version=""1.0"" encoding=""utf-8""?><ArrayOfAddenda xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""><Addenda><IA xmlns=""http://www.aceitera.com.mx/addenda.xsd"">",
                @"<cfdi:Addenda xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:noNamespaceSchemaLocation=""http://www.aceitera.com.mx/addenda.xsd""><IA>");
                xmlString = xmlString.Replace(@"</Addenda></ArrayOfAddenda>",
                    "</cfdi:Addenda>");
                #endregion
                #region Comentar en Productivo
                //xmlString = xmlString.Replace(@"<?xml version=""1.0"" encoding=""utf-8""?><ArrayOfAddenda xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><Addenda><IA xmlns=""http://www.aceitera.com.mx/addenda.xsd"">",
                //    @"<cfdi:Addenda xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:noNamespaceSchemaLocation=""http://www.aceitera.com.mx/addenda.xsd""><IA>");
                //xmlString = xmlString.Replace(@"</Addenda></ArrayOfAddenda>", @"</cfdi:Addenda>");
                #endregion

                //StreamWriter ficheroEscritura = new StreamWriter(strPath + "xmlString.xml");
                //ficheroEscritura.WriteLine(xmlString);
                //ficheroEscritura.Close();
                //ficheroEscritura.Dispose();

                

                resp = xmlString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_Path"></param>
        /// <param name="_archivo"></param>
        private void eliminaArchivosAddenda(int _IdProveedor, int _IdUsuario, string _Path, string _archivo)
        {
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(_Path +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            try
            {
                //Validamos los archivos a procesar
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.*");
                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            string nombre = "";
                            string nombreXML = "";
                            string nombrePDF = "";
                            nombre = Path.GetFileNameWithoutExtension(fi.Name);
                            nombreXML = nombre + ".xml";
                            nombrePDF = nombre + ".pdf";
                            if (nombreXML.ToUpper() == _archivo.ToUpper())
                            {
                                File.Delete(strPath + nombreXML);
                                File.Delete(strPath + nombrePDF);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("btnAddenda", "eliminaArchivosAddenda", "Error", ex.Message);
            }
        }

        #endregion

        #region Eventos y metodos de Procesar
        /// <summary>
        /// Metodo para validar que exista el PDF para cada XM
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_nombrepdf"></param>
        /// <returns></returns>
        private bool existePDF(int _IdProveedor, int _IdUsuario, string _nombrepdf)
        {
            bool resp = false;
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            try
            {
                //Validamos los archivos a procesar
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles(_nombrepdf);

                    if (files != null && files.Count() > 0)
                    {
                        resp = true;
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("btnProcesar", "existePDF", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para buscar al proveedor
        /// </summary>
        /// <param name="_xmlEmisor"></param>
        /// <returns></returns>
        private int buscaProveedor(string _xmlEmisor)
        {
            int idProveedor = -1;
            BLProveedores buscaProveedor = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = buscaProveedor.buscaProveedor(_xmlEmisor);
                if (csProveedores.Count > 0)
                    idProveedor = csProveedores[0].ID_Proveedor;
            }
            catch (Exception ex)
            {
                muestraError("buscaProveedor", "buscaProveedor", "Error", ex.Message);
            }
            return idProveedor;
        }

        /// <summary>
        /// Metodo para bscar sociedad
        /// </summary>
        /// <param name="_xmlRFCReceptor"></param>
        /// <returns></returns>
        private List<Sociedades> buscaSociedad(string _xmlRFCReceptor)
        {
            List<Sociedades> csSociedades = null;
            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                csSociedades = buscaSociedad.buscaSociedad(_xmlRFCReceptor);
            }
            catch (Exception ex)
            {
                muestraError("buscaSociedad", "buscaSociedad", "Error", ex.Message);
            }
            return csSociedades;
        }

        /// <summary>
        /// Metodo para buscar Factura procesada
        /// </summary>
        /// <param name="_hoy"></param>
        /// <param name="_uuid"></param>
        /// <returns></returns>
        private DateTime buscaArchivo(DateTime _hoy, string _uuid)
        {
            DateTime respFecha = new DateTime();
            int noREg = -1;
            respFecha = _hoy;
            BLArchivos1 busca = new BLArchivos1();
            try
            {
                List<Archivos1> csArchivos = busca.buscaArchivoUUID(_uuid);
                noREg = csArchivos.Count;

                for(int n = 0; n<noREg; n++)
                {
                    int _idEstatus = csArchivos[n].ID_Estatus;
                    if (_idEstatus != 1)
                    {
                        respFecha = csArchivos[n].Fecha_Recepcion;
                    }
                }
                //busca.buscaArchivoxuuid(_uuid);
                //noREg = csArchivos.Count;
                //for (int n = 0; n < noREg; n++)
                //{
                //    int idEstatus = -1;
                //    idEstatus = csArchivos[n].ID_Estatus;
                //    if (idEstatus != 1)
                //    {
                //        respFecha = csArchivos[n].Fecha_Recepcion;
                //    }
                //}
            }
            catch (Exception ex)
            {
                muestraError("buscaArchivo", "buscaArchivo", "Error", ex.Message);
            }

            return respFecha;
        }

        /// <summary>
        /// Metodo para enviar al PAC el XML
        /// </summary>
        /// <param name="_usuarioPac"></param>
        /// <param name="_passPac"></param>
        /// <param name="_pathFile"></param>
        /// <returns></returns>
        private string enviaPac(string _usuarioPac, string _passPac, string _pathFile)
        {
            string resp = string.Empty;
            string cadenaBase64 = string.Empty;

            ValidaCFDI.CFDIStatus validaXML = new ValidaCFDI.CFDIStatus();
            try
            {
                cadenaBase64 = convierteBase64(_pathFile);
                resp = validaXML.consultacfdi(_usuarioPac, _passPac, cadenaBase64);
            }
            catch (Exception ex)
            {
                muestraError("btnProcesar", "enviaPac", "Error", ex.Message);
            }

            ////Simulamos la respuesta
            //System.Threading.Thread.Sleep(3000);
            //resp = "Ok";

            return resp;
        }

        /// <summary>
        /// Metodo para convertir a base 64
        /// </summary>
        /// <param name="_rutaarchivo"></param>
        /// <returns></returns>
        private string convierteBase64(string _rutaarchivo)
        {
            string base64String = string.Empty;

            System.IO.FileStream inFile;
            byte[] binaryData;
            try
            {
                inFile = new System.IO.FileStream(_rutaarchivo, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                binaryData = new byte[inFile.Length];
                long bytesRead = inFile.Read(binaryData, 0, (int)inFile.Length);
                inFile.Close();

                base64String = Convert.ToBase64String(binaryData, 0, binaryData.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return base64String;
        }

        /// <summary>
        /// Metodo que envía correo cuando el PAC no envía respuesta
        /// </summary>
        private void enviaCorreoerrorenPAC()
        {
            string resp = "";
            EnviaCorreo envia = new EnviaCorreo();
            try
            {
                string asunto = string.Empty;
                string cuerpo = string.Empty;
                string receptor = string.Empty;
                string strPath = string.Empty;

                asunto = "Problemas para conectarse con el PAC";
                cuerpo = "<body><p style='font-family: Sans-Serif; font-size: 18px; color: #008FC4;'><strong>INDUSTRIAL ACEITERA - Problemas para conectarse cn el PAC</strong></p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Se ha detectado que no se ha podido establecer comunicación con el PAC</p>";
                cuerpo += "<table style='font-family: Sans-Serif; font-size: 12px; color: #000000; margin-left: 50px'>";
                cuerpo += "<tr><td style='text-align: right;'>Fecha :</td><td style='text-align: left;'>" + DateTime.Today.ToShortDateString() + "</td></tr>";
                cuerpo += "<tr><td style='text-align: right;'>Hora :</td><td style='text-align: left;'>" + DateTime.Now.ToShortTimeString() + "</td></tr></table>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Para acceder, debes ingresar en <a href='http://aceitera.com.mx'>www.aceitera.com.mx</a> y dar clic en PORTAL DE PROVEEDORES situado en la partes superior derecha debajo del Menú</p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Nota: Se adjunta Manual de operación y de manera adicional podras encontrarlo dentro del Portal en el pie de página del Menú Inicio</p>";
                cuerpo += "<br /><p style='font-family: Sans-Serif; font-size: 14px; color: #008FC4;'><strong>Atentamente</strong></p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #008FC4;'><strong>CUENTAS POR PAGAR - INDUSTRIAL ACEITERA</strong></p></body>";

                receptor = ConfigurationManager.AppSettings["mailReceptorError"].ToString();
                //receptor = _receptor;
                strPath = Server.MapPath(@"./Doctos/");
                resp = envia.enviaCorreo(receptor, asunto, cuerpo);

            }
            catch (Exception ex)
            {
                guardaLog("Principal", "enviaCorreo", "Error", ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_anio"></param>
        /// <param name="_mes"></param>
        /// <param name="_ArchivoXML"></param>
        /// <param name="_ArchivoPDF"></param>
        /// <returns></returns>
        private bool mueveArchivos(int _IdProveedor, int _IdUsuario, int _anio, int _mes, string _ArchivoXML, string _ArchivoPDF)
        {
            bool resp = false;
            string strPathOrigen = "";
            string strPathDestino = "";

            //Ruta origen
            strPathOrigen = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            //Ruta destino Boveda
            //VAlidamos que la ruta no sea FTP
            try
            {
                bool esFtp = false;
                esFtp = Convert.ToBoolean(ConfigurationManager.AppSettings["esftp"].ToString());
                if (esFtp)
                {
                    #region CArgar por FTP
                    string ftpUsuario = string.Empty;
                    string ftpPass = string.Empty;
                    ftpUsuario = ConfigurationManager.AppSettings["usuarioFtp"].ToString();
                    ftpPass = ConfigurationManager.AppSettings["passwordFtp"].ToString();

                    //Validamos que sea FTP
                    //Uri uriFTP = new Uri(strPathDestino);
                    //if (uriFTP.Scheme != Uri.UriSchemeFtp)
                    //    return false;

                    //VAlidamos que los archivos existan
                    DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                    if (Directory.Exists(strPathOrigen))
                    {
                        FileInfo[] files = null;
                        string nombre = "";
                        nombre = Path.GetFileNameWithoutExtension(_ArchivoXML);
                        files = rootDir.GetFiles(nombre + ".*");

                        if (files != null)
                        {
                            foreach (FileInfo fi in files)
                            {

                                strPathDestino = string.Empty;
                                //Solo la ruta destio del  FTP
                                strPathDestino = ConfigurationManager.AppSettings["ftpXProcesar"].ToString();
                                //Ruta del FTM mas el archivo
                                strPathDestino = strPathDestino + fi.ToString();

                                //Creamos el FTM
                                FtpWebRequest ftpUpload = null;
                                StreamReader archivoOrigen = null;
                                byte[] byteOrigen = null;

                                ftpUpload = (FtpWebRequest)WebRequest.Create(strPathDestino);
                                ftpUpload.Credentials = new NetworkCredential(ftpUsuario, ftpPass);
                                ftpUpload.Method = WebRequestMethods.Ftp.UploadFile;

                                //Leemos el origen
                                archivoOrigen = new StreamReader(strPathOrigen + fi.ToString());
                                byteOrigen = System.Text.Encoding.UTF8.GetBytes(archivoOrigen.ReadToEnd());
                                archivoOrigen.Close();
                                ftpUpload.ContentLength = byteOrigen.Length;

                                //Escribimos en el Destino
                                Stream archivoDestino = null;
                                archivoDestino = ftpUpload.GetRequestStream();
                                archivoDestino.Write(byteOrigen, 0, byteOrigen.Length);
                                archivoDestino.Close();

                                resp = true;
                            }
                        }

                    }
                    #endregion
                }
                else
                {
                    #region Cargar Local
                    strPathDestino = ConfigurationManager.AppSettings["rutaXProcesar"].ToString();
                    //Validamos los archivos a procesar
                    DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                    if (Directory.Exists(strPathOrigen))
                    {
                        //Verificamos que existan los directorios, de lo contrario los creamos
                        if (!Directory.Exists(strPathDestino))
                            Directory.CreateDirectory(strPathDestino);

                        FileInfo[] files = null;
                        string nombre = "";
                        nombre = Path.GetFileNameWithoutExtension(_ArchivoXML);
                        files = rootDir.GetFiles(nombre + ".*");

                        if (files != null)
                        {
                            foreach (FileInfo fi in files)
                            {
                                if (fi.Name == _ArchivoXML)
                                {
                                    if (File.Exists(strPathDestino + _ArchivoXML))
                                        File.Delete(strPathDestino + _ArchivoXML);
                                    File.Copy(strPathOrigen + _ArchivoXML, strPathDestino + _ArchivoXML);
                                }

                                if (fi.Name == _ArchivoPDF)
                                {
                                    if (File.Exists(strPathDestino + _ArchivoPDF))
                                        File.Delete(strPathDestino + _ArchivoPDF);

                                    File.Copy(strPathOrigen + _ArchivoPDF, strPathDestino + _ArchivoPDF);
                                }
                                resp = true;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                muestraError("btnProcesar", "mueveArchivos", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para guardar la recepción del archivo
        /// </summary>
        /// <param name="_Nombrexml"></param>
        /// <param name="_Nombrepdf"></param>
        /// <param name="_pathXML"></param>
        /// <param name="_pathPDF"></param>
        /// <param name="_Idpproveedor"></param>
        /// <param name="_Idsociedad"></param>
        /// <param name="_folio"></param>
        /// <param name="_serie"></param>
        /// <param name="_uuid"></param>
        /// <param name="_fechafactura"></param>
        /// <param name="_importe"></param>
        /// <param name="_Idestatus"></param>
        /// <param name="_obsevaciones"></param>
        /// <param name="_versionxml"></param>
        /// <returns></returns>
        private int guardaRecepcion(string _Nombrexml, string _Nombrepdf, string _pathXML, string _pathPDF, int _Idpproveedor,
            int _Idsociedad, string _folio, string _serie, string _uuid, DateTime _fechafactura,
            decimal _importe, int _Idestatus, string _obsevaciones, string _versionxml)
        {
            int intResp = -1;

            //Primero convertimos en Bits los arhivos para almacenarlos en la base de datos
            byte[] archivoXML = null;
            byte[] archivoPDF = null;

            archivoXML = combierteArchivo(_pathXML);
            archivoPDF = combierteArchivo(_pathPDF);

            BLArchivos1 guarda = new BLArchivos1();
            try
            {
                intResp = guarda.insertaArchivo(_Nombrexml, _Nombrepdf, archivoXML, archivoPDF, _Idpproveedor,
                    _Idsociedad, _folio, _serie, _uuid, _fechafactura,
                    _importe, _Idestatus, _obsevaciones, _versionxml);
            }
            catch (Exception ex)
            {
                muestraError("guardaRecepcion", "guardaRecepcion", "Error", ex.Message);
            }
            return intResp;
        }

        /// <summary>
        /// Metodo para convertir archivo para almacenar.
        /// </summary>
        /// <param name="_strRuta"></param>
        /// <returns></returns>
        private byte[] combierteArchivo(string _strRuta)
        {
            byte[] resp = null;
            try
            {
                FileStream fs = new FileStream(_strRuta, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                resp = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                muestraError("guardaRecepcion", "combierteArchivo", "Error", ex.Message);
            }

            return resp;
        }
        #endregion

    }
    public class RespuestaSAT
    {
        public string UUID { get; set; }
        public string CodigoEstatus { get; set; }
        public string EsCancelable { get; set; }
        public string Estado { get; set; }
    }
}