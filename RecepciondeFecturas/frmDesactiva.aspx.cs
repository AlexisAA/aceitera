﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text.RegularExpressions;
using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Catalogos;
using BLRecepcion.Varios;
using System.Configuration;

namespace RecepciondeFecturas
{
    public partial class frmDesactiva : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Idioma"] == null)
                {
                    Session["Idioma"] = "es-MX";
                }
                
                txtCorreo.Text = string.Empty;
                txtUsuario.Text = string.Empty;
                txtUsuario.Focus();
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["Idioma"] == null)
            {
                UICulture = "ES";
            }
            else
            {
                UICulture = Session["Idioma"].ToString();
            }

            base.InitializeCulture();
        }

        protected void btnDesactivar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            string correo = string.Empty;
            string userName = string.Empty;
            string strFormat = string.Empty;
            strFormat = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            userName = txtUsuario.Text.Trim();
            if (userName.Length >= 4)
            {
                correo = txtCorreo.Text.Trim();
                if (Regex.IsMatch(correo, strFormat))
                {
                    //Buscamos al usuario
                    BLUsuarios buscaUsuario = new BLUsuarios();
                    try
                    {
                        List<Usuarios> csUsuario = buscaUsuario.buscaUsuario(userName);
                        if (csUsuario.Count > 0)
                        {
                            int Id_Proveedor = -1;
                            int Id_Usuario = -1;
                            bool resp = false;
                            Id_Usuario = csUsuario[0].Id_Usuario;
                            Id_Proveedor = csUsuario[0].Id_Proveedor;

                            resp = buscaContacto(Id_Proveedor, correo);
                            if (resp)
                            {
                                resp = desactivaUsuario(Id_Usuario);
                                if (resp)
                                    muestraError("recuperaPass", "recuperaPass", "Error", (string)GetLocalResourceObject("Usuario_DesActivado"));
                                else
                                    muestraError("recuperaPass", "recuperaPass", "Error", (string)GetLocalResourceObject("Usuario_NodesActivado"));
                            }
                            else
                                muestraMensaje((string)GetLocalResourceObject("Correo_Noencontrado"));
                        }
                        else
                            muestraMensaje((string)GetLocalResourceObject("Error_noUsername"));
                    }
                    catch (Exception ex)
                    {
                        muestraError("btnDesactivar", "btnDesactivar", "Error", ex.Message);
                    }
                }
                else
                    muestraMensaje((string)GetLocalResourceObject("Error_Correo"));
            }
            else
                muestraMensaje((string)GetLocalResourceObject("Error_Username"));
        }

        /// <summary>
        /// Metodo para buscar Contacto
        /// </summary>
        /// <param name="_correo"></param>
        /// <returns></returns>
        private bool buscaContacto(int _idproveedor, string _correo)
        {
            bool resp = false;
            BLContacto busca = new BLContacto();
            try
            {
                List<Contacto> csContacto = busca.buscaContacto(_idproveedor, _correo);
                if (csContacto.Count > 0)
                {
                    resp = true;
                }
            }
            catch (Exception ex)
            {
                muestraError("btnDesactivar", "buscaContacto", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para desbloquear usuario
        /// </summary>
        /// <param name="_idusuario"></param>
        /// <returns></returns>
        private bool desactivaUsuario(int _idusuario)
        {
            bool resp = false;
            BLUsuarios busca = new BLUsuarios();
            try
            {
                resp = busca.desactivaUsuario(_idusuario);
            }
            catch (Exception ex)
            {
                muestraError("btnDesactivar", "desactivaUsuario", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("DesactivaUsuario", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("DesactivaUsuario", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

    }
}