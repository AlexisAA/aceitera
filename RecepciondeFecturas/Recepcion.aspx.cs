﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using DALRecepcion.Bean.Portal;
using BLRecepcion.Portal;
using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Catalogos;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Net;
using BLRecepcion.Varios;

namespace RecepciondeFecturas
{
    public partial class Recepcion : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                    //Mostramos que hay archivos pendientes por procesar
                    if (buscaArchivos(idProveedor, idUsuario))
                    {
                        lblporProcesar.Visible = true;
                        btnMostrar.Visible = true;
                    }

                    //Inicializamos las fechas
                    CargarFechas();
                    llenaddlSocieddes();
                    llenaddlEstatus();
                    limpiaTextos();

                    llenaGrid();

                    dgvRecepcionArchivos.Columns[7].Visible = false;

                    if (Session["Errores"] != null)
                    {
                        lblMensaje.Visible = true;
                        lblMensaje.Text = Session["Errores"].ToString();
                        Session["Errores"] = null;
                    }
                }
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["Idioma"] == null)
                UICulture = "es-MX";
            else
                UICulture = Session["Idioma"].ToString();

            base.InitializeCulture();
        }

        protected void dgvRecepcionArchivos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Image imgEstatus = (Image)e.Row.FindControl("estatus");
                ImageButton btnPdf = (ImageButton)e.Row.FindControl("imgPdf");
                ImageButton btnXml = (ImageButton)e.Row.FindControl("imgXml");
                int idEstatus = -1;
                string idRecepcion = "";
                idEstatus = Convert.ToInt32(e.Row.Cells[7].Text);
                idRecepcion = e.Row.Cells[0].Text;

                btnPdf.CommandArgument = idRecepcion;
                btnXml.CommandArgument = idRecepcion;
                if (idEstatus > 0)
                {

                    BLEstatus buscaEstatus = new BLEstatus();
                    try
                    {
                        List<Estatus> csEstatus = buscaEstatus.buscaEstatus(idEstatus);
                        if (csEstatus.Count > 0)
                        {
                            imgEstatus.ImageUrl = csEstatus[0].Ruta.ToString();
                            imgEstatus.ToolTip = csEstatus[0].Descripcion.ToString();
                            //btnEstatus.CommandArgument = csEstatus[0].ID_Estatus.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        muestraError("dgvRecepcionArchivos", "buscaEstatus", "Error", ex.Message);
                    }

                }
            }
        }

        protected void dgvRecepcionArchivos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int IdRecepcion = -1;
            string Tipo = string.Empty;
            switch (e.CommandName)
            {
                case "cmdPDF":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/pdf";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
                case "cmdXML":
                    IdRecepcion = Convert.ToInt32(e.CommandArgument.ToString());
                    Tipo = "application/xml";
                    mustrarArchivo(IdRecepcion, Tipo);
                    break;
            }
        }

        protected void dgvRecepcionArchivos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvRecepcionArchivos.PageIndex = e.NewPageIndex;

            cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
            cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);

            dgvRecepcionArchivos.Columns[7].Visible = true;
            llenaGrid();
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }

        protected void afuXML_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        {
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                int idProveedor = -1;
                int idUsuario = -1;
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                string strPath = "";
                string nombreArchivo = "";

                //Ruta en la que se cargaran los archivos
                strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                    idProveedor.ToString() + "/" +
                    idUsuario.ToString() + "/");

                try
                {
                    //Si no existe el directorio lo creamos
                    if (!Directory.Exists(strPath))
                        Directory.CreateDirectory(strPath);

                    //Obtenemos el nombre del archivo
                    nombreArchivo = Path.GetFileName(e.FileName);

                    //Verificamos que no existan los archivos, de ser asi los eliminamos para reemplazarlos
                    if (File.Exists(strPath + nombreArchivo))
                        File.Delete(strPath + nombreArchivo);

                    afuXML.SaveAs(strPath + nombreArchivo);
                }
                catch (Exception ex)
                {
                    muestraError("afuXML_UploadComplete", "Upload", "Error", ex.Message);
                }

            }
        }

        protected void btnProcesar_Click(object sender, EventArgs e)
        {
            //Sesion para mosrar los mensajes de error
            Session["Errores"] = null;


            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            //Validamos que halla archivos en el directorio
            int idProveedor = -1;
            int idUsuario = -1;
            idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            if(buscaArchivos(idProveedor, idUsuario))
            {
                //Validamos que para cada XML corresponda un PDF con el mismo nombre
                string strPath = "";

                //Ruta en la que se cargaran los archivos
                strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                    idProveedor.ToString() + "/" +
                    idUsuario.ToString() + "/");

                //Validamos los archivos a procesar
                if (Directory.Exists(strPath))
                {
                    DirectoryInfo rootDir = new DirectoryInfo(strPath);
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.xml");

                    #region Cuenta archivos en directorio
                    if (files != null && files.Count() > 0)
                    {
                        foreach (FileInfo fi in files)
                        {
                            string nombrexml = "";
                            string nombre = "";
                            string nombrepdf = "";

                            //Variable para manejar los eventos
                            string msjError = string.Empty;
                            //string msjErrorSistema = string.Empty;
                            string accion = "";
                            string msj = "Error";

                            #region Variables para el archivo XML
                            string[] datosXML = new string[8];
                            string xmlVersion = string.Empty;
                            string xmlrfcReceptor = string.Empty;
                            string xmlrfcEmisor = string.Empty;
                            string xmlFolio = string.Empty;
                            string xmlSerie = string.Empty;
                            string xmlUuid = string.Empty;
                            DateTime xmlFfactura = new DateTime();
                            decimal xmlImporte = 0;
                            #endregion

                            #region Variables de la recepción del archivo
                            string respPac = string.Empty;
                            int idEstatus = 1;
                            int idRecepcion = -1;
                            #endregion

                            #region Variables de busqueda
                            int idSociedad = 1;
                            string usuarioPac = string.Empty;
                            string passPac = string.Empty;
                            #endregion

                            nombrexml = fi.Name;
                            nombre = Path.GetFileNameWithoutExtension(nombrexml);
                            nombrepdf = nombre + ".pdf";

                            #region Existe PDF?
                            if (existePDF(idProveedor, idUsuario, nombrepdf))
                            {

                                #region Variables para mover archivos
                                int intAnio = -1;
                                int intMes = -1;
                                bool respuesta = false;
                                #endregion

                                accion = "Lee archivo";
                                #region Extraemos información del XML
                                datosXML = obtenDatosXML(strPath + "/" + nombrexml);
                                if (datosXML != null)
                                {
                                    xmlVersion = datosXML[0] == null ? string.Empty : datosXML[0].ToString();
                                    xmlrfcReceptor = datosXML[1] == null ? string.Empty : datosXML[1].ToString();
                                    xmlrfcEmisor = datosXML[2] == null ? string.Empty : datosXML[2].ToString();
                                    xmlFolio = datosXML[3] == null ? string.Empty : datosXML[3].ToString();
                                    xmlSerie = datosXML[4] == null ? string.Empty : datosXML[4].ToString();
                                    xmlUuid = datosXML[5] == null ? string.Empty : datosXML[5].ToString();
                                    xmlFfactura = datosXML[6] == null ? DateTime.Now : Convert.ToDateTime(datosXML[6].ToString());
                                    xmlImporte = datosXML[7] == null ? 0 : Convert.ToDecimal(datosXML[7].ToString());
                                }
                                else
                                {
                                    msjError = "Error en XML";
                                }
                                #endregion

                                accion = "Busca Proveedor";
                                #region Validaciones del portal
                                if (msjError == string.Empty)
                                {
                                    if (idProveedor == buscaProveedor(xmlrfcEmisor))
                                    {
                                        accion = "Busca Sociedad";
                                        List<Sociedades> csSociedad = buscaSociedad(xmlrfcReceptor);
                                        if (csSociedad.Count > 0)
                                        {
                                            //Hacemos la validación del PAC
                                            idSociedad = Convert.ToInt32(csSociedad[0].Id_Sociedad.ToString());
                                            usuarioPac = csSociedad[0].Usuario_ME;
                                            passPac = csSociedad[0].Contrasenia_ME;
                                        }
                                        else
                                            msjError = "El RFC del Receptor de la factura no existe.";
                                    }
                                    else
                                        msjError = "El RFC del proveedor no corresponde al RFC de la factura.";
                                }
                                #endregion

                                accion = "Busca Archivo en base d datos";
                                #region Busca Archivo en TBL_ARchivos_Recibidos
                                if (msjError == string.Empty)
                                {
                                    DateTime fechaRecepcion = new DateTime();
                                    DateTime fechaHoy = new DateTime();
                                    fechaHoy = DateTime.Now;
                                    fechaRecepcion = buscaArchivo(fechaHoy, xmlUuid);
                                    if (fechaHoy != fechaRecepcion)
                                    {
                                        msjError = "La factura ya fue procesada el día " + fechaRecepcion.ToString();
                                    }
                                }
                                #endregion

                                accion = "Envio de XML al PAC";
                                #region Envío de archivo al PAC (Validación fiscal)
                                if (msjError == string.Empty)
                                {
                                    respPac = enviaPac(usuarioPac, passPac, strPath + nombrexml);
                                    //respPac = "ok";
                                    if (respPac != string.Empty)
                                    {
                                        #region Comnetamos el siguiente codigo para no enviar al PAC
                                        XmlDocument xmlRespuesta = new XmlDocument();
                                        xmlRespuesta.LoadXml(respPac);
                                        XmlNodeList nodoRespuesta = xmlRespuesta.GetElementsByTagName("Respuesta");
                                        //XmlElement nodoDatos = new XmlElement();
                                        foreach (XmlElement nodoDatos in nodoRespuesta)
                                        {
                                            string Fecha = string.Empty;
                                            string Codigo = string.Empty;
                                            string Descripcion = string.Empty;

                                            XmlNodeList nFecha = nodoDatos.GetElementsByTagName("fecha");
                                            XmlNodeList nCodigo = nodoDatos.GetElementsByTagName("codigo");
                                            XmlNodeList nDescripcion = nodoDatos.GetElementsByTagName("descripcion");

                                            Fecha = nFecha[0].InnerText.ToString() == "" ? null : nFecha[0].InnerText.ToString();
                                            Codigo = nCodigo[0].InnerText.ToString() == "" ? null : nCodigo[0].InnerText.ToString();
                                            Descripcion = nDescripcion[0].InnerText.ToString() == "" ? null : nDescripcion[0].InnerText.ToString();

                                            if (Codigo.Substring(0, 1) == "S")
                                            {
                                                idEstatus = 5;
                                                msjError = string.Empty;
                                            }
                                            else
                                            {
                                                idEstatus = 2;
                                                msjError = Codigo + " " + Descripcion;
                                            }
                                        }
                                        #endregion

                                        #region Emula la respuesta del PAC
                                        //idEstatus = 5;
                                        //msjError = string.Empty;
                                        #endregion

                                    }
                                    else
                                    {
                                        idEstatus = 1;
                                        msjError = "El PAC no envíó respuesta, intentelo nuevamente";

                                        //TODO: Cambio solicitado por Israel Palacios solicitando envío de correo cuando el PAC no envíe respuesta
                                        enviaCorreoerrorenPAC();

                                    }
                                        
                                }
                                #endregion

                                accion = "Mueve archivo";
                                #region Mueve archivos XML
                                if (msjError == string.Empty)
                                {
                                    intAnio = xmlFfactura.Year;
                                    intMes = xmlFfactura.Month;
                                    respuesta = mueveArchivos(idProveedor, idUsuario, intAnio, intMes, nombrexml, nombrepdf);
                                    if (respuesta)
                                    {
                                        msjError = "Archivo válido fiscalmente y Enviado a SAP para su revisión";
                                    }
                                    else
                                    {
                                        msjError = "Archivo valido fiscalmente pero no puede ser envíado a SAP. contacte al Administrador";
                                    }
                                }
                                #endregion

                                #region Guardamos la recepción
                                accion = "Guara archivo";
                                idRecepcion = guardaRecepcion(nombrexml, nombrepdf, strPath + nombrexml, strPath + nombrepdf, idProveedor,
                                        idSociedad, xmlFolio, xmlSerie, xmlUuid, xmlFfactura,
                                        xmlImporte, idEstatus, msjError);

                                if (msjError == "Archivo válido fiscalmente y Enviado a SAP para su revisión")
                                {
                                    msj = "Ok";
                                    msjError = "Recepción guardada correctamente. Id Recepción : " + idRecepcion.ToString(); ;
                                }
                                #endregion

                                #region Guarda Log
                                guardaLog("Recepcion archivo", accion, msj, msjError);
                                #endregion
                            }
                            else
                            {
                                if (Session["Errores"] == null)
                                    Session["Errores"] = "Archivo no procesado, No se encontró PDF con el mismo nombre del XML : " + nombre;
                                else
                                    Session["Errores"] = Session["Errores"].ToString() + ", " + "Archivo no procesado, No se encontró PDF con el mismo nombre del XML : " + nombre;
                                idEstatus = 1;
                            }
                            #endregion
                        }

                        bool r = eliminaArchivos(idProveedor, idUsuario);
                        Response.Redirect("~/Recepcion.aspx");
                    }
                    else
                    {
                        Session["Errores"] = "No existe archivos XML para procesar";
                        bool r = eliminaArchivos(idProveedor, idUsuario);
                    }
                    #endregion
                }
                else
                {
                    Session["Errores"] = "No existe el directorio tmp en el servidor, favor de contactar al administrador";
                }
            }
            else
            {
                Session["Errores"] = "No hay archivos cargados, Debe dar clic en SELECCIONAR, cargar los archivos y dar clic en SUBIR, por último dar clic en PROCESAR";
            }

            Response.Redirect("~/Recepcion.aspx");

        }

        protected void btnActualizar_Click(object sender, ImageClickEventArgs e)
        {
            dgvRecepcionArchivos.Columns[7].Visible = true;
            llenaGrid();
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            int idProveedor = -1;
            int idUsuario = -1;
            idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            muestraArchivos(idProveedor, idUsuario);
            mppMostrar.Show();
        }

        #region Metodos y eventos de Busqueda
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            dgvRecepcionArchivos.Columns[7].Visible = true;
            if (btnBuscar.Text == "Buscar")
            {
                inicializadgvRecepcion();
                limpiaTextos();
                btnCancel.Visible = true;
                btnBuscar.Text = "Aceptar";
                pnlBuscar.Visible = true;
                dgvRecepcionArchivos.Columns[7].Visible = false;
            }
            else
            {
                dgvRecepcionArchivos.DataSource = null;
                dgvRecepcionArchivos.DataBind();

                cleFechaini.SelectedDate = Convert.ToDateTime(txtFechaini.Text);
                cleFechafin.SelectedDate = Convert.ToDateTime(txtFechafin.Text);
                llenaGrid();
                dgvRecepcionArchivos.Columns[7].Visible = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            dgvRecepcionArchivos.Columns[7].Visible = true;
            CargarFechas();
            limpiaTextos();
            ddlSociedad.SelectedValue = "0";
            ddlEstatus.SelectedValue = "-1";
            llenaGrid();
            btnBuscar.Text = "Buscar";
            btnCancel.Visible = false;
            pnlBuscar.Visible = false;
            dgvRecepcionArchivos.Columns[7].Visible = false;
        }
        #endregion

        #region Eventos y metodos del formulario
        /// <summary>
        /// Metodo para llenad DDL de sociedades
        /// </summary>
        private void llenaddlSocieddes()
        {
            ddlSociedad.Items.Clear();

            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                List<Sociedades> csSociedad = buscaSociedad.buscaSociedad();
                if (csSociedad.Count > 0)
                {
                    ddlSociedad.DataValueField = "Id_Sociedad";
                    ddlSociedad.DataTextField = "Razon_Social";
                    ddlSociedad.Items.Clear();
                    ddlSociedad.Items.Add(new ListItem("Todas", "0"));
                    ddlSociedad.AppendDataBoundItems = true;
                    ddlSociedad.DataSource = csSociedad;
                    ddlSociedad.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("btnBuscar", "llenaddlSocieddes", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar ddl estatus
        /// </summary>
        private void llenaddlEstatus()
        {
            ddlEstatus.Items.Clear();

            BLEstatus buscaEstatus = new BLEstatus();
            try
            {
                List<Estatus> csEstatus = buscaEstatus.buscaEstatus();
                if (csEstatus.Count > 0)
                {
                    ddlEstatus.DataValueField = "ID_Estatus";
                    ddlEstatus.DataTextField = "Nomnre";
                    ddlEstatus.Items.Clear();
                    ddlEstatus.Items.Add(new ListItem("Todas", "-1"));
                    ddlEstatus.AppendDataBoundItems = true;
                    ddlEstatus.DataSource = csEstatus;
                    ddlEstatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                muestraError("btnBuscar", "llenaddlEstatus", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para buscar archivos pendientes por procesar
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <returns></returns>
        private bool buscaArchivos(int _IdProveedor, int _IdUsuario)
        {
            bool resp = false;
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            //Validamos los archivos a procesar
            DirectoryInfo rootDir = new DirectoryInfo(strPath);
            if (Directory.Exists(strPath))
            {
                FileInfo[] files = null;
                //files = rootDir.GetFiles("*.xml");
                files = rootDir.GetFiles("*.*");

                if (files != null)
                    if(files.Count() > 0)
                        resp = true;
            }

            return resp;

        }

        /// <summary>
        /// Metodo para validar que exista el PDF para cada XM
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_nombrepdf"></param>
        /// <returns></returns>
        private bool existePDF(int _IdProveedor, int _IdUsuario, string _nombrepdf)
        {
            bool resp = false;
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            try
            {
                //Validamos los archivos a procesar
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles(_nombrepdf);

                    if (files != null && files.Count() > 0)
                    {
                        resp = true;
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("btnProcesar", "existePDF", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para extraer información del XML
        /// </summary>
        /// <param name="pathFile"></param>
        /// <returns></returns>
        private string[] obtenDatosXML(string pathFile)
        {
            string[] arrRespuesta = new string[8];

            //Documento XML
            XmlDocument xmlFactura = new XmlDocument();
            xmlFactura.Load(pathFile);
            //Nodos del documento
            XmlNode nodoFactura = xmlFactura.DocumentElement;
            //Atributos Root

            XmlAttribute rootAtributo = nodoFactura.Attributes["version"];
            if (rootAtributo != null)
                arrRespuesta[0] = rootAtributo.Value.ToString();

            rootAtributo = null;
            if (nodoFactura.Prefix == "cfdi")
                arrRespuesta[1] = nodoFactura["cfdi:Receptor"].GetAttribute("rfc");
            else
                arrRespuesta[1] = nodoFactura["Receptor"].GetAttribute("rfc");

            rootAtributo = null;
            if (nodoFactura.Prefix == "cfdi")
                arrRespuesta[2] = nodoFactura["cfdi:Emisor"].GetAttribute("rfc");
            else
                arrRespuesta[2] = nodoFactura["Emisor"].GetAttribute("rfc");

            rootAtributo = null;
            rootAtributo = nodoFactura.Attributes["folio"];
            if (rootAtributo != null)
                arrRespuesta[3] = rootAtributo.Value.ToString();

            rootAtributo = null;
            rootAtributo = nodoFactura.Attributes["serie"];
            if (rootAtributo != null)
                arrRespuesta[4] = rootAtributo.Value.ToString();

            XmlNodeList nodeList = xmlFactura.GetElementsByTagName("cfdi:Complemento");
            foreach (XmlElement nodo in nodeList)
            {
                arrRespuesta[5] = nodo["tfd:TimbreFiscalDigital"].GetAttribute("UUID");
            }

            rootAtributo = null;
            rootAtributo = nodoFactura.Attributes["fecha"];
            if (rootAtributo != null)
                arrRespuesta[6] = rootAtributo.Value.ToString();

            rootAtributo = null;
            rootAtributo = nodoFactura.Attributes["total"];
            if (rootAtributo != null)
                arrRespuesta[7] = rootAtributo.Value.ToString();

            return arrRespuesta;
        }

        /// <summary>
        /// Metodo para buscar Factura procesada
        /// </summary>
        /// <param name="_hoy"></param>
        /// <param name="_uuid"></param>
        /// <returns></returns>
        private DateTime buscaArchivo(DateTime _hoy, string _uuid)
        {
            DateTime respFecha = new DateTime();
            int noREg = -1;
            respFecha = _hoy;
            BLArchivos1 busca = new BLArchivos1();
            try
            {
                List<Archivos1> csArchivos = busca.buscaArchivoUUID(_uuid);
                noREg = csArchivos.Count;
                for (int n = 0; n < noREg; n++)
                {
                    int idEstatus = -1;
                    idEstatus = csArchivos[n].ID_Estatus;
                    if (idEstatus != 1)
                    {
                        respFecha = csArchivos[n].Fecha_Recepcion;
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("buscaArchivo", "buscaArchivo", "Error", ex.Message);
            }

            return respFecha;
        }


        /// <summary>
        /// Metodo para buscar al proveedor
        /// </summary>
        /// <param name="_xmlEmisor"></param>
        /// <returns></returns>
        private int buscaProveedor(string _xmlEmisor)
        {
            int idProveedor = -1;
            BLProveedores buscaProveedor = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = buscaProveedor.buscaProveedor(_xmlEmisor);
                if (csProveedores.Count > 0)
                    idProveedor = csProveedores[0].ID_Proveedor;
            }
            catch (Exception ex)
            {
                muestraError("buscaProveedor", "buscaProveedor", "Error", ex.Message);
            }
            return idProveedor;
        }

        /// <summary>
        /// Metodo para bscar sociedad
        /// </summary>
        /// <param name="_xmlRFCReceptor"></param>
        /// <returns></returns>
        private List<Sociedades> buscaSociedad(string _xmlRFCReceptor)
        {
            List<Sociedades> csSociedades = null;
            BLSociedades buscaSociedad = new BLSociedades();
            try
            {
                csSociedades = buscaSociedad.buscaSociedad(_xmlRFCReceptor);
            }
            catch (Exception ex)
            {
                muestraError("buscaSociedad", "buscaSociedad", "Error", ex.Message);
            }
            return csSociedades;
        }

        /// <summary>
        /// Metodo para enviar al PAC el XML
        /// </summary>
        /// <param name="_usuarioPac"></param>
        /// <param name="_passPac"></param>
        /// <param name="_pathFile"></param>
        /// <returns></returns>
        private string enviaPac(string _usuarioPac, string _passPac, string _pathFile)
        {
            string resp = string.Empty;
            string cadenaBase64 = string.Empty;

            ValidaCFDI.CFDIStatus validaXML = new ValidaCFDI.CFDIStatus();
            try
            {
                cadenaBase64 = convierteBase64(_pathFile);
                resp = validaXML.consultacfdi(_usuarioPac, _passPac, cadenaBase64);
            }
            catch (Exception ex)
            {
                muestraError("btnProcesar", "enviaPac", "Error", ex.Message);
            }

            ////Simulamos la respuesta
            //System.Threading.Thread.Sleep(3000);
            //resp = "Ok";

            return resp;
        }

        /// <summary>
        /// Metodo para convertir a base 64
        /// </summary>
        /// <param name="_rutaarchivo"></param>
        /// <returns></returns>
        private string convierteBase64(string _rutaarchivo)
        {
            string base64String = string.Empty;

            System.IO.FileStream inFile;
            byte[] binaryData;
            try
            {
                inFile = new System.IO.FileStream(_rutaarchivo, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                binaryData = new byte[inFile.Length];
                long bytesRead = inFile.Read(binaryData, 0, (int)inFile.Length);
                inFile.Close();

                base64String = Convert.ToBase64String(binaryData, 0, binaryData.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return base64String;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        /// <param name="_anio"></param>
        /// <param name="_mes"></param>
        /// <param name="_ArchivoXML"></param>
        /// <param name="_ArchivoPDF"></param>
        /// <returns></returns>
        private bool mueveArchivos(int _IdProveedor, int _IdUsuario, int _anio, int _mes, string _ArchivoXML, string _ArchivoPDF)
        {
            bool resp = false;
            string strPathOrigen = "";
            string strPathDestino = "";

            //Ruta origen
            strPathOrigen = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            //Ruta destino Boveda
            //VAlidamos que la ruta no sea FTP
            try
            {
                bool esFtp = false;
                esFtp = Convert.ToBoolean(ConfigurationManager.AppSettings["esftp"].ToString());
                if (esFtp)
                {
                    #region CArgar por FTP
                    string ftpUsuario = string.Empty;
                    string ftpPass = string.Empty;
                    ftpUsuario = ConfigurationManager.AppSettings["usuarioFtp"].ToString();
                    ftpPass = ConfigurationManager.AppSettings["passwordFtp"].ToString();

                    //Validamos que sea FTP
                    //Uri uriFTP = new Uri(strPathDestino);
                    //if (uriFTP.Scheme != Uri.UriSchemeFtp)
                    //    return false;

                    //VAlidamos que los archivos existan
                    DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                    if (Directory.Exists(strPathOrigen))
                    {
                        FileInfo[] files = null;
                        string nombre = "";
                        nombre = Path.GetFileNameWithoutExtension(_ArchivoXML);
                        files = rootDir.GetFiles(nombre + ".*");

                        if (files != null)
                        {
                            foreach (FileInfo fi in files)
                            {

                                strPathDestino = string.Empty;
                                //Solo la ruta destio del  FTP
                                strPathDestino = ConfigurationManager.AppSettings["ftpXProcesar"].ToString();
                                //Ruta del FTM mas el archivo
                                strPathDestino = strPathDestino + fi.ToString();

                                //Creamos el FTM
                                FtpWebRequest ftpUpload = null;
                                StreamReader archivoOrigen = null;
                                byte[] byteOrigen = null;

                                ftpUpload = (FtpWebRequest)WebRequest.Create(strPathDestino);
                                ftpUpload.Credentials = new NetworkCredential(ftpUsuario, ftpPass);
                                ftpUpload.Method = WebRequestMethods.Ftp.UploadFile;
                                
                                //Leemos el origen
                                archivoOrigen = new StreamReader(strPathOrigen + fi.ToString());
                                byteOrigen = System.Text.Encoding.UTF8.GetBytes(archivoOrigen.ReadToEnd());
                                archivoOrigen.Close();
                                ftpUpload.ContentLength = byteOrigen.Length;

                                //Escribimos en el Destino
                                Stream archivoDestino = null;
                                archivoDestino = ftpUpload.GetRequestStream();
                                archivoDestino.Write(byteOrigen, 0, byteOrigen.Length);
                                archivoDestino.Close();

                                resp = true;
                            }
                        }
                        
                    }
                    #endregion
                }
                else
                {
                    #region Cargar Local
                    strPathDestino = ConfigurationManager.AppSettings["rutaXProcesar"].ToString();
                    //Validamos los archivos a procesar
                    DirectoryInfo rootDir = new DirectoryInfo(strPathOrigen);
                    if (Directory.Exists(strPathOrigen))
                    {
                        //Verificamos que existan los directorios, de lo contrario los creamos
                        if (!Directory.Exists(strPathDestino))
                            Directory.CreateDirectory(strPathDestino);

                        FileInfo[] files = null;
                        string nombre = "";
                        nombre = Path.GetFileNameWithoutExtension(_ArchivoXML);
                        files = rootDir.GetFiles(nombre + ".*");

                        if (files != null)
                        {
                            foreach (FileInfo fi in files)
                            {
                                if (fi.Name == _ArchivoXML)
                                {
                                    if (File.Exists(strPathDestino + _ArchivoXML))
                                        File.Delete(strPathDestino + _ArchivoXML);
                                    File.Copy(strPathOrigen + _ArchivoXML, strPathDestino + _ArchivoXML);
                                }

                                if (fi.Name == _ArchivoPDF)
                                {
                                    if (File.Exists(strPathDestino + _ArchivoPDF))
                                        File.Delete(strPathDestino + _ArchivoPDF);

                                    File.Copy(strPathOrigen + _ArchivoPDF, strPathDestino + _ArchivoPDF);
                                }
                                resp = true;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                muestraError("btnProcesar", "mueveArchivos", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para guardar la recepción del archivo
        /// </summary>
        /// <param name="_Nombrexml"></param>
        /// <param name="_Nombrepdf"></param>
        /// <param name="_pathXML"></param>
        /// <param name="_pathPDF"></param>
        /// <param name="_Idpproveedor"></param>
        /// <param name="_Idsociedad"></param>
        /// <param name="_folio"></param>
        /// <param name="_serie"></param>
        /// <param name="_uuid"></param>
        /// <param name="_fechafactura"></param>
        /// <param name="_importe"></param>
        /// <param name="_Idestatus"></param>
        /// <param name="_obsevaciones"></param>
        /// <returns></returns>
        private int guardaRecepcion(string _Nombrexml, string _Nombrepdf, string _pathXML, string _pathPDF, int _Idpproveedor,
            int _Idsociedad, string _folio, string _serie, string _uuid, DateTime _fechafactura,
            decimal _importe, int _Idestatus, string _obsevaciones)
        {
            int intResp = -1;

            //Primero convertimos en Bits los arhivos para almacenarlos en la base de datos
            byte[] archivoXML = null;
            byte[] archivoPDF = null;

            archivoXML = combierteArchivo(_pathXML);
            archivoPDF = combierteArchivo(_pathPDF);

            BLArchivos guarda = new BLArchivos();
            try
            {
                intResp = guarda.insertaArchivo(_Nombrexml, _Nombrepdf, archivoXML, archivoPDF, _Idpproveedor,
                    _Idsociedad, _folio, _serie, _uuid, _fechafactura,
                    _importe, _Idestatus, _obsevaciones);
            }
            catch (Exception ex)
            {
                muestraError("guardaRecepcion", "guardaRecepcion", "Error", ex.Message);
            }
            return intResp;
        }

        /// <summary>
        /// Metodo para convertir archivo para almacenar.
        /// </summary>
        /// <param name="_strRuta"></param>
        /// <returns></returns>
        private byte[] combierteArchivo(string _strRuta)
        {
            byte[] resp = null;
            try
            {
                FileStream fs = new FileStream(_strRuta, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                resp = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                muestraError("guardaRecepcion", "combierteArchivo", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metro para mostrar archivos
        /// </summary>
        /// <param name="_idrecepcion"></param>
        /// <param name="_tipo"></param>
        private void mustrarArchivo(int _idrecepcion, string _tipo)
        {
            if (Session["id_Usuario"] != null)
            {
                int ID_Usuario = -1;
                string nombreArchivo = string.Empty;

                ID_Usuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                //Primero buscmos el archivo
                BLArchivos busca = new BLArchivos();
                try
                {
                    List<Archivos> csArchivos = busca.buscaPdfXml(_idrecepcion);
                    if (csArchivos.Count > 0)
                    {
                        byte[] datosArchivo = null;
                        if (_tipo == "application/pdf")
                        {
                            datosArchivo = csArchivos[0].Archivo_PDF;
                            nombreArchivo = csArchivos[0].Nombre_PDF;
                        }
                        else
                        {
                            datosArchivo = csArchivos[0].Archivo_XML;
                            nombreArchivo = csArchivos[0].Nombre_XML;
                        }

                        Response.Clear();
                        Response.ContentType = _tipo;
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + nombreArchivo);
                        Response.BinaryWrite(datosArchivo);
                        Response.End();
                        Response.Close();
                    }

                    //Response.Redirect("~/Recepcion.aspx", false);
                }
                catch (Exception ex)
                {
                    muestraError("dgvAclraciones", "mustrarArchivo", "Error", ex.Message);
                }
            }
        }

        /// <summary>
        /// Metodo que envía correo cuando el PAC no envía respuesta
        /// </summary>
        private void enviaCorreoerrorenPAC()
        {
            string resp = "";
            EnviaCorreo envia = new EnviaCorreo();
            try
            {
                string asunto = string.Empty;
                string cuerpo = string.Empty;
                string receptor = string.Empty;
                string strPath = string.Empty;

                asunto = "Problemas para conectarse con el PAC";
                cuerpo = "<body><p style='font-family: Sans-Serif; font-size: 18px; color: #008FC4;'><strong>INDUSTRIAL ACEITERA - Problemas para conectarse cn el PAC</strong></p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Se ha detectado que no se ha podido establecer comunicación con el PAC</p>";
                cuerpo += "<table style='font-family: Sans-Serif; font-size: 12px; color: #000000; margin-left: 50px'>";
                cuerpo += "<tr><td style='text-align: right;'>Fecha :</td><td style='text-align: left;'>" + DateTime.Today.ToShortDateString() + "</td></tr>";
                cuerpo += "<tr><td style='text-align: right;'>Hora :</td><td style='text-align: left;'>" + DateTime.Now.ToShortTimeString() + "</td></tr></table>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Para acceder, debes ingresar en <a href='http://aceitera.com.mx'>www.aceitera.com.mx</a> y dar clic en PORTAL DE PROVEEDORES situado en la partes superior derecha debajo del Menú</p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Nota: Se adjunta Manual de operación y de manera adicional podras encontrarlo dentro del Portal en el pie de página del Menú Inicio</p>";
                cuerpo += "<br /><p style='font-family: Sans-Serif; font-size: 14px; color: #008FC4;'><strong>Atentamente</strong></p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #008FC4;'><strong>CUENTAS POR PAGAR - INDUSTRIAL ACEITERA</strong></p></body>";

                receptor = ConfigurationManager.AppSettings["mailReceptorError"].ToString();
                //receptor = _receptor;
                strPath = Server.MapPath(@"./Doctos/");
                resp = envia.enviaCorreo(receptor, asunto, cuerpo);

            }
            catch (Exception ex)
            {
                guardaLog("Principal", "enviaCorreo", "Error", ex.Message);
            }
        }
        #endregion

        #region Metodos comunes
        /// <summary>
        /// Metodo que inicializa las fechas
        /// </summary>
        private void CargarFechas()
        {
            DateTime fecha = DateTime.Now.Date;
            this.cleFechaini.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
            this.cleFechafin.SelectedDate = new DateTime(fecha.Year, fecha.Month, fecha.Day);
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        private void llenaGrid()
        {
            dgvRecepcionArchivos.DataSource = null;
            dgvRecepcionArchivos.DataBind();

            DataTable dtBusca = new DataTable();
            try
            {
                dtBusca = buscaArchivos();
                if (dtBusca.Rows.Count > 0)
                {
                    dgvRecepcionArchivos.DataSource = dtBusca;
                    dgvRecepcionArchivos.DataBind();
                }
                else
                    inicializadgvRecepcion();

            }
            catch (Exception ex)
            {
                muestraError("btnBuscar", "llenaGrid", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para buscar archivos
        /// </summary>
        /// <returns></returns>
        private DataTable buscaArchivos()
        {
            #region VAriables locales
            int ID_Proveedor = -1;
            int ID_Sociedad = -1;
            string Nombre_archivo = string.Empty;
            string Serie = string.Empty;
            string Folio = string.Empty;
            DateTime Fecha_Ini = new DateTime();
            DateTime Fecha_Fin = new DateTime();
            int Id_Estatus = -1;
            #endregion

            #region Asignación
            ID_Proveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            ID_Sociedad = Convert.ToInt32(ddlSociedad.SelectedValue.ToString());
            Nombre_archivo = txtNombrearchivo.Text.Trim() == string.Empty ? "" : txtNombrearchivo.Text.Trim();
            Serie = txtSerie.Text.Trim() == string.Empty ? "" : txtSerie.Text.Trim();
            Folio = txtFolio.Text.Trim() == string.Empty ? "" : txtFolio.Text.Trim();
            Fecha_Ini = Convert.ToDateTime(cleFechaini.SelectedDate.ToString());
            Fecha_Fin = Convert.ToDateTime(cleFechafin.SelectedDate.ToString());
            Id_Estatus = Convert.ToInt32(ddlEstatus.SelectedValue.ToString());
            #endregion

            DataTable dtArchivos = null;
            BLArchivos busca = new BLArchivos();
            try
            {
                dtArchivos = new DataTable();
                dtArchivos = busca.buscaArchivos(ID_Proveedor, ID_Sociedad, Nombre_archivo, Serie, Folio, Fecha_Ini, Fecha_Fin, Id_Estatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtArchivos;

        }

        /// <summary>
        /// Metodo para inicializar el Grid
        /// </summary>
        private void inicializadgvRecepcion()
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("Id_Recepcion", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Fecha_Recepcion", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("Nombre_XML", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Nombre_PDF", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Folio", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Serie", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("ID_Estatus", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Observaciones", typeof(String)));

            DataRow drVacio = dtArchivos.NewRow();
            drVacio[0] = 0;
            drVacio[1] = DateTime.Now;
            drVacio[2] = "";
            drVacio[3] = "";
            drVacio[4] = "";
            drVacio[5] = "";
            drVacio[6] = 0;
            drVacio[7] = "";

            dtArchivos.Rows.Add(drVacio);
            dgvRecepcionArchivos.DataSource = dtArchivos;
            dgvRecepcionArchivos.DataBind();
            int totalcolums = dgvRecepcionArchivos.Rows[0].Cells.Count;
            dgvRecepcionArchivos.Rows[0].Cells.Clear();
            dgvRecepcionArchivos.Rows[0].Cells.Add(new TableCell());
            dgvRecepcionArchivos.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvRecepcionArchivos.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }

        /// <summary>
        /// Metodo para limpiar textos
        /// </summary>
        private void limpiaTextos()
        {
            txtNombrearchivo.Text = "";
            CargarFechas();
            txtFolio.Text = "";
            txtSerie.Text = "";
        }

        /// <summary>
        /// Meodos para eliminar archivs de el Path
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        private bool eliminaArchivos(int _IdProveedor, int _IdUsuario)
        {
            bool resp = true;
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");
            try
            {
                //Validamos los archivos a procesar
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.*");

                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            fi.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("validaArchivospendientes", "Buscaarchivo", "Error", ex.Message);
            }

            return resp;
        }
        #endregion

        #region Eventos y metodos del PopUp
        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                int idProveedor = -1;
                int idUsuario = -1;
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                eliminaArchivos(idProveedor, idUsuario);

                int idSociedad = Convert.ToInt32(ddlSociedad.SelectedValue.ToString());
                string nombreArchivo = txtNombrearchivo.Text.Trim() == string.Empty ? "" : txtNombrearchivo.Text.Trim();
                string serie = txtSerie.Text.Trim() == string.Empty ? "" : txtSerie.Text.Trim();
                string folio = txtFolio.Text.Trim() == string.Empty ? "" : txtFolio.Text.Trim();
                DateTime finicio = Convert.ToDateTime(txtFechaini.Text + " 00:01:00 a.m.");
                DateTime ffin = Convert.ToDateTime(txtFechafin.Text + " 12:59:59 p.m.");
                int idEstatus = Convert.ToInt32(ddlEstatus.SelectedValue.ToString());

                Response.Redirect("~/Recepcion.aspx");
            }
        }

        /// <summary>
        /// Metodo para mostrar Archivos pendientes por cargar
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        private void muestraArchivos(int _IdProveedor, int _IdUsuario)
        {
            //Creamos el datatTable
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("No", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("ArchivoXML", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("ArchivoPDF", typeof(String)));
            //dtArchivos.Columns.Add(new DataColumn("Accion", typeof(String)));

            string strPath = "";
            string nombrexml = "";
            string nombre = "";
            //string accion = "";

            int noArchivos = 0;

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");

            try
            {
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.xml");

                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            nombrexml = fi.Name;
                            nombre = Path.GetFileNameWithoutExtension(nombrexml);
                            noArchivos = noArchivos + 1;

                            DataRow drArchivo = dtArchivos.NewRow();
                            drArchivo["No"] = noArchivos;
                            drArchivo["ArchivoXML"] = nombrexml;
                            drArchivo["ArchivoPDF"] = nombre + ".pdf";

                            dtArchivos.Rows.Add(drArchivo);
                        }
                    }
                }
                dgvArchivos.DataSource = dtArchivos;
                dgvArchivos.DataBind();
            }
            catch (Exception ex)
            {
                muestraError("llenagrvArchivos", "validaArchivos", "Error", ex.Message);
            }
        }
        #endregion

        #region Manejo de errores
        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Recepcion", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Recepcion", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }
        #endregion

        #region Metodos que no se utilizan
        ///// <summary>
        ///// Metodo para validar que exista el XML para cada PDF
        ///// </summary>
        ///// <param name="_IdProveedor"></param>
        ///// <param name="_IdUsuario"></param>
        ///// <returns></returns>
        //private bool existeXML(int _IdProveedor, int _IdUsuario)
        //{
        //    bool resp = false;
        //    string strPath = "";
        //    string nombrexml = "";
        //    string nombre = "";

        //    //Ruta en la que se cargaran los archivos
        //    strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
        //        _IdProveedor.ToString() + "/" +
        //        _IdUsuario.ToString() + "/");

        //    try
        //    {
        //        //Validamos los archivos a procesar
        //        DirectoryInfo rootDir = new DirectoryInfo(strPath);
        //        if (Directory.Exists(strPath))
        //        {
        //            FileInfo[] files = null;
        //            files = rootDir.GetFiles("*.pdf");

        //            if (files != null)
        //            {
        //                foreach (FileInfo fi in files)
        //                {
        //                    nombrexml = fi.Name;
        //                    nombre = Path.GetFileNameWithoutExtension(nombrexml);

        //                    if (!File.Exists(rootDir + nombre + ".xml"))
        //                    {
        //                        File.Delete(rootDir + nombrexml);
        //                        resp = true;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        muestraError("btnValidar", "existeXML", "Error", ex.Message);
        //    }

        //    return resp;
        //}

        ///// <summary>
        ///// Metodo para llenar el Grid de recepción
        ///// </summary>
        //private void cargaGridRecepcion()
        //{
        //    dgvRecepcionArchivos.DataSource = null;
        //    dgvRecepcionArchivos.DataBind();

        //    int idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());

        //    BLArchivos buscaArchivos = new BLArchivos();
        //    try
        //    {
        //        List<Archivos> csArchivos = buscaArchivos.buscaArchivos(idProveedor);
        //        if (csArchivos.Count > 0)
        //        {
        //            dgvRecepcionArchivos.DataSource = csArchivos;
        //            dgvRecepcionArchivos.DataBind();
        //        }
        //        else
        //            inicializadgvRecepcion();

        //        dgvRecepcionArchivos.Columns[0].Visible = false;
        //        dgvRecepcionArchivos.Columns[7].Visible = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        muestraError("Load", "cargaGridRecepcion", "Error", ex.Message);
        //    }
        //}

        ///// <summary>
        ///// Metodo para validar los archivos a cargar
        ///// </summary>
        ///// <param name="_IdProveedor"></param>
        ///// <param name="_IdUsuario"></param>
        //private DataTable validaArchivos(int _IdProveedor, int _IdUsuario)
        //{
        //    //Creamos el datatTable
        //    DataTable dtArchivos = new DataTable();
        //    dtArchivos.Columns.Add(new DataColumn("No", typeof(Int32)));
        //    dtArchivos.Columns.Add(new DataColumn("ArchivoXML", typeof(String)));
        //    dtArchivos.Columns.Add(new DataColumn("ArchivoPDF", typeof(String)));
        //    dtArchivos.Columns.Add(new DataColumn("Accion", typeof(String)));

        //    string strPath = "";
        //    string nombrexml = "";
        //    string nombre = "";
        //    string accion = "";

        //    int noArchivos = 0;

        //    //Ruta en la que se cargaran los archivos
        //    strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
        //        _IdProveedor.ToString() + "/" +
        //        _IdUsuario.ToString() + "/");

        //    //Buscamos si el archivo ya se encuentra en la base de datos
        //    BLArchivos buscaArchivo = new BLArchivos();
        //    try
        //    {
        //        //Validamos los archivos a procesar
        //        DirectoryInfo rootDir = new DirectoryInfo(strPath);
        //        if (Directory.Exists(strPath))
        //        {
        //            FileInfo[] files = null;
        //            files = rootDir.GetFiles("*.xml");

        //            if (files != null)
        //            {
        //                foreach (FileInfo fi in files)
        //                {
        //                    nombrexml = fi.Name;
        //                    nombre = Path.GetFileNameWithoutExtension(nombrexml);
        //                    noArchivos = noArchivos + 1;

        //                    DataRow drArchivo = dtArchivos.NewRow();
        //                    drArchivo["No"] = noArchivos;
        //                    drArchivo["ArchivoXML"] = nombrexml;
        //                    drArchivo["ArchivoPDF"] = nombre + ".pdf";

        //                    //Validamos que el archivo no este cargado con anterioridad
        //                    List<Archivos> csArchivos = buscaArchivo.buscaArchivos(_IdProveedor, nombrexml);
        //                    if (csArchivos.Count > 0)
        //                    {
        //                        accion = "Ya Procesada";
        //                    }
        //                    else
        //                    {
        //                        accion = "Cargar";

        //                        drArchivo["Accion"] = accion;

        //                        dtArchivos.Rows.Add(drArchivo);
        //                    }
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        muestraError("llenagrvArchivos", "validaArchivos", "Error", ex.Message);
        //    }

        //    return dtArchivos;

        //}


        //protected void btnCargar_Click(object sender, EventArgs e)
        //{
        //    lblMensaje.Visible = false;
        //    lblMensaje.Text = "";

        //    if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
        //    {
        //        mppUpload.Show();
        //    }
        //}

        //protected void btnLimpiar_Click(object sender, EventArgs e)
        //{
        //    
        //}

        //protected void btnProcesar_Click(object sender, EventArgs e)
        //{
        //    //Primero se hace las validaciones del portal
        //    lblMensaje.Visible = false;
        //    lblMensaje.Text = "";

        //    string accion = string.Empty;

        //    if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
        //    {
        //        int idProveedor = -1;
        //        int idUsuario = -1;
        //        idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
        //        idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

        //        accion = "Busca Archivo";

        //        string strPath = "";
        //        //Ruta en la que se cargaran los archivos
        //        strPath = Server.MapPath(@"./UpdateFiles/xProcesar/" +
        //            idProveedor.ToString() + "/" +
        //            idUsuario.ToString() + "/");

        //        BLArchivos buscaArchivo = new BLArchivos();
        //        try
        //        {
        //            DirectoryInfo rootDir = new DirectoryInfo(strPath);
        //            if (Directory.Exists(strPath))
        //            {
        //                FileInfo[] files = null;
        //                files = rootDir.GetFiles("*.xml");

        //                if (files != null)
        //                {
        //                    foreach (FileInfo fi in files)
        //                    {
        //                        

        //                        accion = "Valida archivo";
        //                        #region Valida estaus de archivo
        //                        List<Archivos> csArchivos = buscaArchivo.buscaArchivos(idProveedor, nombrexml);
        //                        if (csArchivos.Count > 0)
        //                        {
        //                            //El archivo no exite en la base de datos por o que continua con la validación.
        //                            if (lblMensaje.Text != "")
        //                                muestraMensaje(", El archivo ya fue procesado con anteriorida. Archivo : " + nombrexml);
        //                            else
        //                                muestraMensaje("El archivo ya fue procesado con anteriorida. Archivo : " + nombrexml);

        //                            msjError = "Error";
        //                        }
        //                        #endregion

        //                        

        //                        

        //                        

        //                        
        //                    }
        //                }
        //            }

        //            cargaGridRecepcion();
        //        }
        //        catch (Exception ex)
        //        {
        //            muestraError("btnProcesar", accion, "Error", ex.Message);
        //        }
        //    }
        //}
        //#endregion

        ///// <summary>
        ///// Metodo que llena el grid de archivos por cargar
        ///// </summary>
        ///// <param name="_IdProveedor"></param>
        ///// <param name="_IdUsuario"></param>
        //protected void llenagrvArchivos(int _IdProveedor, int _IdUsuario)
        //{
        //    dgvArchivos.DataSource = null;
        //    dgvArchivos.DataBind();

        //    DataTable dtRespuesta = new DataTable();
        //    dtRespuesta = validaArchivos(_IdProveedor, _IdUsuario);
        //    if (dtRespuesta.Rows.Count > 0)
        //    {
        //        dgvArchivos.DataSource = dtRespuesta;
        //        dgvArchivos.DataBind();

        //        if (dtRespuesta.Rows.Count < 5)
        //        {
        //            btnProcesar.Visible = true;
        //            btnLimpiar.Visible = true;
        //            btnCargar.Visible = true;
        //        }
        //        else
        //        {
        //            btnProcesar.Visible = true;
        //            btnLimpiar.Visible = true;
        //            btnCargar.Visible = false;
        //        }
        //    }
        //    else
        //    {
        //        btnProcesar.Visible = false;
        //        btnLimpiar.Visible = false;
        //        btnCargar.Visible = true;

        //        dgvArchivos.DataSource = null;
        //        dgvArchivos.DataBind();
        //        inicializadgvArchivos();
        //    }
        //}

        ///// <summary>
        ///// Metodo para inicializar el Grid de Archivos
        ///// </summary>
        //private void inicializadgvArchivos()
        //{
        //    DataTable dtArchivos = new DataTable();
        //    dtArchivos.Columns.Add(new DataColumn("No", typeof(Int32)));
        //    dtArchivos.Columns.Add(new DataColumn("ArchivoXML", typeof(String)));
        //    dtArchivos.Columns.Add(new DataColumn("ArchivoPDF", typeof(String)));
        //    dtArchivos.Columns.Add(new DataColumn("Accion", typeof(String)));

        //    DataRow drVacio = dtArchivos.NewRow();
        //    drVacio[0] = 0;
        //    drVacio[1] = "";
        //    drVacio[2] = "";
        //    drVacio[3] = "";

        //    dtArchivos.Rows.Add(drVacio);
        //    dgvArchivos.DataSource = dtArchivos;
        //    dgvArchivos.DataBind();
        //    int totalcolums = dgvArchivos.Rows[0].Cells.Count;
        //    dgvArchivos.Rows[0].Cells.Clear();
        //    dgvArchivos.Rows[0].Cells.Add(new TableCell());
        //    dgvArchivos.Rows[0].Cells[0].ColumnSpan = totalcolums;
        //    dgvArchivos.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        //}

        //#region Eventos y metodos del Pop Up Upload
        //protected void btnValidar_Click(object sender, EventArgs e)
        //{
        //    lblMensaje.Visible = false;
        //    lblMensaje.Text = "";
            
        //    string strMsj = "";

        //    if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
        //    {
                
        //        int idProveedor = -1;
        //        int idUsuario = -1;
        //        int noArchivos = 0;
        //        idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
        //        idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

        //        //Buscamos que existan archivos en lacarpeta
        //        string strpathOrigen = "";

        //        //Ruta en la que se leen los archivos
        //        strpathOrigen = Server.MapPath(@"./UpdateFiles/tmp/" +
        //            idProveedor.ToString() + "/" +
        //            idUsuario.ToString() + "/");

        //        if (Directory.Exists(strpathOrigen))
        //        {
        //            DirectoryInfo rootOrigen = new DirectoryInfo(strpathOrigen);
        //            FileInfo[] files = null;
        //            files = rootOrigen.GetFiles("*.xml");
        //            foreach (FileInfo fi in files)
        //            {
        //                noArchivos = noArchivos + 1;
        //            }
        //        }

        //        if (noArchivos > 0)
        //        {
        //            if (existePDF(idProveedor, idUsuario) || existeXML(idProveedor, idUsuario))
        //            {
        //                strMsj = "Los archivos mostrados en la rejilla solo corresponden a los XML que tengan un PDF con el mismo nombre";
        //            }

        //            if (traspasaArchivos(idProveedor, idUsuario))
        //            {
        //                if (strMsj != "")
        //                    strMsj = strMsj + "  y los archivos duplicados no son contempadosos.";
        //                else
        //                    strMsj = "Los archivos duplicados no son contempados.";
        //            }

        //            muestraMensaje(strMsj);

        //            llenagrvArchivos(idProveedor, idUsuario);
        //        }
        //        else
        //        {
        //            muestraMensaje("No se valido ningun archivo. Antes de dar clic en Validar, debes dar clic en Subir");
        //        }
        //    }
        //}

        //protected void btnCancelar_Click(object sender, EventArgs e)
        //{
        //    lblMensaje.Visible = false;
        //    lblMensaje.Text = "";

        //    string strPath = "";
        //    string nombre = "";

        //    if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
        //    {
        //        int idProveedor = -1;
        //        int idUsuario = -1;
        //        idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
        //        idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

        //        strPath = Server.MapPath(@"./UpdateFiles/tmp/" +
        //            idProveedor.ToString() + "/" +
        //            idUsuario.ToString() + "/");

        //        try
        //        {
        //            //Validamos los archivos a procesar
        //            DirectoryInfo rootDir = new DirectoryInfo(strPath);
        //            if (Directory.Exists(strPath))
        //            {
        //                FileInfo[] files = null;
        //                files = rootDir.GetFiles("*.*");

        //                if (files != null)
        //                {
        //                    foreach (FileInfo fi in files)
        //                    {
        //                        nombre = fi.Name;

        //                        if (!File.Exists(rootDir + nombre))
        //                        {
        //                            File.Delete(rootDir + nombre);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            muestraError("btnValidar", "existePDF", "Error", ex.Message);
        //        }
        //    }
        //}

        ///// <summary>
        ///// mETODO PARA TRASPASAR LOS ARCHIVOS DE CARPETA
        ///// </summary>
        ///// <param name="_IdProveedor"></param>
        ///// <param name="_IdUsuario"></param>
        ///// <returns></returns>
        //private bool traspasaArchivos(int _IdProveedor, int _IdUsuario)
        //{
        //    bool resp = false;
        //    string strpathOrigen = "";
        //    string strpatDestino = "";
            

        //    //Ruta en la que se leen los archivos
        //    strpathOrigen = Server.MapPath(@"./UpdateFiles/tmp/" +
        //        _IdProveedor.ToString() + "/" +
        //        _IdUsuario.ToString() + "/");

        //    strpatDestino = Server.MapPath(@"./UpdateFiles/xProcesar/" +
        //        _IdProveedor.ToString() + "/" +
        //        _IdUsuario.ToString() + "/");

        //    try
        //    {
        //        //Validamos los archivos a procesar
        //        DirectoryInfo rootOrigen = new DirectoryInfo(strpathOrigen);
        //        DirectoryInfo rootDestino = new DirectoryInfo(strpatDestino);
        //        if (Directory.Exists(strpathOrigen))
        //        {
        //            FileInfo[] files = null;
        //            files = rootOrigen.GetFiles("*.*");

        //            if (files != null)
        //            {
        //                foreach (FileInfo fi in files)
        //                {
        //                    string nombreOrigen = "";
        //                    nombreOrigen = fi.Name;

        //                    if (!Directory.Exists(strpatDestino))
        //                        Directory.CreateDirectory(strpatDestino);

        //                    if (!File.Exists(rootDestino + nombreOrigen))
        //                        File.Move(rootOrigen + nombreOrigen, rootDestino + nombreOrigen);
        //                    else
        //                        resp = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        muestraError("btnValidar", "existeXML", "Error", ex.Message);
        //    }
        //    return resp;
        //}

        //private void mustraPdfXml(bool esPdf, int _idRecepcion)
        //{
        //    byte[] archivopdf = null;
        //    byte[] archivoxml = null;
        //    BLArchivos buscaArchivo = new BLArchivos();
        //    try
        //    {
        //        List<Archivos> csArchivos = buscaArchivo.buscaPdfXml(_idRecepcion);
        //        if (csArchivos.Count > 0)
        //        {
        //            if (esPdf)
        //                archivopdf = csArchivos[0].Archivo_PDF;
        //            else
        //                archivoxml = csArchivos[0].Archivo_XML;
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        muestraError("dgvRecepcionArchivos", "mustraPdfXml", "Error", ex.Message);
        //    }
        //}
        #endregion
    }
}