﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Comentarios.aspx.cs" Inherits="RecepciondeFecturas.Comentarios" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        }

        function pageLoad(sender, args) {
            //
            document.getElementsByClassName('ajax__fileupload_selectFileButton')[0].innerHTML = "Seleccionar archivo (s)";
            document.getElementsByClassName('ajax__fileupload_dropzone')[0].innerHTML = "Suelte aqui los archivos";
            document.getElementsByClassName('ajax__fileupload_topFileStatus')[0].innerHTML = "";
            document.getElementsByClassName('ajax__fileupload_uploadbutton')[0].innerHTML = "Subir";
            document.getElementsByClassName('ajax__fileupload_removeButton')[0].innerHTML = "Borrar";
            document.getElementsByClassName('ajax__fileupload_Pending ')[0].innerHTML = "Pendiente";
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="mainDatos">
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" CssClass="Textonormal"
            meta:resourcekey="lblMensajeResource1"></asp:Label>
        <div class="divBienvenido">
            <asp:Label ID="lblMisdatos" runat="server" Text="Comentarios" CssClass="Titulo1" meta:resourcekey="lblMisdatosResource1"></asp:Label>
        </div>
        <table style="width: 100%; height: 100%; text-align: center; vertical-align: middle;">
            <tr>
                <td align="left">
                    <asp:Label ID="lblNombre" runat="server" Text="Nombre :" CssClass="Textonormal"
                        meta:resourcekey="lblNombreResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtNombre" runat="server"  CssClass="Textonormal" Width="500px" meta:resourcekey="txtNombreResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblIdProveedor" runat="server" Text="ID Proveedor :" CssClass="Textonormal"
                        meta:resourcekey="lblIdProveedorResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtIDProveedor" runat="server" CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtIDProveedorResource1" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblCorreo" runat="server" Text="Correo :" CssClass="Textonormal"
                        meta:resourcekey="lblCorreoResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtCorreo" runat="server" CssClass="Textonormal" Width="500px" meta:resourcekey="txtCorreoResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblTelefono" runat="server" Text="Teléfono :" CssClass="Textonormal"
                        meta:resourcekey="lblTelefonoResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtTelefono" runat="server" CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtTelefonoResource1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblClasificacion" runat="server" Text="Clasificación :" CssClass="Textonormal"
                        meta:resourcekey="lblClasificacionResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlClasificacion" runat="server" CssClass="Textonormal" 
                        Width="500px" meta:resourcekey="ddlClasificacionResource1">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblMensajeusuario" runat="server" Text="Mensaje :" CssClass="Textonormal"
                        meta:resourcekey="lblMensajeusuarioResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtMensaje" runat="server" 
                        TextMode="MultiLine" CssClass="Textonormal" Width="500px"
                        meta:resourcekey="txtMensajeResource1" Height="50px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblArchivos" runat="server" Text="Archivos :" CssClass="Textonormal"
                        meta:resourcekey="lblArchivosResource1"></asp:Label>
                </td>
                <td align="left">
                    <asp:UpdatePanel ID="updatePanelAttachments" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <cc1:AjaxFileUpload ID="afuXML" runat="server" ThrobberID="myThrobber"
                                AllowedFileTypes="xml,pdf,jpg,png"
                                MaximumNumberOfFiles="5"
                                Width="500px" onuploadcomplete="afuXML_UploadComplete" 
                                meta:resourcekey="afuXMLResource1"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" CssClass="botonchico" 
            onclick="btnEnviar_Click" meta:resourcekey="btnEnviarResource1" />
    </div>
</asp:Content>
