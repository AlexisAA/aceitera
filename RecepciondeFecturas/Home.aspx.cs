﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLRecepcion.Portal;
using DALRecepcion.Bean.Portal;
using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;

namespace RecepciondeFecturas
{
    public partial class Home : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    buscaNombre(idProveedor);
                    buscaAvisos(idProveedor);
                    registraAcceso(Convert.ToInt32(Session["id_Usuario"].ToString()));
                }
            }
            
        }

        protected override void InitializeCulture()
        {
            if (Session["Idioma"] == null)
                UICulture = "es-MX";
            else
                 UICulture = Session["Idioma"].ToString();

            base.InitializeCulture();
        }

        /// <summary>
        /// Metodo para buscar avisos
        /// </summary>
        /// <param name="_idProveedor"></param>
        private void buscaAvisos(int _idProveedor)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            int noReg = -1;
            BLAvisos buscaAviso = new BLAvisos();
            try
            {
                List<Avisos> csAvisos = buscaAviso.buscaAviso();
                noReg = csAvisos.Count;
                for (int n = 0; n < noReg; n++)
                {
                    int Id_Proveedor = -1;
                    Id_Proveedor = csAvisos[n].ID_Proveedor;
                    if (_idProveedor == Id_Proveedor || Id_Proveedor == 0)
                    {
                        if (Session["Idioma"].ToString() == "es-MX")
                        {
                            lblTituloAvisoAdmin.Text = csAvisos[n].Titulo;
                            lblAvisoAdmin.Text = csAvisos[n].Descripcion;
                        }
                        else
                        {
                            lblTituloAvisoAdmin.Text = csAvisos[n].Titulo_en;
                            lblAvisoAdmin.Text = csAvisos[n].Descripcion_en;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("buscaAvisos", "buscaAvisos", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para buscar el nombe del proveedor
        /// </summary>
        /// <param name="_id_Proveedor"></param>
        /// <returns></returns>
        private void buscaNombre(int _id_Proveedor)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            BLProveedores buscaProveedor = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = buscaProveedor.buscaProveedorxID(_id_Proveedor);
                if (csProveedores.Count > 0)
                {
                    Session["ID_Proveedor_SAP"] = csProveedores[0].ID_Proveedor_SAP.ToString();
                }
            }
            catch (Exception ex)
            {
                muestraError("buscaNombre", "buscaNombre", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para registrar acceso al portal
        /// </summary>
        /// <param name="_idUsuario"></param>
        private void registraAcceso(int _idUsuario)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";
            BLUsuarios actualiza = new BLUsuarios();
            try
            {
                bool resp = actualiza.registraAcceso(_idUsuario);
                muestraError("registraAcceso", "Inicio de sesion", "OK", "Se inicia sesion correctamente. Usuario : " + _idUsuario);
                lblMensaje.Visible = false;
                lblMensaje.Text = "";
            }
            catch (Exception ex)
            {
                muestraError("registraAcceso", "Inicio de sesion", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Home", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

    }
}