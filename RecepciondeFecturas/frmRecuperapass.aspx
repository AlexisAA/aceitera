﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmRecuperapass.aspx.cs" Inherits="RecepciondeFecturas.frmRecuperapass" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Style/Site.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" 
        meta:resourcekey="lblMensajeResource1"></asp:Label>
    <div class="mainLogin">
        <table class="TablaUno">
            <tr>
                <td style="text-align: left; padding-bottom: 10px;">
                    <asp:Label ID="lblTituloRecuperar" runat="server" Text="Reucpera Contraseña" 
                        CssClass="Titulo1" meta:resourcekey="lblTituloRecuperarResource1"></asp:Label>            
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 300px; text-align: center;">
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblCorreo" runat="server" Text="Correo" Font-Bold="True" 
                                    CssClass="Textonormal" meta:resourcekey="lblCorreoResource1"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtCorreo" runat="server" CssClass="txtLogin" Width="250px" 
                                    meta:resourcekey="txtCorreoResource1"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnEnviar" runat="server" Text="Enviar" CssClass="botonchico" 
                                    meta:resourcekey="btnEnviarResource1" onclick="btnEnviar_Click"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top: 10px;">
                                <asp:HyperLink ID="hplRegresar" runat="server" 
                                    NavigateUrl="~/Default.aspx" Text="Regresar" CssClass="Textonormal" 
                                    meta:resourcekey="hplRegresarResource1"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
