﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Catalogos;
using System.IO;
using System.Text.RegularExpressions;
using BLRecepcion.Varios;
using System.Configuration;

namespace RecepciondeFecturas
{
    public partial class Comentarios : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    string idProveedor_SAP = string.Empty;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                    idProveedor_SAP = Session["ID_Proveedor_SAP"].ToString();

                    buscaContacto(idUsuario, idProveedor);
                    txtIDProveedor.Text = idProveedor_SAP;
                    llenaddlComentarios();
                }
            }

        }

        protected override void InitializeCulture()
        {
            if (Request.QueryString["lang"] == null)
            {
                UICulture = "ES";
            }
            else
            {
                UICulture = Request.QueryString["lang"].ToString();
            }

            base.InitializeCulture();
        }

        protected void afuXML_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        {
            if (Session["Id_Proveedor"] != null && Session["id_Usuario"] != null)
            {
                int idProveedor = -1;
                int idUsuario = -1;
                idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                string strPath = "";
                string nombreArchivo = "";

                //Ruta en la que se cargaran los archivos
                strPath = Server.MapPath(@"./UpdateFiles/attach/" +
                    idProveedor.ToString() + "/" +
                    idUsuario.ToString() + "/");

                try
                {
                    //Si no existe el directorio lo creamos
                    if (!Directory.Exists(strPath))
                        Directory.CreateDirectory(strPath);

                    //Obtenemos el nombre del archivo
                    nombreArchivo = Path.GetFileName(e.FileName);

                    //Verificamos que no existan los archivos, de ser asi los eliminamos para reemplazarlos
                    if (File.Exists(strPath + nombreArchivo))
                        File.Delete(strPath + nombreArchivo);

                    afuXML.SaveAs(strPath + nombreArchivo);
                }
                catch (Exception ex)
                {
                    muestraError("afuXML_UploadComplete", "Upload", "Error", ex.Message);
                }

            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            //Validamos el correo
            string strFormat = string.Empty;
            string correo = string.Empty;
            int idProveedor = -1;
            int idUsuario = -1;
            string strPath = "";

            idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
            idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());
            strFormat = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            correo = txtCorreo.Text.Trim();
            strPath = Server.MapPath(@"./UpdateFiles/attach/" +
                    idProveedor.ToString() + "/" +
                    idUsuario.ToString() + "/");

            if (Regex.IsMatch(correo, strFormat))
            {
                if (txtMensaje.Text.Trim() != string.Empty)
                {
                    if (ddlClasificacion.SelectedValue != "-1")
                    {
                        string cuerpo = string.Empty;
                        string receptor = string.Empty;
                        string asunto = string.Empty;
                        EnviaCorreo envia = new EnviaCorreo();
                        try
                        {
                            asunto = "PORTAL RECEPCIÓN DE FACTURAS, COMENTARIO : " + ddlClasificacion.SelectedItem.ToString();
                            cuerpo = "<body><p style='font-family: Sans-Serif; font-size: 18px; color: #008FC4;'><strong>COMENTARIOS PORTAL RECEPCION DE FACTURAS</strong></p>";
                            cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Se ha agregado un comentario en el portal Recepción de Facturas</p>";
                            cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'><strong>Datos del usuario :</strong></p>";
                            cuerpo += "<table style='font-family: Sans-Serif; font-size: 12px; color: #000000; margin-left: 50px'>";
                            cuerpo += "<tr><td style='text-align: right;'>Nombre : </td><td style='text-align: left;'>" + txtNombre.Text.Trim() + "</td></tr>";
                            cuerpo += "<tr><td style='text-align: right;'>ID Proveedor : </td><td style='text-align: left;'>" + txtIDProveedor.Text.Trim() + "</td></tr>";
                            cuerpo += "<tr><td style='text-align: right;'>Correo : </td><td style='text-align: left;'>" + txtCorreo.Text.Trim() + "</td></tr>";
                            cuerpo += "<tr><td style='text-align: right;'>Teléfono : </td><td style='text-align: left;'>" + txtTelefono.Text.Trim() + "</td></tr></table>";
                            cuerpo += "<br /><p style='font-family: Sans-Serif; font-size: 14px; color: #008FC4;'><strong>Comentario :</strong></p>";
                            cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'><strong>" + txtMensaje.Text.Trim() + "</strong></p>";

                            receptor = ConfigurationManager.AppSettings["mailReceptor"].ToString();

                            string resp = envia.enviaCorreo(receptor, asunto, cuerpo, strPath);
                            if (resp == "Ok")
                            {
                                muestraMensaje((string)GetLocalResourceObject("OK_Enviado"));
                                txtMensaje.Text = "";
                                ddlClasificacion.SelectedValue = "-1";
                            }
                            else
                                muestraMensaje((string)GetLocalResourceObject("Error_Noenviado"));

                            eliminaArchivos(idProveedor, idUsuario);
                        }
                        catch (Exception ex)
                        {
                            muestraError("btnEnviar", "btnEnviar", "Error", ex.Message);
                        }
                    }
                    else
                        muestraMensaje((string)GetLocalResourceObject("Error_Clasificacion"));
                }
                else
                    muestraMensaje((string)GetLocalResourceObject("Error_Sinmensaje"));
            }
            else
                muestraMensaje((string)GetLocalResourceObject("Error_Correo"));
                
        }

        private void buscaContacto(int _idusuario, int _idproveedor)
        {
            BLUsuarios buscaUsuario = new BLUsuarios();
            BLContacto buscaContacto = new BLContacto();
            try
            {
                List<Usuarios> csUsuario = buscaUsuario.buscaUsuarioXId(_idusuario);
                if (csUsuario.Count > 0)
                {
                    txtNombre.Text = csUsuario[0].Nombre + " " + csUsuario[0].APaterno + " " + csUsuario[0].AMaterno;
                }

                List<Contacto> csContacto = buscaContacto.buscaContacto(_idproveedor);
                if (csContacto.Count > 0)
                {
                    txtCorreo.Text = csContacto[0].Correo;
                    txtTelefono.Text = csContacto[0].Telefono;
                }

            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "buscaContacto", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para llenar el ddl Clasificacion
        /// </summary>
        private void llenaddlComentarios()
        {
            ddlClasificacion.Items.Clear();

            BLClasificacion busca = new BLClasificacion();
            try
            {
                List<Clasificacion> csClasificacion = busca.buscaClasificacion();
                if (csClasificacion.Count > 0)
                {
                    if (Session["Idioma"].ToString() == "es-MX")
                    {
                        ddlClasificacion.DataValueField = "ID_Comentario";
                        ddlClasificacion.DataTextField = "Comentario";
                        ddlClasificacion.Items.Clear();
                        ddlClasificacion.Items.Add(new ListItem("----- Seleccionar -----", "-1"));
                        ddlClasificacion.AppendDataBoundItems = true;
                        ddlClasificacion.DataSource = csClasificacion;
                        ddlClasificacion.DataBind();
                    }
                    else
                    {
                        ddlClasificacion.DataValueField = "ID_Comentario";
                        ddlClasificacion.DataTextField = "Comentario_en";
                        ddlClasificacion.Items.Clear();
                        ddlClasificacion.Items.Add(new ListItem("----- Select -----", "-1"));
                        ddlClasificacion.AppendDataBoundItems = true;
                        ddlClasificacion.DataSource = csClasificacion;
                        ddlClasificacion.DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaddlComentarios", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Meodos para eliminar archivs de el Path
        /// </summary>
        /// <param name="_IdProveedor"></param>
        /// <param name="_IdUsuario"></param>
        private bool eliminaArchivos(int _IdProveedor, int _IdUsuario)
        {
            bool resp = true;
            string strPath = "";

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./UpdateFiles/attach/" +
                _IdProveedor.ToString() + "/" +
                _IdUsuario.ToString() + "/");
            try
            {
                //Validamos los archivos a procesar
                DirectoryInfo rootDir = new DirectoryInfo(strPath);
                if (Directory.Exists(strPath))
                {
                    FileInfo[] files = null;
                    files = rootDir.GetFiles("*.*");

                    if (files != null)
                    {
                        foreach (FileInfo fi in files)
                        {
                            fi.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("btnEnviar", "eliminaArchivos", "Error", ex.Message);
            }

            return resp;
        }

        #region Manejo de errores
        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Comentarios", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Comentarios", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }
        #endregion
    }
}