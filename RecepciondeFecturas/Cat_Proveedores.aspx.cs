﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using System.Data;

namespace RecepciondeFecturas
{
    public partial class Cat_Proveedores : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    int idProveedor = -1;
                    int idUsuario = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

                    Session["AccionProveedores"] = null;

                    llenaGrid();
                }
            }
        }

        protected void dgvProveedores_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton btn = (ImageButton)e.Row.FindControl("Seleccionar");
                string idProveedor = string.Empty;
                idProveedor = e.Row.Cells[1].Text;
                btn.CommandArgument = idProveedor;
            }
        }

        protected void dgvProveedores_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "CSeleccionar")
            {
                int idProveedor = -1;
                idProveedor = Convert.ToInt32(e.CommandArgument.ToString());

                if (idProveedor > -1)
                {
                    datosProveedor(idProveedor);
                    if (btnGuardar.Text == "Aceptar")
                    {
                        habilitaActualiza();
                    }
                    pnlDatos.Visible = true;
                    btnNuevo.Visible = false;
                    btnActualizar.Visible = true;
                    btnBuscar.Visible = false;
                    btnGuardar.Visible = false;
                    btnCancelar.Visible = true;
                }
            }
        }

        protected void dgvProveedores_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvProveedores.PageIndex = e.NewPageIndex;
            llenaGrid();
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            //Limpia textos
            limpiaTextos();

            //Habilita textos
            habiltaTextos();

            //Muestra panel
            pnlDatos.Visible = true;

            //Habilitamos botones
            btnNuevo.Visible = false;
            btnActualizar.Visible = false;
            btnBuscar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            dgvProveedores.Enabled = false;
            Session["AccionProveedores"] = "Nuevo";
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            habilitaActualiza();
            btnNuevo.Visible = false;
            btnActualizar.Visible = false;
            btnBuscar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            dgvProveedores.Enabled = false;
            Session["AccionProveedores"] = "Actualizar";
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            //Limpia textos
            limpiaTextos();

            //Habilita busqueda
            habilitaBusqueda();

            //Muestra panel
            pnlDatos.Visible = true;

            //Habilitamos botones
            btnNuevo.Visible = false;
            btnBuscar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            //dgvProveedores.Enabled = false;
            btnGuardar.Text = "Aceptar";

            chbValido.Checked = true;

            Session["AccionProveedores"] = "Buscar";
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            string strAccion = string.Empty;

            if (Session["AccionProveedores"] != null)
            {
                strAccion = Session["AccionProveedores"].ToString();

                switch (strAccion)
                {
                    case "Nuevo":
                        #region Guarda registro
                        //Validamos datos
                        if (validaDatos())
                        {
                            if (validaDuplicado(txtRFC.Text.Trim()))
                            {
                                //Guradamos
                                string idProveedor_SAP = string.Empty;
                                string Nombre = string.Empty;
                                DateTime fAlta = new DateTime();
                                string RFC = string.Empty;
                                string Telefono = string.Empty;
                                string Fax = string.Empty;
                                string Banco = string.Empty;
                                string Cuenta = string.Empty;
                                int Credito = 0;
                                string Direccion = string.Empty;
                                bool Valido = false;
                                bool Addenda = false;
                                
                                idProveedor_SAP = txtIdSAP.Text.Trim();
                                Nombre = txtNombre.Text.Trim();
                                RFC = txtRFC.Text.Trim();
                                fAlta = DateTime.Now;
                                Telefono = txtTelefono.Text == "" ? null : txtTelefono.Text.Trim();
                                Fax = txtFax.Text == "" ? null : txtFax.Text.Trim();
                                Banco = txtBanco.Text == "" ? null : txtBanco.Text.Trim();
                                Cuenta = txtCuenta.Text == "" ? null : txtCuenta.Text.Trim();
                                Credito = txtCredito.Text == "" ? 0 : Convert.ToInt32(txtCredito.Text.Trim());
                                Direccion = txtDireccion.Text == "" ? null : txtDireccion.Text.Trim();
                                Valido = chbValido.Checked == true ? true : false;
                                Addenda = chbAddenda.Checked == true ? true : false;

                                bool respuesta = false;
                                respuesta = insertaProveedor(idProveedor_SAP, Nombre, fAlta, RFC, Telefono, Fax, Banco, Cuenta, Credito, Direccion, Valido, Addenda);
                                if (respuesta)
                                {
                                    muestraError("btnGuardar", "Guarda", "Ok", "Registro guardado correctamente. Proveedor : " + Nombre);
                                    guardaLog("Guardar", "Alta", "Ok", "Registro guardado correctamente. Proveedor : " + Nombre);
                                }

                                llenaGrid();
                                limpiaTextos();
                                inhabilitaTextos();

                                btnNuevo.Visible = true;
                                btnActualizar.Visible = false;
                                btnBuscar.Visible = true;
                                btnGuardar.Visible = false;
                                btnCancelar.Visible = false;
                                pnlDatos.Visible = false;
                                dgvProveedores.Enabled = true;
                            }
                        }
                        #endregion
                        break;
                    case "Actualizar":
                        #region Actualiza Registro
                        if (validaDatosActualiza())
                        {
                            //Guradamos
                            int idProveedor = -1;
                            string Nombre = string.Empty;
                            //DateTime fAlta = new DateTime();
                            string RFC = string.Empty;
                            string Telefono = string.Empty;
                            string Fax = string.Empty;
                            string Banco = string.Empty;
                            string Cuenta = string.Empty;
                            int Credito = 0;
                            string Direccion = string.Empty;
                            bool Valido = false;
                            bool Addenda = false;

                            idProveedor = Convert.ToInt32(txtIdProveedor.Text.Trim());
                            Nombre = txtNombre.Text.Trim();
                            RFC = txtRFC.Text.Trim();
                            Telefono = txtTelefono.Text == "" ? null : txtTelefono.Text.Trim();
                            Fax = txtFax.Text == "" ? null : txtFax.Text.Trim();
                            Banco = txtBanco.Text == "" ? null : txtBanco.Text.Trim();
                            Cuenta = txtCuenta.Text == "" ? null : txtCuenta.Text.Trim();
                            Credito = txtCredito.Text == "" ? 0 : Convert.ToInt32(txtCredito.Text.Trim());
                            Direccion = txtDireccion.Text == "" ? null : txtDireccion.Text.Trim();
                            Valido = chbValido.Checked == true ? true : false;
                            Addenda = chbAddenda.Checked == true ? true : false;

                            bool respuesta = false;
                            respuesta = actualizaProveedor(idProveedor, Nombre, RFC, Telefono, Fax, Banco, Cuenta, Credito, Direccion, Valido, Addenda);
                            if (respuesta)
                            {
                                muestraError("btnGuardar", "Actualiza", "Ok", "Registro Actualizado correctamente. Proveedor : " + Nombre);
                                guardaLog("Guardar", "Actualiza", "Ok", "Registro Actualizado correctamente. Proveedor : " + Nombre);
                            }

                            llenaGrid();
                            limpiaTextos();
                            inhabilitaTextos();

                            btnNuevo.Visible = true;
                            btnActualizar.Visible = false;
                            btnBuscar.Visible = true;
                            btnGuardar.Visible = false;
                            btnCancelar.Visible = false;
                            pnlDatos.Visible = false;
                            dgvProveedores.Enabled = true;

                        }
                        #endregion
                        break;
                    case "Buscar":
                        #region Busca Registro
                        string idSAP = string.Empty;
                        string nomre = string.Empty;
                        string rfc = string.Empty;
                        bool valido = false;

                        idSAP = txtIdSAP.Text == "" ? null : txtIdSAP.Text.Trim();
                        nomre = txtNombre.Text == "" ? null : txtNombre.Text.Trim();
                        rfc = txtRFC.Text == "" ? null : txtRFC.Text.Trim();
                        valido = chbValido.Checked == true ? true : false;

                        buscaRegistro(idSAP, nomre, rfc, valido);
                        #endregion
                        break;
                }

            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiaTextos();
            inhabilitaTextos();
            
            btnNuevo.Visible = true;
            btnActualizar.Visible = false;
            btnBuscar.Visible = true;
            btnGuardar.Visible = false;
            btnGuardar.Text = "Guardar";
            btnCancelar.Visible = false;
            dgvProveedores.Enabled = true;
            pnlDatos.Visible = false;
            Session["AccionProveedores"] = null;
            llenaGrid();
        }

        /// <summary>
        /// Metodo para llenar el Grid
        /// </summary>
        private void llenaGrid()
        {
            BLProveedores buca = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = buca.buscaProveedores();
                if (csProveedores.Count > 0)
                {
                    dgvProveedores.DataSource = csProveedores;
                    dgvProveedores.DataBind();
                }
                else
                {
                    inicializaGrid();
                }
            }
            catch (Exception ex)
            {
                muestraError("Page_Load", "llenaGrid", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para inicializar el Grid
        /// </summary>
        private void inicializaGrid()
        {
            DataTable dtArchivos = new DataTable();
            dtArchivos.Columns.Add(new DataColumn("ID_Proveedor", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("ID_Proveedor_SAP", typeof(Int32)));
            dtArchivos.Columns.Add(new DataColumn("Nombre", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Fecha_Alta", typeof(DateTime)));
            dtArchivos.Columns.Add(new DataColumn("RFC", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Telefono", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Fax", typeof(String)));
            dtArchivos.Columns.Add(new DataColumn("Dias_Credito", typeof(Int32)));

            DataRow drVacio = dtArchivos.NewRow();
            drVacio[0] = 0;
            drVacio[1] = 0;
            drVacio[2] = "";
            drVacio[3] = DateTime.Now;
            drVacio[4] = "";
            drVacio[5] = "";
            drVacio[6] = "";
            drVacio[7] = 0;

            dtArchivos.Rows.Add(drVacio);
            dgvProveedores.DataSource = dtArchivos;
            dgvProveedores.DataBind();
            int totalcolums = dgvProveedores.Rows[0].Cells.Count;
            dgvProveedores.Rows[0].Cells.Clear();
            dgvProveedores.Rows[0].Cells.Add(new TableCell());
            dgvProveedores.Rows[0].Cells[0].ColumnSpan = totalcolums;
            dgvProveedores.Rows[0].Cells[0].Text = "No hay datos para mostrar";
        }

        /// <summary>
        /// Metodo para buscar proveedor por ID
        /// </summary>
        /// <param name="_Id_Proveedor"></param>
        private void datosProveedor(int _Id_Proveedor)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            BLProveedores busca = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = busca.buscaProveedorxID(_Id_Proveedor);
                if (csProveedores.Count > 0)
                {
                    txtIdProveedor.Text = csProveedores[0].ID_Proveedor.ToString();
                    txtIdSAP.Text = csProveedores[0].ID_Proveedor_SAP.ToString();
                    txtNombre.Text = csProveedores[0].Nombre;
                    txtRFC.Text = csProveedores[0].RFC;
                    txtTelefono.Text = csProveedores[0].Telefono;
                    txtFax.Text = csProveedores[0].Fax;
                    txtBanco.Text = csProveedores[0].Banco;
                    txtCuenta.Text = csProveedores[0].Cuenta_Bancaria;
                    txtCredito.Text = csProveedores[0].Dias_Credito.ToString();
                    txtDireccion.Text = csProveedores[0].Direccion;
                    chbValido.Checked = csProveedores[0].Valido;
                    chbAddenda.Checked = csProveedores[0].Addenda;
                }
            }
            catch (Exception ex)
            {
                muestraError("dgvProveedores", "datosProveedor", "Error", ex.Message);
            }

        }

        /// <summary>
        /// Metodo para limpar textos
        /// </summary>
        private void limpiaTextos()
        {
            txtIdProveedor.Text = string.Empty;
            txtIdSAP.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtRFC.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtBanco.Text = string.Empty;
            txtCuenta.Text = string.Empty;
            txtCredito.Text = string.Empty;
            txtDireccion.Text = string.Empty;
        }

        /// <summary>
        /// Metodo para habilitar textos
        /// </summary>
        private void habiltaTextos()
        {
            txtIdSAP.Enabled = true;
            txtNombre.Enabled = true;
            txtRFC.Enabled = true;
            txtTelefono.Enabled = true;
            txtFax.Enabled = true;
            txtBanco.Enabled = true;
            txtCuenta.Enabled = true;
            txtCredito.Enabled = true;
            txtDireccion.Enabled = true;
            chbValido.Enabled = true;
            chbAddenda.Enabled = true;
        }

        /// <summary>
        /// Metodo para habilitar textos para actualizacion
        /// </summary>
        private void habilitaActualiza()
        {
            txtNombre.Enabled = true;
            txtTelefono.Enabled = true;
            txtFax.Enabled = true;
            txtBanco.Enabled = true;
            txtCuenta.Enabled = true;
            txtCredito.Enabled = true;
            txtDireccion.Enabled = true;
            chbValido.Enabled = true;
            chbAddenda.Enabled = true;
        }

        /// <summary>
        /// Metodo para habiltar textos de busqueda
        /// </summary>
        private void habilitaBusqueda()
        {
            txtIdSAP.Enabled = true;
            txtNombre.Enabled = true;
            txtRFC.Enabled = true;
            chbValido.Enabled = true;
        }

        /// <summary>
        /// Metodo para validar datos
        /// </summary>
        /// <returns></returns>
        private bool validaDatos()
        {
            bool bResp = true;
            string idProveedor = string.Empty;
            string nombre = string.Empty;
            string rfc = string.Empty;

            idProveedor = txtIdSAP.Text.Trim();
            nombre = txtNombre.Text.Trim();
            rfc = txtRFC.Text.Trim();

            if (idProveedor.ToString().Length < 4)
            {
                muestraMensaje("Ingresar Id de Proveedor de SAP o ID demaciado corto");
                return false;
            }
            else
                txtIdSAP.Text = idProveedor.ToString();

            if (nombre.Length < 10)
            {
                muestraMensaje("Ingresar Nombre de Proveedor o Nombre demaciado corto");
                return false;
            }
            else
                txtNombre.Text = nombre;

            if (rfc.Length < 10)
            {
                muestraMensaje("Ingresar RFC de Proveedor o RFC demaciado corta");
                return false;
            }
            else
                txtRFC.Text = rfc;

            return bResp;
        }

        /// <summary>
        /// Metodo para validar RFC Duplicado
        /// </summary>
        /// <param name="_rfc"></param>
        /// <returns></returns>
        private bool validaDuplicado(string _rfc)
        {
            bool bResp = true;
            BLProveedores busca = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = busca.buscaProveedor(_rfc);
                if (csProveedores.Count > 0)
                {
                    string nombre = "";
                    nombre = csProveedores[0].Nombre;
                    muestraMensaje("No se puede duplicar el RFC. El Proveedor : " + nombre + " ya tiene ese RFC");
                    bResp = false;
                }
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "validaDuplicado", "Error", ex.Message);
            }
            return bResp;
        }

        /// <summary>
        /// Metodo para insertar Registro
        /// </summary>
        /// <param name="_IdSAP"></param>
        /// <param name="_nombre"></param>
        /// <param name="_falta"></param>
        /// <param name="_rfc"></param>
        /// <param name="_telefono"></param>
        /// <param name="_fax"></param>
        /// <param name="_banco"></param>
        /// <param name="_cuenta"></param>
        /// <param name="_credito"></param>
        /// <param name="_direccion"></param>
        /// <param name="_valido"></param>
        /// <returns></returns>
        private bool insertaProveedor(string _IdSAP, string _nombre, DateTime _falta, string _rfc, string _telefono, 
            string _fax, string _banco, string _cuenta, int _credito, string _direccion, 
            bool _valido, bool _addenda)
        {
            bool bResp = false;
            BLProveedores inserta = new BLProveedores();
            try
            {
                bResp = inserta.insertaProveedor(_IdSAP, _nombre, _falta, _rfc, _telefono, _fax, _banco, _cuenta, _credito, _direccion, _valido, _addenda);
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "insertaSociedad", "Error", ex.Message);
            }

            return bResp;

        }

        /// <summary>
        /// Metodo para inhabilitar textos
        /// </summary>
        private void inhabilitaTextos()
        {
            txtIdSAP.Enabled = false;
            txtNombre.Enabled = false;
            txtRFC.Enabled = false;
            txtTelefono.Enabled = false;
            txtFax.Enabled = false;
            txtBanco.Enabled = false;
            txtCuenta.Enabled = false;
            txtCredito.Enabled = false;
            txtDireccion.Enabled = false;
            chbValido.Enabled = false;
            chbAddenda.Enabled = false;
        }

        /// <summary>
        /// Metodo para validar datos de actualizacion
        /// </summary>
        /// <returns></returns>
        private bool validaDatosActualiza()
        {
            bool bResp = true;
            string nombre = string.Empty;

            nombre = txtNombre.Text.Trim();

            if (nombre.Length < 10)
            {
                muestraMensaje("Ingresar Nombre de Proveedor o Nombre demaciado corto");
                return false;
            }
            else
                txtNombre.Text = nombre;

            return bResp;
        }

        /// <summary>
        /// Metodo para actualizar registro
        /// </summary>
        /// <param name="_ID_Proveedor"></param>
        /// <param name="_nombre"></param>
        /// <param name="_telefono"></param>
        /// <param name="_fax"></param>
        /// <param name="_banco"></param>
        /// <param name="_cunta"></param>
        /// <param name="_credito"></param>
        /// <param name="_direccion"></param>
        /// <param name="_valido"></param>
        /// <returns></returns>
        private bool actualizaProveedor(int _ID_Proveedor, string _nombre, string _rfc, string _telefono, string _fax, 
            string _banco, string _cunta, int _credito, string _direccion, bool _valido, bool _addenda)
        {
            bool bResp = false;
            BLProveedores actualiza = new BLProveedores();
            try
            {
                bResp = actualiza.actualizaProveedor(_ID_Proveedor, _nombre, _rfc, _telefono, _fax, _banco, _cunta, _credito, _direccion, _valido, _addenda);
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "actualizaProveedor", "Error", ex.Message);
            }

            return bResp;
        }

        /// <summary>
        /// Metodo para buscar proveedores
        /// </summary>
        /// <param name="_IdProveedor_sap"></param>
        /// <param name="_nombre"></param>
        /// <param name="_rfc"></param>
        /// <param name="_valido"></param>
        private void buscaRegistro(string _IdProveedor_sap, string _nombre, string _rfc, bool _valido)
        {
            BLProveedores busca = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = busca.buscaProveedor(_IdProveedor_sap, _nombre, _rfc, _valido);
                if (csProveedores.Count > 0)
                {
                    dgvProveedores.DataSource = csProveedores;
                    dgvProveedores.DataBind();
                }
                else
                    inicializaGrid();
            }
            catch (Exception ex)
            {
                muestraError("btnGuardar", "buscaRegistro", "Error", ex.Message);
            }

        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Proveedores", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Proveedores", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

    }
}