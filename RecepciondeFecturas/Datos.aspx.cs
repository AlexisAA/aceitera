﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text.RegularExpressions;
using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Catalogos;
using BLRecepcion.Varios;

namespace RecepciondeFecturas
{
    public partial class Datos : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Session["id_Usuario"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Default.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    Session["Contacto"] = null;
                    Session["Boton"] = null;
                    int idProveedor = -1;
                    idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());
                    buscaNombre(idProveedor);
                    buscaContacto(idProveedor);
                }
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["Idioma"] == null)
                UICulture = "es-MX";
            else
                UICulture = Session["Idioma"].ToString();

            base.InitializeCulture();
        }

        protected void btnCambiar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";
            mppCambia.Show();
        }

        protected void btnAceptaCambia_Click(object sender, EventArgs e)
        {
            //Primero validamos los textos
            string strOldpass = string.Empty;
            string strNewpass = string.Empty;
            string strConfpass = string.Empty;
            int Id_Usuario = -1;
            strOldpass = txtOldpass.Text.Trim();
            strNewpass = txtNewpass.Text.Trim();
            strConfpass = txtConfpass.Text.Trim();

            Seguridad seg = new Seguridad();
            if (Session["id_Usuario"] != null)
                Id_Usuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            if (strOldpass.Length >= 8)
            {
                //VAlidamos que la contraseña actual corresponda
                if (validaOldpass(strOldpass))
                {
                    if (strNewpass.Length >= 8)
                    {
                        if (seg.encriptar(strNewpass) == seg.encriptar(strConfpass))
                        {
                            //Cambiamos la contraseña
                            bool resp = false;

                            resp = cambiaContrasenia(Id_Usuario, seg.encriptar(strNewpass));
                            if (resp)
                                muestraError("CambiaPass", "CAmbiaPass", "Ok", (string)GetLocalResourceObject("Ok_cambiopass"));
                            else
                                muestraError("CambiaPass", "CAmbiaPass", "Error", (string)GetLocalResourceObject("Error_cambiopass"));
                        }
                        else
                            muestraMensaje((string)GetLocalResourceObject("Error_passDesigual"));
                    }
                    else
                        muestraMensaje((string)GetLocalResourceObject("Error_oldpassInvalido"));
                }
                else
                    muestraMensaje((string)GetLocalResourceObject("Error_passErroneo"));
            }
            else
                muestraMensaje((string)GetLocalResourceObject("Error_passInvalido"));
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";
            Session["Boton"] = "Nuevo";
            limpiaTextos();
            habilitaTextos();
            btnNuevo.Visible = false;
            btnGuardar.Visible = true;
            btnActualizar.Visible = false;
            btnCancelar.Visible = true;
            btnCambiar.Visible = false;

            txtNombreContacto.Focus();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            //VAlidamos la información.
            if (Session["Boton"] != null)
            {
                string respuesta = string.Empty;
                respuesta = validaTextos();
                if (respuesta == string.Empty)
                {
                    string _nombre = string.Empty;
                    string _telefono = string.Empty;
                    string _celular = string.Empty;
                    string _correo = string.Empty;
                    int _idProveedor = -1;

                    _nombre = txtNombreContacto.Text.Trim();
                    _telefono = txtTelContacto.Text.Trim();
                    _celular = txtCelContacto.Text.Trim();
                    _correo = txtCorreoContacto.Text.Trim();
                    _idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());

                    string sSesion = string.Empty;
                    sSesion = Session["Boton"].ToString();
                    if (sSesion == "Nuevo")
                    {
                        //Guardamos
                        BLContacto guarda = new BLContacto();
                        try
                        {
                            bool resp = guarda.insertaContacto(_nombre, _telefono, _celular, _correo, _idProveedor);
                            muestraError("btnGuardar", "Guarda", "OK", (string)GetLocalResourceObject("Ok_Contacto_Guardado"));
                        }
                        catch (Exception ex)
                        {
                            muestraError("btnGuardar", "Guarda", "Error", ex.Message);
                        }
                    }
                    else
                    {
                        //Actualizamos
                        int _idContacto = -1;
                        _idContacto = Convert.ToInt32(Session["Contacto"].ToString());
                        BLContacto actualiza = new BLContacto();
                        try
                        {
                            bool resp = actualiza.actualizaContacto(_idContacto, _nombre, _telefono, _celular, _correo);
                            muestraError("btnGuardar", "Actualiza", "OK", (string)GetLocalResourceObject("Ok_Contacto_Actualizado"));
                        }
                        catch (Exception ex)
                        {
                            muestraError("btnGuardar", "Guarda", "Error", ex.Message);
                        }
                    }

                    inhabilitaTextos();
                    btnNuevo.Visible = false;
                    btnGuardar.Visible = false;
                    btnActualizar.Visible = true;
                    btnCancelar.Visible = false;
                    btnCambiar.Visible = true;
                }
                else
                {
                    muestraMensaje(respuesta);
                }
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";
            Session["Boton"] = "Actualizar";
            habilitaTextos();
            btnNuevo.Visible = false;
            btnGuardar.Visible = true;
            btnActualizar.Visible = false;
            btnCancelar.Visible = true;
            btnCambiar.Visible = false;

            txtNombreContacto.Focus();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";
            int idProveedor = -1;
            idProveedor = Convert.ToInt32(Session["Id_Proveedor"].ToString());

            btnNuevo.Visible = false;
            btnGuardar.Visible = false;
            btnActualizar.Visible = false;
            btnCancelar.Visible = false;
            btnCambiar.Visible = true;

            buscaContacto(idProveedor);
            inhabilitaTextos();
        }

        /// <summary>
        /// Metodo para buscar el nombe del proveedor
        /// </summary>
        /// <param name="_id_Proveedor"></param>
        /// <returns></returns>
        private void buscaNombre(int _id_Proveedor)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            BLProveedores buscaProveedor = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = buscaProveedor.buscaProveedorxID(_id_Proveedor);
                if (csProveedores.Count > 0)
                {
                    txtIdproveedorSAP.Text = csProveedores[0].ID_Proveedor_SAP.ToString();
                    txtRazonsocial.Text = csProveedores[0].Nombre;
                    txtRFC.Text = csProveedores[0].RFC;
                    txtDireccion.Text = csProveedores[0].Direccion;
                    txtTelefono.Text = csProveedores[0].Telefono;
                    txtFax.Text = csProveedores[0].Fax;
                    txtDiascredito.Text = csProveedores[0].Dias_Credito.ToString();
                    txtBanco.Text = csProveedores[0].Banco;
                    txtCtabancaria.Text = csProveedores[0].Cuenta_Bancaria;
                    txtCreaAddenda.Text = csProveedores[0].Addenda == true ? "Si" : "No";
                }
            }
            catch (Exception ex)
            {
                muestraError("buscaNombre", "buscaNombre", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para buscar contacto
        /// </summary>
        /// <param name="_id_Proveedor"></param>
        private void buscaContacto(int _id_Proveedor)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";
            BLContacto buscaContacto = new BLContacto();
            try
            {
                List<Contacto> csContacto = buscaContacto.buscaContacto(_id_Proveedor);
                if (csContacto.Count > 0)
                {
                    txtNombreContacto.Text = csContacto[0].Nombre;
                    txtTelContacto.Text = csContacto[0].Telefono;
                    txtCelContacto.Text = csContacto[0].Celular;
                    txtCorreoContacto.Text = csContacto[0].Correo;
                    Session["Contacto"] = csContacto[0].ID_Contacto.ToString();
                    btnActualizar.Visible = true;
                }
                else
                    btnNuevo.Visible = true;
            }
            catch (Exception ex)
            {
                muestraError("buscaContacto", "buscaContacto", "Error", ex.Message);
            }
                
        }

        /// <summary>
        /// Metodo para limpiar textos
        /// </summary>
        private void limpiaTextos()
        {
            txtNombreContacto.Text = string.Empty;
            txtTelContacto.Text = string.Empty;
            txtCelContacto.Text = string.Empty;
            txtCorreoContacto.Text = string.Empty;
        }

        /// <summary>
        /// Metodo para habilitar textos
        /// </summary>
        private void habilitaTextos()
        {
            txtNombreContacto.Enabled = true;
            txtTelContacto.Enabled = true;
            txtCelContacto.Enabled = true;
            txtCorreoContacto.Enabled = true;
        }

        /// <summary>
        /// Metodo para validar campos de contacto.
        /// </summary>
        /// <returns></returns>
        private string validaTextos()
        {
            string strResp = string.Empty;
            string nombre = string.Empty;
            string telefono = string.Empty;
            string celular = string.Empty;
            string correo = string.Empty;
            string strFormat = string.Empty;
            strFormat = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            nombre = txtNombreContacto.Text.Trim();
            telefono = txtTelContacto.Text.Trim();
            celular = txtCelContacto.Text.Trim();
            correo = txtCorreoContacto.Text.Trim();
            if (nombre.Length < 5)
                return (string)GetLocalResourceObject("Error_Nombre");
            if (telefono.Length < 10)
                return (string)GetLocalResourceObject("Error_Telefono");
            if (celular.Length < 10)
                return (string)GetLocalResourceObject("Error_Celular");
            if (!Regex.IsMatch(correo, strFormat))
                return (string)GetLocalResourceObject("Error_Correo");

            return strResp;
        }

        /// <summary>
        /// Metodo para inhabilotar textos
        /// </summary>
        private void inhabilitaTextos()
        {
            txtNombreContacto.Enabled = false;
            txtTelContacto.Enabled = false;
            txtCelContacto.Enabled = false;
            txtCorreoContacto.Enabled = false;
        }

        /// <summary>
        /// Metodo para validar la contraseña actual
        /// </summary>
        /// <param name="_oldPass"></param>
        /// <returns></returns>
        private bool validaOldpass(string _oldPass)
        {
            bool resp = false;
            if (Session["id_Usuario"] != null)
            {
                int Id_Usuario = Convert.ToInt32(Session["id_Usuario"].ToString());
                BLUsuarios buscaUsuario = new BLUsuarios();
                Seguridad seg = new Seguridad();
                try
                {
                    List<Usuarios> busca = buscaUsuario.buscaUsuarioXId(Id_Usuario);
                    if (busca.Count > 0)
                    {
                        string pass = string.Empty;
                        pass = busca[0].Contrasenia;
                        if (pass == seg.encriptar(_oldPass))
                            resp = true;
                    }
                }
                catch (Exception ex)
                {
                    muestraError("CambiaPass", "validaOldpass", "Error", ex.Message);
                }
            }

            return resp;
        }

        /// <summary>
        /// Metodo para cambiar contrasenia
        /// </summary>
        /// <param name="_idusuario"></param>
        /// <param name="_contrasenia"></param>
        /// <returns></returns>
        private bool cambiaContrasenia(int _idusuario, string _contrasenia)
        {
            bool resp = false;
            BLUsuarios cambia = new BLUsuarios();
            try
            {
                resp = cambia.cambiaContrasenia(_idusuario, _contrasenia);
            }
            catch (Exception ex)
            {
                muestraError("CambiaPass", "cambiaContrasenia", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("Datos", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

    }
}