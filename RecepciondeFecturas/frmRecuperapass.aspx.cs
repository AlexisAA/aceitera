﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text.RegularExpressions;
using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Catalogos;
using BLRecepcion.Varios;
using System.Configuration;

namespace RecepciondeFecturas
{
    public partial class frmRecuperapass : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Idioma"] == null)
                {
                    Session["Idioma"] = "es-MX";
                }

                txtCorreo.Text = string.Empty;
                txtCorreo.Focus();
            }
        }

        protected override void InitializeCulture()
        {
            if (Session["Idioma"] == null)
            {
                UICulture = "ES";
            }
            else
            {
                UICulture = Session["Idioma"].ToString();
            }

            base.InitializeCulture();
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";

            string correo = string.Empty;
            string strFormat = string.Empty;
            strFormat = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            correo = txtCorreo.Text.Trim();
            if (Regex.IsMatch(correo, strFormat))
            {
                //Buscamos que exista el correo
                int Id_Proveedor = -1;
                Id_Proveedor = buscaContacto(correo);
                if (Id_Proveedor >= 0)
                {
                    recuperaPass(Id_Proveedor);
                }
            }
            else
                muestraMensaje((string)GetLocalResourceObject("Error_Correo"));
        }

        /// <summary>
        /// Metodo para buscar Contacto
        /// </summary>
        /// <param name="_correo"></param>
        /// <returns></returns>
        private int buscaContacto(string _correo)
        {
            int Id_Proveedor = -1;
            BLContacto busca = new BLContacto();
            try
            {
                List<Contacto> csContacto = busca.buscaCorreo(_correo);
                if (csContacto.Count > 0)
                {
                    Id_Proveedor = csContacto[0].ID_Proveedor;
                }
            }
            catch (Exception ex)
            {
                muestraError("btnEnviar", "buscaContacto", "Error", ex.Message);
            }

            return Id_Proveedor;
        }

        /// <summary>
        /// Metodo para recuperar contraseña
        /// </summary>
        /// <param name="_idproveedor"></param>
        private void recuperaPass(int _idproveedor)
        {
            //Primero buscamos los datos del proveedor
            BLProveedores buscaP = new BLProveedores();
            BLUsuarios busca = new BLUsuarios();
            try
            {
                List<Proveedores> csProveedores = buscaP.buscaProveedorxID(_idproveedor);
                if (csProveedores.Count > 0)
                {
                    string nombre = string.Empty;
                    string idproveedorsap = string.Empty;
                    string usuario = string.Empty;
                    string contrasenia = string.Empty;

                    nombre = csProveedores[0].Nombre;
                    idproveedorsap = csProveedores[0].ID_Proveedor_SAP;

                    List<Usuarios> csUsuarios = busca.buscaUsuariosxproveedor(_idproveedor, true);
                    if (csUsuarios.Count > 0)
                    {
                        Seguridad seg = new Seguridad();
                        usuario = csUsuarios[0].Usuario;
                        contrasenia = seg.desencriptar(csUsuarios[0].Contrasenia.ToString());

                        string resp = string.Empty;
                        resp = enviaCorreo(nombre, idproveedorsap, usuario, contrasenia);
                        if (resp == "Ok")
                        {
                            muestraError("recuperaPass", "recuperaPass", "Ok", (string)GetLocalResourceObject("Correo_Enviado"));
                        }
                        else
                        {
                            muestraError("recuperaPass", "recuperaPass", "Error", (string)GetLocalResourceObject("Correo_Noenviado"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                muestraError("btnEnviar", "enviaCorreo", "Error", ex.Message);
            }
        }

        /// <summary>
        /// Metodo para envíar correo
        /// </summary>
        /// <param name="_nombreproveedor"></param>
        /// <param name="_idproveedorsap"></param>
        /// <param name="_usuario"></param>
        /// <param name="_contrasenia"></param>
        /// <returns></returns>
        private string enviaCorreo(string _nombreproveedor, string _idproveedorsap, string _usuario, string _contrasenia)
        {
            string resp = string.Empty;

            EnviaCorreo envia = new EnviaCorreo();
            try
            {
                string strIdioma = string.Empty;
                string cuerpo = string.Empty;
                string receptor = txtCorreo.Text.Trim();
                string asunto = string.Empty;

                if (Session["Idioma"] != null)
                    strIdioma = Session["Idioma"].ToString();
                else
                    strIdioma = "es-MX";
                

                if (strIdioma == "es-MX")
                {
                    asunto = "Recuperación de contraseña";
                    cuerpo = "<body><p style='font-family: Sans-Serif; font-size: 18px; color: #008FC4;'><strong>INDUSTRIAL ACEITERA - RECUPERACIÓN DE CONTRASEÑA</strong></p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>El motivo del siguiente correo es proporcionar nuevamente los datos de acceso al portal de Recepcion de Facturas de Industrial Aceitera S.A. de C.V. ya que usted lo ha solicitado desde el portal</p>";
                    cuerpo += "<table style='font-family: Sans-Serif; font-size: 12px; color: #000000; margin-left: 50px'>";
                    cuerpo += "<tr><td style='text-align: right;'>Proveedor :</td><td style='text-align: left;'>" + _nombreproveedor + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>ID Proveedor :</td><td style='text-align: left;'>" + _idproveedorsap + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>Usuario :</td><td style='text-align: left;'>" + _usuario + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>Contraseña :</td><td style='text-align: left;'>" + _contrasenia + "</td></tr></table>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Es importante que a la brevedad cambie la contraseña en el Menú Mis Datos - Cambiar contraseña</p>";
                    cuerpo += "<br /><p style='font-family: Sans-Serif; font-size: 14px; color: #008FC4;'><strong>Atentamente</strong></p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #008FC4;'><strong>CUENTAS POR PAGAR - INDUSTRIAL ACEITERA</strong></p>";
                    cuerpo += "<a href='http://aceitera.com.mx'>Industrial Aceitera</a></body>";
                }
                else
                {
                    asunto = "Password Recovery";
                    cuerpo = "<body><p style='font-family: Sans-Serif; font-size: 18px; color: #008FC4;'><strong>INDUSTRIAL ACEITERA - PASSWORD RECOVERY</strong></p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>The reason of this mail is to provide data access to the portal again invoices of Industrial Aceitera S.A. de C.V. as you have requested from the portal</p>";
                    cuerpo += "<table style='font-family: Sans-Serif; font-size: 12px; color: #000000; margin-left: 50px'>";
                    cuerpo += "<tr><td style='text-align: right;'>Provider :</td><td style='text-align: left;'>" + _nombreproveedor + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>ID Provider :</td><td style='text-align: left;'>" + _idproveedorsap + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>User :</td><td style='text-align: left;'>" + _usuario + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>Password :</td><td style='text-align: left;'>" + _contrasenia + "</td></tr></table>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>It is important to promptly change the password in the My Data - Change Password</p>";
                    cuerpo += "<br /><p style='font-family: Sans-Serif; font-size: 14px; color: #008FC4;'><strong>Sincerely</strong></p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #008FC4;'><strong>DEBTS TO PAY - INDUSTRIAL ACEITERA</strong></p>";
                    cuerpo += "<a href='http://aceitera.com.mx'>Industrial Aceitera</a></body>";
                }

                resp = envia.enviaCorreo(receptor, asunto, cuerpo);

            }
            catch (Exception ex)
            {
                guardaLog("enviaCorreo", "enviaCorreo", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para mostrar mensaje
        /// </summary>
        /// <param name="_mensaje"></param>
        private void muestraMensaje(string _mensaje)
        {
            lblMensaje.Text = _mensaje;
            lblMensaje.Visible = true;
        }

        /// <summary>
        /// Metodo para mostrar error y guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void muestraError(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("RecuperaPass", _evento, _accion, _respuesta, _descripción, idUsuario);
                muestraMensaje(_descripción);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = -1;
            if (Session["id_Usuario"] == null)
                idUsuario = 0;
            else
                idUsuario = Convert.ToInt32(Session["id_Usuario"].ToString());

            BLLogs guardaLog = new BLLogs();
            try
            {
                bool insertaLog = guardaLog.insertaLog("RecuperaPass", _evento, _accion, _respuesta, _descripción, idUsuario);
            }
            catch (Exception ex)
            {
                muestraMensaje(ex.Message);
            }
        }

    }
}