﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cat_Avisos.aspx.cs" Inherits="RecepciondeFecturas.Cat_Avisos" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="Style/Site.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //localizar timers
        var iddleTimeoutWarning = null;
        var iddleTimeout = null;

        //esta funcion automaticamente sera llamada por ASP.NET AJAX cuando la pagina cargue y un postback parcial complete
        function pageLoad() {
            //borrar antiguos timers de postbacks anteriores
            if (iddleTimeoutWarning != null)
                clearTimeout(iddleTimeoutWarning);
            if (iddleTimeout != null)
                clearTimeout(iddleTimeout);
            //leer tiempos desde web.config
            var millisecTimeOutWarning = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeoutWarning"]) * 60 * 1000 %>;
            //var millisecTimeOut = Session.Timeout;
            var millisecTimeOut = <%= int.Parse(System.Configuration.ConfigurationManager.AppSettings["SessionTimeout"]) * 60 * 1000 %>;

            //establece tiempo para mostrar advertencia si el usuario ha estado inactivo
            iddleTimeoutWarning = setTimeout("DisplayIddleWarning()", millisecTimeOutWarning);
            iddleTimeout = setTimeout("TimeoutPage()", millisecTimeOut);
        }

        function DisplayIddleWarning() {
            alert("Tu sesion esta a punto de expirar en 5 minutos debido a inactividad.");
        }

        function TimeoutPage() {
            window.location = "~/Default.aspx"; // .reload();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mainDatos">
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="True" CssClass="Textonormal"
            meta:resourcekey="lblMensajeResource1"></asp:Label>
        <div class="divBienvenido">
            <asp:Label ID="lblAvisos" runat="server" Text="Avisos" CssClass="Titulo1"></asp:Label>
        </div>

        <table style="width: 100%; height: 100%; text-align: center; vertical-align: middle;">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblProveedor" runat="server" Text="Proveedor :" CssClass="Textonormal"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlProveedor" runat="server" CssClass="Textonormal" 
                                    Width="500px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblAvisoses" runat="server" Text="Español" CssClass="subTitulo1"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblTituloes" runat="server" Text="Titulo :" CssClass="Textonormal"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtTituloes" runat="server" CssClass="Textonormal" 
                                    Width="500px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblMensajees" runat="server" Text="Mensaje :" CssClass="Textonormal"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtMensajees" runat="server" CssClass="Textonormal" TextMode="MultiLine" 
                                    Width="500px" Height="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblAvisosen" runat="server" Text="Inglés" CssClass="subTitulo1"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblTituloen" runat="server" Text="Titulo :" CssClass="Textonormal"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtTituloen" runat="server" CssClass="Textonormal" 
                                    Width="500px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblMensajeeen" runat="server" Text="Mensaje :" CssClass="Textonormal"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtMensajeen" runat="server" CssClass="Textonormal" TextMode="MultiLine" 
                                    Width="500px" Height="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="botonchico" 
            onclick="btnGuardar_Click" />&nbsp;
    </div>
    
</asp:Content>
