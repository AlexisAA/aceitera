﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.Configuration;
using System.IO;
using System.Xml;
using BLRecepcion.Catalogos;
using BLRecepcion.Portal;
using DALRecepcion.Bean.Portal;
using DALRecepcion.Bean.Catalogos;

namespace wsMovimiento
{
    /// <summary>
    /// ServicioWeb para el registro de movimientos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class Servicio_Mov : System.Web.Services.WebService
    {

        [WebMethod(Description = "Metodo que recibe las notificaciones de SAP en una cadena con base 64")]
        public string notificacionesSAP(string cadenabase64)
        {

            #region Variables locales
            string Respuesta = string.Empty;
            string strPath = string.Empty;
            string nombreArchivo = string.Empty;
            #endregion

            #region Metodo y variables utilizado solo para pruebas
            string respConversion = string.Empty;
            strPath = Server.MapPath(@"./tmp/" + "/");
            nombreArchivo = "notificacion.xml";
            if (Directory.Exists(strPath))
            {
                DirectoryInfo root = new DirectoryInfo(strPath);
                FileInfo[] archivos = null;
                archivos = root.GetFiles(nombreArchivo);
                if (archivos != null && archivos.Count() > 0)
                {
                    respConversion = convierteBase64(strPath + nombreArchivo);
                    cadenabase64 = respConversion;
                }

            }
            #endregion

            #region Metodos para leer cadena y crear XML
            bool respCrea = false;
            respCrea = creaXML(cadenabase64);
            if (respCrea)
            {
                strPath = Server.MapPath(@"./tmp/" + "/");
                nombreArchivo = "notificacion.xml";

                Respuesta = obtenDatosxml(strPath + nombreArchivo);

            }
            else
            {
                Respuesta = "No se puede convertir la cadena recibida";
            }
            #endregion

            return Respuesta;
        }

        /// <summary>
        /// Metodo para extraer la información del XML
        /// </summary>
        /// <param name="_rutaarchivo"></param>
        /// <returns></returns>
        private string obtenDatosxml(string _rutaarchivo)
        {

            #region Variables del metodo
            string respSAP = string.Empty;
            string strRespuesta = string.Empty;
            #endregion

            #region Variables para el XML
            string xmlUUID = string.Empty;
            Int64 xmlPedido = -1;
            string xmlMoneda = string.Empty;
            decimal xmlImporte = 0;
            string xmlAcuse = string.Empty;
            string xmlFecha_Vencimiento = string.Empty;
            string xmlDocto_Compensacion = string.Empty;
            DateTime xmlFecha_Pago = new DateTime();
            string xmlEstatus = string.Empty;
            string xmlObservaciones = string.Empty;
            string xmlId_Proveedor_SAP = string.Empty;
            int xmlId_Sociedad_SAP = -1;
            DateTime xmlFecha_Recepcion = new DateTime();
            string xmlSerie = string.Empty;
            string xmlFolio = string.Empty;
            DateTime xmlFecha_Factura = new DateTime();
            #endregion

            //Abrimos el docuemnto XML
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(_rutaarchivo);

            //Cargamos el nodo principal
            XmlNode nodoPrincipal = xmlDoc.DocumentElement;
            
            XmlAttribute aUUID = nodoPrincipal.Attributes["UUID"];
            if (aUUID != null)
            {
                #region Buscamos los atributos
                XmlAttribute aPedido = nodoPrincipal.Attributes["Pedido"];
                if (aPedido == null)
                    strRespuesta = "No se encontro el atributo Pedido";
                XmlAttribute aMoneda = nodoPrincipal.Attributes["Moneda"];
                if (aMoneda == null)
                    strRespuesta = "No se encontro el atributo Moneda";
                XmlAttribute aImporte = nodoPrincipal.Attributes["Importe"];
                if (aImporte == null)
                    strRespuesta = "No se encontro el atributo Importe";
                XmlAttribute aAcuse = nodoPrincipal.Attributes["Acuse"];
                if (aAcuse == null)
                    strRespuesta = "No se encontro el atributo Acuse";
                XmlAttribute aFVencimiento = nodoPrincipal.Attributes["Fecha_Vencimiento"];
                if (aFVencimiento == null)
                    strRespuesta = "No se encontro el atributo Fecha_Vencimiento";
                XmlAttribute aDCompensacion = nodoPrincipal.Attributes["Docto_Compensacion"];
                if (aDCompensacion == null)
                    strRespuesta = "No se encontro el atributo Docto_Compensacion";
                XmlAttribute aFPago = nodoPrincipal.Attributes["Fecha_Pago"];
                if (aFPago == null)
                    strRespuesta = "No se encontro el atributo Fecha_Pago";
                XmlAttribute aEstatus = nodoPrincipal.Attributes["Estatus"];
                if (aEstatus == null)
                    strRespuesta = "No se encontro el atributo Estatus";
                XmlAttribute aObservaciones = nodoPrincipal.Attributes["Observaciones"];
                if (aObservaciones == null)
                    strRespuesta = "No se encontro el atributo Observaciones";
                XmlAttribute aIdproveedorsap = nodoPrincipal.Attributes["ID_Proveedor_SAP"];
                if (aIdproveedorsap == null)
                    strRespuesta = "No se encontro el atributo ID_Proveedor_SAP";
                XmlAttribute aIdsociedadsap = nodoPrincipal.Attributes["ID_Sociedad_SAP"];
                if (aIdsociedadsap == null)
                    strRespuesta = "No se encontro el atributo ID_Sociedad_SAP";
                XmlAttribute aFRecepcion = nodoPrincipal.Attributes["Fecha_Recepcion"];
                if (aFRecepcion == null)
                    strRespuesta = "No se encontro el atributo Fecha_Recepcion";
                XmlAttribute aSerie = nodoPrincipal.Attributes["Serie"];
                if (aSerie == null)
                    strRespuesta = "No se encontro el atributo Serie";
                XmlAttribute aFolio = nodoPrincipal.Attributes["Folio"];
                if (aFolio == null)
                    strRespuesta = "No se encontro el atributo Folio";
                XmlAttribute aFFactura = nodoPrincipal.Attributes["Fecha_Factura"];
                if (aFFactura == null)
                    strRespuesta = "No se encontro el atributo Fecha_Factura";
                #endregion

                if (strRespuesta == string.Empty)
                {
                    xmlUUID = aUUID.Value.ToString();
                    if (xmlUUID != string.Empty)
                    {
                        #region Asignacion de datos
                        try
                        {
                            xmlPedido = aPedido.Value.ToString() == "" ? 0 : Convert.ToInt64(aPedido.Value.ToString());
                            xmlMoneda = aMoneda.Value.ToString() == "" ? null : aMoneda.Value.ToString();
                            xmlImporte = aImporte.Value.ToString() == "" ? 0 : Convert.ToDecimal(aImporte.Value.ToString());
                            xmlAcuse = aAcuse.Value.ToString() == "" ? null : aAcuse.Value.ToString();
                            xmlFecha_Vencimiento = aFVencimiento.Value.ToString() == "" ? null : aFVencimiento.Value.ToString();
                            xmlDocto_Compensacion = aDCompensacion.Value.ToString() == "" ? null : aDCompensacion.Value.ToString();
                            xmlFecha_Pago = aFPago.Value.ToString() == "" ? DateTime.Today : Convert.ToDateTime(aFPago.Value.ToString());
                            xmlEstatus = aEstatus.Value.ToString() == "" ? null : aEstatus.Value.ToString();
                            xmlObservaciones = aObservaciones.Value.ToString() == "" ? null : aObservaciones.Value.ToString();
                            xmlId_Proveedor_SAP = aIdproveedorsap.Value.ToString() == "" ? null : aIdproveedorsap.Value.ToString();
                            xmlId_Sociedad_SAP = aIdsociedadsap.Value.ToString() == "" ? 0 : Convert.ToInt32(aIdsociedadsap.Value.ToString());
                            xmlFecha_Recepcion = aFRecepcion.Value.ToString() == "" ? DateTime.Today : Convert.ToDateTime(aFRecepcion.Value.ToString());
                            xmlSerie = aSerie.Value.ToString() == "" ? null : aSerie.Value.ToString();
                            xmlFolio = aFolio.Value.ToString() == "" ? null : aFolio.Value.ToString();
                            xmlFecha_Factura = aFFactura.Value.ToString() == "" ? DateTime.Today : Convert.ToDateTime(aFFactura.Value.ToString());
                        }
                        catch (Exception ex)
                        {
                            strRespuesta = ex.Message;
                        }
                        #endregion

                        #region Accion
                        #region Clases consultadas
                        BLArchivos buscaArchivo = null;
                        BLProgramacion buscaProgramacion = null;
                        BLProveedores buscaProveedor = null;
                        BLSociedades buscaSociedad = null;
                        BLMonedas buscaMoneda = null;
                        BLHistorial buscaHistorial = null;
                        #endregion

                        #region Variables Locales
                        int Id_Recepcion = -1;
                        int Id_Estatus = -1;
                        int Id_Proveedor = -1;
                        int Id_Sociedad = -1;
                        int Id_Programacion = -1;
                        int Id_Historial = -1;
                        #endregion

                        switch (xmlEstatus)
                        {
                            case "PRG":
                                #region Programación
                                Id_Estatus = 6;         //Factura propramada para pago
                                buscaArchivo = new BLArchivos();
                                buscaProgramacion = new BLProgramacion();
                                try
                                {
                                    //Buscamos la carga del archivo
                                    List<Archivos> csArchivos = buscaArchivo.buscaArchivoxuuid(xmlUUID);
                                    if (csArchivos.Count > 0)
                                    {
                                        #region Registros cargados desde el portal
                                        //Validamos que no exista en programación
                                        Id_Recepcion = csArchivos[0].Id_Recepcion;
                                        List<Programacion> csProgramacion = buscaProgramacion.buscaProgramacion(xmlUUID, true);
                                        if (csProgramacion.Count == 0)
                                        {
                                            bool x = insertaProgramacion(Id_Recepcion, xmlMoneda, xmlPedido, xmlAcuse, xmlFecha_Vencimiento, Id_Estatus, xmlObservaciones);
                                            if (x)
                                            {
                                                guardaLog("Programacion", "Inserta", "Ok", "Programacion insertada correctamente. Acuse : " + xmlAcuse);
                                                strRespuesta = "Exito";
                                            }
                                        }
                                        else
                                        {
                                            Id_Programacion = csProgramacion[0].ID_Programacion;
                                            bool x = actualizaProgramacion(Id_Programacion, xmlSerie, xmlFolio, xmlImporte, xmlMoneda, xmlPedido, xmlAcuse, xmlFecha_Vencimiento, xmlObservaciones);
                                            if (x)
                                            {
                                                guardaLog("Programacion", "Actualiza", "Ok", "Programacion actualizada correctamente. Acuse : " + xmlAcuse);
                                                strRespuesta = "Exito";
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Registros no cargados desde el portal
                                        List<Programacion> csProgramacion = buscaProgramacion.buscaProgramacion(xmlUUID, true);
                                        if (csProgramacion.Count == 0)
                                        {
                                            //Buscamos los indices
                                            buscaProveedor = new BLProveedores();
                                            List<Proveedores> csProveedores = buscaProveedor.buscaProveedores(xmlId_Proveedor_SAP);
                                            if (csProveedores.Count > 0)
                                            {
                                                Id_Proveedor = csProveedores[0].ID_Proveedor;
                                                buscaSociedad = new BLSociedades();
                                                List<Sociedades> csSociedades = buscaSociedad.buscaIdsociedad(xmlId_Sociedad_SAP);
                                                if (csSociedades.Count > 0)
                                                {
                                                    Id_Sociedad = csSociedades[0].Id_Sociedad;
                                                    buscaMoneda = new BLMonedas();
                                                    List<Monedas> csMonedas = buscaMoneda.buscaMoneda(xmlMoneda);
                                                    if (csMonedas.Count > 0)
                                                    {
                                                        Id_Recepcion = 0;
                                                        xmlObservaciones = "Factura no cargada desde el Portal - " + xmlObservaciones;
                                                        bool x = insertaProgramacion(Id_Recepcion, Id_Proveedor, Id_Sociedad, xmlFecha_Recepcion, xmlSerie,
                                                            xmlFolio, xmlUUID, xmlFecha_Factura, xmlImporte, xmlMoneda,
                                                            xmlPedido, xmlAcuse, xmlFecha_Vencimiento, xmlObservaciones);
                                                        if (x)
                                                        {
                                                            guardaLog("Programacion", "Inserta", "Ok", "Programacion insertada correctamente. Acuse : " + xmlAcuse);
                                                            strRespuesta = "Exito";
                                                        }
                                                    }
                                                    else
                                                        strRespuesta = "No se encontro la Moneda : " + xmlMoneda + " en la base de datos.";
                                                }
                                                else
                                                    strRespuesta = "No se encontro la Sociedad : " + xmlId_Sociedad_SAP + " en la base de datos.";
                                            }
                                            else
                                                strRespuesta = "No se encontro el Proveedor : " + xmlId_Proveedor_SAP + " en la base de datos.";
                                        }
                                        else
                                        {
                                            Id_Programacion = csProgramacion[0].ID_Programacion;
                                            bool x = actualizaProgramacion(Id_Programacion, xmlSerie, xmlFolio, xmlImporte, xmlMoneda, xmlPedido, xmlAcuse, xmlFecha_Vencimiento, xmlObservaciones);
                                            if (x)
                                            {
                                                guardaLog("Programacion", "Inserta", "Ok", "Programacion actualizada correctamente. Acuse : " + xmlAcuse);
                                                strRespuesta = "Exito";
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                catch (Exception ex)
                                {
                                    strRespuesta = ex.Message;
                                }
                                #endregion
                                break;
                            case "PAG":
                                #region Historial
                                Id_Estatus = 8;         //Factura pagada
                                buscaArchivo = new BLArchivos();
                                buscaProgramacion = new BLProgramacion();
                                buscaHistorial = new BLHistorial();
                                try
                                {
                                    List<Archivos> csArchivos = buscaArchivo.buscaArchivoxuuid(xmlUUID);
                                    if (csArchivos.Count > 0)
                                        Id_Recepcion = csArchivos[0].Id_Recepcion;
                                    else
                                        Id_Recepcion = 0;
                                    
                                    List<Programacion> csProgramacion = buscaProgramacion.buscaProgramacion(xmlUUID, true);
                                    if (csProgramacion.Count > 0)
                                    {
                                        Id_Programacion = csProgramacion[0].ID_Programacion;
                                        List<Historial> csHistorial = buscaHistorial.buscaHistorial(xmlUUID, true);
                                        if (csHistorial.Count == 0)
                                        {
                                            xmlObservaciones = "Factura Pagada";
                                            bool x = insertaHistorial(Id_Recepcion, Id_Programacion, xmlMoneda, xmlPedido, xmlImporte, xmlDocto_Compensacion, xmlFecha_Pago, Id_Estatus, xmlObservaciones);
                                            if (x)
                                            {
                                                guardaLog("Historial", "Inserta", "Ok", "Pago insertado correctamente Docto. Compensacion : " + xmlDocto_Compensacion);
                                                strRespuesta = "Exito";
                                            }
                                        }
                                        else
                                        {
                                            bool x = actualizaHistorial(Id_Programacion, xmlDocto_Compensacion, xmlFecha_Pago, xmlObservaciones, true);
                                            if (x)
                                            {
                                                guardaLog("Historial", "Actualiza", "Ok", "Pago insertado correctamente Docto. Compensacion : " + xmlDocto_Compensacion);
                                                strRespuesta = "Exito";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        strRespuesta = "Pago no insertado. El pedido : " + xmlPedido + " no niene una programación en el portal.";
                                        guardaLog("Historial", "Inserta", "Ok", strRespuesta);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    strRespuesta = ex.Message;
                                }
                                #endregion
                                break;
                            case "CAN":
                                #region Cancelación
                                //Primero buscamos si el documento esta en Pagadas
                                buscaHistorial = new BLHistorial();
                                buscaProgramacion = new BLProgramacion();
                                buscaArchivo = new BLArchivos();
                                try
                                {
                                    List<Historial> csHistorial = buscaHistorial.buscaHistorial(xmlUUID, true);
                                    if (csHistorial.Count > 0)
                                    {
                                        Id_Historial = csHistorial[0].ID_Historial;
                                        Id_Programacion = csHistorial[0].ID_Programacion;
                                        Id_Estatus = 7;         //Pago cancelado
                                        //Eliminamos logicamente el Historial, Actualizamos Programación y ca,biamos de estatus el archivo
                                        bool x = eliminaHistorial(Id_Historial);
                                        if (x)
                                        {
                                            x = activaProgramacíon(Id_Programacion);
                                            if (x)
                                            {
                                                List<Programacion> csProgramacion = buscaProgramacion.buscaProgramacionxid(Id_Programacion);
                                                if (csProgramacion.Count > 0)
                                                {
                                                    Id_Recepcion = csProgramacion[0].ID_Recepcion;
                                                    if (Id_Recepcion != 0)
                                                    {
                                                        x = buscaArchivo.cambiaEstatus(Id_Recepcion, Id_Estatus, xmlObservaciones);
                                                        if (x)
                                                        {
                                                            guardaLog("Cancelación", "Cancelacio de pago", "Ok", "Cancelación de pago hecha correctamente. ID_Historial :" + Id_Historial.ToString());
                                                            strRespuesta = "Exito";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        guardaLog("Cancelación", "Cancelacio de pago", "Ok", "No se cambia el estatus de recepción debidoa a que no fue cargada por el portal");
                                                        strRespuesta = "Exito";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Id_Estatus = 5;         //Factura en revisión
                                        List<Programacion> csProgramacion = buscaProgramacion.buscaProgramacion(xmlUUID, true);
                                        if (csProgramacion.Count > 0)
                                        {
                                            Id_Programacion = csProgramacion[0].ID_Programacion;
                                            Id_Recepcion = csProgramacion[0].ID_Recepcion;
                                            bool x = eliminaProgramacion(Id_Programacion);
                                            if (Id_Recepcion != 0)
                                            {
                                                x = buscaArchivo.cambiaEstatus(Id_Recepcion, Id_Estatus, xmlObservaciones);
                                                if (x)
                                                {
                                                    guardaLog("Cancelación", "Cancelacio de CXP", "Ok", "Cancelación de cuenta por pagar. ID_Programacion :" + Id_Programacion.ToString());
                                                    strRespuesta = "Exito";
                                                }
                                            }
                                            else
                                            {
                                                guardaLog("Cancelación", "Cancelacio de CXP", "Ok", "No se cambia el estatus de recepción debidoa a que no fue cargada por el portal");
                                                strRespuesta = "Exito";
                                            }
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    strRespuesta = ex.Message;
                                }
                                #endregion
                                break;
                            case "REC":
                                #region Rechazado
                                Id_Estatus = 4;         //Factura REchazada
                                buscaArchivo = new BLArchivos();
                                try
                                {
                                    //Buscamos la carga del archivo
                                    List<Archivos> csArchivos = buscaArchivo.buscaArchivoxuuid(xmlUUID);
                                    if (csArchivos.Count > 0)
                                    {
                                        #region Registros cargados desde el portal
                                        //Validamos que no exista en programación
                                        Id_Recepcion = csArchivos[0].Id_Recepcion;
                                        bool x = insertaRechazo(Id_Recepcion, Id_Estatus, xmlObservaciones);
                                        if (x)
                                        {
                                            guardaLog("Rechazo", "Inserta", "Ok", "Rechazo insertada correctamente. ID_Recepción : " + Id_Recepcion.ToString());
                                            strRespuesta = "Exito";
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        strRespuesta = "Rechazo no insertado, no existe archivo cargado con UUID : " + xmlUUID;
                                        guardaLog("Rechazo", "Inserta", "Ok", strRespuesta);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    strRespuesta = ex.Message;
                                }
                                #endregion
                                break;
                        }
                        #endregion

                    }
                    else
                        strRespuesta = "El atributo UUID no puede estar vacio";
                }
            }
            else
                strRespuesta = "No se encontro el atributo UUID";

            return strRespuesta;
        }

        #region Metodos Programación
        /// <summary>
        /// Metodo para insertar programación
        /// </summary>
        /// <param name="_idrecepcion"></param>
        /// <param name="_moneda"></param>
        /// <param name="_pedido"></param>
        /// <param name="_acuse"></param>
        /// <param name="_fechavencimiento"></param>
        /// <param name="_idestatus"></param>
        /// <param name="_observaciones"></param>
        /// <returns></returns>
        private bool insertaProgramacion(int _idrecepcion, string _moneda, Int64 _pedido, string _acuse, string _fechavencimiento, int _idestatus, string _observaciones)
        {
            bool resp = false;
            BLProgramacion inserta = new BLProgramacion();
            try
            {
                resp = inserta.insertaProgramacion(_idrecepcion, _moneda, _pedido, _acuse, _fechavencimiento, _idestatus, _observaciones);
            }
            catch (Exception ex)
            {
                guardaLog("insertaProgramacion", "insertaProgramacion", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para actualizar Programacion
        /// </summary>
        /// <param name="_idprogramacion"></param>
        /// <param name="_serie"></param>
        /// <param name="_folio"></param>
        /// <param name="_importe"></param>
        /// <param name="_moneda"></param>
        /// <param name="_pedido"></param>
        /// <param name="_acuse"></param>
        /// <param name="_fvencimiento"></param>
        /// <param name="_observaciones"></param>
        /// <returns></returns>
        private bool actualizaProgramacion(int _idprogramacion, string _serie, string _folio, decimal _importe, string _moneda,
            Int64 _pedido, string _acuse, string _fvencimiento, string _observaciones)
        {
            bool resp = false;
            BLProgramacion actualiza = new BLProgramacion();
            try
            {
                resp = actualiza.actualizaProgramacion(_idprogramacion, _serie, _folio, _importe, _moneda, _pedido, _acuse, _fvencimiento, _observaciones);
            }
            catch (Exception ex)
            {
                guardaLog("actualizaProgramacion", "actualizaProgramacion", "Error", ex.Message);
            }
            return resp;
        }


        /// <summary>
        /// Metodo para insertar Programación cargada manual desde SAP
        /// </summary>
        /// <param name="_idrecepcion"></param>
        /// <param name="_idproveedor"></param>
        /// <param name="_idsociedad"></param>
        /// <param name="_frecepcion"></param>
        /// <param name="_serie"></param>
        /// <param name="_folio"></param>
        /// <param name="_uuid"></param>
        /// <param name="_ffactura"></param>
        /// <param name="_importe"></param>
        /// <param name="_moneda"></param>
        /// <param name="_pedido"></param>
        /// <param name="_acuse"></param>
        /// <param name="_fvencimiento"></param>
        /// <param name="_observaciones"></param>
        /// <returns></returns>
        private bool insertaProgramacion(int _idrecepcion, int _idproveedor, int _idsociedad, DateTime _frecepcion, string _serie,
            string _folio, string _uuid, DateTime _ffactura, decimal _importe, string _moneda,
            Int64 _pedido, string _acuse, string _fvencimiento, string _observaciones)
        {
            bool resp = false;
            BLProgramacion inserta = new BLProgramacion();
            try
            {
                resp = inserta.insertaProgramacion(_idrecepcion, _idproveedor, _idsociedad, _frecepcion, _serie, _folio, _uuid, _ffactura, _importe, _moneda, _pedido, _acuse, _fvencimiento, _observaciones);
            }
            catch (Exception ex)
            {
                guardaLog("insertaProgramacion", "insertaProgramacion", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para activar programación
        /// </summary>
        /// <param name="_idprogramacion"></param>
        /// <returns></returns>
        private bool activaProgramacíon(int _idprogramacion)
        {
            bool resp = false;
            BLProgramacion activa = new BLProgramacion();
            try
            {
                resp = activa.activaProgramacion(_idprogramacion);
            }
            catch (Exception ex)
            {
                guardaLog("activaProgramacíon", "activaProgramacíon", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para eliminar programación
        /// </summary>
        /// <param name="_idprogramacion"></param>
        /// <returns></returns>
        private bool eliminaProgramacion(int _idprogramacion)
        {
            bool resp = false;
            BLProgramacion elimina = new BLProgramacion();
            try
            {
                resp = elimina.eliminarProgramacion(_idprogramacion);
            }
            catch (Exception ex)
            {
                guardaLog("eliminaProgramacion", "eliminaProgramacion", "Error", ex.Message);
            }
            return resp;
        }

        #endregion

        #region Metodos Historial
        /// <summary>
        /// Metodo para insertar historial
        /// </summary>
        /// <param name="_idrecepcion"></param>
        /// <param name="_idprogramacion"></param>
        /// <param name="_moneda"></param>
        /// <param name="_pedido"></param>
        /// <param name="_docto"></param>
        /// <param name="_fpago"></param>
        /// <param name="_idestatus"></param>
        /// <param name="_obsevaciones"></param>
        /// <returns></returns>
        private bool insertaHistorial(int _idrecepcion, int _idprogramacion, string _moneda, Int64 _pedido, decimal _importe, string _docto, DateTime _fpago, int _idestatus, string _obsevaciones)
        {
            bool resp = false;
            BLHistorial inserta = new BLHistorial();
            try
            {
                resp = inserta.insertaHistorial(_idrecepcion, _idprogramacion, _moneda, _pedido, _importe, _docto, _fpago, _idestatus, _obsevaciones);
            }
            catch (Exception ex)
            {
                guardaLog("insertaHistorial", "insertaHistorial", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para actualizar historial
        /// </summary>
        /// <param name="_idhistorial"></param>
        /// <param name="_docto"></param>
        /// <param name="_fechapago"></param>
        /// <param name="_observaciones"></param>
        /// <param name="_valido"></param>
        /// <returns></returns>
        private bool actualizaHistorial(int _idhistorial, string _docto, DateTime _fechapago, string _observaciones, bool _valido)
        {
            bool resp = false;
            BLHistorial actualiza = new BLHistorial();
            try
            {
                resp = actualiza.actualizaHistorial(_idhistorial, _docto, _fechapago, _observaciones, _valido);
            }
            catch (Exception ex)
            {
                guardaLog("actualizaHistorial", "actualizaHistorial", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para eliminar historial
        /// </summary>
        /// <param name="_idhistorial"></param>
        /// <returns></returns>
        private bool eliminaHistorial(int _idhistorial)
        {
            bool resp = false;
            BLHistorial elimina = new BLHistorial();
            try
            {
                resp = elimina.eliminaHistorial(_idhistorial);
            }
            catch (Exception ex)
            {
                guardaLog("eliminaHistorial", "eliminaHistorial", "Error", ex.Message);
            }
            return resp;
        }

        #endregion

        #region Metodos Archivos
        /// <summary>
        /// Metodo para insertar rechazos.
        /// </summary>
        /// <param name="_idrecepcion"></param>
        /// <param name="_idestatus"></param>
        /// <param name="_observaciones"></param>
        /// <returns></returns>
        private bool insertaRechazo(int _idrecepcion, int _idestatus, string _observaciones)
        {
            bool resp = false;
            BLArchivos actualiza = new BLArchivos();
            try
            {
                resp = actualiza.cambiaEstatus(_idrecepcion, _idestatus, _observaciones);
            }
            catch (Exception ex)
            {
                guardaLog("insertaRechazo", "insertaRechazo", "Error", ex.Message);
            }
            return resp;
        }

        #endregion

        #region Metodos Varios
        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = 0;
            
            BLLogs guardaLog = new BLLogs();
            bool insertaLog = guardaLog.insertaLog("WS_Notificaciones", _evento, _accion, _respuesta, _descripción, idUsuario);
        }

        /// <summary>
        /// Metodo para convertir a base 64
        /// </summary>
        /// <param name="_path"></param>
        /// <returns></returns>
        private string convierteBase64(string _path)
        {
            string base64String = string.Empty;

            System.IO.FileStream inFile;
            byte[] binaryData;
            try
            {
                inFile = new System.IO.FileStream(_path, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                binaryData = new byte[inFile.Length];
                long bytesRead = inFile.Read(binaryData, 0, (int)inFile.Length);
                inFile.Close();

                base64String = Convert.ToBase64String(binaryData, 0, binaryData.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return base64String;
        }

        /// <summary>
        /// Metodo para crear el XML
        /// </summary>
        /// <param name="_cadena"></param>
        /// <returns></returns>
        private bool creaXML(string _cadena)
        {
            bool resp = false;

            #region Manejamos el archivo temporal que se creara
            //Validamos que para cada XML corresponda un PDF con el mismo nombre
            string strPath = "";
            string nombreArchivo = string.Empty;

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./tmp/" + "/");
            nombreArchivo = "notificacion.xml";

            if (!Directory.Exists(strPath))
                Directory.CreateDirectory(strPath);

            DirectoryInfo rootDir = new DirectoryInfo(strPath);
            FileInfo[] files = null;
            files = rootDir.GetFiles(nombreArchivo);
            if (files != null)
            {
                if (File.Exists(strPath + nombreArchivo))
                    File.Delete(strPath + nombreArchivo);
            }
            #endregion

            #region Convertimos la cadena a XML
            try
            {
                byte[] binaryData;
                binaryData = Convert.FromBase64String(_cadena);

                FileStream guardaXml;
                guardaXml = new FileStream(strPath + nombreArchivo, FileMode.Create, FileAccess.Write);
                guardaXml.Write(binaryData, 0, binaryData.Length);
                guardaXml.Close();
                resp = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion
            return resp;
        }

        #endregion

    }
}
