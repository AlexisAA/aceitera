﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.Xml;
using System.Configuration;
using System.IO;
using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Varios;
using System.Net.Mail;

namespace wsProveedores
{
    /// <summary>
    /// Servicio Web para el control de Proveedores, Usuarios y contactos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class Servicio : System.Web.Services.WebService
    {

        [WebMethod(Description = "Metodo ABC de Proveedores, Usuarios y contactos en una cadena con base 64")]
        public string datosEntrada(string cadenabase64)
        {

            #region Variables locales
            string Respuesta = string.Empty;
            string strPath = string.Empty;
            string nombreArchivo = string.Empty;
            #endregion

            #region Metodo y variables utilizado solo para pruebas
            //string respConversion = string.Empty;
            //strPath = Server.MapPath(@"./tmp/" + "/");
            //nombreArchivo = "Proveedor Rebeca.xml";
            //if (Directory.Exists(strPath))
            //{
            //    DirectoryInfo root = new DirectoryInfo(strPath);
            //    FileInfo[] archivos = null;
            //    archivos = root.GetFiles(nombreArchivo);
            //    if (archivos != null && archivos.Count() > 0)
            //    {
            //        respConversion = convierteBase64(strPath + nombreArchivo);
            //        cadenabase64 = respConversion;
            //    }
            //}
            #endregion

            #region Metodos para leer cadena y crear XML
            bool respCrea = false;
            respCrea = creaXML(cadenabase64);
            if (respCrea)
            {
                strPath = Server.MapPath(@"./tmp/" + "/");
                nombreArchivo = "Proveedores.xml";

                Respuesta = obtenDatosxml(strPath + nombreArchivo);

            }
            else
            {
                Respuesta = "No se puede convertir la cadena recibida";
            }
            #endregion
            
            return Respuesta;

        }

        /// <summary>
        /// Metodo para convertir a base 64
        /// </summary>
        /// <param name="_path"></param>
        /// <returns></returns>
        private string convierteBase64(string _path)
        {
            string base64String = string.Empty;

            System.IO.FileStream inFile;
            byte[] binaryData;
            try
            {
                inFile = new System.IO.FileStream(_path, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                binaryData = new byte[inFile.Length];
                long bytesRead = inFile.Read(binaryData, 0, (int)inFile.Length);
                inFile.Close();

                base64String = Convert.ToBase64String(binaryData, 0, binaryData.Length);
            }
            catch (Exception ex)
            {
                //string _evento, string _accion, string _respuesta, string _descripción
                guardaLog("pruebas", "convierteBase64", "Error", ex.Message);
                return ex.Message;
            }

            return base64String;
        }

        /// <summary>
        /// Metodo para crear el XML
        /// </summary>
        /// <param name="_cadena"></param>
        /// <returns></returns>
        private bool creaXML(string _cadena)
        {
            bool resp = false;

            #region Manejamos el archivo temporal que se creara
            //Validamos que para cada XML corresponda un PDF con el mismo nombre
            string strPath = "";
            string nombreArchivo = string.Empty;

            //Ruta en la que se cargaran los archivos
            strPath = Server.MapPath(@"./tmp/" + "/");
            nombreArchivo = "Proveedores.xml";

            if (!Directory.Exists(strPath))
                Directory.CreateDirectory(strPath);

            DirectoryInfo rootDir = new DirectoryInfo(strPath);
            FileInfo[] files = null;
            files = rootDir.GetFiles(nombreArchivo);
            if (files != null)
            {
                if (File.Exists(strPath + nombreArchivo))
                    File.Delete(strPath + nombreArchivo);
            }
            #endregion

            #region Convertimos la cadena a XML
            try
            {
                byte[] binaryData;
                binaryData = Convert.FromBase64String(_cadena);

                FileStream guardaXml;
                guardaXml = new FileStream(strPath + nombreArchivo, FileMode.Create, FileAccess.Write);
                guardaXml.Write(binaryData, 0, binaryData.Length);
                guardaXml.Close();
                resp = true;
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "creaXML", "Error", ex.Message);
            }
            #endregion

            return resp;
        }

        private string obtenDatosxml(string _rutaarchivo)
        {
            #region Variables del metodo
            string respSAP = string.Empty;
            string strRespuesta = string.Empty;
            #endregion

            #region Variables para el XML
            string xmlId_Proveedor_SAP = string.Empty;
            string xmlNombre = string.Empty;
            string xmlRFC = string.Empty;
            string xmlTelefono = string.Empty;
            string xmlFax = string.Empty;
            string xmlBanco = string.Empty;
            string xmlCuenta_Bancaria = string.Empty;
            int xmlDias_Credito = -1;
            string xmlDireccion = string.Empty;
            bool xmlValido = false;
            string xmlContacto = string.Empty;
            string xmlCelular = string.Empty;
            string xmlCorreo = string.Empty;
            int xmlPerfil = -1;
            #endregion

            //Abrimos el docuemnto XML
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(_rutaarchivo);

            //Buscamos el nodo principal
            XmlNode nodoPrincipal = xmlDoc.DocumentElement;
            XmlAttribute aId_Proveedor_SAP = nodoPrincipal.Attributes["ID_Proveedor_SAP"];
            if (aId_Proveedor_SAP != null)
            {
                #region Si encontro Id Proveedor SAP
                
                #region Buscamos los atributos
                XmlAttribute aNombre = nodoPrincipal.Attributes["Nombre"];
                if (aNombre == null)
                    strRespuesta = "No se encontro el atributo Nombre";
                XmlAttribute aRFC = nodoPrincipal.Attributes["RFC"];
                if (aRFC == null)
                    strRespuesta = "No se encontro el atributo RFC";
                XmlAttribute aTelefono = nodoPrincipal.Attributes["Telefono"];
                if (aTelefono == null)
                    strRespuesta = "No se encontro el atributo Telefono";
                XmlAttribute aFax = nodoPrincipal.Attributes["Fax"];
                if (aFax == null)
                    strRespuesta = "No se encontro el atributo Fax";
                XmlAttribute aBanco = nodoPrincipal.Attributes["Banco"];
                if (aBanco == null)
                    strRespuesta = "No se encontro el atributo Banco";
                XmlAttribute aCuenta_Bancaria = nodoPrincipal.Attributes["Cuenta_Bancaria"];
                if (aCuenta_Bancaria == null)
                    strRespuesta = "No se encontro el atributo Cuenta_Bancaria";
                XmlAttribute aDias_Credito = nodoPrincipal.Attributes["Dias_Credito"];
                if (aDias_Credito == null)
                    strRespuesta = "No se encontro el atributo Credito";
                XmlAttribute aDireccion = nodoPrincipal.Attributes["Direccion"];
                if (aDireccion == null)
                    strRespuesta = "No se encontro el atributo Direccion";
                XmlAttribute aValido = nodoPrincipal.Attributes["Valido"];
                if (aValido == null)
                    strRespuesta = "No se encontro el atributo Valido";
                XmlAttribute aContacto = nodoPrincipal.Attributes["Contacto"];
                if (aContacto == null)
                    strRespuesta = "No se encontro el atributo Contacto";
                XmlAttribute aCelular = nodoPrincipal.Attributes["Celular"];
                if (aCelular == null)
                    strRespuesta = "No se encontro el atributo Celular";
                XmlAttribute aCorreo = nodoPrincipal.Attributes["Correo"];
                if (aCorreo == null)
                    strRespuesta = "No se encontro el atributo Correo";
                XmlAttribute aPerfil = nodoPrincipal.Attributes["Perfil"];
                if (aPerfil == null)
                    strRespuesta = "No se encontro el atributo Perfil";
                #endregion

                #region Validamos la informacion del Proveedor
                
                if (strRespuesta == string.Empty)
                {
                    xmlId_Proveedor_SAP = aId_Proveedor_SAP.Value.ToString();
                    if (xmlId_Proveedor_SAP != string.Empty)
                    {
                        #region Asignacion de datos
                        xmlNombre = aNombre.Value.ToString() == "" ? null : aNombre.Value.ToString();
                        if (xmlNombre == null)
                            strRespuesta = "El atributo Id_Proveedor_SAP no puede estar vacio";

                        xmlRFC = aRFC.Value.ToString() == "" ? null : aRFC.Value.ToString();
                        if (xmlRFC == null)
                        {
                            if (strRespuesta == string.Empty)
                                strRespuesta = "El atributo RCC no puede estar vacio";
                            else
                                strRespuesta = strRespuesta + ", El atributo RCC no puede estar vacio";
                        }

                        xmlTelefono = aTelefono.Value.ToString() == "" ? "" : aTelefono.Value.ToString();
                        xmlFax = aFax.Value.ToString() == "" ? null : aFax.Value.ToString();
                        xmlBanco = aBanco.Value.ToString() == "" ? null : aBanco.Value.ToString();
                        xmlCuenta_Bancaria = aCuenta_Bancaria.Value.ToString() == "" ? null : aCuenta_Bancaria.Value.ToString();
                        xmlDias_Credito = aDias_Credito.Value.ToString() == "" ? 0 : Convert.ToInt32(aDias_Credito.Value.ToString());
                        xmlDireccion = aDireccion.Value.ToString() == "" ? null : aDireccion.Value.ToString();

                        string strValido = aValido.Value.ToString() == "" ? null : aValido.Value.ToString();
                        if (strValido == null)
                        {
                            if (strRespuesta == string.Empty)
                                strRespuesta = "El atributo Valido no puede estar vacio";
                            else
                                strRespuesta = strRespuesta + ", El atributo Valido no puede estar vacio";
                        }
                        else
                            xmlValido = Convert.ToBoolean(strValido.ToString());

                        xmlContacto = aContacto.Value.ToString() == "" ? "" : aContacto.Value.ToString();
                        //if (xmlContacto == null)
                        //{
                        //    if (strRespuesta == string.Empty)
                        //        strRespuesta = "El atributo Contacto no puede estar vacio";
                        //    else
                        //        strRespuesta = strRespuesta + ", El atributo Contacto no puede estar vacio";
                        //}

                        xmlCelular = aCelular.Value.ToString() == "" ? null : aCelular.Value.ToString();

                        xmlCorreo = aCorreo.Value.ToString() == "" ? null : aCorreo.Value.ToString();
                        if (xmlCorreo == null)
                        {
                            if (strRespuesta == string.Empty)
                                strRespuesta = "El atributo Correo no puede estar vacio";
                            else
                                strRespuesta = strRespuesta + ", El atributo Correo no puede estar vacio";
                        }
                        else
                        {
                            string strFormat = string.Empty;
                            strFormat = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
                            if (!System.Text.RegularExpressions.Regex.IsMatch(xmlCorreo, strFormat))
                            {
                                if (strRespuesta == string.Empty)
                                    strRespuesta = "El formato del correo no es valido";
                                else
                                    strRespuesta = strRespuesta + ", El formato del correo no es valido";
                            }
                        }

                        string strPerfil = aPerfil.Value.ToString() == "" ? null : aPerfil.Value.ToString();
                        if (strPerfil == null)
                        {
                            if (strRespuesta == string.Empty)
                                strRespuesta = "El atributo Perfil no puede estar vacio";
                            else
                                strRespuesta = strRespuesta + ", El atributo Perfil no puede estar vacio";
                        }
                        else
                            xmlPerfil = Convert.ToInt32(strPerfil.ToString());
                        #endregion
                    }
                    else
                        strRespuesta = "El atributo Id_Proveedor_SAP no puede estar vacio";
                }
                #endregion

                if (strRespuesta == string.Empty)
                {
                    #region Validación correcta
                    //Validamos que no exista un proveedor con el mismo Id de SAP
                    int Id_Proveedor = -1;
                    Id_Proveedor = buscaProveedor(xmlId_Proveedor_SAP, xmlRFC);
                    if (Id_Proveedor > 0)
                    {
                        #region Actualiza Proveedor
                        //El proveedor ya exste, lo actualizamos
                        bool respActualiza = false;
                        respActualiza = actualizaProvvedor(Id_Proveedor, xmlNombre, xmlRFC, xmlTelefono, xmlFax, xmlBanco, xmlCuenta_Bancaria, xmlDias_Credito, xmlDireccion, xmlValido);
                        if (respActualiza)
                        {
                            //Buscamos el Id de usuario o usuarios
                            int Id_Usuario = -1;

                            #region Divide nombre
                            string Nombre_usuario = string.Empty;
                            string APaterno = string.Empty;
                            string AMaterno = string.Empty;
                            string[] busca = xmlNombre.Split(new char[] { ' ' });
                            for (int n = 0; n < busca.Count(); n++)
                            {
                                if (n == 0)
                                    Nombre_usuario = busca[n].ToString();
                                else if (n == 1)
                                    APaterno = busca[n].ToString();
                                else
                                    if (AMaterno == "")
                                        AMaterno = busca[n].ToString();
                                    else
                                        AMaterno = AMaterno + " " + busca[n].ToString();
                            }

                            Nombre_usuario = Nombre_usuario == "" ? xmlNombre.Trim() : Nombre_usuario.Trim();
                            APaterno = APaterno == "" ? null : APaterno.Trim();
                            AMaterno = AMaterno == "" ? null : AMaterno.Trim();
                            #endregion
                            
                            List<Usuarios> userxProveedor = null;
                            userxProveedor = buscaUsuarios(Id_Proveedor);
                            if (userxProveedor.Count > 0)
                            {
                                #region Actualiza Usuario
                                Id_Usuario = userxProveedor[0].Id_Usuario;
                                respActualiza = actualizaUsuario(Id_Usuario, Nombre_usuario, APaterno, AMaterno, xmlValido);
                                if (respActualiza)
                                {
                                    #region Actualiza contacto
                                    respActualiza = actualizaContacto(Id_Proveedor, xmlContacto, xmlTelefono, xmlCelular, xmlCorreo);

                                    if (respActualiza)
                                    {
                                        strRespuesta = "Registro actualizado correctamente, Id_Proveedor : " + Id_Proveedor;
                                        guardaLog("Proveedor", "Actualiza", "Ok", strRespuesta);
                                        strRespuesta = "Exito";
                                    }
                                    else
                                    {
                                        strRespuesta = "No se pudo actualizar el Contacto. Id_Proveedor : " + Id_Proveedor;
                                        guardaLog("Proveedor", "Actualiza", "Error", strRespuesta);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    strRespuesta = "No se pudo actualizar el Usuario, Id_Proveedor : " + Id_Proveedor;
                                    guardaLog("Proveedor", "Actualiza", "Error", strRespuesta);
                                }

                                #endregion
                            }
                            else
                            {
                                #region Insertamos Usuario
                               
                                #region Obten y encripta contraseña
                                string Contrasenia = string.Empty;
                                BLRecepcion.Varios.Seguridad seg = new BLRecepcion.Varios.Seguridad();
                                Contrasenia = seg.encriptar(seg.generaRandom());
                                #endregion

                                Id_Usuario = insertaUsuario(xmlId_Proveedor_SAP.ToLower(), Nombre_usuario, APaterno, AMaterno, Contrasenia, xmlValido, Id_Proveedor);
                                if (Id_Usuario > 0)
                                {
                                    #region Inserta Perfil
                                    respActualiza = insertaUemP(Id_Usuario, xmlPerfil);
                                    if (respActualiza)
                                    {
                                        //Inserta Contacto
                                        //string Nombre, string Telefon, string Celular, string Correo, int Id_Proveedor
                                        #region Inserta Contacto
                                        respActualiza = insertaContacto(xmlContacto, xmlTelefono, xmlCelular, xmlCorreo, Id_Proveedor);
                                        if (respActualiza)
                                        {
                                            #region Envía correo
                                            string resp = string.Empty;
                                            resp = enviaCorreo(xmlNombre, xmlId_Proveedor_SAP, xmlId_Proveedor_SAP.ToLower(), seg.desencriptar(Contrasenia), xmlCorreo, xmlPerfil);
                                            if (resp == "Ok")
                                            {
                                                strRespuesta = "Proveedor creado exitosamente : " + xmlId_Proveedor_SAP;
                                                guardaLog("Proveedor", "Actualiza", "Ok", strRespuesta);
                                                strRespuesta = "Exito";
                                            }
                                            else
                                            {
                                                strRespuesta = "No se pudo enviar correo: Correo : " + xmlCorreo;
                                                guardaLog("Proveedor", "Actualiza", "Error", strRespuesta);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            strRespuesta = "No se pudo insertal el Contacto del proveedor Id Proveedor : " + xmlId_Proveedor_SAP; ;
                                            guardaLog("Proveedor", "Actualiza", "Error", strRespuesta);
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        strRespuesta = "No se pudo asignar perfil al usuario Usuario : " + xmlId_Proveedor_SAP;
                                        guardaLog("Proveedor", "Actualiza", "Error", strRespuesta);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    strRespuesta = "No se pudo insertar Usuario, Id_Proveedor : " + Id_Proveedor;
                                    guardaLog("Proveedor", "Actualiza", "Error", strRespuesta);
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            strRespuesta = "No se pudo actualizar el proveedor.. Id_Proveedor : " + Id_Proveedor;
                            guardaLog("Proveedor", "Actualiza", "Error", strRespuesta);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Inserta Proveedor
                        string nombreProveedor = string.Empty;
                        nombreProveedor = buscaProveedorSAP(xmlId_Proveedor_SAP);

                        if (nombreProveedor == string.Empty)
                        {
                            nombreProveedor = buscarfcProveedor(xmlRFC);
                            if (nombreProveedor == string.Empty)
                            {
                                //Insertamos el registro
                                bool respProv = false;
                                respProv = insertaProveedor(xmlId_Proveedor_SAP, xmlNombre, xmlRFC, xmlTelefono, xmlFax, xmlBanco, xmlCuenta_Bancaria, xmlDias_Credito, xmlDireccion, xmlValido);
                                if (respProv)
                                {
                                    #region Inserta Usuario
                                    //Traemos el Id del proveedor creado
                                    Id_Proveedor = -1;
                                    Id_Proveedor = buscaProveedor(xmlId_Proveedor_SAP, xmlRFC);

                                    #region Variable de usuario
                                    string Usuario = string.Empty;
                                    string Contrasenia = string.Empty;
                                    string Nombre_usuario = string.Empty;
                                    string APaterno = string.Empty;
                                    string AMaterno = string.Empty;
                                    #endregion

                                    #region Obten y encripta contraseña
                                    BLRecepcion.Varios.Seguridad seg = new BLRecepcion.Varios.Seguridad();
                                    Contrasenia = seg.encriptar(seg.generaRandom());
                                    #endregion

                                    Usuario = xmlId_Proveedor_SAP.Trim();

                                    #region Divide nombre
                                    string[] busca = xmlNombre.Split(new char[] { ' ' });
                                    for (int n = 0; n < busca.Count(); n++)
                                    {
                                        if (n == 0)
                                            Nombre_usuario = busca[n].ToString();
                                        else if (n == 1)
                                            APaterno = busca[n].ToString();
                                        else
                                            if (AMaterno == "")
                                                AMaterno = busca[n].ToString();
                                            else
                                                AMaterno = AMaterno + " " + busca[n].ToString();
                                    }

                                    Nombre_usuario = Nombre_usuario == "" ? xmlNombre.Trim() : Nombre_usuario.Trim();
                                    APaterno = APaterno == "" ? null : APaterno.Trim();
                                    AMaterno = AMaterno == "" ? null : AMaterno.Trim();
                                    #endregion

                                    int Id_Usuario = -1;
                                    Id_Usuario = insertaUsuario(Usuario, Nombre_usuario, APaterno, AMaterno, Contrasenia, xmlValido, Id_Proveedor);
                                    if (Id_Usuario > 0)
                                    {
                                        #region Inserta Perfil
                                        respProv = insertaUemP(Id_Usuario, xmlPerfil);
                                        if (respProv)
                                        {
                                            //Inserta Contacto
                                            //string Nombre, string Telefon, string Celular, string Correo, int Id_Proveedor
                                            #region Inserta Contacto
                                            respProv = insertaContacto(xmlContacto, xmlTelefono, xmlCelular, xmlCorreo, Id_Proveedor);
                                            if (respProv)
                                            {
                                                #region Envía correo
                                                string resp = string.Empty;
                                                resp = enviaCorreo(xmlNombre, xmlId_Proveedor_SAP, Usuario, seg.desencriptar(Contrasenia), xmlCorreo, xmlPerfil);
                                                if (resp == "Ok")
                                                {
                                                    strRespuesta = "Proveedor creado exitosamente : " + xmlId_Proveedor_SAP;
                                                    guardaLog("Proveedor", "Nuevo", "Ok", strRespuesta);
                                                    strRespuesta = "Exito";
                                                }
                                                else
                                                {
                                                    strRespuesta = "No se pudo enviar correo: Correo : " + xmlCorreo;
                                                    guardaLog("Proveedor", "Nuevo", "Error", strRespuesta);
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                strRespuesta = "No se pudo insertal el Contacto del proveedor Id Proveedor : " + xmlId_Proveedor_SAP; ;
                                                guardaLog("Proveedor", "Nuevo", "Error", strRespuesta);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            strRespuesta = "No se pudo asignar perfil al usuario Usuario : " + Usuario;
                                            guardaLog("Proveedor", "Nuevo", "Error", strRespuesta);
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        strRespuesta = "No se pudo insertar el usuario para el proveedor Id_Proveedor" + xmlId_Proveedor_SAP;
                                        guardaLog("Proveedor", "Nuevo", "Error", strRespuesta);
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                strRespuesta = "El Proveedor : " + nombreProveedor + " ya teiene asignado el RFC : " + xmlRFC;
                                guardaLog("Proveedor", "Nuevo", "Error", strRespuesta);
                            }
                        }
                        else
                        {
                            strRespuesta = "El Proveedor : " + nombreProveedor + " ya teiene asignado le Id SAP : " + xmlId_Proveedor_SAP;
                            guardaLog("Proveedor", "Nuevo", "Error", strRespuesta);
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            else
            {
                strRespuesta = "No se encontro el ID de proveedor de SAP";
                guardaLog("Principal", "Principal", "Error", strRespuesta);
            }

            return strRespuesta;

        }

        
        private int buscaLog(string _evento, string _descripcion)
        {
            int idLog = -1;
            int idUsuario = 0;
            string Formulario = "WS_Proveedores";
            BLLogs busca = new BLLogs();
            try
            {
                List<Logs> csLogs = busca.buscaLogs(Formulario, _evento, _descripcion, idUsuario);
                if (csLogs.Count > 0)
                    idLog = csLogs[0].ID_Log;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return idLog;
        }

        /// <summary>
        /// Metodo para validar el ID de SAP no exista
        /// </summary>
        /// <param name="_idProveedorSAP"></param>
        /// <param name="_rfc"></param>
        /// <returns></returns>
        private int buscaProveedor(string _idProveedorSAP, string _rfc)
        {
            int id_Proveedor = -1;
            BLProveedores busca = new BLProveedores();
            try
            {
                List<Proveedores> csProveedores = busca.buscaProveedor(_idProveedorSAP, _rfc);
                if (csProveedores.Count > 0)
                    id_Proveedor = csProveedores[0].ID_Proveedor;
                
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "buscaProveedor", "Error", ex.Message);
            }

            return id_Proveedor;
        }

        /// <summary>
        /// Metodo para buscar ID Proveedor SAP
        /// </summary>
        /// <param name="_idproveedorsap"></param>
        /// <returns></returns>
        private string buscaProveedorSAP(string _idproveedorsap)
        {
            string resp = string.Empty;
            BLProveedores busca = new BLProveedores();
            try
            {
                resp = busca.buscaIdproveedorSAP(_idproveedorsap);
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "buscaProveedorSAP", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para buscar el RFC del proveedor
        /// </summary>
        /// <param name="_rfc"></param>
        /// <returns></returns>
        private string buscarfcProveedor(string _rfc)
        {
            string resp = string.Empty;
            BLProveedores busca = new BLProveedores();
            try
            {
                resp = busca.buscaRFCproveedor(_rfc);
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "buscarfcProveedor", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para insertar proveedor
        /// </summary>
        /// <param name="_idproveedorsap"></param>
        /// <param name="_nombre"></param>
        /// <param name="_rfc"></param>
        /// <param name="_telefono"></param>
        /// <param name="_fax"></param>
        /// <param name="_banco"></param>
        /// <param name="_cuenta"></param>
        /// <param name="_credito"></param>
        /// <param name="_direccion"></param>
        /// <param name="_valido"></param>
        /// <returns></returns>
        private bool insertaProveedor(string _idproveedorsap, string _nombre, string _rfc, string _telefono, string _fax, 
            string _banco, string _cuenta, int _credito, string _direccion, bool _valido)
        {
            bool resp = false;
            BLProveedores inserta = new BLProveedores();
            try
            {
                resp = inserta.insertaProveedor(_idproveedorsap, _nombre, DateTime.Now, _rfc, _telefono, _fax, _banco, _cuenta, _credito, _direccion, _valido, false);
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "insertaProveedor", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para inserta usuario
        /// </summary>
        /// <param name="_usuario"></param>
        /// <param name="_nombre"></param>
        /// <param name="_apaterno"></param>
        /// <param name="_amaterno"></param>
        /// <param name="_contrasenia"></param>
        /// <param name="_valido"></param>
        /// <param name="_idProveedor"></param>
        /// <returns></returns>
        private int insertaUsuario(string _usuario, string _nombre, string _apaterno, string _amaterno, string _contrasenia, 
            bool _valido, int _idProveedor)
        {
            int resp = -1;
            BLUsuarios inserta = new BLUsuarios();
            try
            {
                int _Intentos = 0;
                string _bloqueado = "No";
                bool _activo = false;

                resp = inserta.insertaUsuario(_usuario, _nombre, _apaterno, _amaterno, _contrasenia, _Intentos, _bloqueado, _activo, _valido, _idProveedor);
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "insertaUsuario", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para insertar usuario en perfil
        /// </summary>
        /// <param name="_idUsuario"></param>
        /// <param name="_idPerfil"></param>
        /// <returns></returns>
        private bool insertaUemP(int _idUsuario, int _idPerfil)
        {
            bool resp = false;
            BLUsuarioenPerfil inserta = new BLUsuarioenPerfil();
            try
            {
                resp = inserta.insertaUenP(_idUsuario, _idPerfil);
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "insertaUemP", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para actualizar Contacto
        /// </summary>
        /// <param name="_idProveedor"></param>
        /// <param name="_contacto"></param>
        /// <param name="_telefono"></param>
        /// <param name="_celular"></param>
        /// <param name="_correo"></param>
        /// <returns></returns>
        private bool actualizaContacto(int _idProveedor, string _contacto, string _telefono, string _celular, string _correo)
        {
            bool resp = false;
            int Id_contacto = -1;
            BLContacto buscaContacto = new BLContacto();
            try
            {
                List<Contacto> csContacto = buscaContacto.buscaContacto(_idProveedor);
                if (csContacto.Count > 0)
                {
                    Id_contacto = csContacto[0].ID_Contacto;

                    BLContacto actualiza = new BLContacto();
                    resp = actualiza.actualizaContacto(Id_contacto, _contacto, _telefono, _celular, _correo); ;
                }
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "actualizaContacto", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para insertar contacto
        /// </summary>
        /// <param name="_contacto"></param>
        /// <param name="_telefono"></param>
        /// <param name="_celular"></param>
        /// <param name="_correo"></param>
        /// <param name="_idproveedor"></param>
        /// <returns></returns>
        private bool insertaContacto(string _contacto, string _telefono, string _celular, string _correo, int _idproveedor)
        {
            bool rep = false;
            BLContacto inserta = new BLContacto();
            try
            {
                rep = inserta.insertaContacto(_contacto, _telefono, _celular, _correo, _idproveedor);
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "insertaContacto", "Error", ex.Message);
            }
            return rep;
        }

        /// <summary>
        /// Metodo para envíar correo
        /// </summary>
        /// <param name="_nombreproveedor"></param>
        /// <param name="_idproveedorsap"></param>
        /// <param name="_usuario"></param>
        /// <param name="_contrasenia"></param>
        /// <param name="_receptor"></param>
        /// <param name="_idperfil"></param>
        /// <returns></returns>
        private string enviaCorreo(string _nombreproveedor, string _idproveedorsap, string _usuario, string _contrasenia, string _receptor, int _idperfil)
        {
            string resp = string.Empty;

            EnviaCorreo envia = new EnviaCorreo();
            try
            {
                string asunto = string.Empty;
                string cuerpo = string.Empty;
                string receptor = string.Empty;
                string strPath = string.Empty;

                if (_idperfil != 3)
                {
                    asunto = "Registro de usuario";
                    cuerpo = "<body><p style='font-family: Sans-Serif; font-size: 18px; color: #008FC4;'><strong>INDUSTRIAL ACEITERA - REGISTRO DE USUARIO</strong></p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Industrial Aceitera S.A de C.V. le da la mas cordial bienvenida al nuevo esquema de recepción de facturas. Por tal motivo se ha puesto a su disposición el portal de Recepción de Facturas</p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>El motivo del siguiente correo es proporcionarle sus datos de acceso al Portal:</p>";
                    cuerpo += "<table style='font-family: Sans-Serif; font-size: 12px; color: #000000; margin-left: 50px'>";
                    cuerpo += "<tr><td style='text-align: right;'>Proveedor :</td><td style='text-align: left;'>" + _nombreproveedor + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>ID Proveedor :</td><td style='text-align: left;'>" + _idproveedorsap + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>Usuario :</td><td style='text-align: left;'>" + _usuario + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>Contraseña :</td><td style='text-align: left;'>" + _contrasenia + "</td></tr></table>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Es importante que a la brevedad cambie la contraseña en el Menú Mis Datos - Cambiar contraseña</p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Para acceder, debes ingresar en <a href='http://aceitera.com.mx'>www.aceitera.com.mx</a> y dar clic en PORTAL DE PROVEEDORES situado en la partes superior derecha debajo del Menú</p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Nota: Se adjunta Manual de operación y de manera adicional podras encontrarlo dentro del Portal en el pie de página del Menú Inicio</p>";
                    cuerpo += "<br /><p style='font-family: Sans-Serif; font-size: 14px; color: #008FC4;'><strong>Atentamente</strong></p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #008FC4;'><strong>CUENTAS POR PAGAR - INDUSTRIAL ACEITERA</strong></p></body>";
                }
                else
                {
                    asunto = "User Registration";
                    cuerpo = "<body><p style='font-family: Sans-Serif; font-size: 18px; color: #008FC4;'><strong>INDUSTRIAL ACEITERA - USER REGISTRATION</strong></p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Oil Industrial S.A. de C.V.It gives a warm welcome to the new scheme receiving invoices. For this reason we have made available portal receiving invoices.</p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>The reason of this mail is to provide data access to the Portal:</p>";
                    cuerpo += "<table style='font-family: Sans-Serif; font-size: 12px; color: #000000; margin-left: 50px'>";
                    cuerpo += "<tr><td style='text-align: right;'>Provider :</td><td style='text-align: left;'>" + _nombreproveedor + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>ID Provider :</td><td style='text-align: left;'>" + _idproveedorsap + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>User :</td><td style='text-align: left;'>" + _usuario + "</td></tr>";
                    cuerpo += "<tr><td style='text-align: right;'>Password :</td><td style='text-align: left;'>" + _contrasenia + "</td></tr></table>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>It is important to promptly change the password in the My Data - Change Password</p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>To enter, you must enter at <a href='http://aceitera.com.mx'>www.aceitera.com.mx</a> and clicking PORTAL DE PROVEEDORES in the upper right part below the menu</p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Note: Attached Operation Manual and Additionally you can find in the Portal in the footer Home Menu</p>";
                    cuerpo += "<br /><p style='font-family: Sans-Serif; font-size: 14px; color: #008FC4;'><strong>Sincerely</strong></p>";
                    cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #008FC4;'><strong>DEBTS TO PAY - INDUSTRIAL ACEITERA</strong></p></body>";
                }

                //string receptor = ConfigurationManager.AppSettings["mailReceptor"].ToString();
                receptor = _receptor;
                strPath = Server.MapPath(@"./Doctos/");
                resp = envia.enviaCorreo(receptor, asunto, cuerpo, strPath);
                
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "enviaCorreo", "Error", ex.Message);
            }

            return resp;
        }

        /// <summary>
        /// Metodo para actualizar proveedor
        /// </summary>
        /// <param name="_idproveedor"></param>
        /// <param name="_nombre"></param>
        /// <param name="_rfc"></param>
        /// <param name="_telefono"></param>
        /// <param name="_fax"></param>
        /// <param name="_banco"></param>
        /// <param name="_cuenta"></param>
        /// <param name="_credito"></param>
        /// <param name="_direccion"></param>
        /// <param name="_valido"></param>
        /// <returns></returns>
        private bool actualizaProvvedor(int _idproveedor, string _nombre, string _rfc, string _telefono, string _fax, 
            string _banco, string _cuenta, int _credito, string _direccion, bool _valido)
        {
            bool resp = false;
            BLProveedores actualiza = new BLProveedores();
            try
            {
                resp = actualiza.actualizaProveedor(_idproveedor, _nombre, _rfc, _telefono, _fax, _banco, _cuenta, _credito, _direccion, _valido);
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "actualizaProvvedor", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para buscar usuarios por proveedor
        /// </summary>
        /// <param name="_idproveedor"></param>
        /// <returns></returns>
        private List<Usuarios> buscaUsuarios(int _idproveedor)
        {
            List<Usuarios> resp = null;
            BLUsuarios busca = new BLUsuarios();
            try
            {
                resp = busca.buscaUsuariosxproveedor(_idproveedor);
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "buscaUsuarios", "Error", ex.Message);
            }
            return resp;

        }

        /// <summary>
        /// Metodo para actualizar usuario
        /// </summary>
        /// <param name="_idusuario"></param>
        /// <param name="_nombre"></param>
        /// <param name="_apaterno"></param>
        /// <param name="_amaterno"></param>
        /// <param name="_valido"></param>
        /// <returns></returns>
        private bool actualizaUsuario(int _idusuario, string _nombre, string _apaterno, string _amaterno, bool _valido)
        {
            bool resp = false;
            BLUsuarios actualiza = new BLUsuarios();
            try
            {
                resp = actualiza.actualizaUsuario(_idusuario, _nombre, _apaterno, _amaterno, _valido);
            }
            catch (Exception ex)
            {
                guardaLog("Principal", "actualizaUsuario", "Error", ex.Message);
            }
            return resp;
        }

        /// <summary>
        /// Metodo para solo guardar en el Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_accion"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripción"></param>
        private void guardaLog(string _evento, string _accion, string _respuesta, string _descripción)
        {
            int idUsuario = 0;

            BLLogs guardaLog = new BLLogs();
            bool insertaLog = guardaLog.insertaLog("WS_Proveedores", _evento, _accion, _respuesta, _descripción, idUsuario);
        }

        /// <summary>
        /// Metodo para actualizar log
        /// </summary>
        /// <param name="_idlog"></param>
        /// <param name="_descripcion"></param>
        private void actualizaLog(int _idlog, string _descripcion)
        {
            BLLogs busca = new BLLogs();
            string Descripcion = string.Empty;
            try
            {
                List<Logs> csLog = busca.buscaLogs(_idlog);
                if (csLog.Count > 0)
                {
                    Descripcion = csLog[0].Descripcion;
                }

                bool resp = busca.actualizaLog(_idlog, Descripcion + " | " + _descripcion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}