﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Portal;

namespace DALRecepcion.DataAccess.Portal
{
    /// <summary>
    /// Clase para los movimientos de la clase Addenda
    /// </summary>
    internal sealed class DAAdenda : DataAccess<Adenda>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAAdenda _instance = new DAAdenda();

        internal static DAAdenda Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Addenda
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Adenda> Buscar(IBean adenda)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Addenda]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(adenda, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Addenda
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean adenda)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Addenda]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(adenda, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Addenda
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean adenda)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Addenda]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(adenda, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Addenda
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean adenda)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Addenda]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(adenda, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
