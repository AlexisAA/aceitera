﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Portal;

namespace DALRecepcion.DataAccess.Portal
{
    /// <summary>
    /// Clase para los movimientos de la clase Historial
    /// </summary>
    internal sealed class DAHistorial : DataAccess<Historial>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAHistorial _instance = new DAHistorial();

        internal static DAHistorial Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Historial
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Historial> Buscar(IBean historial)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Historial]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(historial, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Historial
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean historial)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Historial]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(historial, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Historial
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean historial)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Historial]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(historial, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Historial
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean historial)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Historial]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(historial, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

    }
}
