﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;

namespace DALRecepcion.DataAccess.Portal
{
    /// <summary>
    /// Clase de Acceso a datos programacion de pagos para varios Stores
    /// </summary>
    public class DAProgramacion_Varios
    {
        /// <summary>
        /// Metodo para buscar Programación (Llena Grid)
        /// </summary>
        /// <param name="ID_Proveedor"></param>
        /// <param name="ID_Sociedad"></param>
        /// <param name="Fecha_Inicio"></param>
        /// <param name="Fecha_Fin"></param>
        /// <param name="Id_Moneda"></param>
        /// <returns></returns>
        public static DataTable daBuscaprogramacion(int ID_Proveedor, int ID_Sociedad, DateTime Fecha_Inicio, DateTime Fecha_Fin, string Id_Moneda)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Programacion_Varios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);
            GenericDataAccess.AddInParameter(comm, "@ID_Proveedor_in", DbType.Int32, ID_Proveedor);
            GenericDataAccess.AddInParameter(comm, "@ID_Sociedad_in", DbType.Int32, ID_Sociedad);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, Fecha_Inicio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_fin_in", DbType.DateTime, Fecha_Fin);
            GenericDataAccess.AddInParameter(comm, "@Moneda_in", DbType.String, Id_Moneda);

            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtArchivos;
        }
    }
}
