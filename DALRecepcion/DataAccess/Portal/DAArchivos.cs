﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Portal;
using System.Data.SqlClient;
using System.Configuration;

namespace DALRecepcion.DataAccess.Portal
{
    /// <summary>
    /// Clase para los movimientos de la clase Archivos
    /// </summary>
    internal sealed class DAArchivos : DataAccess<Archivos>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAArchivos _instance = new DAArchivos();

        internal static DAArchivos Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Archivos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Archivos> Buscar(IBean archivo)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Archivosrecibidos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(archivo, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Archivos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean archivo)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Archivosrecibidos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(archivo, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Archivos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean archivo)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Archivosrecibidos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(archivo, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Archivos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean archivo)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Archivosrecibidos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(archivo, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

    }
}
