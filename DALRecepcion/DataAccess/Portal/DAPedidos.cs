﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Portal;

namespace DALRecepcion.DataAccess.Portal
{
    /// <summary>
    /// Clase para los movimientos de la clase Pedidos
    /// </summary>
    internal sealed class DAPedidos : DataAccess<Pedidos>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAPedidos _instance = new DAPedidos();

        internal static DAPedidos Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Pedidos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Pedidos> Buscar(IBean pedido)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pedidos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(pedido, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Pedidos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean pedido)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pedidos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(pedido, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Pedidos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean pedido)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pedidos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(pedido, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Pedidos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean pedido)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pedidos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(pedido, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
