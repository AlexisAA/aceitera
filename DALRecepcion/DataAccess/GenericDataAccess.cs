﻿using System;

using System.Data;
using System.Data.Common;
using System.Configuration;

/// <summary>
/// Clase con métodos estaticos para acceso a datos.
/// </summary>
internal class GenericDataAccess
{
/// <summary>
    /// Agrega un objeto DbParameter al comando dado.
    /// </summary>
    /// <param name="command">Comando al que se le agrega el parametro de entrada</param>
    /// <param name="name">Nombre del parametro</param>
    /// <param name="dbType">Uno de los valores de System.Data.DbType</param>
    /// <param name="value">El valor del parametro</param>
    public static void AddInParameter(DbCommand command, string name, DbType dbType, object value)
    {
        DbParameter param = command.CreateParameter();
        param.ParameterName = name;
        param.DbType = dbType;
        param.Direction = ParameterDirection.Input;
        param.Value = value;
        command.Parameters.Add(param);
    }

    /// <summary>
    /// Agrega un objeto DbParameter al comando dado.
    /// </summary>
    /// <param name="command">Comando al que se le agrega el parametro de entrada</param>
    /// <param name="name">Nombre del parametro</param>
    /// <param name="value">El valor del parametro</param>
    public static void AddInParameter(DbCommand command, string name, object value)
    {
        DbParameter param = command.CreateParameter();
        param.ParameterName = name;
        param.DbType = DbType.Object;
        param.Direction = ParameterDirection.Input;
        param.Value = value;
        command.Parameters.Add(param);
    }

    /// <summary>
    /// Crea instancia de DBCommand configurada con conexion a bas de datos predeterminada
    /// Tipo Texto
    /// </summary>
    /// <returns>Instancia de DbCommand</returns>
    public static DbCommand CreateCommand()
    {
        DbConnection conn = GenericDataAccess.CreateConnection();
        DbCommand comm = conn.CreateCommand();
        comm.CommandType = CommandType.Text;
        return comm;
    }

    /// <summary>
    /// Crea instancia de DbCommand configurado con la conexion a base de datos predeterminada
    /// Tipo Texto
    /// </summary>
    /// <param name="comando">Sentencia T-Sql</param>
    /// <returns>Instancia de DbCommand</returns>
    public static DbCommand CreateCommand(string comando)
    {
        DbConnection conn = GenericDataAccess.CreateConnection();
        DbCommand comm = conn.CreateCommand();
        comm.CommandType = CommandType.Text;
        comm.CommandText = comando;
        return comm;
    }

    /// <summary>
    /// Crea instancia de DbCommand configurado con la conexion a base de datos predeterminada
    /// Tipo Store Procedure
    /// </summary>
    /// <returns>Instancia de DbCommand</returns>
    public static DbCommand CreateCommandSP()
    {
        DbConnection conn = GenericDataAccess.CreateConnection();
        DbCommand comm = conn.CreateCommand();
        comm.CommandType = CommandType.StoredProcedure;
        return comm;
    }

    /// <summary>
    /// Crea instancia de DbCommand configurado con la conexion a base de datos predeterminada
    /// Tipo Store Procedure
    /// </summary>
    /// <param name="comando">Sentencia T-Sql</param>
    /// <returns>Instancia de DbCommand</returns>
    public static DbCommand CreateCommandSP(string storeProcedure)
    {
        DbConnection conn = GenericDataAccess.CreateConnection();
        DbCommand comm = conn.CreateCommand();
        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandText = storeProcedure;
        return comm;
    }

    /// <summary>
    /// Crea instancia de DbCommand configurado con la conexion a base de datos predeterminada
    /// Tipo Store Procedure
    /// </summary>
    /// <param name="comando">Sentencia T-Sql</param>
    /// <param name="dbTransaction">Transaccion</param>
    /// <returns>Instancia de DbCommand</returns>
    public static DbCommand CreateCommandSP(string storeProcedure, DbTransaction dbTransaction)
    {
        DbCommand comm = dbTransaction.Connection.CreateCommand();
        comm.Transaction = dbTransaction;
        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandText = storeProcedure;
        return comm;
    }

    /// <summary>
    /// Crea un objeto dbTransaction y lo configura con una conexion
    /// </summary>
    /// <returns>Instancia de DbTransaction</returns>
    public static DbTransaction BeginTransaction()
    {
        DbConnection conn = GenericDataAccess.CreateConnection();
        conn.Open();
        DbTransaction dbTransaction = conn.BeginTransaction(IsolationLevel.Serializable);
        return dbTransaction;
    }

    /// <summary>
    /// Realiza la confirmación para la operación realizada dentro de la transaccion especificada
    /// </summary>
    /// <param name="dbTransaction">Transaccion</param>
    public static void CommitTransaction(DbTransaction dbTransaction)
    {
        dbTransaction.Commit();
        dbTransaction.Dispose();
    }

    /// <summary>
    /// Deshace la operación realizada dentro de la transacción especificada
    /// </summary>
    /// <param name="dbTransaction">Transaccion</param>
    public static void RollbackTransaction(DbTransaction dbTransaction)
    {
        dbTransaction.Rollback();
        dbTransaction.Dispose();
    }

    /// <summary>
    /// Ejecuta un comando Select y regresa un juego de datos
    /// </summary>
    /// <param name="command">Instancía DbCommand configurada con una sentencia Select y Conexion</param>
    /// <returns>Resultado de la consulta dentro de un DataTable</returns>
    public static DataTable ExecuteSelectCommand(DbCommand command)
    {
        DataTable tabla;
        try
        {
            command.Connection.Open();
            DbDataReader reader = command.ExecuteReader();
            tabla = new DataTable();
            tabla.Load(reader);
            reader.Close();
            reader.Dispose();
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
        finally
        {
            command.Connection.Close();
            command.Connection.Dispose();
        }
        return tabla;
    }

    /// <summary>
    /// Ejecuta un comando Select y regresa la primera columna del prime registro del resultado de la consulta
    /// </summary>
    /// <param name="command">Instancía DbCommand configurado con una sentencia de consulta</param>
    /// <returns>Valor consultado en la base de datos</returns>
    public static string ExecuteScalar(DbCommand command)
    {
        string value = string.Empty;
        try
        {
            command.Connection.Open();
            value = command.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
        finally
        {
            command.Connection.Close();
            command.Connection.Dispose();
        }
        return value;
    }

    /// <summary>
    /// Ejecuta un comando Select y regresa el parametro de salida indicado
    /// </summary>
    /// <param name="command">Instancia DbCommand configurado con una sentencia de consulta</param>
    /// <param name="parametroSalida">Nombre del parametro de salida del cual se devuelve el valor consultado por el command</param>
    /// <returns>Valor consultado</returns>
    public static string ExecuteScalar(DbCommand command, string parametroSalida)
    {
        string value = string.Empty;
        try
        {
            command.Connection.Open();
            command.ExecuteScalar();
            value = command.Parameters[parametroSalida].Value.ToString();
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
        finally
        {
            command.Connection.Close();
            command.Connection.Dispose();
        }
        return value;
    }

    /// <summary>
    /// Ejecuta sentencia SQL sobre un objeto de la conexion
    /// </summary>
    /// <param name="command">Instancia DBCommand configurado con una sentencia SQL y conexión.</param>
    /// <returns>Devuelve el numero de registros afectados o -1 si no se ejecuto correctamente</returns>
    public static int ExecuteNonQuery(DbCommand command)
    {
        int affectedRows = -1;
        try
        {
            command.Connection.Open();
            affectedRows = command.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
        finally
        {
            command.Connection.Close();
            command.Connection.Dispose();
        }
        return affectedRows;
    }

    /// <summary>
    /// Crea un objeto DbConnection y le configura su cadena de conexión
    /// </summary>
    /// <returns>DbConnection</returns>
    public static DbConnection CreateConnection()
    {
        string providerFactory = string.Empty;
        string connectionString = string.Empty;
        providerFactory = "System.Data.SqlClient";
        connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

        DbProviderFactory factory = DbProviderFactories.GetFactory(providerFactory);
        DbConnection conn = factory.CreateConnection();
        conn.ConnectionString = connectionString;
        return conn;
    }
}
