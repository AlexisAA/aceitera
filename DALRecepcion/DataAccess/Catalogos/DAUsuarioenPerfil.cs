﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Catalogos;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase para los movimientos de la clase Usuarios en perfil
    /// </summary>
    internal sealed class DAUsuarioenPerfil : DataAccess<UsuarioenPerfil>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAUsuarioenPerfil _instance = new DAUsuarioenPerfil();

        internal static DAUsuarioenPerfil Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Usuario en perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<UsuarioenPerfil> Buscar(IBean uenp)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_UsuarioenPerfil]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(uenp, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Usuario en perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean uenp)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_UsuarioenPerfil]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(uenp, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Usuario en perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean uenp)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_UsuarioenPerfil]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(uenp, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Usuario en perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean uenp)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_UsuarioenPerfil]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(uenp, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
