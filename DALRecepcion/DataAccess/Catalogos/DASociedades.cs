﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Catalogos;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase de acceso a datos Sociedades
    /// </summary>
    internal sealed class DASociedades : DataAccess<Sociedades>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DASociedades _instance = new DASociedades();

        internal static DASociedades Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca   
        /// </summary>
        /// <param name="empresa">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Sociedades> Buscar(IBean sociedad)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Sociedades]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(sociedad, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina
        /// </summary>
        /// <param name="empresa">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean sociedad)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Sociedades]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(sociedad, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza
        /// </summary>
        /// <param name="empresa">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean sociedad)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Sociedades]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(sociedad, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta
        /// </summary>
        /// <param name="empresa">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean sociedad)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Sociedades]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(sociedad, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

    }
}
