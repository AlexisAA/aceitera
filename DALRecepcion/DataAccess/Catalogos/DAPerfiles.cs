﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Catalogos;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase para los movimientos de la clase Perfiles
    /// </summary>
    internal sealed class DAPerfiles : DataAccess<Perfiles>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAPerfiles _instance = new DAPerfiles();

        internal static DAPerfiles Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Perfiles> Buscar(IBean perfil)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Perfiles]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(perfil, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean perfil)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Perfiles]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(perfil, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean perfil)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Perfiles]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(perfil, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean perfil)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Perfiles]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(perfil, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }

}
