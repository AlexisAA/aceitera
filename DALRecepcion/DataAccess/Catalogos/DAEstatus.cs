﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Catalogos;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase para los movimientos de la clase Estatus
    /// </summary>
    internal sealed class DAEstatus : DataAccess<Estatus>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAEstatus _instance = new DAEstatus();

        internal static DAEstatus Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Estatus
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Estatus> Buscar(IBean estatus)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Estatus]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(estatus, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Estatus
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean estatus)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Estatus]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(estatus, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Estatus
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean estatus)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Estatus]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(estatus, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Estatus
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean estatus)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Estatus]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(estatus, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

    }
}
