﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Catalogos;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase para los movimientos de la clase Clasificacion
    /// </summary>
    internal sealed class DAClasificacion : DataAccess<Clasificacion>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAClasificacion _instance = new DAClasificacion();

        internal static DAClasificacion Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Clasificacion
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Clasificacion> Buscar(IBean clasifica)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Comentarios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(clasifica, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Clasificacion
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean clasifica)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Comentarios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(clasifica, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Clasificacion
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean clasifica)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Comentarios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(clasifica, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Clasificacion
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean clasifica)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Comentarios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(clasifica, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

    }
}
