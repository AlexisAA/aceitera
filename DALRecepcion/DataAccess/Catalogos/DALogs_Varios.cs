﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase de Acceso a datos Logs para varios Stores
    /// </summary>
    public class DALogs_Varios
    {
        /// <summary>
        /// Metodo DA para busqueda de Logs
        /// </summary>
        /// <param name="ID_Usuario"></param>
        /// <param name="FInicio"></param>
        /// <param name="FFin"></param>
        /// <returns></returns>
        public static DataTable DAbuscaLogs(int ID_Usuario, DateTime FInicio, DateTime FFin)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Logs_Varios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);
            GenericDataAccess.AddInParameter(comm, "@ID_Usuario_in", DbType.Int32, ID_Usuario);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, FInicio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_fin_in", DbType.DateTime, FFin);

            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtArchivos;
        }

    }
}
