﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Catalogos;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase de acceso a datos Monedas
    /// </summary>
    internal sealed class DAMonedas : DataAccess<Monedas>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAMonedas _instance = new DAMonedas();

        internal static DAMonedas Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca   
        /// </summary>
        /// <param name="empresa">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Monedas> Buscar(IBean moneda)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Monedas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(moneda, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina
        /// </summary>
        /// <param name="empresa">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean moneda)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Monedas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(moneda, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza
        /// </summary>
        /// <param name="empresa">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean moneda)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Monedas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(moneda, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta
        /// </summary>
        /// <param name="empresa">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean moneda)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Monedas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(moneda, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

    }
}
