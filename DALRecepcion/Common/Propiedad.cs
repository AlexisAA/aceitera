﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALRecepcion.Common
{
    /// <summary>
    /// Clase Propiedades de los elementos
    /// </summary>
    public class Propiedad
    {
        public string Nombre;
        public Type Tipo;
    }
}
