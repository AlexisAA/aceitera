﻿using System;
using System.Collections.Generic;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Clase Usuarios
    /// </summary>
    public class Usuarios : IBean
    {
        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Usuarios());
        private Dictionary<string, object> _values;

        private int _id_Usuario;
        private string _usuario;
        private string _contrasenia;
        private string _nombre;
        private string _aPaterno;
        private string _aMaterno;
        private DateTime _fecha_Alta;
        private string _bloqueado;
        private DateTime _fecha_Bloqueo;
        private bool _acepta_Contrato;
        private DateTime _fecha_Acpt_Contrato;
        private DateTime _fUltimo_Acceso;
        private bool _activo;
        private bool _valido;
        private int _no_Intentos;
        private int _id_Proveedor;
        #endregion

        #region Constructor
        public Usuarios()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int Id_Usuario
        {
            get { return _id_Usuario; }
            set { _id_Usuario = value; Values["Id_Usuario"] = value; }
        }
        public string Usuario
        {
            get { return _usuario; }
            set { _usuario = value; Values["Usuario"] = value; }
        }
        public string Contrasenia
        {
            get { return _contrasenia; }
            set { _contrasenia = value; Values["Contrasenia"] = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; Values["Nombre"] = value; }
        }
        public string APaterno
        {
            get { return _aPaterno; }
            set { _aPaterno = value; Values["APaterno"] = value; }
        }
        public string AMaterno
        {
            get { return _aMaterno; }
            set { _aMaterno = value; Values["AMaterno"] = value; }
        }
        public DateTime Fecha_Alta
        {
            get { return _fecha_Alta; }
            set { _fecha_Alta = value; Values["Fecha_Alta"] = value; }
        }
        public string Bloqueado
        {
            get { return _bloqueado; }
            set { _bloqueado = value; Values["Bloqueado"] = value; }
        }
        public DateTime Fecha_Bloqueo
        {
            get { return _fecha_Bloqueo; }
            set { _fecha_Bloqueo = value; Values["Fecha_Bloqueo"] = value; }
        }
        public bool Acepta_Contrato
        {
            get { return _acepta_Contrato; }
            set { _acepta_Contrato = value; Values["Acepta_Contrato"] = value; }
        }
        public DateTime Fecha_Acpt_Contrato
        {
            get { return _fecha_Acpt_Contrato; }
            set { _fecha_Acpt_Contrato = value; Values["Fecha_Acpt_Contrato"] = value; }
        }
        public DateTime FUltimo_Acceso
        {
            get { return _fUltimo_Acceso; }
            set { _fUltimo_Acceso = value; Values["FUltimo_Acceso"] = value; }
        }
        public bool Activo
        {
            get { return _activo; }
            set { _activo = value; Values["Activo"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        public int No_Intentos
        {
            get { return _no_Intentos; }
            set { _no_Intentos = value; Values["No_Intentos"] = value; }
        }
        public int Id_Proveedor
        {
            get { return _id_Proveedor; }
            set { _id_Proveedor = value; Values["Id_Proveedor"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("Id_Usuario"))
                Id_Usuario = Convert.ToInt32(values["Id_Usuario"]);
            if (values.ContainsKey("Usuario"))
                Usuario = Convert.ToString(values["Usuario"]);
            if (values.ContainsKey("Contrasenia"))
                Contrasenia = Convert.ToString(values["Contrasenia"]);
            if (values.ContainsKey("Nombre"))
                Nombre = Convert.ToString(values["Nombre"]);
            if (values.ContainsKey("APaterno"))
                APaterno = Convert.ToString(values["APaterno"]);
            if (values.ContainsKey("AMaterno"))
                AMaterno = Convert.ToString(values["AMaterno"]);
            if (values.ContainsKey("Fecha_Alta"))
                Fecha_Alta = Convert.ToDateTime(values["Fecha_Alta"]);
            if (values.ContainsKey("Bloqueado"))
                Bloqueado = Convert.ToString(values["Bloqueado"]);
            if (values.ContainsKey("Fecha_Bloqueo"))
                Fecha_Bloqueo = Convert.ToDateTime(values["Fecha_Bloqueo"]);
            if (values.ContainsKey("Acepta_Contrato"))
                Acepta_Contrato = Convert.ToBoolean(values["Acepta_Contrato"]);
            if (values.ContainsKey("Fecha_Acpt_Contrato"))
                Fecha_Acpt_Contrato = Convert.ToDateTime(values["Fecha_Acpt_Contrato"]);
            if (values.ContainsKey("FUltimo_Acceso"))
                FUltimo_Acceso = Convert.ToDateTime(values["FUltimo_Acceso"]);
            if (values.ContainsKey("Activo"))
                Activo = Convert.ToBoolean(values["Activo"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
            if (values.ContainsKey("No_Intentos"))
                No_Intentos = Convert.ToInt32(values["No_Intentos"]);
            if (values.ContainsKey("Id_Proveedor"))
                Id_Proveedor = Convert.ToInt32(values["Id_Proveedor"]);
        }
        #endregion
    }
}
