﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BUMMenu
    {
        #region Metodos Standar
        public static List<MMenu> Buscar(MMenu menu)
        {
            return DAMMenu.Instance.Buscar(menu);
        }
        public static bool Eliminar(MMenu menu)
        {
            return DAMMenu.Instance.Eliminar(menu);
        }
        public static bool Actualizar(MMenu menu)
        {
            return DAMMenu.Instance.Actualizar(menu);
        }
        public static bool Insertar(MMenu menu)
        {
            return DAMMenu.Instance.Insertar(menu);
        }
        #endregion
    }
}
