﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BUContacto
    {
        #region Metodos Standar
        public static List<Contacto> Buscar(Contacto contacto)
        {
            return DAContacto.Instance.Buscar(contacto);
        }
        public static bool Eliminar(Contacto contacto)
        {
            return DAContacto.Instance.Eliminar(contacto);
        }
        public static bool Actualizar(Contacto contacto)
        {
            return DAContacto.Instance.Actualizar(contacto);
        }
        public static bool Insertar(Contacto contacto)
        {
            return DAContacto.Instance.Insertar(contacto);
        }
        #endregion
    }
}
