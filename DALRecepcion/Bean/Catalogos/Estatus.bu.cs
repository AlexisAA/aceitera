﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BUEstatus
    {
        #region Metodos Standar
        public static List<Estatus> Buscar(Estatus estatus)
        {
            return DAEstatus.Instance.Buscar(estatus);
        }
        public static bool Eliminar(Estatus estatus)
        {
            return DAEstatus.Instance.Eliminar(estatus);
        }
        public static bool Actualizar(Estatus estatus)
        {
            return DAEstatus.Instance.Actualizar(estatus);
        }
        public static bool Insertar(Estatus estatus)
        {
            return DAEstatus.Instance.Insertar(estatus);
        }
        #endregion
    }
}
