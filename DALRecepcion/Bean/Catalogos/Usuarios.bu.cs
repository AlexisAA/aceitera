﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BuUsuarios
    {
        #region Metodos Standar
        public static List<Usuarios> Buscar(Usuarios usuario)
        {
            return DAUsuarios.Instance.Buscar(usuario);
        }
        public static bool Eliminar(Usuarios usuario)
        {
            return DAUsuarios.Instance.Eliminar(usuario);
        }
        public static bool Actualizar(Usuarios usuario)
        {
            return DAUsuarios.Instance.Actualizar(usuario);
        }
        public static bool Insertar(Usuarios usuario)
        {
            return DAUsuarios.Instance.Insertar(usuario);
        }
        #endregion
    }
}
