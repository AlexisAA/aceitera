﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Cales Sociedades
    /// </summary>
    public class Sociedades : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Sociedades());
        private Dictionary<string, object> _values;

        private int _id_Sociedad;
        private int _id_Sociedad_SAP;
        private string _nombre_Sociedad;
        private string _razon_Social;
        private string _rFC_Sociedad;
        private string _usuario_ME;
        private string _contrasenia_ME;
        private bool _valido;
        #endregion

        #region Constructor
        public Sociedades()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Proiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int Id_Sociedad
        {
            get { return _id_Sociedad; }
            set { _id_Sociedad = value; Values["Id_Sociedad"] = value; }
        }
        public int Id_Sociedad_SAP
        {
            get { return _id_Sociedad_SAP; }
            set { _id_Sociedad_SAP = value; Values["Id_Sociedad_SAP"] = value; }
        }
        public string Nombre_Sociedad
        {
            get { return _nombre_Sociedad; }
            set { _nombre_Sociedad = value; Values["Nombre_Sociedad"] = value; }
        }
        public string Razon_Social
        {
            get { return _razon_Social; }
            set { _razon_Social = value; Values["Razon_Social"] = value; }
        }
        public string RFC_Sociedad
        {
            get { return _rFC_Sociedad; }
            set { _rFC_Sociedad = value; Values["RFC_Sociedad"] = value; }
        }
        public string Usuario_ME
        {
            get { return _usuario_ME; }
            set { _usuario_ME = value; Values["Usuario_ME"] = value; }
        }
        public string Contrasenia_ME
        {
            get { return _contrasenia_ME; }
            set { _contrasenia_ME = value; Values["Contrasenia_ME"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodos
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("Id_Sociedad"))
                Id_Sociedad = Convert.ToInt32(values["Id_Sociedad"]);
            if (values.ContainsKey("Id_Sociedad_SAP"))
                Id_Sociedad_SAP = Convert.ToInt32(values["Id_Sociedad_SAP"]);
            if (values.ContainsKey("Nombre_Sociedad"))
                Nombre_Sociedad = Convert.ToString(values["Nombre_Sociedad"]);
            if (values.ContainsKey("Razon_Social"))
                Razon_Social = Convert.ToString(values["Razon_Social"]);
            if (values.ContainsKey("RFC_Sociedad"))
                RFC_Sociedad = Convert.ToString(values["RFC_Sociedad"]);
            if (values.ContainsKey("Usuario_ME"))
                Usuario_ME = Convert.ToString(values["Usuario_ME"]);
            if (values.ContainsKey("Contrasenia_ME"))
                Contrasenia_ME = Convert.ToString(values["Contrasenia_ME"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
