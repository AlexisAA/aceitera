﻿using System;
using System.Collections.Generic;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Clase Usuarios en Perfil
    /// </summary>
    public class UsuarioenPerfil : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new UsuarioenPerfil());
        private Dictionary<string, object> _values;

        private int _iD_UenP;
        private int _iD_Usuario;
        private int _iD_Perfil;
        private bool _valido;
        #endregion

        #region Constructor
        public UsuarioenPerfil()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_UenP
        {
            get { return _iD_UenP; }
            set { _iD_UenP = value; Values["ID_UenP"] = value; }
        }
        public int ID_Usuario
        {
            get { return _iD_Usuario; }
            set { _iD_Usuario = value; Values["ID_Usuario"] = value; }
        }
        public int ID_Perfil
        {
            get { return _iD_Perfil; }
            set { _iD_Perfil = value; Values["ID_Perfil"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_UenP"))
                ID_UenP = Convert.ToInt32(values["ID_UenP"]);
            if (values.ContainsKey("ID_Usuario"))
                ID_Usuario = Convert.ToInt32(values["ID_Usuario"]);
            if (values.ContainsKey("ID_Perfil"))
                ID_Perfil = Convert.ToInt32(values["ID_Perfil"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
