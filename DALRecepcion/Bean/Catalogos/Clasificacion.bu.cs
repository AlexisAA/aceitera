﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BUClasificacion
    {
        #region Metodos Standar
        public static List<Clasificacion> Buscar(Clasificacion clasifica)
        {
            return DAClasificacion.Instance.Buscar(clasifica);
        }
        public static bool Eliminar(Clasificacion clasifica)
        {
            return DAClasificacion.Instance.Eliminar(clasifica);
        }
        public static bool Actualizar(Clasificacion clasifica)
        {
            return DAClasificacion.Instance.Actualizar(clasifica);
        }
        public static bool Insertar(Clasificacion clasifica)
        {
            return DAClasificacion.Instance.Insertar(clasifica);
        }
        #endregion
    }
}
