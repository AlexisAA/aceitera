﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// 
    /// </summary>
    public class BUSociedades
    {
        #region Metodos Standar
        public static List<Sociedades> Buscar(Sociedades sociedad)
        {
            return DASociedades.Instance.Buscar(sociedad);
        }
        public static bool Eliminar(Sociedades sociedad)
        {
            return DASociedades.Instance.Eliminar(sociedad);
        }
        public static bool Actualizar(Sociedades sociedad)
        {
            return DASociedades.Instance.Actualizar(sociedad);
        }
        public static bool Insertar(Sociedades sociedad)
        {
            return DASociedades.Instance.Insertar(sociedad);
        }
        #endregion
    }
}
