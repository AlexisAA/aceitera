﻿using System;
using System.Collections.Generic;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Clase Proveedores
    /// </summary>
    public class Proveedores : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Proveedores());
        private Dictionary<string, object> _values;

        private int _iD_Proveedor;
        private string _iD_Proveedor_SAP;
        private string _nombre;
        private DateTime _fecha_Alta;
        private string _rFC;
        private string _telefono;
        private string _fax;
        private string _banco;
        private string _cuenta_Bancaria;
        private int _dias_Credito;
        private string _direccion;
        private bool _valido;
        private bool _addenda;
        #endregion

        #region Constructor
        public Proveedores()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Proveedor
        {
            get { return _iD_Proveedor; }
            set { _iD_Proveedor = value; Values["ID_Proveedor"] = value; }
        }
        public string ID_Proveedor_SAP
        {
            get { return _iD_Proveedor_SAP; }
            set { _iD_Proveedor_SAP = value; Values["ID_Proveedor_SAP"] = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; Values["Nombre"] = value; }
        }
        public DateTime Fecha_Alta
        {
            get { return _fecha_Alta; }
            set { _fecha_Alta = value; Values["Fecha_Alta"] = value; }
        }
        public string RFC
        {
            get { return _rFC; }
            set { _rFC = value; Values["RFC"] = value; }
        }
        public string Telefono
        {
            get { return _telefono; }
            set { _telefono = value; Values["Telefono"] = value; }
        }
        public string Fax
        {
            get { return _fax; }
            set { _fax = value; Values["Fax"] = value; }
        }
        public string Banco
        {
            get { return _banco; }
            set { _banco = value; Values["Banco"] = value; }
        }
        public string Cuenta_Bancaria
        {
            get { return _cuenta_Bancaria; }
            set { _cuenta_Bancaria = value; Values["Cuenta_Bancaria"] = value; }
        }
        public int Dias_Credito
        {
            get { return _dias_Credito; }
            set { _dias_Credito = value; Values["Dias_Credito"] = value; }
        }
        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; Values["Direccion"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        public bool Addenda
        {
            get { return _addenda; }
            set { _addenda = value; Values["Addenda"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Proveedor"))
                ID_Proveedor = Convert.ToInt32(values["ID_Proveedor"]);
            if (values.ContainsKey("ID_Proveedor_SAP"))
                ID_Proveedor_SAP = Convert.ToString(values["ID_Proveedor_SAP"]);
            if (values.ContainsKey("Nombre"))
                Nombre = Convert.ToString(values["Nombre"]);
            if (values.ContainsKey("Fecha_Alta"))
                Fecha_Alta = Convert.ToDateTime(values["Fecha_Alta"]);
            if (values.ContainsKey("RFC"))
                RFC = Convert.ToString(values["RFC"]);
            if (values.ContainsKey("Telefono"))
                Telefono = Convert.ToString(values["Telefono"]);
            if (values.ContainsKey("Fax"))
                Fax = Convert.ToString(values["Fax"]);
            if (values.ContainsKey("Banco"))
                Banco = Convert.ToString(values["Banco"]);
            if (values.ContainsKey("Cuenta_Bancaria"))
                Cuenta_Bancaria = Convert.ToString(values["Cuenta_Bancaria"]);
            if (values.ContainsKey("Dias_Credito"))
                Dias_Credito = Convert.ToInt32(values["Dias_Credito"]);
            if (values.ContainsKey("Direccion"))
                Direccion = Convert.ToString(values["Direccion"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
            if (values.ContainsKey("Addenda"))
                Addenda = Convert.ToBoolean(values["Addenda"]);
        }
        #endregion

    }
}
