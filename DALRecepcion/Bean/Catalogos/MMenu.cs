﻿using System;
using System.Collections.Generic;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Clase Menu
    /// </summary>
    public class MMenu :IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new MMenu());
        private Dictionary<string, object> _values;

        private int _iD_Menu;
        private string _nombre;
        private string _nombre_en;
        private string _uRL_Menu;
        private string _descripcion;
        private bool _valido;
        #endregion

        #region Constructor
        public MMenu()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Menu
        {
            get { return _iD_Menu; }
            set { _iD_Menu = value; Values["ID_Menu"] = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; Values["Nombre"] = value; }
        }
        public string Nombre_en
        {
            get { return _nombre_en; }
            set { _nombre_en = value; Values["Nombre_en"] = value; }
        }
        public string URL_Menu
        {
            get { return _uRL_Menu; }
            set { _uRL_Menu = value; Values["URL_Menu"] = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Menu"))
                ID_Menu = Convert.ToInt32(values["ID_Menu"]);
            if (values.ContainsKey("Nombre"))
                Nombre = Convert.ToString(values["Nombre"]);
            if (values.ContainsKey("Nombre_en"))
                Nombre_en = Convert.ToString(values["Nombre_en"]);
            if (values.ContainsKey("URL_Menu"))
                URL_Menu = Convert.ToString(values["URL_Menu"]);
            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
