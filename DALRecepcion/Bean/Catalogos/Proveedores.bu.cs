﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BuProveedores
    {
        #region Metodos Standar
        public static List<Proveedores> Buscar(Proveedores proveedor)
        {
            return DAProveedores.Instance.Buscar(proveedor);
        }
        public static bool Eliminar(Proveedores proveedor)
        {
            return DAProveedores.Instance.Eliminar(proveedor);
        }
        public static bool Actualizar(Proveedores proveedor)
        {
            return DAProveedores.Instance.Actualizar(proveedor);
        }
        public static bool Insertar(Proveedores proveedor)
        {
            return DAProveedores.Instance.Insertar(proveedor);
        }
        #endregion
    }
}
