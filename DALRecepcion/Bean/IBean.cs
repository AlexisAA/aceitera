﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALRecepcion.Bean
{
    /// <summary>
    /// Clase Base para entidades Bean
    /// </summary>
    public interface IBean
    {
        /// <summary>
        /// Diccionario de valores de cada propiedad de private Dictionary<string, object> _values;
        /// </summary>
        Dictionary<string, object> Values { get; }

        /// <summary>
        /// List de propiedades contenidas en clase derivada.
        /// </summary>
        IList<DALRecepcion.Common.Propiedad> Propiedades { get; }

        /// <summary>
        /// Pobla propiedades de clase derivada a partir de diccionario de datos( con la misma estructura que Dictionary Values).
        /// </summary>
        /// <param name="values">Dictionary que contiene los valores a asignar.</param>
        void BindFromValues(Dictionary<string, object> values);
    }
}
