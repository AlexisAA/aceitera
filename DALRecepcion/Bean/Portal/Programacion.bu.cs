﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Portal;

namespace DALRecepcion.Bean.Portal
{
    public class BUProgramacion
    {
        #region Metodos Standar
        public static List<Programacion> Buscar(Programacion programacion)
        {
            return DAProgramacion.Instance.Buscar(programacion);
        }
        public static bool Eliminar(Programacion programacion)
        {
            return DAProgramacion.Instance.Eliminar(programacion);
        }
        public static bool Actualizar(Programacion programacion)
        {
            return DAProgramacion.Instance.Actualizar(programacion);
        }
        public static bool Insertar(Programacion programacion)
        {
            return DAProgramacion.Instance.Insertar(programacion);
        }
        #endregion
    }
}
