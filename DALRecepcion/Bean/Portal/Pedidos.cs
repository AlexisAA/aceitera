﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Portal
{
    /// <summary>
    /// Clase Pedidos
    /// </summary>
    public class Pedidos : IBean
    {

        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Pedidos());
        private Dictionary<string, object> _values;

        private int _iD_Pedido;
        private int _iD_Proveedor;
        private string _pedido;
        private string _entrada;
        private string _posicion;
        private string _descripcion;
        private decimal _importe;
        private int _iD_Recepcion;
        #endregion

        #region Constructor
        public Pedidos()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Pedido
        {
            get { return _iD_Pedido; }
            set { _iD_Pedido = value; Values["ID_Pedido"] = value; }
        }
        public int ID_Proveedor
        {
            get { return _iD_Proveedor; }
            set { _iD_Proveedor = value; Values["ID_Proveedor"] = value; }
        }
        public string Pedido
        {
            get { return _pedido; }
            set { _pedido = value; Values["Pedido"] = value; }
        }
        public string Entrada
        {
            get { return _entrada; }
            set { _entrada = value; Values["Entrada"] = value; }
        }
        public string Posicion
        {
            get { return _posicion; }
            set { _posicion = value; Values["Posicion"] = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }
        public decimal Importe
        {
            get { return _importe; }
            set { _importe = value; Values["Importe"] = value; }
        }
        public int ID_Recepcion
        {
            get { return _iD_Recepcion; }
            set { _iD_Recepcion = value; Values["ID_Recepcion"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Pedido"))
                ID_Pedido = Convert.ToInt32(values["ID_Pedido"]);
            if (values.ContainsKey("ID_Proveedor"))
                ID_Proveedor = Convert.ToInt32(values["ID_Proveedor"]);
            if (values.ContainsKey("Pedido"))
                Pedido = Convert.ToString(values["Pedido"]);
            if (values.ContainsKey("Entrada"))
                Entrada = Convert.ToString(values["Entrada"]);
            if (values.ContainsKey("Posicion"))
                Posicion = Convert.ToString(values["Posicion"]);
            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
            if (values.ContainsKey("Importe"))
                Importe = Convert.ToDecimal(values["Importe"]);
            if (values.ContainsKey("ID_Recepcion"))
                ID_Recepcion = Convert.ToInt32(values["ID_Recepcion"]);
            
        }
        #endregion
    }
}
