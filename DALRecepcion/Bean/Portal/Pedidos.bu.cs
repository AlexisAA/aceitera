﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Portal;

namespace DALRecepcion.Bean.Portal
{
    public class BUPedidos
    {
        #region Metodos Standar
        public static List<Pedidos> Buscar(Pedidos pedido)
        {
            return DAPedidos.Instance.Buscar(pedido);
        }
        public static bool Eliminar(Pedidos pedido)
        {
            return DAPedidos.Instance.Eliminar(pedido);
        }
        public static bool Actualizar(Pedidos pedido)
        {
            return DAPedidos.Instance.Actualizar(pedido);
        }
        public static bool Insertar(Pedidos pedido)
        {
            return DAPedidos.Instance.Insertar(pedido);
        }
        #endregion
    }
}
