﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Portal;

namespace DALRecepcion.Bean.Portal
{
    public class BUArchivos
    {
        #region Metodos Standar
        public static List<Archivos> Buscar(Archivos archivo)
        {
            //SqlConnection conSQL;
            //conSQL = new SqlConnection();
            //string connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            //conSQL.ConnectionString = connectionString;
            ////sqlCommand = conSQL.CreateCommand();
            //conSQL.Open();
            //SqlCommand SqlCommand = new SqlCommand("sp_Archivosrecibidos", conSQL);
            //SqlCommand.CommandType = CommandType.StoredProcedure;
            //SqlCommand.Parameters.AddWithValue("@Propiedades_in", "val");
            //SqlDataReader datareader = SqlCommand.ExecuteReader();

            return DAArchivos.Instance.Buscar(archivo);

        }
        public static bool Eliminar(Archivos archivo)
        {
            return DAArchivos.Instance.Eliminar(archivo);
        }
        public static bool Actualizar(Archivos archivo)
        {
            return DAArchivos.Instance.Actualizar(archivo);
        }
        public static bool Insertar(Archivos archivo)
        {
            return DAArchivos.Instance.Insertar(archivo);
        }
        #endregion
    }
}
