﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Portal
{
    /// <summary>
    /// Clase Addenda
    /// </summary>
    public class Adenda : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Adenda());
        private Dictionary<string, object> _values;

        private int _iD_Addenda;
        private string _nombre_XML;
        private int _iD_Proveedor;
        private string _iD_Proveedor_SAP;
        private string _addenda;

        
        #endregion

        #region Constructor
        public Adenda()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propidedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Addenda
        {
            get { return _iD_Addenda; }
            set { _iD_Addenda = value; Values["ID_Addenda"] = value; }
        }
        public string Nombre_XML
        {
            get { return _nombre_XML; }
            set { _nombre_XML = value; Values["Nombre_XML"] = value; }
        }
        public int ID_Proveedor
        {
            get { return _iD_Proveedor; }
            set { _iD_Proveedor = value; Values["ID_Proveedor"] = value; }
        }
        public string ID_Proveedor_SAP
        {
            get { return _iD_Proveedor_SAP; }
            set { _iD_Proveedor_SAP = value; Values["ID_Proveedor_SAP"] = value; }
        }
        public string Addenda
        {
            get { return _addenda; }
            set { _addenda = value; Values["Addenda"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Addenda"))
                ID_Addenda = Convert.ToInt32(values["ID_Addenda"]);
            if (values.ContainsKey("Nombre_XML"))
                Nombre_XML = Convert.ToString(values["Nombre_XML"]);
            if (values.ContainsKey("ID_Proveedor"))
                ID_Proveedor = Convert.ToInt32(values["ID_Proveedor"]);
            if (values.ContainsKey("ID_Proveedor_SAP"))
                ID_Proveedor_SAP = Convert.ToString(values["ID_Proveedor_SAP"]);
            if (values.ContainsKey("Addenda"))
                Addenda = Convert.ToString(values["Addenda"]);
        }
        #endregion
    }
}
