﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Portal;

namespace DALRecepcion.Bean.Portal
{
    public class BUAdenda
    {
        #region Metodos Standar
        public static List<Adenda> Buscar(Adenda addenda)
        {
            return DAAdenda.Instance.Buscar(addenda);
        }
        public static bool Eliminar(Adenda addenda)
        {
            return DAAdenda.Instance.Eliminar(addenda);
        }
        public static bool Actualizar(Adenda addenda)
        {
            return DAAdenda.Instance.Actualizar(addenda);
        }
        public static bool Insertar(Adenda addenda)
        {
            return DAAdenda.Instance.Insertar(addenda);
        }
        #endregion
    }
}
