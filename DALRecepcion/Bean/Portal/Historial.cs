﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Portal
{
    /// <summary>
    /// Clase Historial
    /// </summary>
    public class Historial : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Historial());
        private Dictionary<string, object> _values;

        private int _iD_Historial;
        private int _iD_Programacion;
        private int _iD_Proveedor;
        private int _iD_Sociedad;
        private DateTime _fecha_Recepcion;
        private string _serie;
        private string _folio;
        private string _uUID;
        private DateTime _fecha_Factura;
        private decimal _importe;
        private string _moneda;
        private Int64 _pedido;
        private string _docto_Compensacion;
        private DateTime _fecha_Pago;
        private string _observaciones;
        private bool _valido;
        #endregion

        #region Constructor
        public Historial()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Historial
        {
            get { return _iD_Historial; }
            set { _iD_Historial = value; Values["ID_Historial"] = value; }
        }
        public int ID_Programacion
        {
            get { return _iD_Programacion; }
            set { _iD_Programacion = value; Values["ID_Programacion"] = value; }
        }
        public int ID_Proveedor
        {
            get { return _iD_Proveedor; }
            set { _iD_Proveedor = value; Values["ID_Proveedor"] = value; }
        }
        public int ID_Sociedad
        {
            get { return _iD_Sociedad; }
            set { _iD_Sociedad = value; Values["ID_Sociedad"] = value; }
        }
        public DateTime Fecha_Recepcion
        {
            get { return _fecha_Recepcion; }
            set { _fecha_Recepcion = value; Values["Fecha_Recepcion"] = value; }
        }
        public string Serie
        {
            get { return _serie; }
            set { _serie = value; Values["Serie"] = value; }
        }
        public string Folio
        {
            get { return _folio; }
            set { _folio = value; Values["Folio"] = value; }
        }
        public string UUID
        {
            get { return _uUID; }
            set { _uUID = value; Values["UUID"] = value; }
        }
        public DateTime Fecha_Factura
        {
            get { return _fecha_Factura; }
            set { _fecha_Factura = value; Values["Fecha_Factura"] = value; }
        }
        public decimal Importe
        {
            get { return _importe; }
            set { _importe = value; Values["Importe"] = value; }
        }
        public string Moneda
        {
            get { return _moneda; }
            set { _moneda = value; Values["Moneda"] = value; }
        }
        public Int64 Pedido
        {
            get { return _pedido; }
            set { _pedido = value; Values["Pedido"] = value; }
        }
        public string Docto_Compensacion
        {
            get { return _docto_Compensacion; }
            set { _docto_Compensacion = value; Values["Docto_Compensacion"] = value; }
        }
        public DateTime Fecha_Pago
        {
            get { return _fecha_Pago; }
            set { _fecha_Pago = value; Values["Fecha_Pago"] = value; }
        }
        public string Observaciones
        {
            get { return _observaciones; }
            set { _observaciones = value; Values["Observaciones"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Historial"))
                ID_Historial = Convert.ToInt32(values["ID_Historial"]);
            if (values.ContainsKey("ID_Programacion"))
                ID_Programacion = Convert.ToInt32(values["ID_Programacion"]);
            if (values.ContainsKey("ID_Proveedor"))
                ID_Proveedor = Convert.ToInt32(values["ID_Proveedor"]);
            if (values.ContainsKey("ID_Sociedad"))
                ID_Sociedad = Convert.ToInt32(values["ID_Sociedad"]);
            if (values.ContainsKey("Fecha_Recepcion"))
                Fecha_Recepcion = Convert.ToDateTime(values["Fecha_Recepcion"]);
            if (values.ContainsKey("Serie"))
                Serie = Convert.ToString(values["Serie"]);
            if (values.ContainsKey("Folio"))
                Folio = Convert.ToString(values["Folio"]);
            if (values.ContainsKey("UUID"))
                UUID = Convert.ToString(values["UUID"]);
            if (values.ContainsKey("Fecha_Factura"))
                Fecha_Factura = Convert.ToDateTime(values["Fecha_Factura"]);
            if (values.ContainsKey("Importe"))
                Importe = Convert.ToDecimal(values["Importe"]);
            if (values.ContainsKey("Moneda"))
                Moneda = Convert.ToString(values["Moneda"]);
            if (values.ContainsKey("Pedido"))
                Pedido = Convert.ToInt64(values["Pedido"]);
            if (values.ContainsKey("Docto_Compensacion"))
                Docto_Compensacion = Convert.ToString(values["Docto_Compensacion"]);
            if (values.ContainsKey("Fecha_Pago"))
                Fecha_Pago = Convert.ToDateTime(values["Fecha_Pago"]);
            if (values.ContainsKey("Observaciones"))
                Observaciones = Convert.ToString(values["Observaciones"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
