﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using DALRecepcion.Common;

namespace DALRecepcion.Bean
{
    /// <summary>
    /// 
    /// </summary>
    internal class BeanHelper
    {
        /// <summary>
        /// Obtiene la lista de propiedades que pertenecen a la clase del objeto dado
        /// </summary>
        /// <param name="objectValue"></param>
        /// <returns>List</returns>
        public static List<Propiedad> obtenPropiedades(object objectValue)
        {
            List<Propiedad> propiedades = new List<Propiedad>();
            PropertyInfo[] infoPropiedades = objectValue.GetType().GetProperties();
            foreach (PropertyInfo info in infoPropiedades)
            {
                if (info.PropertyType.BaseType != typeof(IBean) && info.Name.ToLower() != "values" && info.Name.ToLower() != "propiedad")
                {
                    propiedades.Add(new Propiedad { Nombre = info.Name, Tipo = info.PropertyType });
                }
            }
            return propiedades;
        }

        /// <summary>
        /// Obtiene el listado de los valores que seran asignados a las propiedades de un objeto
        /// </summary>
        /// <param name="propiedades"></param>
        /// <returns>Valor</returns>
        public static Dictionary<string, object> getListValues(List<Propiedad> propiedades)
        {
            if (propiedades != null)
            {
                Dictionary<string, object> values = new Dictionary<string, object>();
                foreach (Propiedad propiedad in propiedades)
                    values.Add(propiedad.Nombre, null);

                return values;
            }
            return null;
        }

        /// <summary>
        /// Obtiene el nombre que se maneja como parametro de entrada en las funciones de la BD
        /// con base en el nombre de la propiedad del Bean
        /// </summary>
        /// <param name="propiedad"></param>
        /// <returns></returns>
        public static string getCampoFromPropiedad(string propiedad)
        {
            return string.Format("@{0}_in", propiedad);
        }

    }
}
