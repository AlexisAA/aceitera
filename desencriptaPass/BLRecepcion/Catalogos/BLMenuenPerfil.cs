﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLMenuenPerfil
    {
        /// <summary>
        /// Metodo para buscar el menu asignado al perfil
        /// </summary>
        /// <param name="ID_Perfil"></param>
        /// <returns></returns>
        public List<MenuenPerfil> buscaMenPxperfil(int ID_Perfil)
        {
            MenuenPerfil menp = new MenuenPerfil();
            menp.ID_Perfil = ID_Perfil;
            menp.Valido = true;
            return BUMenuenPerfil.Buscar(menp);
        }
    }
}
