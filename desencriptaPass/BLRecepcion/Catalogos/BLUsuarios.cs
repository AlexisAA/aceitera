﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLUsuarios
    {

        #region buscaUsuario Varios
        /// <summary>
        /// Meodo para buscar usuario por ID
        /// </summary>
        /// <param name="idUsuario">ID deusuario</param>
        /// <returns>Lista de usuario</returns>
        public List<Usuarios> buscaUsuarioXId(int idUsuario)
        {
            Usuarios usuario = new Usuarios();
            usuario.Id_Usuario = idUsuario;
            try
            {
                return BuUsuarios.Buscar(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar todos los usuarios por proveedor
        /// </summary>
        /// <param name="Id_Proveedor"></param>
        /// <returns></returns>
        public List<Usuarios> buscaUsuariosxproveedor(int Id_Proveedor)
        {
            Usuarios usuario = new Usuarios();
            usuario.Id_Proveedor = Id_Proveedor;
            try
            {
                return BuUsuarios.Buscar(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar todos los usuarios por proveedor
        /// </summary>
        /// <param name="Id_Proveedor"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public List<Usuarios> buscaUsuariosxproveedor(int Id_Proveedor, bool Valido)
        {
            Usuarios usuario = new Usuarios();
            usuario.Id_Proveedor = Id_Proveedor;
            usuario.Valido = Valido;
            try
            {
                return BuUsuarios.Buscar(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region buscaUsuario sobrecargado
        /// <summary>
        /// Metodo para buscar todos los usuarios
        /// </summary>
        /// <returns></returns>
        public List<Usuarios> buscaUsuario()
        {
            Usuarios usuarios = new Usuarios();
            try
            {
                return BuUsuarios.Buscar(usuarios);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar usuario por nombre de usuario
        /// </summary>
        /// <param name="nombreusuario">Usuario</param>
        /// <returns>Lista de Usuario</returns>
        public List<Usuarios> buscaUsuario(string nombreusuario)
        {
            Usuarios usuario = new Usuarios();
            usuario.Usuario = nombreusuario;
            try
            {
                return BuUsuarios.Buscar(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar usuario
        /// </summary>
        /// <param name="ID_Usuario"></param>
        /// <param name="Usuario"></param>
        /// <param name="Nombre"></param>
        /// <param name="APaterno"></param>
        /// <param name="AMaterno"></param>
        /// <param name="ID_Proveedor"></param>
        /// <param name="Bloqueado"></param>
        /// <param name="Activo"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public List<Usuarios> buscaUsuario(int ID_Usuario, string Usuario, string Nombre, string APaterno, string AMaterno,
            int ID_Proveedor, string Bloqueado, bool Activo, bool Valido)
        {
            Usuarios usuarios = new Usuarios();
            usuarios.Id_Usuario = ID_Usuario;
            usuarios.Usuario = Usuario;
            usuarios.Nombre = Nombre;
            usuarios.APaterno = APaterno;
            usuarios.AMaterno = AMaterno;
            usuarios.Id_Proveedor = ID_Proveedor;
            usuarios.Bloqueado = Bloqueado;
            usuarios.Activo = Activo;
            usuarios.Valido = Valido;
            try
            {
                return BuUsuarios.Buscar(usuarios);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region ABC Usuarios
        /// <summary>
        /// Metodo para desbloquear usuario
        /// </summary>
        /// <param name="Id_Usuario">ID de usuario</param>
        /// <returns>True si se desbloque, Fals si no</returns>
        public bool desbloquea(int Id_Usuario)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(Id_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = buscausuario[0].Contrasenia;
                    usuario.Nombre = buscausuario[0].Nombre;
                    usuario.APaterno = buscausuario[0].APaterno;
                    usuario.AMaterno = buscausuario[0].AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = "NO";
                    usuario.Fecha_Bloqueo = buscausuario[0].Fecha_Bloqueo;
                    usuario.Acepta_Contrato = buscausuario[0].Acepta_Contrato;
                    usuario.Fecha_Acpt_Contrato = buscausuario[0].Fecha_Acpt_Contrato;
                    usuario.FUltimo_Acceso = buscausuario[0].FUltimo_Acceso;
                    usuario.Activo = buscausuario[0].Activo;
                    usuario.Valido = buscausuario[0].Valido;
                    usuario.No_Intentos = 0;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para agregar un intento
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <param name="No_Intento"></param>
        /// <returns></returns>
        public bool intentos(int Id_Usuario, int No_Intento)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(Id_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = buscausuario[0].Contrasenia;
                    usuario.Nombre = buscausuario[0].Nombre;
                    usuario.APaterno = buscausuario[0].APaterno;
                    usuario.AMaterno = buscausuario[0].AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = buscausuario[0].Bloqueado;
                    usuario.Fecha_Bloqueo = buscausuario[0].Fecha_Bloqueo;
                    usuario.Acepta_Contrato = buscausuario[0].Acepta_Contrato;
                    usuario.Fecha_Acpt_Contrato = buscausuario[0].Fecha_Acpt_Contrato;
                    usuario.FUltimo_Acceso = buscausuario[0].FUltimo_Acceso;
                    usuario.Activo = buscausuario[0].Activo;
                    usuario.Valido = buscausuario[0].Valido;
                    usuario.No_Intentos = No_Intento;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para Bloquear usuario
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public bool bloquea(int Id_Usuario)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(Id_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = buscausuario[0].Contrasenia;
                    usuario.Nombre = buscausuario[0].Nombre;
                    usuario.APaterno = buscausuario[0].APaterno;
                    usuario.AMaterno = buscausuario[0].AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = "SI";
                    usuario.Fecha_Bloqueo = DateTime.Now;
                    usuario.Acepta_Contrato = buscausuario[0].Acepta_Contrato;
                    usuario.Fecha_Acpt_Contrato = buscausuario[0].Fecha_Acpt_Contrato;
                    usuario.FUltimo_Acceso = buscausuario[0].FUltimo_Acceso;
                    usuario.Activo = buscausuario[0].Activo;
                    usuario.Valido = buscausuario[0].Valido;
                    usuario.No_Intentos = buscausuario[0].No_Intentos;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para aceptar oliticas.
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public bool aceptaPoliticas(int Id_Usuario)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(Id_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = buscausuario[0].Contrasenia;
                    usuario.Nombre = buscausuario[0].Nombre;
                    usuario.APaterno = buscausuario[0].APaterno;
                    usuario.AMaterno = buscausuario[0].AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = buscausuario[0].Bloqueado;
                    usuario.Fecha_Bloqueo = buscausuario[0].Fecha_Bloqueo;
                    usuario.Acepta_Contrato = true;
                    usuario.Fecha_Acpt_Contrato = DateTime.Now;
                    usuario.FUltimo_Acceso = buscausuario[0].FUltimo_Acceso;
                    usuario.Activo = buscausuario[0].Activo;
                    usuario.Valido = buscausuario[0].Valido;
                    usuario.No_Intentos = buscausuario[0].No_Intentos;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para registrar el Acceso del usuario
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public bool registraAcceso(int Id_Usuario)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(Id_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = buscausuario[0].Contrasenia;
                    usuario.Nombre = buscausuario[0].Nombre;
                    usuario.APaterno = buscausuario[0].APaterno;
                    usuario.AMaterno = buscausuario[0].AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = buscausuario[0].Bloqueado;
                    usuario.Fecha_Bloqueo = buscausuario[0].Fecha_Bloqueo;
                    usuario.Acepta_Contrato = buscausuario[0].Acepta_Contrato;
                    usuario.Fecha_Acpt_Contrato = buscausuario[0].Fecha_Acpt_Contrato;
                    usuario.FUltimo_Acceso = DateTime.Now;
                    usuario.Activo = true;
                    usuario.Valido = buscausuario[0].Valido;
                    usuario.No_Intentos = buscausuario[0].No_Intentos;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para cerrar sesión del usuario
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public bool cerrarSesion(int Id_Usuario)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(Id_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = buscausuario[0].Contrasenia;
                    usuario.Nombre = buscausuario[0].Nombre;
                    usuario.APaterno = buscausuario[0].APaterno;
                    usuario.AMaterno = buscausuario[0].AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = buscausuario[0].Bloqueado;
                    usuario.Fecha_Bloqueo = buscausuario[0].Fecha_Bloqueo;
                    usuario.Acepta_Contrato = buscausuario[0].Acepta_Contrato;
                    usuario.Fecha_Acpt_Contrato = buscausuario[0].Fecha_Acpt_Contrato;
                    usuario.FUltimo_Acceso = DateTime.Now;
                    usuario.Activo = false;
                    usuario.Valido = buscausuario[0].Valido;
                    usuario.No_Intentos = buscausuario[0].No_Intentos;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para desactivar usuario
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public bool desactivaUsuario(int Id_Usuario)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(Id_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = buscausuario[0].Contrasenia;
                    usuario.Nombre = buscausuario[0].Nombre;
                    usuario.APaterno = buscausuario[0].APaterno;
                    usuario.AMaterno = buscausuario[0].AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = buscausuario[0].Bloqueado;
                    usuario.Fecha_Bloqueo = DateTime.Now;
                    usuario.Acepta_Contrato = buscausuario[0].Acepta_Contrato;
                    usuario.Fecha_Acpt_Contrato = buscausuario[0].Fecha_Acpt_Contrato;
                    usuario.FUltimo_Acceso = buscausuario[0].FUltimo_Acceso;
                    usuario.Activo = false;
                    usuario.Valido = buscausuario[0].Valido;
                    usuario.No_Intentos = buscausuario[0].No_Intentos;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para insertar usuario
        /// </summary>
        /// <param name="Usuario"></param>
        /// <param name="Nombre"></param>
        /// <param name="APaterno"></param>
        /// <param name="AMaterno"></param>
        /// <param name="Contrasenia"></param>
        /// <param name="No_Intentos"></param>
        /// <param name="Bloqueado"></param>
        /// <param name="Activo"></param>
        /// <param name="Valido"></param>
        /// <param name="Id_Proveedor"></param>
        /// <returns></returns>
        public int insertaUsuario(string Usuario, string Nombre, string APaterno, string AMaterno, string Contrasenia,
            int No_Intentos, string Bloqueado, bool Activo, bool Valido, int Id_Proveedor)
        {
            int id_Usuario = -1;
            Usuarios usuario = new Usuarios();
            usuario.Usuario = Usuario;
            usuario.Contrasenia = Contrasenia;
            usuario.Nombre = Nombre;
            usuario.APaterno = APaterno;
            usuario.AMaterno = AMaterno;
            usuario.Fecha_Alta = DateTime.Now;
            usuario.Bloqueado = Bloqueado;
            usuario.Fecha_Bloqueo = DateTime.Now;
            usuario.Acepta_Contrato = false;
            usuario.Fecha_Acpt_Contrato = DateTime.Now;
            usuario.Activo = Activo;
            usuario.Valido = Valido;
            usuario.No_Intentos = No_Intentos;
            usuario.Id_Proveedor = Id_Proveedor;

            try
            {
                bool bresp = false;
                bresp = BuUsuarios.Insertar(usuario);
                if (bresp)
                {
                    List<Usuarios> busca = new List<Usuarios>();
                    busca = buscaUsuario(Usuario);
                    if (busca.Count > 0)
                    {
                        id_Usuario = busca[0].Id_Usuario;
                    }
                    else
                        id_Usuario = -1;
                }
                else
                    id_Usuario = -1;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return id_Usuario;
        }

        /// <summary>
        /// Metodo para actualizar usuario
        /// </summary>
        /// <param name="ID_Usuario"></param>
        /// <param name="Nombre"></param>
        /// <param name="APaterno"></param>
        /// <param name="AMaterno"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public bool actualizaUsuario(int ID_Usuario, string Nombre, string APaterno, string AMaterno, bool Valido)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(ID_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = buscausuario[0].Contrasenia; ;
                    usuario.Nombre = Nombre;
                    usuario.APaterno = APaterno;
                    usuario.AMaterno = AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = buscausuario[0].Bloqueado;
                    usuario.Fecha_Bloqueo = buscausuario[0].Fecha_Bloqueo;
                    usuario.Acepta_Contrato = buscausuario[0].Acepta_Contrato;
                    usuario.Fecha_Acpt_Contrato = buscausuario[0].Fecha_Acpt_Contrato;
                    usuario.FUltimo_Acceso = buscausuario[0].FUltimo_Acceso;
                    usuario.Activo = buscausuario[0].Activo;
                    usuario.Valido = Valido;
                    usuario.No_Intentos = buscausuario[0].No_Intentos;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para actualizar usuario
        /// </summary>
        /// <param name="ID_Usuario"></param>
        /// <param name="Nombre"></param>
        /// <param name="APaterno"></param>
        /// <param name="AMaterno"></param>
        /// <param name="Contrasenia"></param>
        /// <param name="No_Intentos"></param>
        /// <param name="Bloqueado"></param>
        /// <param name="Activo"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public bool actualizaUsuario(int ID_Usuario, string Nombre, string APaterno, string AMaterno, string Contrasenia,
            int No_Intentos, string Bloqueado, bool Activo, bool Valido)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(ID_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = Contrasenia;
                    usuario.Nombre = Nombre;
                    usuario.APaterno = APaterno;
                    usuario.AMaterno = AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = Bloqueado;
                    usuario.Fecha_Bloqueo = buscausuario[0].Fecha_Bloqueo;
                    usuario.Acepta_Contrato = buscausuario[0].Acepta_Contrato;
                    usuario.Fecha_Acpt_Contrato = buscausuario[0].Fecha_Acpt_Contrato;
                    usuario.FUltimo_Acceso = buscausuario[0].FUltimo_Acceso;
                    usuario.Activo = Activo;
                    usuario.Valido = Valido;
                    usuario.No_Intentos = No_Intentos;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para cambiar contrasenia
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <param name="Contrasenia"></param>
        /// <returns></returns>
        public bool cambiaContrasenia(int Id_Usuario, string Contrasenia)
        {
            Usuarios usuario = new Usuarios();
            try
            {
                List<Usuarios> buscausuario = buscaUsuarioXId(Id_Usuario);
                if (buscausuario.Count > 0)
                {
                    usuario.Id_Usuario = buscausuario[0].Id_Usuario;
                    usuario.Usuario = buscausuario[0].Usuario;
                    usuario.Contrasenia = Contrasenia;
                    usuario.Nombre = buscausuario[0].Nombre;
                    usuario.APaterno = buscausuario[0].APaterno;
                    usuario.AMaterno = buscausuario[0].AMaterno;
                    usuario.Fecha_Alta = buscausuario[0].Fecha_Alta;
                    usuario.Bloqueado = buscausuario[0].Bloqueado;
                    usuario.Fecha_Bloqueo = buscausuario[0].Fecha_Bloqueo;
                    usuario.Acepta_Contrato = buscausuario[0].Acepta_Contrato;
                    usuario.Fecha_Acpt_Contrato = buscausuario[0].Fecha_Acpt_Contrato;
                    usuario.FUltimo_Acceso = buscausuario[0].FUltimo_Acceso;
                    usuario.Activo = buscausuario[0].Activo;
                    usuario.Valido = buscausuario[0].Valido;
                    usuario.No_Intentos = buscausuario[0].No_Intentos;
                    usuario.Id_Proveedor = buscausuario[0].Id_Proveedor;

                    return BuUsuarios.Actualizar(usuario);
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
