﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLSociedades
    {

        #region Busquedas Varias
        /// <summary>
        /// Metodo para buscar Sociedad por ID
        /// </summary>
        /// <param name="Id_Sociedad"></param>
        /// <returns></returns>
        public List<Sociedades> buscaSociedadxID(int Id_Sociedad)
        {
            Sociedades sociedad = new Sociedades();
            sociedad.Id_Sociedad = Id_Sociedad;
            try
            {
                return BUSociedades.Buscar(sociedad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar sociedad por ID SAP
        /// </summary>
        /// <param name="Id_Sociedad_SAP"></param>
        /// <returns></returns>
        public List<Sociedades> buscaIdsociedad(int Id_Sociedad_SAP)
        {
            Sociedades sociedad = new Sociedades();
            sociedad.Id_Sociedad_SAP = Id_Sociedad_SAP;
            try
            {
                return BUSociedades.Buscar(sociedad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region buscaSociedad sobrecargado
        /// <summary>
        /// Metodo para buscar sociedad
        /// </summary>
        /// <returns></returns>
        public List<Sociedades> buscaSociedad()
        {
            Sociedades sociedad = new Sociedades();
            sociedad.Valido = true;
            try
            {
                return BUSociedades.Buscar(sociedad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar sociedad por RFC
        /// </summary>
        /// <param name="RFC"></param>
        /// <returns></returns>
        public List<Sociedades> buscaSociedad(string RFC)
        {
            Sociedades sociedad = new Sociedades();
            sociedad.RFC_Sociedad = RFC;
            try
            {
                return BUSociedades.Buscar(sociedad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metosdo para buscar sociedad
        /// </summary>
        /// <param name="ID_Sociedad_SAP"></param>
        /// <param name="Nombre_Sociedad"></param>
        /// <param name="Razon_Social"></param>
        /// <param name="RFC_Sociedad"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public List<Sociedades> buscaSociedad(int ID_Sociedad_SAP, string Nombre_Sociedad, string Razon_Social, string RFC_Sociedad, bool Valido)
        {
            Sociedades sociedad = new Sociedades();
            sociedad.Id_Sociedad_SAP = ID_Sociedad_SAP;
            sociedad.Nombre_Sociedad = Nombre_Sociedad;
            sociedad.Razon_Social = Razon_Social;
            sociedad.RFC_Sociedad = RFC_Sociedad;
            sociedad.Valido = Valido;
            try
            {
                return BUSociedades.Buscar(sociedad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ABC Sociedades
        /// <summary>
        /// Metodo para insertar Sociedad
        /// </summary>
        /// <param name="ID_Sociedad_SAP"></param>
        /// <param name="Nombre_Sociedad"></param>
        /// <param name="Razon_Social"></param>
        /// <param name="RFC_Sociedad"></param>
        /// <param name="Usuario_ME"></param>
        /// <param name="Cntrasenia_ME"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public bool insertaSociedad(int ID_Sociedad_SAP, string Nombre_Sociedad, string Razon_Social, string RFC_Sociedad, string Usuario_ME, string Cntrasenia_ME, bool Valido)
        {
            Sociedades sociedad = new Sociedades();
            sociedad.Id_Sociedad_SAP = ID_Sociedad_SAP;
            sociedad.Nombre_Sociedad = Nombre_Sociedad;
            sociedad.Razon_Social = Razon_Social;
            sociedad.RFC_Sociedad = RFC_Sociedad;
            sociedad.Usuario_ME = Usuario_ME;
            sociedad.Contrasenia_ME = Cntrasenia_ME;
            sociedad.Valido = Valido;

            try
            {
                return BUSociedades.Insertar(sociedad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para actualizar Sociedad
        /// </summary>
        /// <param name="ID_Sociedad"></param>
        /// <param name="Nombre_Sociedad"></param>
        /// <param name="Razon_Social"></param>
        /// <param name="Usuario_ME"></param>
        /// <param name="Cntrasenia_ME"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public bool actualizaSociedad(int ID_Sociedad, string Nombre_Sociedad, string Razon_Social, string Usuario_ME, string Cntrasenia_ME, bool Valido)
        {
            Sociedades sociedad = new Sociedades();
            sociedad.Id_Sociedad = ID_Sociedad;
            sociedad.Nombre_Sociedad = Nombre_Sociedad;
            sociedad.Razon_Social = Razon_Social;
            sociedad.Usuario_ME = Usuario_ME;
            sociedad.Contrasenia_ME = Cntrasenia_ME;
            sociedad.Valido = Valido;

            try
            {
                return BUSociedades.Actualizar(sociedad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
