﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLClasificacion
    {

        /// <summary>
        /// Metodo para buscar clasificacion
        /// </summary>
        /// <returns></returns>
        public List<Clasificacion> buscaClasificacion()
        {
            Clasificacion clasifica = new Clasificacion();
            clasifica.Valido = true;
            try
            {
                return BUClasificacion.Buscar(clasifica);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
