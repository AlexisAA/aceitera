﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Catalogos;

namespace BLRecepcion.Catalogos
{
    public class BLUsuarioenPerfil
    {
        /// <summary>
        /// Metodo para buscar usuario en perfil por usuario Validos
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public List<UsuarioenPerfil> buscaporUsuario(int Id_Usuario)
        {
            UsuarioenPerfil uenp = new UsuarioenPerfil();
            uenp.ID_Usuario = Id_Usuario;
            uenp.Valido = true;
            try
            {
                return BuUsuarioenPerfil.Buscar(uenp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar usuarios en perfil por perfil
        /// </summary>
        /// <param name="ID_Perfil"></param>
        /// <returns></returns>
        public List<UsuarioenPerfil> buscaporPerfil(int ID_Perfil)
        {
            UsuarioenPerfil uenp = new UsuarioenPerfil();
            uenp.ID_Perfil = ID_Perfil;
            uenp.Valido = true;
            try
            {
                return BuUsuarioenPerfil.Buscar(uenp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar usuario
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <param name="Id_Perfil"></param>
        /// <returns></returns>
        public List<UsuarioenPerfil> buscaporUsuario(int Id_Usuario, int Id_Perfil)
        {
            UsuarioenPerfil uenp = new UsuarioenPerfil();
            uenp.ID_Usuario = Id_Usuario;
            uenp.ID_Perfil = Id_Perfil;
            try
            {
                return BuUsuarioenPerfil.Buscar(uenp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para insertar usuario en perfil
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <param name="Id_Perfil"></param>
        /// <returns></returns>
        public bool insertaUenP(int Id_Usuario, int Id_Perfil)
        {
            UsuarioenPerfil uenp = new UsuarioenPerfil();
            uenp.ID_Usuario = Id_Usuario;
            uenp.ID_Perfil = Id_Perfil;
            uenp.Valido = true;

            try
            {
                return BuUsuarioenPerfil.Insertar(uenp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
