﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Portal;
using DALRecepcion.DataAccess.Portal;
using System.Data;

namespace BLRecepcion.Portal
{
    /// <summary>
    /// 
    /// </summary>
    public class BLHistorial
    {

        #region buscaHistorial sobrecargado
        /// <summary>
        /// Metodo para buscar historial
        /// </summary>
        /// <param name="UUID"></param>
        /// <returns></returns>
        public List<Historial> buscaHistorial(string UUID)
        {
            Historial historial = new Historial();
            historial.UUID = UUID;
            try
            {
                return BUHistorial.Buscar(historial);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar historial valido
        /// </summary>
        /// <param name="UUID"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public List<Historial> buscaHistorial(string UUID, bool Valido)
        {
            Historial historial = new Historial();
            historial.UUID = UUID;
            historial.Valido = Valido;
            try
            {
                return BUHistorial.Buscar(historial);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para buscar historial
        /// </summary>
        /// <param name="Id_Proveedor"></param>
        /// <param name="Id_Sociedad"></param>
        /// <param name="Fecha_Ini"></param>
        /// <param name="Fecha_Fin"></param>
        /// <param name="Id_Moneda"></param>
        /// <returns></returns>
        public DataTable buscaHistorial(int Id_Proveedor, int Id_Sociedad, DateTime Fecha_Ini, DateTime Fecha_Fin, string Id_Moneda)
        {
            DataTable dtHistorial = new DataTable();
            try
            {
                dtHistorial = DAHistorial_Varios.buscaHistorial(Id_Proveedor, Id_Sociedad, Fecha_Ini, Fecha_Fin, Id_Moneda);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtHistorial;
        }
        #endregion

        #region ABC Historial
        /// <summary>
        /// Metodo para insertar Historial (Actualiza estatus Archivos_Ecibidos y elimina Programación)
        /// </summary>
        /// <param name="ID_Recepcion"></param>
        /// <param name="ID_Programacion"></param>
        /// <param name="Moneda"></param>
        /// <param name="Pedido"></param>
        /// <param name="Importe"></param>
        /// <param name="Docto"></param>
        /// <param name="Fecha_Pago"></param>
        /// <param name="ID_Estatus"></param>
        /// <param name="Observaciones"></param>
        /// <returns></returns>
        public bool insertaHistorial(int ID_Recepcion, int ID_Programacion, string Moneda, Int64 Pedido, decimal Importe, string Docto, DateTime Fecha_Pago, int ID_Estatus, string Observaciones)
        {
            //string _moneda, Int64 _pedido, string _acuse, DateTime _fechavencimiento, int _idestatus, string _observaciones
            bool resp = false;
            BLArchivos buscaRecepcion = new BLArchivos();
            BLProgramacion buscaProgramacion = new BLProgramacion();
            BLProgramacion elmina = new BLProgramacion();
            try
            {
                List<Programacion> csProgramacion = buscaProgramacion.buscaProgramacionxid(ID_Programacion);
                if (csProgramacion.Count > 0)
                {
                    Historial historial = new Historial();
                    historial.ID_Programacion = ID_Programacion;
                    historial.ID_Proveedor = csProgramacion[0].ID_Proveedor;
                    historial.ID_Sociedad = csProgramacion[0].ID_Sociedad;
                    historial.Fecha_Recepcion = csProgramacion[0].Fecha_Recepcion;
                    historial.Serie = csProgramacion[0].Serie;
                    historial.Folio = csProgramacion[0].Folio;
                    historial.UUID = csProgramacion[0].UUID;
                    historial.Fecha_Factura = csProgramacion[0].Fecha_Factura;
                    historial.Importe = Importe;
                    historial.Moneda = Moneda;
                    historial.Pedido = Pedido;
                    historial.Docto_Compensacion = Docto;
                    historial.Fecha_Pago = Fecha_Pago;
                    historial.Observaciones = Observaciones;
                    historial.Valido = true;

                    resp = BUHistorial.Insertar(historial);
                    if (resp)
                        if (ID_Recepcion != 0)
                            resp = buscaRecepcion.cambiaEstatus(ID_Recepcion, ID_Estatus, Observaciones);

                    if (resp)
                        resp = elmina.eliminarProgramacion(ID_Programacion);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }

        /// <summary>
        /// Metodo para actualizar hisorial
        /// </summary>
        /// <param name="ID_Historial"></param>
        /// <param name="Docto_Compensacion"></param>
        /// <param name="Fecha_Pago"></param>
        /// <param name="Observaciones"></param>
        /// <param name="Valido"></param>
        /// <returns></returns>
        public bool actualizaHistorial(int ID_Historial, string Docto_Compensacion, DateTime Fecha_Pago, string Observaciones, bool Valido)
        {
            Historial historia = new Historial();
            historia.ID_Historial = ID_Historial;
            historia.Docto_Compensacion = Docto_Compensacion;
            historia.Fecha_Pago = Fecha_Pago;
            historia.Observaciones = Observaciones;
            historia.Valido = Valido;
            try
            {
                return BUHistorial.Actualizar(historia);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para eliminar logicamente el historial
        /// </summary>
        /// <param name="Id_Historial"></param>
        /// <returns></returns>
        public bool eliminaHistorial(int Id_Historial)
        {
            Historial historia = new Historial();
            historia.ID_Historial = Id_Historial;
            historia.Valido = false;
            try
            {
                return BUHistorial.Eliminar(historia);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
