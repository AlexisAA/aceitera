﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Bean.Portal;

namespace BLRecepcion.Portal
{
    public class BLAvisos
    {
        /// <summary>
        /// Metodo para buscar avisos
        /// </summary>
        /// <returns></returns>
        public List<Avisos> buscaAviso()
        {
            Avisos aviso = new Avisos();
            aviso.Valido = true;
            try
            {
                return BUAvisos.Buscar(aviso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para insertar aviso
        /// </summary>
        /// <param name="ID_Proveedor"></param>
        /// <param name="Titulo_es"></param>
        /// <param name="Mensaje_es"></param>
        /// <param name="Titulo_en"></param>
        /// <param name="Mensaje_en"></param>
        /// <returns></returns>
        public bool insertaAviso(int ID_Proveedor, string Titulo_es, string Mensaje_es, string Titulo_en, string Mensaje_en)
        {
            Avisos aviso = new Avisos();
            aviso.ID_Proveedor = ID_Proveedor;
            aviso.Titulo = Titulo_es;
            aviso.Descripcion = Mensaje_es;
            aviso.Titulo_en = Titulo_en;
            aviso.Descripcion_en = Mensaje_en;
            aviso.Valido = true;

            try
            {
                return BUAvisos.Insertar(aviso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
