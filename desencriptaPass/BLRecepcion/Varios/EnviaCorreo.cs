﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using System.Net.Mail;
using System.IO;

namespace BLRecepcion.Varios
{
    public class EnviaCorreo
    {
        /// <summary>
        /// Metodo para el envío de correos
        /// </summary>
        /// <param name="mailReseptor">Correo Reseptor</param>
        /// <param name="mailSaunto">Asunto del correo</param>
        /// <param name="mailCuerpo">Cuerpo del Correo</param>
        /// <returns></returns>
        public string enviaCorreo(string mailReseptor, string mailSaunto, string mailCuerpo)
        {
            string respMail = string.Empty;

            //Datos para la configuración del correo
            string strServer = ConfigurationManager.AppSettings["mailServer"].ToString();
            string strRemitente = ConfigurationManager.AppSettings["mailRemitente"].ToString();
            string strContrasenia = ConfigurationManager.AppSettings["mailContrasenia"].ToString();
            int intPuerto = Convert.ToInt32(ConfigurationManager.AppSettings["mailPuerto"].ToString());
            bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["mailSSL"].ToString());

            SmtpClient datosSmtp = new SmtpClient();
            datosSmtp.Credentials = new System.Net.NetworkCredential(strRemitente, strContrasenia);
            datosSmtp.Host = strServer;
            datosSmtp.Port = intPuerto; //26
            
            //datosSmtp.UseDefaultCredentials = true;
            datosSmtp.EnableSsl = ssl;

            //Datos del correo
            MailMessage objetoMail = new MailMessage();
            MailAddress mailRemitente = new MailAddress(strRemitente);
            objetoMail.From = mailRemitente;

            MailAddress mailDestinatario = new MailAddress(mailReseptor);
            objetoMail.To.Add(mailDestinatario);

            objetoMail.Subject = mailSaunto;
            objetoMail.Body = mailCuerpo;
            objetoMail.IsBodyHtml = true;

            try
            {
                datosSmtp.Send(objetoMail);
                datosSmtp.Dispose();
                respMail = "Ok";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return respMail;
        }

        /// <summary>
        /// Envia correo con archivos adjuntos
        /// </summary>
        /// <param name="mailReseptor"></param>
        /// <param name="mailSaunto"></param>
        /// <param name="mailCuerpo"></param>
        /// <param name="ruta"></param>
        /// <returns></returns>
        public string enviaCorreo(string mailReseptor, string mailSaunto, string mailCuerpo, string ruta)
        {
            string respMail = string.Empty;

            //Datos para la configuración del correo
            //string strServer = ConfigurationManager.AppSettings["mailServer"].ToString();
            //string strRemitente = ConfigurationManager.AppSettings["mailRemitente"].ToString();
            //string strContrasenia = ConfigurationManager.AppSettings["mailContrasenia"].ToString();
            //int intPuerto = Convert.ToInt32(ConfigurationManager.AppSettings["mailPuerto"].ToString());
            //bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["mailSSL"].ToString());

            string strServer = "mail.aceitera.com.mx";
            string strRemitente = "proveedores@aceitera.com.mx";
            string strContrasenia = "Pr0v3edoRE5";
            int intPuerto = 25;
            bool ssl = false;


            SmtpClient datosSmtp = new SmtpClient();
            datosSmtp.Credentials = new System.Net.NetworkCredential(strRemitente, strContrasenia);
            datosSmtp.Host = strServer;
            datosSmtp.Port = intPuerto; //26
            //datosSmtp.UseDefaultCredentials = true;
            datosSmtp.EnableSsl = ssl;
            

            //Datos del correo
            MailMessage objetoMail = new MailMessage();
            MailAddress mailRemitente = new MailAddress(strRemitente);
            objetoMail.From = mailRemitente;

            MailAddress mailDestinatario = new MailAddress(mailReseptor);
            MailAddress mailCC = new MailAddress("proveedores@aceitera.com.mx");
            objetoMail.To.Add(mailDestinatario);
            objetoMail.CC.Add(mailCC);
            

            if (Directory.Exists(ruta))
            {
                string[] archivos = Directory.GetFiles(ruta);
                foreach (string archivo in archivos)
                {
                    Attachment adjuntos = new Attachment(archivo);
                    objetoMail.Attachments.Add(adjuntos);
                }
            }

            objetoMail.Subject = mailSaunto;
            objetoMail.Body = mailCuerpo;
            objetoMail.IsBodyHtml = true;

            try
            {
                datosSmtp.Send(objetoMail);
                datosSmtp.Dispose();
                respMail = "Ok";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return respMail;
        }

    }
}
