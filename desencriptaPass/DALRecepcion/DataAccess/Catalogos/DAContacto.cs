﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Catalogos;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase para los movimientos de la clase Contacto
    /// </summary>
    internal sealed class DAContacto : DataAccess<Contacto>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAContacto _instance = new DAContacto();

        internal static DAContacto Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Contacto
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Contacto> Buscar(IBean contacto)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Contacto]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(contacto, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Contacto
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean contacto)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Contacto]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(contacto, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Contacto
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean contacto)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Contacto]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(contacto, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Contacto
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean contacto)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Contacto]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(contacto, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
