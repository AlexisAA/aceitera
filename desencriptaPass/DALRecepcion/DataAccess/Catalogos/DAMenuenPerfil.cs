﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Catalogos;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase para los movimientos de la clase Menu en perfil
    /// </summary>
    internal sealed class DAMenuenPerfil : DataAccess<MenuenPerfil>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAMenuenPerfil _instance = new DAMenuenPerfil();

        internal static DAMenuenPerfil Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Menu en perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<MenuenPerfil> Buscar(IBean menp)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_MenuenPerfil]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(menp, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Menu en perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean menp)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_MenuenPerfil]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(menp, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Menu en perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean menp)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_MenuenPerfil]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(menp, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Menu en perfil
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean menp)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_MenuenPerfil]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(menp, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
