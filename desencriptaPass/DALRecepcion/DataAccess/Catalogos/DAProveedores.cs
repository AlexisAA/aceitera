﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Catalogos;

namespace DALRecepcion.DataAccess.Catalogos
{
    /// <summary>
    /// Clase para los movimientos de la clase Proveedores
    /// </summary>
    internal sealed class DAProveedores : DataAccess<Proveedores>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAProveedores _instance = new DAProveedores();

        internal static DAProveedores Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Proveedores
        /// </summary>
        /// <param name="proveedores">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Proveedores> Buscar(IBean proveedores)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Proveedores]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(proveedores, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Proveedores
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean proveedores)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Proveedores]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(proveedores, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Proveedores
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean proveedores)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Proveedores]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(proveedores, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Proveedores
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean proveedores)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Proveedores]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(proveedores, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
