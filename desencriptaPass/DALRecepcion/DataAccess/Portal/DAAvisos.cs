﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Bean.Portal;

namespace DALRecepcion.DataAccess.Portal
{
    /// <summary>
    /// Clase para los movimientos de la clase Avisos
    /// </summary>
    internal sealed class DAAvisos : DataAccess<Avisos>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DAAvisos _instance = new DAAvisos();

        internal static DAAvisos Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Avisos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Avisos> Buscar(IBean aviso)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Avisos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(aviso, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Avisos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean aviso)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Avisos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(aviso, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Avisos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean aviso)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Avisos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(aviso, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Avisos
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean aviso)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Avisos]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(aviso, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

    }
}
