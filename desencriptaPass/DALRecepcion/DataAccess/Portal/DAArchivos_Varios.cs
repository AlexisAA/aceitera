﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Common;

namespace DALRecepcion.DataAccess.Portal
{
    /// <summary>
    /// Clase de Acceso a datos Archivos recibidos para varios Stores
    /// </summary>
    public class DAArchivos_Varios
    {
        /// <summary>
        /// Metodo DA para busqueda de archivos
        /// </summary>
        /// <param name="ID_Sociedad"></param>
        /// <param name="ArchivXML"></param>
        /// <param name="Serie"></param>
        /// <param name="Folio"></param>
        /// <param name="FInicio"></param>
        /// <param name="FFin"></param>
        /// <param name="ID_Estatus"></param>
        /// <returns></returns>
        public static DataTable DAbuscaArchivos(int ID_Proveedor, int ID_Sociedad, string ArchivXML, string Serie, string Folio, DateTime FInicio, DateTime FFin, int ID_Estatus)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Archivosrecibidos_Varios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);
            GenericDataAccess.AddInParameter(comm, "@ID_Proveedor_in", DbType.Int32, ID_Proveedor);
            GenericDataAccess.AddInParameter(comm, "@ID_Sociedad_in", DbType.Int32, ID_Sociedad);
            GenericDataAccess.AddInParameter(comm, "@Nombre_XML_in", DbType.String, ArchivXML);
            GenericDataAccess.AddInParameter(comm, "@Serie_in", DbType.String, Serie);
            GenericDataAccess.AddInParameter(comm, "@Folio_in", DbType.String, Folio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, FInicio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_fin_in", DbType.DateTime, FFin);
            GenericDataAccess.AddInParameter(comm, "@ID_Estatus_in", DbType.Int32, ID_Estatus);

            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtArchivos;
        }

        /// <summary>
        /// Metodo para buscar aclarciones
        /// </summary>
        /// <param name="ID_Proveedor"></param>
        /// <param name="ID_Sociedad"></param>
        /// <param name="ArchivXML"></param>
        /// <param name="Serie"></param>
        /// <param name="Folio"></param>
        /// <param name="FInicio"></param>
        /// <param name="FFin"></param>
        /// <param name="ID_Estatus"></param>
        /// <returns></returns>
        public static DataTable DAbuscaAclaraciones(int ID_Proveedor, int ID_Sociedad, string ArchivXML, string Serie, string Folio, DateTime FInicio, DateTime FFin, int ID_Estatus)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Archivosrecibidos_Varios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 2);
            GenericDataAccess.AddInParameter(comm, "@ID_Proveedor_in", DbType.Int32, ID_Proveedor);
            GenericDataAccess.AddInParameter(comm, "@ID_Sociedad_in", DbType.Int32, ID_Sociedad);
            GenericDataAccess.AddInParameter(comm, "@Nombre_XML_in", DbType.String, ArchivXML);
            GenericDataAccess.AddInParameter(comm, "@Serie_in", DbType.String, Serie);
            GenericDataAccess.AddInParameter(comm, "@Folio_in", DbType.String, Folio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, FInicio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_fin_in", DbType.DateTime, FFin);
            GenericDataAccess.AddInParameter(comm, "@ID_Estatus_in", DbType.Int32, ID_Estatus);

            DataTable dtAclaraciones = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtAclaraciones;
        }

        /// <summary>
        /// Metodo para buscar Archivos
        /// </summary>
        /// <param name="ID_Proveedor"></param>
        /// <param name="ID_Sociedad"></param>
        /// <param name="ArchivXML"></param>
        /// <param name="Serie"></param>
        /// <param name="Folio"></param>
        /// <param name="FInicio"></param>
        /// <param name="FFin"></param>
        /// <param name="ID_Estatus"></param>
        /// <returns></returns>
        public static DataTable DAbuscaArchivos_CAT(int ID_Proveedor, int ID_Sociedad, string ArchivXML, string Serie, string Folio, DateTime FInicio, DateTime FFin, int ID_Estatus)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Archivosrecibidos_Varios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 3);
            GenericDataAccess.AddInParameter(comm, "@ID_Proveedor_in", DbType.Int32, ID_Proveedor);
            GenericDataAccess.AddInParameter(comm, "@ID_Sociedad_in", DbType.Int32, ID_Sociedad);
            GenericDataAccess.AddInParameter(comm, "@Nombre_XML_in", DbType.String, ArchivXML);
            GenericDataAccess.AddInParameter(comm, "@Serie_in", DbType.String, Serie);
            GenericDataAccess.AddInParameter(comm, "@Folio_in", DbType.String, Folio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, FInicio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_fin_in", DbType.DateTime, FFin);
            GenericDataAccess.AddInParameter(comm, "@ID_Estatus_in", DbType.Int32, ID_Estatus);

            DataTable dtAclaraciones = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtAclaraciones;
        }

    }
}
