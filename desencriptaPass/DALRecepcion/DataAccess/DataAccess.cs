﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using DALRecepcion.Bean;
using DALRecepcion.Common;

namespace DALRecepcion.DataAccess
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="B"></typeparam>
    internal abstract class DataAccess<B> where B : IBean, new()
    {
        public abstract List<B> Buscar(IBean objeto);
        public abstract bool Eliminar(IBean objeto);
        public abstract bool Actualizar(IBean objeto);
        public abstract bool Insertar(IBean objeto);

        /// <summary>
        /// >Agrega los parametros de dase de datos al comando especifico
        /// </summary>
        /// <param name="businessObject">Bean a partir del cual se obtendráan los parámetros para la BD</param>
        /// <param name="command">DbCommand al que se agregarán los parámetros</param>
        public void PopulateParameters(IBean businessObject, DbCommand command)
        {
            foreach (Propiedad propiedad in businessObject.Propiedades)
            {
                if (businessObject.Values[propiedad.Nombre] == null)
                {
                    if (propiedad.Tipo.Name == "Byte[]")
                    {
                        SqlParameter paramImage = new SqlParameter(string.Format("@{0}_in", propiedad.Nombre), SqlDbType.Image);
                        paramImage.Value = DBNull.Value;
                        command.Parameters.Add(paramImage);
                    }
                    else
                        GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.String, DBNull.Value);
                }
                else if (propiedad.Tipo.BaseType.Name == "Enum")
                {
                    GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.Int32, businessObject.Values[propiedad.Nombre]);
                }
                else
                {
                    switch (propiedad.Tipo.Name)
                    {
                        case "Boolean":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.Int16, businessObject.Values[propiedad.Nombre]);
                            break;

                        case "DateTime":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.DateTime, businessObject.Values[propiedad.Nombre]);
                            break;

                        case "String":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.String, businessObject.Values[propiedad.Nombre]);
                            break;

                        case "Int16":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.Int16, businessObject.Values[propiedad.Nombre]);
                            break;

                        case "Int32":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.Int32, businessObject.Values[propiedad.Nombre]);
                            break;

                        case "Int64":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.Int64, businessObject.Values[propiedad.Nombre]);
                            break;

                        case "Single":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.Single, businessObject.Values[propiedad.Nombre]);
                            break;

                        case "Decimal":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.Decimal, businessObject.Values[propiedad.Nombre]);
                            break;

                        case "Double":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.Double, businessObject.Values[propiedad.Nombre]);
                            break;
                        case "Byte[]":
                            SqlParameter paramImage = new SqlParameter(string.Format("@{0}_in", propiedad.Nombre), SqlDbType.Image);
                            paramImage.Value = businessObject.Values[propiedad.Nombre];
                            command.Parameters.Add(paramImage);
                            break;
                        case "Nullable`1":
                            GenericDataAccess.AddInParameter(command, BeanHelper.getCampoFromPropiedad(propiedad.Nombre), DbType.String, businessObject.Values[propiedad.Nombre]);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Pobla businessObject con los datos existentes en data
        /// </summary>
        /// <param name="businessObject">Objeto de tipo BEEstructura</param>
        /// <param name="data">DataRow con datos a poblar</param>
        public void PopulateBusinessObjectFromRow(IBean businessObject, DataRow data)
        {
            Dictionary<string, object> values = new Dictionary<string, object>();
            foreach (Propiedad propiedad in businessObject.Propiedades)
                if (data.Table.Columns.Contains(propiedad.Nombre) && data[propiedad.Nombre] != DBNull.Value)
                    values.Add(propiedad.Nombre, data[propiedad.Nombre]);

            businessObject.BindFromValues(values);
        }

        /// <summary>
        /// Pobla una lista con los datos existentes en data
        /// </summary>
        /// <param name="data">DataTable con datos a poblar</param>
        /// <returns>List</returns>
        public List<B> PopulateObjectsFromDataTable(DataTable data)
        {
            List<B> list = new List<B>();

            foreach (DataRow row in data.Rows)
            {
                B businessObject = new B();
                PopulateBusinessObjectFromRow(businessObject, row);
                list.Add(businessObject);
            }

            return list;
        }
    }
}
