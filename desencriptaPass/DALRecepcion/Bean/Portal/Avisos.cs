﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Portal
{
    /// <summary>
    /// Clase Avisos
    /// </summary>
    public class Avisos : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Avisos());
        private Dictionary<string, object> _values;

        private int _iD_Aviso;
        private string _titulo;
        private string _titulo_en;
        private string _descripcion;
        private string _descripcion_en;
        private int _iD_Proveedor;
        private bool _valido;
        #endregion

        #region Constructor.
        public Avisos()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Aviso
        {
            get { return _iD_Aviso; }
            set { _iD_Aviso = value; Values["ID_Aviso"] = value; }
        }
        public string Titulo
        {
            get { return _titulo; }
            set { _titulo = value; Values["Titulo"] = value; }
        }
        public string Titulo_en
        {
            get { return _titulo_en; }
            set { _titulo_en = value; Values["Titulo_en"] = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }
        public string Descripcion_en
        {
            get { return _descripcion_en; }
            set { _descripcion_en = value; Values["Descripcion_en"] = value; }
        }
        public int ID_Proveedor
        {
            get { return _iD_Proveedor; }
            set { _iD_Proveedor = value; Values["ID_Proveedor"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Aviso"))
                ID_Aviso = Convert.ToInt32(values["ID_Aviso"]);
            if (values.ContainsKey("Titulo"))
                Titulo = Convert.ToString(values["Titulo"]);
            if (values.ContainsKey("Titulo_en"))
                Titulo_en = Convert.ToString(values["Titulo_en"]);
            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
            if (values.ContainsKey("Descripcion_en"))
                Descripcion_en = Convert.ToString(values["Descripcion_en"]);
            if (values.ContainsKey("ID_Proveedor"))
                ID_Proveedor = Convert.ToInt32(values["ID_Proveedor"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
