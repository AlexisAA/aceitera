﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Portal;

namespace DALRecepcion.Bean.Portal
{
    public class BUArchivos
    {
        #region Metodos Standar
        public static List<Archivos> Buscar(Archivos archivo)
        {
            return DAArchivos.Instance.Buscar(archivo);
        }
        public static bool Eliminar(Archivos archivo)
        {
            return DAArchivos.Instance.Eliminar(archivo);
        }
        public static bool Actualizar(Archivos archivo)
        {
            return DAArchivos.Instance.Actualizar(archivo);
        }
        public static bool Insertar(Archivos archivo)
        {
            return DAArchivos.Instance.Insertar(archivo);
        }
        #endregion
    }
}
