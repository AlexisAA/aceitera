﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Portal
{
    /// <summary>
    /// Clase Archivos
    /// </summary>
    public class Archivos : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Archivos());
        private Dictionary<string, object> _values;

        private int _id_Recepcion;
        private string _nombre_XML;
        private string _nombre_PDF;
        private byte[] _archivo_XML;
        private byte[] _archivo_PDF;
        private DateTime _fecha_Recepcion;
        private int _iD_Proveedor;
        private int _iD_Sociedad;
        private string _folio;
        private string _serie;
        private string _uUID;
        private DateTime _fecha_Factura;
        private decimal _importe;
        private int _iD_Estatus;
        private string _observaciones;
        private bool _valido;
        #endregion

        #region Constructor
        public Archivos()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }
        
        public int Id_Recepcion
        {
            get { return _id_Recepcion; }
            set { _id_Recepcion = value; Values["Id_Recepcion"] = value; }
        }
        public string Nombre_XML
        {
            get { return _nombre_XML; }
            set { _nombre_XML = value; Values["Nombre_XML"] = value; }
        }
        public string Nombre_PDF
        {
            get { return _nombre_PDF; }
            set { _nombre_PDF = value; Values["Nombre_PDF"] = value; }
        }
        public byte[] Archivo_XML
        {
            get { return _archivo_XML; }
            set { _archivo_XML = value; Values["Archivo_XML"] = value; }
        }
        public byte[] Archivo_PDF
        {
            get { return _archivo_PDF; }
            set { _archivo_PDF = value; Values["Archivo_PDF"] = value; }
        }
        public DateTime Fecha_Recepcion
        {
            get { return _fecha_Recepcion; }
            set { _fecha_Recepcion = value; Values["Fecha_Recepcion"] = value; }
        }
        public int ID_Proveedor
        {
            get { return _iD_Proveedor; }
            set { _iD_Proveedor = value; Values["ID_Proveedor"] = value; }
        }
        public int ID_Sociedad
        {
            get { return _iD_Sociedad; }
            set { _iD_Sociedad = value; Values["ID_Sociedad"] = value; }
        }
        public string Folio
        {
            get { return _folio; }
            set { _folio = value; Values["Folio"] = value; }
        }
        public string Serie
        {
            get { return _serie; }
            set { _serie = value; Values["Serie"] = value; }
        }
        public string UUID
        {
            get { return _uUID; }
            set { _uUID = value; Values["UUID"] = value; }
        }
        public DateTime Fecha_Factura
        {
            get { return _fecha_Factura; }
            set { _fecha_Factura = value; Values["Fecha_Factura"] = value; }
        }
        public decimal Importe
        {
            get { return _importe; }
            set { _importe = value; Values["Importe"] = value; }
        }
        public int ID_Estatus
        {
            get { return _iD_Estatus; }
            set { _iD_Estatus = value; Values["ID_Estatus"] = value; }
        }
        public string Observaciones
        {
            get { return _observaciones; }
            set { _observaciones = value; Values["Observaciones"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("Id_Recepcion"))
                Id_Recepcion = Convert.ToInt32(values["Id_Recepcion"]);
            if (values.ContainsKey("Nombre_XML"))
                Nombre_XML = Convert.ToString(values["Nombre_XML"]);
            if (values.ContainsKey("Nombre_PDF"))
                Nombre_PDF = Convert.ToString(values["Nombre_PDF"]);
            if (values.ContainsKey("Archivo_XML"))
                Archivo_XML = (byte[])values["Archivo_XML"];           //Convert.ToUInt64(values["Archivo_XML"]);
            if (values.ContainsKey("Archivo_PDF"))
                Archivo_PDF = (byte[])values["Archivo_PDF"];           //Convert.ToUInt64(values["Archivo_XML"]);
            if (values.ContainsKey("Fecha_Recepcion"))
                Fecha_Recepcion = Convert.ToDateTime(values["Fecha_Recepcion"]);
            if (values.ContainsKey("ID_Proveedor"))
                ID_Proveedor = Convert.ToInt32(values["ID_Proveedor"]);
            if (values.ContainsKey("ID_Sociedad"))
                ID_Sociedad = Convert.ToInt32(values["ID_Sociedad"]);
            if (values.ContainsKey("Folio"))
                Folio = Convert.ToString(values["Folio"]);
            if (values.ContainsKey("Serie"))
                Serie = Convert.ToString(values["Serie"]);
            if (values.ContainsKey("UUID"))
                UUID = Convert.ToString(values["UUID"]);
            if (values.ContainsKey("Fecha_Factura"))
                Fecha_Factura = Convert.ToDateTime(values["Fecha_Factura"]);
            if (values.ContainsKey("Importe"))
                Importe = Convert.ToDecimal(values["Importe"]);
            if (values.ContainsKey("ID_Estatus"))
                ID_Estatus = Convert.ToInt32(values["ID_Estatus"]);
            if (values.ContainsKey("Observaciones"))
                Observaciones = Convert.ToString(values["Observaciones"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
