﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Portal;

namespace DALRecepcion.Bean.Portal
{
    public class BUAvisos
    {
        #region Metodos Standar
        public static List<Avisos> Buscar(Avisos avisos)
        {
            return DAAvisos.Instance.Buscar(avisos);
        }
        public static bool Eliminar(Avisos avisos)
        {
            return DAAvisos.Instance.Eliminar(avisos);
        }
        public static bool Actualizar(Avisos avisos)
        {
            return DAAvisos.Instance.Actualizar(avisos);
        }
        public static bool Insertar(Avisos avisos)
        {
            return DAAvisos.Instance.Insertar(avisos);
        }
        #endregion
    }
}
