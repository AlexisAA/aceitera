﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Portal;

namespace DALRecepcion.Bean.Portal
{
    public class BUHistorial
    {
        #region Metodos Standar
        public static List<Historial> Buscar(Historial historial)
        {
            return DAHistorial.Instance.Buscar(historial);
        }
        public static bool Eliminar(Historial historial)
        {
            return DAHistorial.Instance.Eliminar(historial);
        }
        public static bool Actualizar(Historial historial)
        {
            return DAHistorial.Instance.Actualizar(historial);
        }
        public static bool Insertar(Historial historial)
        {
            return DAHistorial.Instance.Insertar(historial);
        }
        #endregion
    }
}
