﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BuUsuarioenPerfil
    {
        #region Metodos Standar
        public static List<UsuarioenPerfil> Buscar(UsuarioenPerfil uenp)
        {
            return DAUsuarioenPerfil.Instance.Buscar(uenp);
        }
        public static bool Eliminar(UsuarioenPerfil uenp)
        {
            return DAUsuarioenPerfil.Instance.Eliminar(uenp);
        }
        public static bool Actualizar(UsuarioenPerfil uenp)
        {
            return DAUsuarioenPerfil.Instance.Actualizar(uenp);
        }
        public static bool Insertar(UsuarioenPerfil uenp)
        {
            return DAUsuarioenPerfil.Instance.Insertar(uenp);
        }
        #endregion
    }
}
