﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Cales Moneda
    /// </summary>
    public class Monedas : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Monedas());
        private Dictionary<string, object> _values;

        private string _moneda;
        private string _descripcion;
        #endregion

        #region Constructor
        public Monedas()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public string Moneda
        {
            get { return _moneda; }
            set { _moneda = value; Values["Moneda"] = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }
        #endregion

        #region Metodos
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("Moneda"))
                Moneda = Convert.ToString(values["Moneda"]);
            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
        }
        #endregion
    }
}
