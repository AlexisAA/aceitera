﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// 
    /// </summary>
    public class BUMonedas
    {
        #region Metodos Standar
        public static List<Monedas> Buscar(Monedas moneda)
        {
            return DAMonedas.Instance.Buscar(moneda);
        }
        public static bool Eliminar(Monedas moneda)
        {
            return DAMonedas.Instance.Eliminar(moneda);
        }
        public static bool Actualizar(Monedas moneda)
        {
            return DAMonedas.Instance.Actualizar(moneda);
        }
        public static bool Insertar(Monedas moneda)
        {
            return DAMonedas.Instance.Insertar(moneda);
        }
        #endregion
    }
}
