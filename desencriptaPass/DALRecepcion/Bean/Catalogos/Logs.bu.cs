﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BULogs
    {
        #region Metodos Standar
        public static List<Logs> Buscar(Logs logs)
        {
            return DALogs.Instance.Buscar(logs);
        }
        public static bool Eliminar(Logs logs)
        {
            return DALogs.Instance.Eliminar(logs);
        }
        public static bool Actualizar(Logs logs)
        {
            return DALogs.Instance.Actualizar(logs);
        }
        public static bool Insertar(Logs logs)
        {
            return DALogs.Instance.Insertar(logs);
        }
        #endregion
    }
}
