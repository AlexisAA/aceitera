﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BUMenuenPerfil
    {
        #region Metodos Standar
        public static List<MenuenPerfil> Buscar(MenuenPerfil menp)
        {
            return DAMenuenPerfil.Instance.Buscar(menp);
        }
        public static bool Eliminar(MenuenPerfil menp)
        {
            return DAMenuenPerfil.Instance.Eliminar(menp);
        }
        public static bool Actualizar(MenuenPerfil menp)
        {
            return DAMenuenPerfil.Instance.Actualizar(menp);
        }
        public static bool Insertar(MenuenPerfil menp)
        {
            return DAMenuenPerfil.Instance.Insertar(menp);
        }
        #endregion
    }
}
