﻿using System;
using System.Collections.Generic;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Clase Menu en Perfil
    /// </summary>
    public class MenuenPerfil : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new MenuenPerfil());
        private Dictionary<string, object> _values;

        private int _iD_MenP;
        private int _iD_Menu;
        private int _iD_Perfil;
        private int _orden;
        private bool _valido;
        #endregion

        #region Constructor
        public MenuenPerfil()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_MenP
        {
            get { return _iD_MenP; }
            set { _iD_MenP = value; Values["ID_MenP"] = value; }
        }
        public int ID_Menu
        {
            get { return _iD_Menu; }
            set { _iD_Menu = value; Values["ID_Menu"] = value; }
        }
        public int ID_Perfil
        {
            get { return _iD_Perfil; }
            set { _iD_Perfil = value; Values["ID_Perfil"] = value; }
        }
        public int Orden
        {
            get { return _orden; }
            set { _orden = value; Values["Orden"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_MenP"))
                ID_MenP = Convert.ToInt32(values["ID_MenP"]);
            if (values.ContainsKey("ID_Menu"))
                ID_Menu = Convert.ToInt32(values["ID_Menu"]);
            if (values.ContainsKey("ID_Perfil"))
                ID_Perfil = Convert.ToInt32(values["ID_Perfil"]);
            if (values.ContainsKey("Orden"))
                Orden = Convert.ToInt32(values["Orden"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
