﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Clase Contacto
    /// </summary>
    public class Contacto : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Contacto());
        private Dictionary<string, object> _values;

        private int _iD_Contacto;
        private string _nombre;
        private string _telefono;
        private string _celular;
        private string _correo;
        private int _iD_Proveedor;
        #endregion

        #region Constructor
        public Contacto()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Contacto
        {
            get { return _iD_Contacto; }
            set { _iD_Contacto = value; Values["ID_Contacto"] = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; Values["Nombre"] = value; }
        }
        public string Telefono
        {
            get { return _telefono; }
            set { _telefono = value; Values["Telefono"] = value; }
        }
        public string Celular
        {
            get { return _celular; }
            set { _celular = value; Values["Celular"] = value; }
        }
        public string Correo
        {
            get { return _correo; }
            set { _correo = value; Values["Correo"] = value; }
        }
        public int ID_Proveedor
        {
            get { return _iD_Proveedor; }
            set { _iD_Proveedor = value; Values["ID_Proveedor"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Contacto"))
                ID_Contacto = Convert.ToInt32(values["ID_Contacto"]);
            if (values.ContainsKey("Nombre"))
                Nombre = Convert.ToString(values["Nombre"]);
            if (values.ContainsKey("Telefono"))
                Telefono = Convert.ToString(values["Telefono"]);
            if (values.ContainsKey("Celular"))
                Celular = Convert.ToString(values["Celular"]);
            if (values.ContainsKey("Correo"))
                Correo = Convert.ToString(values["Correo"]);
            if (values.ContainsKey("ID_Proveedor"))
                ID_Proveedor = Convert.ToInt32(values["ID_Proveedor"]);
        }
        #endregion
    }
}
