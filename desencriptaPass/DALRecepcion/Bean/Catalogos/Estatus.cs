﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Clase Estatus
    /// </summary>
    public class Estatus : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Estatus());
        private Dictionary<string, object> _values;

        private int _iD_Estatus;
        private string _nomnre;
        private string _descripcion;
        private string _ruta;
        private bool _valido;
        #endregion

        #region Constructor
        public Estatus()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Estatus
        {
            get { return _iD_Estatus; }
            set { _iD_Estatus = value; Values["ID_Estatus"] = value; }
        }
        public string Nomnre
        {
            get { return _nomnre; }
            set { _nomnre = value; Values["Nomnre"] = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }
        public string Ruta
        {
            get { return _ruta; }
            set { _ruta = value; Values["Ruta"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Estatus"))
                ID_Estatus = Convert.ToInt32(values["ID_Estatus"]);
            if (values.ContainsKey("Nomnre"))
                Nomnre = Convert.ToString(values["Nomnre"]);
            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
            if (values.ContainsKey("Ruta"))
                Ruta = Convert.ToString(values["Ruta"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
