﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion;
using DALRecepcion.DataAccess.Catalogos;

namespace DALRecepcion.Bean.Catalogos
{
    public class BUPerfiles
    {
        #region Metodos Standar
        public static List<Perfiles> Buscar(Perfiles perfil)
        {
            return DAPerfiles.Instance.Buscar(perfil);
        }
        public static bool Eliminar(Perfiles perfil)
        {
            return DAPerfiles.Instance.Eliminar(perfil);
        }
        public static bool Actualizar(Perfiles perfil)
        {
            return DAPerfiles.Instance.Actualizar(perfil);
        }
        public static bool Insertar(Perfiles perfil)
        {
            return DAPerfiles.Instance.Insertar(perfil);
        }
        #endregion
    }
}
