﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALRecepcion.Common;

namespace DALRecepcion.Bean.Catalogos
{
    /// <summary>
    /// Clase Usuarios
    /// </summary>
    public class Clasificacion : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Clasificacion());
        private Dictionary<string, object> _values;

        private int _iD_Comentario;
        private string _comentario;
        private string _comentario_en;
        private bool _valido;
        #endregion

        #region Constructor
        public Clasificacion()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Metodos
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Comentario
        {
            get { return _iD_Comentario; }
            set { _iD_Comentario = value; Values["ID_Comentario"] = value; }
        }
        public string Comentario
        {
            get { return _comentario; }
            set { _comentario = value; Values["Comentario"] = value; }
        }
        public string Comentario_en
        {
            get { return _comentario_en; }
            set { _comentario_en = value; Values["Comentario_en"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Comentario"))
                ID_Comentario = Convert.ToInt32(values["ID_Comentario"]);
            if (values.ContainsKey("Comentario"))
                Comentario = Convert.ToString(values["Comentario"]);
            if (values.ContainsKey("Comentario_en"))
                Comentario_en = Convert.ToString(values["Comentario_en"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
