﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using BLRecepcion.Catalogos;
using DALRecepcion.Bean.Catalogos;
using BLRecepcion.Varios;

namespace desencriptaPass
{
    public partial class Form1 : Form
    {
        DataTable dtUsuarios = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'rececpionDataSet.TBL_Usuarios' Puede moverla o quitarla según sea necesario.
            llenaGrid();

        }

        private void llenaGrid()
        {
            dtUsuarios = new DataTable();
            dtUsuarios.Columns.Add(new DataColumn("Id_Usuario", typeof(Int32)));
            dtUsuarios.Columns.Add(new DataColumn("Usuario", typeof(String)));
            dtUsuarios.Columns.Add(new DataColumn("Contrasenia", typeof(String)));
            dtUsuarios.Columns.Add(new DataColumn("No", typeof(String)));
            dtUsuarios.Columns.Add(new DataColumn("ID_Proveedor", typeof(String)));

            BLUsuarios buscaUsuario = new BLUsuarios();
            Seguridad seg = new Seguridad();
            try
            {
                int noReg = 0;
                int idUsuario = 0;
                string usuario = string.Empty;
                string pass = string.Empty;
                string contrasenia = string.Empty;
                int noCaracteres = 0;
                int idProveedor = 0;
                List<Usuarios> csUsuarios = buscaUsuario.buscaUsuario();
                noReg = csUsuarios.Count;
                for (int n = 0; n < noReg; n++)
                {
                    idUsuario = Convert.ToInt32(csUsuarios[n].Id_Usuario.ToString());
                    usuario = csUsuarios[n].Usuario;
                    pass = csUsuarios[n].Contrasenia.ToString();
                    contrasenia = seg.desencriptar(pass);
                    noCaracteres = contrasenia.Length;
                    idProveedor = Convert.ToInt32(csUsuarios[n].Id_Proveedor.ToString());

                    if (noCaracteres > 10)
                    {
                        DataRow drUsuario = dtUsuarios.NewRow();
                        drUsuario["Id_Usuario"] = idUsuario;
                        drUsuario["Usuario"] = usuario;
                        drUsuario["Contrasenia"] = contrasenia;
                        drUsuario["No"] = noCaracteres;
                        drUsuario["ID_Proveedor"] = idProveedor;

                        dtUsuarios.Rows.Add(drUsuario);
                    }
                }

                if (dtUsuarios.Rows.Count > 0)
                {
                    dgvUsuarios.DataSource = dtUsuarios;
                }

            }
            catch (Exception ex)
            {

            }
        }

        private void btnCambiar_Click(object sender, EventArgs e)
        {
            int no = 0;
            no = dtUsuarios.Rows.Count;
            for (int n = 0; n < no; n++)
            {
                int idUsuario = 0;
                int idProveedor = 0;
                string Usuario = string.Empty;
                string Correo = string.Empty;
                string Pass = string.Empty;
                string newPass = string.Empty;
                bool resp = false;
                string sResp = string.Empty;

                idProveedor = Convert.ToInt32(dtUsuarios.Rows[n]["ID_Proveedor"].ToString());
                idUsuario = Convert.ToInt32(dtUsuarios.Rows[n]["Id_Usuario"].ToString());
                Usuario = dtUsuarios.Rows[n]["Usuario"].ToString();

                BLContacto busca = new BLContacto();
                Seguridad seg = new Seguridad();
                BLUsuarios actualiza = new BLUsuarios();
                try
                {
                    List<Contacto> csContacto = busca.buscaContacto(idProveedor);
                    if (csContacto.Count > 0)
                    {
                        Correo = csContacto[0].Correo.ToString();
                        //Correo = "carlos.arellano@prime-consultoria.com.mx";
                        Pass = seg.generaRandom();
                        newPass = seg.encriptar(Pass);
                        resp = actualiza.cambiaContrasenia(idUsuario, newPass);
                        //resp = true;
                        if (resp)
                        {
                            sResp = enviaCorreo(Usuario, Pass, Correo);
                            if (sResp == "Ok")
                            {
                                MessageBox.Show("Se ha enviado nueva contraseña a ID_Usuario : " + idUsuario);
                            }
                            else
                            {
                                MessageBox.Show("No se ha podido enviar correo a ID_Usuario : " + idUsuario);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No se actualizo contraseña de ID_Usuario : " + idUsuario);
                        }
                    }
                    else
                    {
                        MessageBox.Show("No se encontro correo de ID_Usuario : " + idUsuario);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private string enviaCorreo(string _usuario, string _contrasenia, string _correo)
        {
            string resp = string.Empty;

            EnviaCorreo envia = new EnviaCorreo();
            try
            {
                string asunto = string.Empty;
                string cuerpo = string.Empty;
                string receptor = string.Empty;
                string strPath = string.Empty;

                asunto = "Cambio de contraseña";
                cuerpo = "<body><p style='font-family: Sans-Serif; font-size: 18px; color: #008FC4;'><strong>INDUSTRIAL ACEITERA - CAMBIO DE CONTRASEÑA</strong></p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Se les comunica que por motivos de seguridad se han realizado ajustes dentro del Portal de Recepción de Facturas y por tal motivo se ha hecho el cambio de la contraseña</p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Los datos de acceso con la nueva contraseña son ::</p>";
                cuerpo += "<table style='font-family: Sans-Serif; font-size: 12px; color: #000000; margin-left: 50px'>";
                cuerpo += "<tr><td style='text-align: right;'>Usuario :</td><td style='text-align: left;'>" + _usuario + "</td></tr>";
                cuerpo += "<tr><td style='text-align: right;'>Contraseña :</td><td style='text-align: left;'>" + _contrasenia + "</td></tr></table>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Es importante que a la brevedad cambie la contraseña en el Menú Mis Datos - Cambiar contraseña</p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #000000;'>Para acceder, debes ingresar en <a href='http://aceitera.com.mx'>www.aceitera.com.mx</a> y dar clic en PORTAL DE PROVEEDORES situado en la partes superior derecha debajo del Menú</p>";
                cuerpo += "<br /><p style='font-family: Sans-Serif; font-size: 14px; color: #008FC4;'><strong>Atentamente</strong></p>";
                cuerpo += "<p style='font-family: Sans-Serif; font-size: 12px; color: #008FC4;'><strong>CUENTAS POR PAGAR - INDUSTRIAL ACEITERA</strong></p></body>";

                //string receptor = ConfigurationManager.AppSettings["mailReceptor"].ToString();
                receptor = _correo;
                strPath = "C:/XX"; // Server.MapPath(@"./Doctos/");
                resp = envia.enviaCorreo(receptor, asunto, cuerpo, strPath);

            }
            catch (Exception ex)
            {
                
            }

            return resp;
        }
    }
}
